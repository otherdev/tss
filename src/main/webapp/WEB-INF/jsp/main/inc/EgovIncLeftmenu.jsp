<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>교육모의시스템</title>
<link href="<c:url value='/css/style.css' />" rel="stylesheet" type="text/css">


<script type="text/javascript" src="<c:url value='/js/lib/jquery-1.4.2.min.js' />" ></script>
<script type="text/javascript">
$(document).ready(function() {
	//$("a.link").first().trigger("click");
	$("a.link")[0].click();
});

//레프트메뉴 활서화
var fn_setLeft = function(obj){
	$("li > a").removeClass('active');
	$(obj).addClass('active');
}
</script>

</head>

<body style="background: #fff;">
<c:set var="isMai" value="false"/>

<div id="wrap"> 
<!-- container -->
<div id="container"  class="content" > 
	<div id="content" >
		<div class="contents-box"   > 
			<!--side menu-->
			<div class="left-menu" >
				<h1><img 
					  	<c:choose>
					  		<c:when test="${upperMenuNo eq '10'}">src="<c:url value='/images/left_icon_01.png' />"</c:when>
					  		<c:when test="${upperMenuNo eq '20'}">src="<c:url value='/images/left_icon_02.png' />"</c:when>
					  		<c:when test="${upperMenuNo eq '30'}">src="<c:url value='/images/left_icon_03.png' />"</c:when>
					  		<c:when test="${upperMenuNo eq '40'}">src="<c:url value='/images/left_icon_04.png' />"</c:when>
					  		<c:when test="${upperMenuNo eq '50'}">src="<c:url value='/images/left_icon_05.png' />"</c:when>
					  		<c:otherwise>class="header-img-box-01"</c:otherwise>
					  	</c:choose> 
				 > ${upperMenuNm}</h1>

				<ul class="side-menu">
					<c:forEach var="result" items="${list_menulist}" varStatus="status">
						<li>
				    	<a href="${pageContext.request.contextPath}<c:out value="${result.chkUrl}"/>?baseMenuNo=${result.menuNo}" target="_content" class="link <c:if test="status.index==0">active</c:if>" onclick="fn_setLeft(this);">  
				    		<c:out value="${result.menuNm}"/> </a>
						</li>
					</c:forEach>
				</ul>
			</div>
		</div>
	</div>
</div>
</div>




</body>
</html>
