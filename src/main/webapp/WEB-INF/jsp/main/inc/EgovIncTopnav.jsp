<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="<c:url value='/css/style.css' />" rel="stylesheet" type="text/css">
<title>사후관리시스템</title>

<script type="text/javascript" src="<c:url value='/js/lib/jquery-1.4.2.min.js' />" ></script>
<script type="text/javascript">
$(document).ready(function() {
	try{
		// 초기메뉴 보이기
		$("a.link")[0].click();
		//$("a.link").first().trigger("click");
	}catch(e){}
	//탑이미지 선택적용
	$("#imgTop").removeAttr('class');
	$("#imgTop").addClass('header-img-box-01');
	
	

});

//탑이미지 선택적용
var fn_setImg = function(menuNo){
	$("#imgTop").removeAttr('class');
	$("#imgTop").addClass('header-img-box-0'+menuNo.substring(0,1));
	
}
</script>
</head>
<body>
<div id="wrap">

  <!-- header -->
  <div id="header" class="header" >
    <div class="index_navbar"> 
      <a href="#" onclick="window.location.reload();"> <img src="${pageContext.request.contextPath}/images/ci.png"  alt="교육모의시스템"></a>
      <div class="member">${sessionScope.name} 님 환영합니다.  <a href="#" onclick="javascript:window.top.location.href='/tts/uat/uia/actionLogout.do?login_error=-1';">로그아웃</a></div>
      
      <ul class="index_nav">
      	<c:forEach var="result" items="${list_headmenu}" varStatus="status">
	        <li >
				<a class="link" href="EgovMainMenuLeft.do?upperMenuNo=<c:out value="${result.menuNo}"/>" target="_left" abbr="${result.menuNo}" onclick="fn_setImg('${result.menuNo}');" >
	    	 	<c:out value="${result.menuNm}"/></a>	        
	        </li>
      	</c:forEach>
      </ul>
    </div>
  </div>
  
  <div id="imgTop" class="header-img-box-01" >
  <div class="header-img" >
  <p >안전하고 맛있는 울산 수돗물</p>  
  </div>
  </div>
  
</div>
</body>
</html>
