<%--
  Class Name : EgovFileList.jsp
  Description : 파일 목록화면(include)
  Modification Information
 
      수정일         수정자                   수정내용
    -------    --------    ---------------------------
     2009.03.26   이삼섭          최초 생성
     2011.08.31   JJY       경량환경 버전 생성
 
    author   : 공통서비스 개발팀 이삼섭
    since    : 2009.03.26 
--%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<script type="text/javascript">
<!--
    function fn_egov_downFile(fileSeq, seq){
        window.open("<c:url value='/cmm/fms/FileDown.do?fileSeq="+fileSeq+"&seq="+seq+"'/>");
    }   
    
    function fn_egov_deleteFile(fileSeq, seq) {
        forms = document.getElementsByTagName("form");

        for (var i = 0; i < forms.length; i++) {
            if (typeof(forms[i].fileSeq) != "undefined" &&
                    typeof(forms[i].seq) != "undefined" &&
                    typeof(forms[i].fileListCnt) != "undefined") {
                form = forms[i];
            }
        }
        //form = document.forms[0];
        form.fileSeq.value = fileSeq;
        form.seq.value = seq;
        form.action = "<c:url value='/cmm/fms/deleteFileInfs.do'/>";
        form.submit();
    }
    
    function fn_egov_check_file(flag) {
        if (flag=="Y") {
            document.getElementById('file_upload_posbl').style.display = "block";
            document.getElementById('file_upload_imposbl').style.display = "none";          
        } else {
            document.getElementById('file_upload_posbl').style.display = "none";
            document.getElementById('file_upload_imposbl').style.display = "block";
        }
    }
//-->    
</script>

<input type="hidden" name="fileSeq" value="${fileSeq}">
<input type="hidden" name="seq" >
<input type="hidden" name="fileListCnt" value="${fileListCnt}">
<c:set var="fileCount" value="${fn:length(fileList) }" />
	<table id="egov_file_view_table" style="border:0px solid #666;" >
	
        <c:forEach var="fileVO" items="${fileList}" varStatus="status">
		<tr id="egov_file_view_table_tr_${status.count}" >
			<td style="border:0px solid #666;text-align:left;padding:0 0 0 0;margin:0 0 0 0;" height="22">
           <c:choose>
               <c:when test="${updateFlag=='Y'}">
                   <c:out value="${fileVO.orignlFileNm}"/><span style="padding-left:20px;" >l</span><c:out value="${fileVO.fileSizeMb}"/><span style="padding-left:20px;">l</span>
                   <img alt="파일 삭제" src="<c:url value='/images/btn/icon_file_delete.png'/>" 
                        style="vertical-align:middle;" width="19" height="18" onClick="fn_egov_deleteFile('<c:out value="${fileVO.fileSeq}"/>','<c:out value="${fileVO.seq}"/>');" />
               </c:when>
               <c:otherwise>
                   <a href="#LINK" onclick="javascript:fn_egov_downFile('<c:out value="${fileVO.fileSeq}"/>','<c:out value="${fileVO.seq}"/>')">
                   <c:out value="${fileVO.orignlFileNm}"/><span style="padding-left:20px;">l</span><c:out value="${fileVO.fileSizeMb}"/><span style="padding-left:20px;">l</span>
                   <img  alt="파일 다운" src="<c:url value='/images/icon_file_down.png'/>" 
                        style="vertical-align:middle;" width="19" height="18" onClick="fn_egov_downFile('<c:out value="${fileVO.fileSeq}"/>','<c:out value="${fileVO.seq}"/>');" />
                   </a>        
               </c:otherwise>
           </c:choose>
        </c:forEach>
        <c:if test="${fn:length(fileList) == 0}">
			<tr>
				<td style="border:0px solid #666;padding:0 0 0 0;margin:0 0 0 0;"></td>
			</tr>
	    </c:if>
			</td>
		</tr>
	    
	</table>
<c:if test="${updateFlag eq 'Y'}">
<div style="display:none;"><iframe name="iframe_egov_file_delete" src=""></iframe></div>
<script type="text/javaScript">
//multi_selector.update_count = <c:out value="${fileCount}"/>;
var _multi_selector;
function fn_egov_multi_selector_update_setting(multi_selector){
	_multi_selector = multi_selector;
	multi_selector.update_count = <c:out value="${fileCount}"/>;
}
function fn_egov_multi_selector_update_delete(){
	_multi_selector.update_count = _multi_selector.update_count -1;
	//alert(_multi_selector.update_count);
}

</script>
</c:if>
      