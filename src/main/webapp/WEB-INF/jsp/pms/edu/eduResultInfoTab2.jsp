<%--
  Class Name : eduResultInfoTab2.jsp
  Description : 교육 성과관리  상세정보 tab2 JSP
  Modification Information
 
      수정일         수정자                   수정내용
    -------    --------    ---------------------------
     2018.03.15                 최초 생성
 
    author   : 아무개
    since    : 2018.03.15
--%>
<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<script type="text/javaScript" >
function fnEduResultTab2Info(sEduSeq , sTaskSeq ){
	
}
</script>
 <table class="table table-hover table-list center ">
   <colgroup>
	   <col width="7%">	  
	   <col width="26%">
	   <col width="17%">
	   <col width="15%">
	   <col width="15%">
   </colgroup>
   <thead>
     <tr>
       <th>번호</th>
       <th>과제명</th>
       <th>제출기한</th>
       <th>제출인원</th>
       <th>전체인원</th>
     </tr>
   </thead>
   <tbody>
   <%-- 데이터를 없을때 화면에 메세지를 출력해준다 --%>
	<c:if test="${fn:length(tab2ResultList) == 0}">
		<tr>
			<td class="lt_text3" colspan="7">
				<spring:message code="common.nodata.msg" />
			</td>
		</tr>
	</c:if>
	<c:forEach items="${tab2ResultList}" var="result" varStatus="status">
		<tr >
			<td ><strong><c:out value="${result.rowId2}"/></strong></td>
			<td class="left"><c:out value="${result.ttl}"/></td>
			<td><c:out value="${result.eduSchdul}"/></td>			
			<td><c:out value="${result.submitCnt}"/></td>
			<td><c:out value="${result.userCnt}"/></td>			
		</tr>
	</c:forEach>
   </tbody>
 </table>
          