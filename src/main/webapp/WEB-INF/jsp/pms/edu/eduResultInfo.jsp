<%--
  Class Name : eduDataInfo.jsp
  Description : 교육자료 상세정보 JSP
  Modification Information
 
      수정일         수정자                   수정내용
    -------    --------    ---------------------------
     2017.03.03                 최초 생성
 
    author   : 공통서비스 개발팀
    since    : 2017.03.03
--%>
<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>교육자료 상세</title>
<%@ include file="/WEB-INF/jsp/pms/include-pms.jspf"%>
<script type="text/javaScript" language="javascript" defer="defer">
<!--
$(document).ready(function() {
	$("#tab1_info").hide();
	$("#tab2_info").hide();
	$("#tab3_info").hide();
	
	
	// var sEduMngYn = "${sessionScope.isAdmYn}"; // "<c:out value='${eduMngYn}'/>";
	/*
	$("#btnUpdate").hide();
	$("#btnDelete").hide();
	
	if( sEduMngYn == 'Y' || (sUserId == sRegId) ) {
		$("#btnUpdate").show();
		$("#btnDelete").show();
	} 
	*/
	fnTabChange('1');
});

function fnTabChange(sTab){
	if(sTab=='2') {
		$("#li_tab1").removeClass("active");
		$("#tab1").removeClass("active");
		$("#tab1_info").hide();

		$("#li_tab2").addClass("active");
		$("#tab2").addClass("active");
		$("#tab2_info").show();

		$("#li_tab3").removeClass("active");
		$("#tab3").removeClass("active");
		$("#tab3_info").hide();
	} else if(sTab=='3') {
		$("#li_tab1").removeClass("active");
		$("#tab1").removeClass("active");
		$("#tab1_info").hide();

		$("#li_tab2").removeClass("active");
		$("#tab2").removeClass("active");
		$("#tab2_info").hide();

		$("#li_tab3").addClass("active");
		$("#tab3").addClass("active");
		$("#tab3_info").show();
	} else {
		$("#li_tab1").addClass("active");
		$("#tab1").addClass("active");
		$("#tab1_info").show();

		$("#li_tab2").removeClass("active");
		$("#tab2").removeClass("active");
		$("#tab2_info").hide();

		$("#li_tab3").removeClass("active");
		$("#tab3").removeClass("active");
		$("#tab3_info").hide();
	}
}
function fnRefresh(){

    document.eduDataModVO.action = "<c:url value='/pms/data/eduFaqInfo.do'/>";
    document.eduDataModVO.submit();	
}
function fnListPage(){
    document.eduDataModVO.seq.value = -1;
    document.eduDataModVO.mode.value = "VIEW";
    document.eduDataModVO.action = "<c:url value='/pms/data/eduFaqList.do'/>";
    document.eduDataModVO.submit();	
}
//-->
</script>

</head>
<body>
	<noscript class="noScriptTitle">자바스크립트를 지원하지 않는 브라우저에서는 일부 기능을 사용하실 수 없습니다.</noscript>

<!-- container -->
<div id="container"  class="content" > 
  
  <!-- content -->
  <div id="content" > 
    
    <!--cont-box-->
    
    <div class="contents-box"  > 
	<!-- 현재위치 네비게이션 시작 -->
	<div class="conts" >
		<h1><i class="fa fa-chevron-circle-right  "></i> 교육성과관리</h1>
				
	    <!-- write -->
	    <form:form commandName="eduSchdulModVO" action="${pageContext.request.contextPath}/pms/data/eduDataInfo.do" name="eduDataModVO" method="post" >
			<!-- 상세정보 작업 삭제시 prameter 전달용 input -->
			<div class="search-group">
				<!-- 	        검색조건 유지 -->
				<input name="mode"            type="hidden" value="<c:out value='${eduSchdulModVO.mode}'/>"/> 
				<input name="eduSeq"          type="hidden" value="<c:out value='${eduSchdulModVO.eduSeq}'/>" />
				<input name="pageIndex"       type="hidden" value="<c:out value='${eduSchdulModVO.pageIndex}'/>" />
				<input name="searchCondition" type="hidden" value="<c:out value='${eduSchdulModVO.searchCondition}'/>"/>
		        <input name="searchKeyword"   type="hidden" value="<c:out value='${eduSchdulModVO.searchKeyword}'/>"/>
		        <input name="pageIndex"       type="hidden" value="<c:out value='${eduSchdulModVO.pageIndex}'/>"/>
	        </div>
			<!-- info -->
          <div class="conts-gray" style="padding:15px 20px">
            <table class="table table-border-no "  >
              <colgroup>
              <col width="15%">
              <col width="35%">
              <col width="15%">
              <col width="35%">
              </colgroup>
              <tbody>
                <tr>
                  <th > <p>교육구분</p></th>
                  <td> <c:out value='${eduSchdulModVO.ttl}'/></td>
                  <th><p>교육명</p></th>
                  <td> <c:out value='${eduSchdulModVO.eduNm}'/></td>
                </tr>
                <tr>
                  <th><p>교육기간</p></th>
                  <td> <c:out value='${eduSchdulModVO.eduSchdul}'/></td>
                  <th><p>대상자</p></th>
                  <td> <c:out value='${eduSchdulModVO.eduTarget}'/></td>
                </tr>
              </tbody>
            </table>
          </div>
          
          <!-- //info --> 
          <!-- tab -->
          
          <div class="content-tab">
            <ul class="content-tab" style="line-height:1.5;" >
              <li id="li_tab1" ><a id="tab1" href="#" onclick="fnTabChange('1'); return false;">교육진행 현황</a></li>
              <li id="li_tab2" ><a id="tab2" href="#" onclick="fnTabChange('2'); return false;">과제제출 현황</a></li>
              <li id="li_tab3" ><a id="tab3" href="#" onclick="fnTabChange('3'); return false;">대상자별 진도 현황</a></li>
            </ul>
          </div>
          <!-- //tab --> 
          
          <div id="tab1_info" class="conts-gray of-y-auto" style="border-top:none">          
			<c:import charEncoding="utf-8" url="/pms/edu/eduResultInfoTab1.do" >
			<c:param name="param_eduSeq" value="${eduSchdulModVO.eduSeq}" />
			</c:import>
          </div>
          
          
          <div id="tab2_info" class="conts-gray of-y-auto" style="border-top:none">    
			<c:import charEncoding="utf-8" url="/pms/edu/eduResultInfoTab2.do" >
			<c:param name="param_eduSeq" value="${eduSchdulModVO.eduSeq}" />
			</c:import>
          </div>
          
          
          <div id="tab3_info" class="conts-gray of-y-auto" style="border-top:none">  
			<c:import charEncoding="utf-8" url="/pms/edu/eduResultInfoTab3.do" >
			<c:param name="param_eduSeq" value="${eduSchdulModVO.eduSeq}" />
			</c:import>
          </div>
          
    	</form:form>
	      
   	</div>
	<!-- //content 끝 -->
	        
    </div>
    <!--//cont-box--> 
    
  </div>
  <!-- //content --> 
</div>
<!-- //container --> 
</body>
</html>