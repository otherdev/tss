<%--
  Class Name : eduDataList.jsp
  Description : 교육자료(조회,삭제) JSP
  Modification Information
 
      수정일         수정자                   수정내용
    -------    --------    ---------------------------
     2017.03.03                 최초 생성
 
    author   : 공통서비스 개발팀 
    since    : 2017.03.03
--%>
<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>그룹 선택</title>

<%@ include file="/WEB-INF/jsp/pms/include-pms_popup.jspf"%>
<script type="text/javascript" src="<c:url value='/js/showModalDialogCallee.js'/>" ></script>
<script type="text/javaScript" language="javascript" defer="defer">

$(document).ready(function() {
	var sGrpId = '${grpId}';

	var aGrpId = sGrpId.split("|");
	for(var l = 0 ; l < aGrpId.length ; l ++ ) {
		if(aGrpId[l]!='' && aGrpId[l] != null  ){
			$('input:checkbox[name="'+ aGrpId[l] + '"]').each(function() { this.checked = true; });//checked 처리 
		}
	}

	//var sss = parent.fn_OpenerValue("GRP");
	//alert("sss=> " + sss);
});
function fnListPage(){
    document.listForm.action = "<c:url value='/pms/edu/eduSchdulList.do'/>";
    document.listForm.submit();	
}
function fnAddGrp(){
	var aRtn = new Array();
	var aRtnValue ="";
	var nChkCnt = $( "input:checked" ).length;
	if(nChkCnt>0) {
		$( "input:checked" ).each(function(index,item){
			var obj = new Object();
			
			var sItemNm = item.name;
			$( "#"+sItemNm +" a").each(function(i,t){
				aRtnValue+= t.name + "#S#" + t.id + "#C#";
				//obj[t.name] = t.id;
			});
			aRtnValue+="#R#";
			//aRtn[index] = JSON.stringify(obj);
		});
		// alert("aRtnValue=> " + aRtnValue);
        fn_setArguments(aRtnValue);
	} else {
		alert("선택된 그룹이 없습니다.");
	}
}

function fn_setArguments(retVal){

	if (opener != null && !opener.closed) {
		try {
			// return opener.dialogArguments;
			
			opener.fn_SetProject(retVal);
			opener.returnValue = retVal;
			debugger;
			opener.close();
		} catch (err) {
			alert('팝업 처리 시 오류가 발생하였습니다. \n오류내용 : ' + err);
		}
	} else if (parent.opener != null && !parent.opener.closed) {
		try {
			parent.opener.fn_SetGrp(retVal);
			parent.opener.returnValue = retVal;
			parent.self.close();
		} catch (err) {
			alert('팝업 처리 시 오류가 발생하였습니다. \n오류내용 : ' + err);
		}
	} else {
		parent.fn_egov_returnValue(retVal);	
	}
}
'<c:if test="${!empty resultMsg}">alert("<spring:message code="${resultMsg}" />");</c:if>'
</script>

</head>
<body>
	<noscript class="noScriptTitle">자바스크립트를 지원하지 않는 브라우저에서는 일부 기능을 사용하실 수 없습니다.</noscript>
	<form name="listForm" action="<c:url value='/pms/edu/eduSchdulGrpPopup.do'/>" method="post">

    <!-- UI Object -->
	<div id="wrap"> 
		<h1><i class="fa fa-chevron-right   "></i> 그룹선택</h1>
		<div class="conts"  >
	        <!-- write -->
	        <div class="table-responsive of-y-auto " style="height:320px;">
	         <table class="table table-in">
                <colgroup>
                <col width="150px"> <col width="200px">
                <col width="*">
                <col width="0px">
                <col width="0px">
               
                </colgroup>
                <thead>
                  <tr>
                    <th>선택</th>
                    <th>그룹명</th>
                    <th>구성원</th>
                    <th style="display:none;">id</th>
                    <th style="display:none;">seq</th>
                  </tr>
                </thead>
                <tbody>
	        	<%-- 데이터를 없을때 화면에 메세지를 출력해준다 --%>
				<c:if test="${fn:length(grpResultList) == 0}">
					<tr>
						<td class="lt_text3" colspan="7">
							<spring:message code="common.nodata.msg" />
						</td>
					</tr>
				</c:if>
				<c:forEach items="${grpResultList}" var="result" varStatus="status">
					<tr id='${result.grpId}' >
						<td ><input type='checkbox' name='${result.grpId}'  ></td>
						<td ><a name="grpNm" id='${result.grpNm}'></a><c:out value="${result.grpNm}"/></td>
						<td ><a name="grpDesc" id='${result.grpDesc}'></a><c:out value="${result.grpDesc}"/></td>
						<td style="display:none;" ><a name="grpId" id='${result.grpId}'></a><c:out value="${result.grpId}"/></td>
						<td style="display:none;" ><a name="eduSeq" id='${result.eduSeq}'></a><c:out value="${result.eduSeq}"/></td>
						
					</tr>
				</c:forEach>
	        	<!-- div id="grdGrpPopup" style="height:300px;">
	        	</div-->
	        	</tbody>
	        	</table>
	        </div>
	        
		    <div class="btn_group center m-t-15  "  > 
	           <button id="button" class="btn btn-default"  onclick="javascript:fnAddGrp(); return false;"  > <i class="fa fa-check" aria-hidden="true"></i> 저장</button>
	           <button id="button" class="btn btn-default " onclick="JavaScript:parent.self.close();"  > <i class="fa fa-close" aria-hidden="true"></i> 닫기</button>
	        </div>	          
		    <!-- //write --> 
		</div>
	    <!--conts--> 
	   </div>
	<!-- //UI Object -->
	</form>
</body>
</html>