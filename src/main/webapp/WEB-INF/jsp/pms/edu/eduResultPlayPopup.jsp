<%--
  Class Name : eduSchdulProjectPopup.jsp
  Description : 과제선택 JSP
  Modification Information
 
      수정일         수정자                   수정내용
    -------    --------    ---------------------------
     2017.03.03                 최초 생성
 
    author   : 공통서비스 개발팀 
    since    : 2017.03.03
--%>
<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>과제 등록</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<!-- jstl태그-->
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<link href="<c:url value='/css/style_popup.css' />" rel="stylesheet" type="text/css">
<link href="<c:url value='/css/date-style.css' />" rel="stylesheet" type="text/css">
<link href="<c:url value='/css/jquery-ui.css' />" rel="stylesheet" type="text/css">
<link href="<c:url value='/fonts/font-awesome/css/font-awesome.min.css' />" rel="stylesheet" />

<script type="text/javascript" src="<c:url value="/validator.do"/>"></script>
<script type="text/javascript" src="<c:url value='/js/lib/jquery-1.12.4.js' />" ></script>
<script type="text/javascript" src="<c:url value='/js/lib/jquery-ui.js' />" ></script>
<script type="text/javaScript" language="javascript" defer="defer">
$(document).ready(function() {

});
</script>

</head>
<body>
	<noscript class="noScriptTitle">자바스크립트를 지원하지 않는 브라우저에서는 일부 기능을 사용하실 수 없습니다.</noscript>
	<form name="eduSchdulModVO" action="<c:url value='/pms/edu/eduResultPlayPopup.do'/>" method="post">

    <!-- UI Object -->
	<div id="wrap"> 
		<h1><i class="fa fa-chevron-right   "></i> 강의진행현황</h1>
		<div class="conts"  >
	        
    <h2 id="h2" > <i class="fa fa-arrow-right   "></i> <c:out value="${eduSchdulModVO.ttl2}"/> </h2>
          
            <!-- info -->
          <div class="conts-gray" style="padding:15px 20px">
            <table class="table table-border-no "  >
              <colgroup>
              <col width="20%">
              <col width="80%">
           
              </colgroup>
              <tbody>
                <tr>
                  <th > <p>교육구분</p></th>
                  <td><c:out value="${eduSchdulModVO.ttl}"/> </td>
                </tr>
                <tr>
                  <th><p>교육명</p></th>
                  <td><c:out value="${eduSchdulModVO.eduNm}"/></td>
                </tr>
                <tr>
                  <th><p>교육기간</p></th>
                  <td><c:out value="${eduSchdulModVO.eduSchdul}"/></td>
                </tr>
              </tbody>
            </table>
          </div>
          <!-- //info --> 
    <!-- write -->
          
          <div class="table-responsive of-y-auto " style="overflow-y: scroll; height:304px;">
           <table class="table table-in">
                <colgroup>
                <col width="30%"> 
                <col width="15%">
                <col width="15%">
                <col width="15%">
                <col width="25%"> 
               
                </colgroup>
                <thead>
                  <tr>
                    <th>대분류</th>
                    <th>총교육시간</th>
                    <th>학습시간</th>
                    <th>진행율</th>
                    <th>최근 학습시간</th>
                  </tr>
                </thead>
                <tbody>
                  
				    <%-- 데이터를 없을때 화면에 메세지를 출력해준다 --%>
					<c:if test="${fn:length(resultList) == 0}">
						<tr>
							<td class="lt_text3" colspan="7">
								<spring:message code="common.nodata.msg" />
							</td>
						</tr>
					</c:if>
					<c:forEach items="${resultList}" var="result" varStatus="status">
						<tr <c:if test="${result.mnuType == 'total'}"> class="total" </c:if  >>						
							<td ><strong><c:out value="${result.mnuNm}"/></strong></td>
							<td><c:out value="${result.flePlayTm}"/></td>
							<td><c:out value="${result.sumPlayTm}"/></td>			
							<td><c:out value="${result.playProcess}"/> %</td>
							<td><c:out value="${result.regDt}"/></td>							
						</tr>
					</c:forEach>
                </tbody>
              </table>
          </div>
    <div class="btn_group center m-t-15  "  >
           
           
            <button id="button" class="btn btn-default " onclick="JavaScript:parent.self.close();" > <i class="fa fa-close" aria-hidden="true"></i> 닫기</button>
          </div>
          
          <!-- //write --> 
	    <!--conts--> 
	   </div>
	<!-- //UI Object -->
	</form>
</body>
</html>