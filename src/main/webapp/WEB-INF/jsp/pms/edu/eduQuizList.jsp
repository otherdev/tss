<%--
  Class Name : eduDataList.jsp
  Description : 교육자료(조회,삭제) JSP
  Modification Information
 
      수정일         수정자                   수정내용
    -------    --------    ---------------------------
     2017.03.03                 최초 생성
 
    author   : 공통서비스 개발팀 
    since    : 2017.03.03
--%>
<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>퀴즈관리</title>
<%@ include file="/WEB-INF/jsp/pms/include-pms.jspf"%>
<script type="text/javaScript" language="javascript" defer="defer">
var objQuiz = new Object();
var objAnswer = new Object();
var fsMnuCd = "";
$(document).ready(function() {

	var sEduMngYn = "${sessionScope.isAdmYn}";
	// $("#btnDel").hide();
	$("#btnUpdate").attr("disabled",true);
	$("#btnUpdate").hide();
	if(sEduMngYn == 'Y'){
		$("#btnUpdate").show();		
	}
	
	$("#quizCnt").val(0);
});
$(function(){
	// 퀴주 갯수 변경 
	var quizCnt = $('#quizCnt');
	// var quizCnt = $('#answerCnt');
	quizCnt.on('input',function(e){
		var nQuizCnt= $(this).val();
		if(nQuizCnt!=null && nQuizCnt != '' && nQuizCnt != 0 ){
			var objTarget = $("#quizList");
			objTarget.empty();
			for(var i = 0 ; i < nQuizCnt ; i++){
				var objParam = new Object();
				$.each(objQuiz, function(idx, map){
					var nQuizNum = map['quizNum'];
					if(nQuizNum == (i+1) ) {
						objParam.questionSeq 		= map['questionSeq'];
						objParam.mnuCd 				= map['mnuCd'];
						objParam.questionContents 	= map['questionContents'];
						objParam.quizCnt 			= map['quizCnt'];
					}
				});
				objParam.quizNum = ( i+1) ;
				fn_QuizAdd(objTarget,objParam);
			}			
		}
		$('input[id="answerCnt"]').on('input',function(){
			// console.log("answerCnt===> function " + $(this).attr("id") + " == "  + $(this).attr("name") );
			fn_AnswerChange($(this).attr("name"));
		});	
	});	
});

function fn_QuizAdd(obj,param){

	var sDivInfo = "";
	var no = param.quizNum ; 
	var sQuestion = param.questionContents;
	var sQuizCnt = param.quizCnt;
	var sQuestionSeq = param.questionSeq;
	var sMnuCd = param.mnuCd;
	if(sQuestion == null || sQuestion == '' || sQuestion == undefined ){
		sQuestion = "";
	}
	if(sQuizCnt == null || sQuizCnt == '' || sQuizCnt == undefined ){
		sQuizCnt = "0";
	}
	if(sQuestionSeq == null || sQuestionSeq == '' || sQuestionSeq == undefined ){
		sQuestionSeq = "";
	}
	if(sMnuCd == null || sMnuCd == '' || sMnuCd == undefined ){
		sMnuCd = fsMnuCd;
	}
	// 문제 영역 
	sDivInfo += '<div class="quiz-group" id="quizArea" name="quizArea_' + no + '">';
	sDivInfo += '<input type="hidden" id="questionSeq" name="questionSeq_'+no+'" class="questionSeq" value="' +  sQuestionSeq + '" >';
	sDivInfo += '<input type="hidden" id="mnuCd" name="mnuCd_' +no+ '" class="mnuCd"  value="' +  sMnuCd+ '" >';
	sDivInfo += '<p class="q">';
	sDivInfo += no +'. ';
	sDivInfo += '<input type="text" id="question" name="question_'+no+'" class="form-control question" style="width:94%" value="' +  sQuestion + '">';
	sDivInfo += '</p>';
	sDivInfo += '<p class="example-n">보기수 ';
	sDivInfo += '<input type="text" id="answerCnt" name="answer_' + no +'" class="form-control answerCnt" value="' +  sQuizCnt+ '" style="width:30px"> ';
	sDivInfo += '</p>';
	sDivInfo += '<div id="answerArea" name="answer_' + no +'">';	
	sDivInfo += '<p class="example" >';
	if( param.quizCnt != null && param.quizCnt != '' && param.quizCnt > 0  ){
		for(var j = 0 ; j < param.quizCnt ; j ++ ){
			var sDivSubInfo = "";
			var sDivSubInfo2 = "";
			var sRightYn = "";
			
			$.each(objQuiz, function(idx, map){
				
				if(map.questionSeq == param.questionSeq && map.answerNum == (j+1)){
					sDivSubInfo = map.contents;
					sDivSubInfo2 = map.optSeq;
					sRightYn = map.rightYn;

				}
			});
			if(sDivSubInfo == null || sDivSubInfo == '' || sDivSubInfo == undefined ){
				sDivSubInfo = "";
			}
			if(sDivSubInfo2 == null || sDivSubInfo2 == '' || sDivSubInfo2 == undefined ){
				sDivSubInfo2 = "";
			}

			sDivInfo += '<input id="rightYn" name="rightYn_' + no + '" type="radio" class="i_radio rightYn_' + (j+1)  + '"  value='+(j+1) ;

			if(sRightYn == 'Y' ){
				sDivInfo += ' checked=checked ';
			}
			
			sDivInfo += ' >'; 
			
			sDivInfo += '<label for="c1" name="answerNum_' + (j+1) + '" style="padding-left:5px;">'; 
			sDivInfo += '<input type="hidden" id="optSeq" name="optSeq_'+(j+1)+'" class="optSeq" value="' +  sDivSubInfo2 + '" >';
			sDivInfo += '<input id="answerNum" name="answerNum_' + (j+1) + '" type="text" class="form-control answerNum" style="width:50px;" value="'+sDivSubInfo+'" >'; ;
			sDivInfo += '</label>'; 
		}
	}
	
	sDivInfo += '</p>';
	sDivInfo += '</div>';
	sDivInfo += '</div>';

	obj.append(sDivInfo);
}

function fn_AnswerChange(sAnswerNm){

	var sQuizId= sAnswerNm.replace("answer_","");
	var nAnswerCnt= $("input[name='"+ sAnswerNm+ "']").val();
	var objDiv= $("div[name='"+ sAnswerNm+ "']");
	var sDivInfo = ""; 	

	// 초기화
	objDiv.empty();
	sDivInfo += '<p class="example" >';

	for(var j = 0 ; j < nAnswerCnt ; j ++ ){
		var sDivSubInfo = "";
		var sOptSeq = "";
		var sRightYn = "";
		var nAnswId = (j+1);

		$.each(objQuiz, function(idx, map){			
			if(map.questionSeq == sQuizId && map.answerNum == nAnswId){
				sDivSubInfo = map.contents;
				sOptSeq = map.optSeq;
				sRightYn = map.rightYn;
			}
		});

		if(sDivSubInfo == null || sDivSubInfo == '' || sDivSubInfo == undefined ){
			sDivSubInfo = "";
		}

		if(sOptSeq == null || sOptSeq == '' || sOptSeq == undefined ){
			sOptSeq = "";
		}
		
		sDivInfo += '<input id="rightYn" name="rightYn_' + sQuizId + '" type="radio" class="i_radio rightYn_' + nAnswId  + '"  value='+nAnswId ;

		if(sRightYn == 'Y' ){
			sDivInfo += ' checked=checked ';  // sRightYn = "N";
		}
		
		sDivInfo += ' >'; 
		sDivInfo += '<label for="c' + nAnswId + '" name="answerNum_' + nAnswId + '" style="padding-left:5px;">'; 

		sDivInfo += '<input type="hidden" id="optSeq" name="optSeq_'+nAnswId+'" class="optSeq" value="' +  sOptSeq + '" >';
		sDivInfo += '<input id="answerNum" name="answerNum_' + nAnswId + '" type="text" class="form-control" style="width:50px;" value="'+sDivSubInfo+'" >'; ;
		sDivInfo += '</label>'; 
	}	
	sDivInfo += '</p>';
	objDiv.append(sDivInfo);
	
}
// 신규등록모드로 상세페이지 호출
function fnSaveQuiz() {

	if(fn_BrforeSave() == false ) return false;
	var nQuizCnt = $("#quizCnt").val();
	var arrQuizData = [];
	var arrAnswerData = [];
	var jj = 0 ;

	for(var i = 0 ; i < nQuizCnt ; i ++ ) {
		var objQuiz = new Object();
		var no = ( i +1 );
		

		objQuiz.quizType = no;
		objQuiz.ord1 = no;

		$("div[name='quizArea_" + no +"'] input").each(function(){
			var sId = $(this).attr("id");
			var sName = $(this).attr("name");
			var sVal = $(this).val();
			if( sId != "optSeq"  && sId != "answerNum" && sId != "rightYn" ) {
				if(sId == "question" ) {
					objQuiz.questionContents = sVal;
				} else if(sId == "questionSeq" ) {
					if(sVal == null || sVal == '' || sVal == undefined  ) {
						sVal = "";
					}
					objQuiz.questionSeq = sVal;
				} else if(sId == "mnuCd" ) {
					if(sVal == null || sVal == '' || sVal == undefined  ) {
						sVal = fsMnuCd;
					}
					
					objQuiz.mnuCd = sVal;
				} 
				// fsMnuCd
				
				else {
					objQuiz[sId] = sVal;
				}
			}
		});
		
		arrQuizData.push(objQuiz);
		
		var nAnswerCnt = objQuiz.answerCnt;
		var sRightYnSeq =  $(":input:radio[name='rightYn_" + no + "']:checked").val();

		for( var j = 0 ; j < nAnswerCnt ; j ++ ){
			var no2 = ( j +1 );
			var objAnswer = {};
			
			objAnswer.questionSeq = objQuiz.questionSeq;
			objAnswer.mnuCd = objQuiz.mnuCd;
			objAnswer.quizType = objQuiz.quizType;
			
			var sOptSeq = $("div[name='quizArea_" + no +"'] input[name='optSeq_" + no2 + "']").val();
			var sAnswerNum = $("div[name='quizArea_" + no +"'] input[name='answerNum_" + no2 + "']").val();
			var sRightYn = "";
			if(sOptSeq == null || sOptSeq == '' || sOptSeq == undefined  ) {
				sOptSeq = "";
			}
			if( sRightYnSeq == no2 ) {
				sRightYn = "Y";
			} else {
				sRightYn = "N";
			}
			
			objAnswer.optSeq = sOptSeq;
			objAnswer.rightYn = sRightYn;
			objAnswer.sRightYnSeq = sRightYnSeq;
			objAnswer.contents = sAnswerNum;
			objAnswer.ord2 = no2;

			arrAnswerData.push( objAnswer);
			jj++;
		}
	}

	//저장처리
	gfn_saveList(
			"/tts/pms/edu/eduQuizSave.do"
			,{mnuCd:fsMnuCd , mainData:arrQuizData , detailData: arrAnswerData }
			, function(data){
				if (data.result) {	
					alert("저장되었습니다.");
					fnSelectQuiz( fsMnuCd , nQuizCnt);
					$("#quizCnt").focus();
					return;
				}
				else{
					alert("저장에 실패하였습니다.","E");
					return;
				}
	});
	
}
function fn_BrforeSave(){
	var nCnt = 0;
	var nOk = 0 ;
	/// 질문 체크 
	var objQuiz = $("input[id='question']");
	$.each(objQuiz, function(idx, e){
		nOk ++;
		var sOk = $(":input:radio[name='rightYn_" + nOk + "']:checked").val();
		
		if( $(this).val()  == null ||  $(this).val()  == '' ||  $(this).val()  == undefined ){
			alert("질문정보가 존재하지 않습니다.");
			$(this).focus();
			nCnt++;
			return false;
		} else if( $(this).val().length > 120 ){
			alert("질문정보는 120글자를 넘을수 없습니다.");
			$(this).focus();
			nCnt++;
			return false;			
		} else if( sOk == null || sOk == ''  ) {
			alert("질문의 정답 정보가 존재하지 않습니다.");
			$(this).focus();
			nCnt++;
			return false;		
		}
	});

	if( nCnt > 0 ){ 
		return false;
	}

	var objQuiz = $("input[id='answerCnt']");
	var objAnswerCnt = $("input[id='answerCnt']");
	$.each(objAnswerCnt, function(idx, e){
		if( $(this).val()  == null ||  $(this).val()  == '' ||  $(this).val()  == undefined ||   $(this).val()  < 0){
			alert("답변의 보기수 정보가 존재하지 않습니다.");
			$(this).focus();
			nCnt++;
			return false;
		} 
	});

	if( nCnt > 0 ){
		return false;
	}

	var objAnswer = $("input[id='answerNum']");
	$.each(objAnswer, function(idx, e){

		if( $(this).val()  == null ||  $(this).val()  == '' ||  $(this).val()  == undefined ){
			alert("답변 정보가 존재하지 않습니다.");
			$(this).focus();
			nCnt++;
			return false;
		} else if( $(this).val().length > 120 ){
			alert("답변 정보는 120글자를 넘을수 없습니다.");
			$(this).focus();
			nCnt++;
			return false;			
		}
	});

	if( nCnt > 0 ){
		return false;
	}
	
	return true;
}

//교육시스템 메뉴동영상 리스트	
function fnRefresh(sysCd){ 
	location.href = "<c:url value='/pms/edu/eduQuizList.do'/>" + "?sysCd=" + sysCd;
}
function fnSelectQuiz(sMnuCd , nQuizCnt) {
	fsMnuCd = sMnuCd;
	$("#quizCnt").val(nQuizCnt);
	var nOrgQuiz = 0 ;
	var nAnswer = 0 ;
	/// Quiz 정보 가져오기 
	gfn_selectList({sqlId: 'selectAnswerList', mnuCd:sMnuCd}, function(ret){
		// var lst = [];
		objQuiz = ret.data;

		$("#btnUpdate").attr("disabled",false);
		var objTarget = $("#quizList");
		objTarget.empty();
		// 배열데이터순회
		$.each(objQuiz, function(idx, map){
			var nQuizNum = map['quizNum'];
			// var nAnswerNum = map['answerNum'];
			if(nQuizNum != nOrgQuiz ) {
				var objParam = new Object();
				objParam.quizNum 			= map['quizNum'];
				objParam.questionSeq 		= map['questionSeq'];
				objParam.mnuCd 				= map['mnuCd'];
				objParam.questionContents 	= map['questionContents'];
				objParam.quizCnt 			= map['quizCnt'];

				fn_QuizAdd(objTarget,objParam);
			}
			
			nOrgQuiz = nQuizNum;
		});
		$('input[id="answerCnt"]').on('input',function(){
			fn_AnswerChange($(this).attr("name"));
		});	
		
	});	
}

//강제 Submit 막기 
var fnNoSubmit = function(e){
	e.preventDefault();
	e.stopPropagation();
	/* do something with Error */
};
$("form[name='frm']").bind("submit",fnNoSubmit);
</script>

</head>
<body>
	<noscript class="noScriptTitle">자바스크립트를 지원하지 않는 브라우저에서는 일부 기능을 사용하실 수 없습니다.</noscript>
	<form name="frm" id="frm" onkeydown="gfn_fireKey(event);"  >

    <div class="contents-box"  > 
	<!-- 현재위치 네비게이션 시작 -->
	<div class="conts" >
		<h1><i class="fa fa-chevron-circle-right  "></i> 퀴즈관리</h1>
        <!-- list -->
        <div style="width:20%;  margin-right:10px ; display:inline-block; position:relative; min-height:400px;">
         <div class="search-group "  style=" margin-bottom:3px">       
        <span><b>시스템명</b> </span>
	       	<select class="form-control input-inline" path="sysCd" id="sysCd" style="width:70%" value="${sysCd}" onchange="javascript:fnRefresh(this.options[this.selectedIndex].value);" >
				<c:forEach items="${SYCD_result}" var="result" varStatus="status">
				<c:if test="${sysCd == result.dtlCd}">
	       			<option selected="selected" value="${result.dtlCd}">${result.cdNm}</option>
	           	</c:if>
	           	<c:if test="${sysCd != result.dtlCd}">                
	       			<option value="${result.dtlCd}">${result.cdNm}</option>
	           	</c:if>
	       		<!-- option value="${result.dtlCd}">${result.cdNm}</option-->
				</c:forEach>
	       	</select>
       </div>     
         <div class="conts-gray of-y-scroll" style="height:480px;">
           <table class="table table-hover table-list center "  >          
           <colgroup>
             <col width="*">
            </colgroup>
            <thead>
              <tr>
                <th>대분류</th>
                </tr>
            </thead>
            <tbody>
              <c:forEach items="${resultList}" var="result" varStatus="status">
					<tr style="cursor:hand;" onclick="javascript:fnSelectQuiz('<c:out value="${result.mnuCd}"/>','<c:out value="${result.quizCnt}"/>'); return false;" >
						<td style="cursor:pointer;cursor:hand" > 
						 	<span class="link left"><a href="#" onclick="javascript:fnSelectQuiz('<c:out value="${result.mnuCd}"/>','<c:out value="${result.quizCnt}"/>'); return false;">
						 	<c:out value="${result.mnuNm}"/></a>
					 		</span>
						</td>
					</tr>
				</c:forEach>
            </tbody>
          </table>
        </div>
        </div>
      <!-- //list --> 
	<!-- list2 -->
	<div id="quizArea" style="width:77%;  display:inline-block; float:right; min-height:400px;">
		<h2>퀴즈항목
		<span class="f_r"  > 
			<b>문제수</b> 
			<input id="quizCnt" name="quizCnt" type="text" class="form-control" value="" style="width:100px">
			<button id="btnUpdate" class="btn btn-default" onclick="javascript:fnSaveQuiz(); return false;"> <i class="fa fa-check" aria-hidden="true"></i> 저장</button>
		</span>
		</h2>
		<div id="quizList" class="conts-gray of-y-scroll " style="padding:0px 15px; height:480px;width:97%;margin-top:15px;">
		</div>
	</div>
	<!-- //list 2-->  
   	</div>
   	</div>
	<!-- //content 끝 -->
    
	</form>
</body>
</html>