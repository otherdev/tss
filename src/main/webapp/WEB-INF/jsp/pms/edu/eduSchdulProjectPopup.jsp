<%--
  Class Name : eduSchdulProjectPopup.jsp
  Description : 과제선택 JSP
  Modification Information
 
      수정일         수정자                   수정내용
    -------    --------    ---------------------------
     2017.03.03                 최초 생성
 
    author   : 공통서비스 개발팀 
    since    : 2017.03.03
--%>
<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>과제 등록</title>
<%@ include file="/WEB-INF/jsp/pms/include-pms_popup.jspf"%>
<script type="text/javascript" src="<c:url value='/js/showModalDialogCallee.js'/>" ></script>
<script type="text/javaScript" language="javascript" defer="defer">

$(document).ready(function() {

	$( "#startDt,#endDt" ).datepicker({
      showOn: "button",
      buttonImage: "/tts/images/calendar.gif",
      buttonImageOnly: true,
      buttonText: "Select date",
      dateFormat: "yy-mm-dd"
    });	
	// 달력 스타일 중앙정렬 
	$(".ui-datepicker-trigger").css("vertical-align","middle");
	$(".ui-datepicker-trigger").css("padding-left","3px");

	var parentObj = fn_getArguments();
	var sMode =parentObj.mode;
	
	if(sMode != "UPDATE") {
		$("#btnDel").hide();
	} 
	if(sMode != "ADD") {
		var sTaskSeq = parentObj.taskSeq;
		var sEduSeq = parentObj.eduSeq;
		var sTtl = parentObj.ttl;
		var sContents = parentObj.contents;
		var sStartDt = parentObj.startDt;
		var sEndDt = parentObj.endDt;
		var sCatCd = parentObj.catCd;
		// alert(sStartDt + " = " + sEndDt);
		
		$("#taskSeq").val(sTaskSeq);
		$("#eduSeq").val(sEduSeq);
		$("#ttl").val(sTtl);
		$("#contents").val(sContents);
		$("#startDt").val(sStartDt);
		$("#endDt").val(sEndDt);
		$("#catCd").val(sCatCd);
		
	}

	gfn_init();
});
function fn_getArguments(){
	if (opener != null && !opener.closed) {
		try {
			return opener.dialogArguments;
		} catch (err) {
			alert('팝업 처리 시 오류가 발생하였습니다. \n오류내용 : ' + err);
		}
	} else if (parent.opener != null && !parent.opener.closed) {
		try {
			return parent.opener.dialogArguments;
		} catch (err) {
			alert('팝업 처리 시 오류가 발생하였습니다. \n오류내용 : ' + err);
		}
	} else {
		return parent.window.dialogArguments;
	}
}

function fn_setArguments(retVal){

	if (opener != null && !opener.closed) {
		try {
			// return opener.dialogArguments;
			
			opener.fn_SetProject(retVal);
			opener.returnValue = retVal;
			debugger;
			opener.close();
		} catch (err) {
			alert('팝업 처리 시 오류가 발생하였습니다. \n오류내용 : ' + err);
		}
	} else if (parent.opener != null && !parent.opener.closed) {
		try {
			parent.opener.fn_SetProject(retVal);
			parent.opener.returnValue = retVal;
			parent.self.close();
		} catch (err) {
			alert('팝업 처리 시 오류가 발생하였습니다. \n오류내용 : ' + err);
		}
	} else {
		parent.fn_egov_returnValue(retVal);	
	}
}
// 삭제버튼처리 
function fnDelProject(){
	if(!confirm("삭제하시겠습니까?")) return;	
	
	var sRtnVal = fn_CreateReturnObj("DELETE");
	
	fn_setArguments(sRtnVal);// parent.fn_egov_returnValue(sRtnVal);	
}
// 저장버튼처리 
function fnSaveProject(){
    if(!gfn_formValid())	return;
	
	if( _jsDateCheck($("#startDt").val(), $("#endDt").val()) == false ) {
		return false;
	}
	
	if(!confirm("저장하시겠습니까?")) return;	
	
	var parentObj =fn_getArguments(); //  parent.window.dialogArguments;
	var sRtnVal = fn_CreateReturnObj(parentObj.mode);
	
	fn_setArguments(sRtnVal);
}
// 팝업 -> 입력화면 보낼 리턴값 정의 
function fn_CreateReturnObj(sMode){

	var sObj = "" ;

	sObj+= "mode#S#"		+ sMode						+"#C#";
	sObj+= "taskSeq#S#"		+ $("#taskSeq").val()		+"#C#";
	sObj+= "eduSeq#S#"		+ $("#eduSeq").val() 		+"#C#";
	sObj+= "ttl#S#"			+ $("#ttl").val()			+"#C#";
	sObj+= "contents#S#"	+ $("#contents").val()		+"#C#";
	sObj+= "catCd#S#"		+ $("#catCd").val()			+"#C#";
	sObj+= "catNm#S#"		+ $("#catCd").text()		+"#C#"; 
	sObj+= "startDt#S#"		+ $("#startDt").val()		+"#C#";
	sObj+= "endDt#S#"		+ $("#endDt").val()			+"#C#";
	sObj+= "eduSchdul#S#"	+ $("#startDt").val() + "~" + $("#endDt").val()+"#C#";
	// alert("sObj=> " + sObj);
	return sObj;
}

'<c:if test="${!empty resultMsg}">alert("<spring:message code="${resultMsg}" />");</c:if>'
</script>

</head>
<body>
	<noscript class="noScriptTitle">자바스크립트를 지원하지 않는 브라우저에서는 일부 기능을 사용하실 수 없습니다.</noscript>
	<form name="listForm" action="<c:url value='/pms/edu/eduSchdulProjectPopup.do'/>" method="post">

    <!-- UI Object -->
	<div id="wrap"> 
		<h1><i class="fa fa-chevron-right   "></i> 과제토론</h1>
		<div class="conts"  >
	        
    <!-- write -->
          
          <div class="table-responsive">
	          <input type="hidden" id="eduSeq" >
	          <input type="hidden" id="taskSeq"  >
	          <input type="hidden" id="mode"  >
            <table class="table table-write">
              <caption>
              		입력양식
              </caption>
              <colgroup>
              <col width="30%">
              <col width="70%">
              </colgroup>
              <tbody>
                <tr>
                  <th class="req">구분</th>
                  <td>
                  	<select class="form-control input-inline  reqVal" style="width:100%"  path="catCd" id="catCd"  >
	                    <option value="" label="--선택하세요--"/>
	                    <option value="EDCD01" label="과제"/>
	                    <option value="EDCD02" label="토론"/>
                    </select>
                    </td>
                </tr>
                <tr>
                  <th class="req">제목</th>
                  <td><input type="text" class="form-control  reqVal" id="ttl" value="" style="width:98.5%"  maxLength="60" ></td>
                </tr>
                <tr>
                  <th class="req">내용</th>
                  <td><textarea class="form-control  reqVal" id="contents" name="" rows="60" style="resize: none; width:98.5%; height:100px" maxLength="120" ></textarea></td>
                </tr>
                <tr>
                  <th class="req">기간</th>
                  <td><input path="startDt" id="startDt"  type="text" class="form-control  reqVal" style="width:120px" maxLength="8"  value="">
                    ~
                    <input path="endDt" id="endDt"  type="text" class="form-control   reqVal" style="width:120px" maxLength="8"  value="">
                </tr>
              </tbody>
            </table>
          </div>
    	<div class="btn_group center m-t-15  "  >
      		<button id="btnDel" class="btn btn-default " onclick="JavaScript:fnDelProject(); return false;"> <i class="fa fa-close" aria-hidden="true"></i> 삭제</button>       
      		<button id="btnSave" class="btn btn-default" onclick="JavaScript:fnSaveProject(); return false;"> <i class="fa fa-check" aria-hidden="true"></i> 저장</button>
       		<button id="btnClose" class="btn btn-default " onclick="JavaScript:parent.self.close();"> <i class="fa fa-close" aria-hidden="true"></i> 닫기</button>
          </div>
          
          <!-- //write --> 
		</div>
	    <!--conts--> 
	   </div>
	<!-- //UI Object -->
	</form>
</body>
</html>