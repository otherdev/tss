<%--
  Class Name : eduResultInfoTab3.jsp
  Description : 교육 성과관리  상세정보 tab3 JSP
  Modification Information
 
      수정일         수정자                   수정내용
    -------    --------    ---------------------------
     2018.03.15                 최초 생성
 
    author   : 아무개
    since    : 2018.03.15
--%>
<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<script type="text/javascript" src="<c:url value='/js/showModalDialog.js' />" ></script>
<script type="text/javaScript" >
function fnEduResultPopup(sType , sEduSeq ,sUserId){
	var retVal;
	var sUrl = "";
	var openParam = "";
	
	if(sType == 'quiz') {
		sUrl = "<c:url value='/pms/edu/pmsEduOpenPopup.do?requestUrl=/pms/edu/eduResultQuizPopup.do&typeFlag=Y&width=840&height=628&eduSeq=" + sEduSeq +"&userId=" + sUserId + "' />";
		openParam = "dialogWidth: 850px; dialogHeight: 636px; resizable: 0, scroll: 0, center: 1,copyhistory:0,location:0,scrollbars:0,menubar:no,directories:0,toolbar:no";		
	} else if(sType == 'play') {
		sUrl = "<c:url value='/pms/edu/pmsEduOpenPopup.do?requestUrl=/pms/edu/eduResultPlayPopup.do&typeFlag=Y&width=840&height=628&eduSeq=" + sEduSeq +"&userId=" + sUserId + "' />";
		openParam = "dialogWidth: 850px; dialogHeight: 636px; resizable: 0, scroll: 0, center: 1,copyhistory:0,location:0,scrollbars:0,menubar:no,directories:0,toolbar:no";		
	} else if(sType == 'task') {
		sUrl = "<c:url value='/pms/edu/pmsEduOpenPopup.do?requestUrl=/pms/edu/eduResultTaskPopup.do&typeFlag=Y&width=840&height=628&eduSeq=" + sEduSeq +"&userId=" + sUserId + "' />";
		openParam = "dialogWidth: 850px; dialogHeight: 636px; resizable: 0, scroll: 0, center: 1,copyhistory:0,location:0,scrollbars:0,menubar:no,directories:0,toolbar:no";
	}
	
	if( sUrl != "" ) {
		retVal = window.showModalDialog(sUrl, "p_GrpPopup", openParam);   
	}
}
</script>
<table class="table table-list center ">
 <colgroup>
	 <col width="6%">
	 <col width="15%">
	 <col width="15%">
	 <col width="10%">
	 <col width="10%">
	 <col width="8%">
	 <col width="10%">
	 <col width="8%">
	 <col width="10%">
	 <col width="8%">
 </colgroup>
 <thead>
   <tr>
     <th rowspan="2">번호</th>
     <th rowspan="2">소속</th>
     <th rowspan="2">부서</th>
     <th rowspan="2">이름</th>
     <th colspan="2" class="title01">강의</th>
     <th colspan="2" class="title01">과제</th>
     <th colspan="2" class="title01">퀴즈</th>
   </tr>
   <tr>
     <th class="title02">진행/전체시간</th>
     <th class="title02">진행율</th>
     <th class="title02">진행/전체건수</th>
     <th class="title02">진행율</th>
     <th class="title02">진행/전체건수</th>
     <th class="title02">진행율</th>
   </tr>
 </thead>
 <tbody>
   <%-- 데이터를 없을때 화면에 메세지를 출력해준다 --%>
	<c:if test="${fn:length(tab3ResultList) == 0}">
		<tr>
			<td class="lt_text3" colspan="7">
				<spring:message code="common.nodata.msg" />
			</td>
		</tr>
	</c:if>
	<c:forEach items="${tab3ResultList}" var="result" varStatus="status">
		<tr >
			<td ><strong><c:out value="${result.rowId2}"/></strong></td>
			<td><c:out value="${result.ofcNm}"/></td>
			<td><c:out value="${result.deptNm}"/></td>
			<td><c:out value="${result.userNm}"/></td>
			<!-- 강의 -->
			<td style="cursor:pointer;" onclick="javascript:fnEduResultPopup('play','<c:out value="${result.eduSeq}"/>','<c:out value="${result.userId}"/>'); return false;">
				<c:out value="${result.playFinish}"/> / <c:out value="${result.playCnt}"/>
			</td>	
			<td style="cursor:pointer;" onclick="javascript:fnEduResultPopup('play','<c:out value="${result.eduSeq}"/>','<c:out value="${result.userId}"/>'); return false;">
				<c:out value="${result.playProcess}"/> %
			</td>
			<!-- 과제 -->
			<td style="cursor:pointer;" onclick="javascript:fnEduResultPopup('task','<c:out value="${result.eduSeq}"/>','<c:out value="${result.userId}"/>'); return false;">
				<c:out value="${result.taskFinish}"/> / <c:out value="${result.taskCnt}"/>
			</td>	
			<td style="cursor:pointer;" onclick="javascript:fnEduResultPopup('task','<c:out value="${result.eduSeq}"/>','<c:out value="${result.userId}"/>'); return false;">
				<c:out value="${result.taskProcess}"/> %
			</td>	
			<!-- 퀴즈  -->
			<td style="cursor:pointer;" onclick="javascript:fnEduResultPopup('quiz','<c:out value="${result.eduSeq}"/>','<c:out value="${result.userId}"/>'); return false;">
				<c:out value="${result.quizFinish}"/> / <c:out value="${result.quizCnt}"/>
			</td>		
			<td style="cursor:pointer;" onclick="javascript:fnEduResultPopup('quiz','<c:out value="${result.eduSeq}"/>','<c:out value="${result.userId}"/>'); return false;">
			<c:out value="${result.quizProcess}"/> %
			</td>		
		</tr>
	</c:forEach>
  </tbody>
</table>