<%--
  Class Name : eduResultList.jsp
  Description : 교육 성과 관리 JSP
  Modification Information
 
      수정일         수정자                   수정내용
    -------    --------    ---------------------------
     2017.03.03                 최초 생성
 
    author   : 공통서비스 개발팀 
    since    : 2017.03.03
--%>
<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>교육 성과 관리</title>
<%@ include file="/WEB-INF/jsp/pms/include-pms.jspf"%>
<script type="text/javascript" src="<c:url value='/js/showModalDialog.js' />" ></script>
<script type="text/javaScript" language="javascript" defer="defer">
$(document).ready(function() {
});
// 재조회 
function fnSearch(){
    document.eduVO.pageIndex.value = 1;
    document.eduVO.action = "<c:url value='/pms/edu/eduResultList.do'/>";
    document.eduVO.submit();
}

function fnEduResultPopup(sType , sEduSeq ,sUserId){
	var retVal;
	var sUrl = "";
	var openParam = "";
	
	if(sType == 'quiz') {
		sUrl = "<c:url value='/pms/edu/pmsEduOpenPopup.do?requestUrl=/pms/edu/eduResultQuizPopup.do&typeFlag=Y&width=840&height=628&eduSeq=" + sEduSeq +"&userId=" + sUserId + "' />";
		openParam = "dialogWidth: 850px; dialogHeight: 636px; resizable: 0, scroll: 0, center: 1,copyhistory:0,location:0,scrollbars:0,menubar:no,directories:0,toolbar:no";		
	} else if(sType == 'play') {
		sUrl = "<c:url value='/pms/edu/pmsEduOpenPopup.do?requestUrl=/pms/edu/eduResultPlayPopup.do&typeFlag=Y&width=840&height=628&eduSeq=" + sEduSeq +"&userId=" + sUserId + "' />";
		openParam = "dialogWidth: 850px; dialogHeight: 636px; resizable: 0, scroll: 0, center: 1,copyhistory:0,location:0,scrollbars:0,menubar:no,directories:0,toolbar:no";		
	} else if(sType == 'task') {
		sUrl = "<c:url value='/pms/edu/pmsEduOpenPopup.do?requestUrl=/pms/edu/eduResultTaskPopup.do&typeFlag=Y&width=840&height=628&eduSeq=" + sEduSeq +"&userId=" + sUserId + "' />";
		openParam = "dialogWidth: 850px; dialogHeight: 636px; resizable: 0, scroll: 0, center: 1,copyhistory:0,location:0,scrollbars:0,menubar:no,directories:0,toolbar:no";
	}
	
	if( sUrl != "" ) {
		retVal = window.showModalDialog(sUrl, "p_GrpPopup", openParam);   
	}
}
</script>

</head>
<body>
	<noscript class="noScriptTitle">자바스크립트를 지원하지 않는 브라우저에서는 일부 기능을 사용하실 수 없습니다.</noscript>
	<form name="eduVO" action="<c:url value='/pms/edu/eduSchdulMngList.do'/>" method="post">

	<!-- 현재위치 네비게이션 시작 -->
	<div class="conts" >
		<h1><i class="fa fa-chevron-circle-right  "></i> 나의 학습현황</h1>
		<input type="submit" id="invisible" class="invisible"/>
		
		<!-- 검색 필드 박스 시작 -->
		<div class="search-group">
			<input name="mode"  	type="hidden"  /> 
			<input name="eduSeq"   	type="hidden"  />
			<input name="pageIndex" type="hidden"  />
			
			<select name="searchCondition" id="searchCondition" title="검색조건2-검색어구분" class="form-control input-inline" style="width:80px;">
			 <option value="0" <c:if test="${eduVO.searchCondition == '0'}">selected="selected"</c:if> >제목</option>
			 <option value="1" <c:if test="${empty eduVO.searchCondition || eduVO.searchCondition == '1'}">selected="selected"</c:if> >내용</option>
			</select>
					  
			<input type="text" class="form-control" value="" style="width:200px" name="searchKeyword" id="searchKeyword" >
		
			<button id="button" class="btn btn-default " onclick="fnSearch(); return false;" > <i class="fa fa-search " aria-hidden="true"></i> 검색</button>

		</div>
		<!-- //검색 필드 박스 끝 -->
		
		
		<!-- 리스트 -->
	    <div class="of-y-no">
	       <table class="table table-list center ">
			 <colgroup>
				 <col width="6%">
				 <col width="12%">
				 <col width="12%">
				 <col width="*">
				 <col width="*">
				 <col width="6%">
				 <col width="6%">
				 <col width="7%">
				 <col width="6%">
				 <col width="7%">
				 <col width="6%">
			
			 </colgroup>
			 <thead>
			   <tr>
			     <th rowspan="2">번호</th>
			     <th rowspan="2">교육구분</th>
			     <th rowspan="2">시스템명</th>
			     <th rowspan="2">교육명</th>
			     <th rowspan="2">교육기간</th>
			     <th colspan="2" class="title01">강의</th>
			     <th colspan="2" class="title01">과제</th>
			     <th colspan="2" class="title01">퀴즈</th>
			   </tr>
			   <tr>
			     <th class="title02">진행/<br>총시간</th>
			     <th class="title02">진행율</th>
			     <th class="title02">진행/<br>전체건수</th>
			     <th class="title02">진행율</th>
			     <th class="title02">진행/<br>전체건수</th>
			     <th class="title02">진행율</th>
			   </tr>
			 </thead>
			 <tbody>
			   <%-- 데이터를 없을때 화면에 메세지를 출력해준다 --%>
				<c:if test="${fn:length(resultList) == 0}">
					<tr>
						<td class="lt_text3" colspan="7">
							<spring:message code="common.nodata.msg" />
						</td>
					</tr>
				</c:if>
				<c:forEach items="${resultList}" var="result" varStatus="status">
					<tr >
						<td ><strong><c:out value="${result.rowSort}"/></strong></td>
						<td><c:out value="${result.eduCatNm}"/></td>
						<td><c:out value="${result.sysNm}"/></td>
						<td><c:out value="${result.eduNm}"/></td>
						<td><c:out value="${result.eduSchdul}"/></td>
						<!--  강의  -->
						<td style="cursor:pointer;" onclick="javascript:fnEduResultPopup('play','<c:out value="${result.eduSeq}"/>','<c:out value="${result.userId}"/>'); return false;">
							<c:out value="${result.playFinish}"/> / <c:out value="${result.playCnt}"/>
						</td>	
						<td style="cursor:pointer;" onclick="javascript:fnEduResultPopup('play','<c:out value="${result.eduSeq}"/>','<c:out value="${result.userId}"/>'); return false;">
							<c:out value="${result.playProcess}"/> %
						</td>
						<!--  과제  -->
						<td style="cursor:pointer;" onclick="javascript:fnEduResultPopup('task','<c:out value="${result.eduSeq}"/>','<c:out value="${result.userId}"/>'); return false;">
							<c:out value="${result.taskFinish}"/> / <c:out value="${result.taskCnt}"/>
						</td>	
						<td style="cursor:pointer;" onclick="javascript:fnEduResultPopup('task','<c:out value="${result.eduSeq}"/>','<c:out value="${result.userId}"/>'); return false;">
							<c:out value="${result.taskProcess}"/> %
						</td>							
						<!--  퀴즈  -->
						<td style="cursor:pointer;" onclick="javascript:fnEduResultPopup('quiz','<c:out value="${result.eduSeq}"/>','<c:out value="${result.userId}"/>'); return false;">
							<c:out value="${result.quizFinish}"/> / <c:out value="${result.quizCnt}"/>
						</td>		
						<td style="cursor:pointer;" onclick="javascript:fnEduResultPopup('quiz','<c:out value="${result.eduSeq}"/>','<c:out value="${result.userId}"/>'); return false;">
							<c:out value="${result.quizProcess}"/> %
						</td>
					</tr>
				</c:forEach>
			  </tbody>
			</table>
	      
      </div>
	      
      <div style="width:100%; text-align:center">
        <ul class="pagination pagination-sm  " >
			<ui:pagination paginationInfo = "${paginationInfo}"  type="image" jsFunction="fnLinkPage" />
        </ul>
      </div>	 
	            
   	</div>
	<!-- //content 끝 -->
    
	</form>
	        
	<!-- footer 시작 
	<div id="footer"><c:import url="/EgovPageLink.do?link=main/inc/EgovIncFooter" /></div>
	-->
</body>
</html>