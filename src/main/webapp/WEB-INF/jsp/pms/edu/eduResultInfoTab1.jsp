<%--
  Class Name : eduResultInfoTab1.jsp
  Description : 교육 성과관리  상세정보 tab1 JSP
  Modification Information
 
      수정일         수정자                   수정내용
    -------    --------    ---------------------------
     2018.03.15                 최초 생성
 
    author   : 아무개
    since    : 2018.03.15
--%>
<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<script type="text/javaScript" >
function fnEduResultTab1Info(sEduSeq){
	
}
</script>
<table class="table table-list center ">
	<colgroup>
		<col width="22%">
		<col width="13">
		<col width="13%">
		<col width="13%">
		<col width="13%">
		<col width="13%">
		<col width="13%">
	</colgroup>
  <thead>
    <tr>
      <th rowspan="2">대분류</th>
      <th colspan="3" class="title01">교육진행현황(명)</th>
      <th colspan="3" class="title01">퀴즈진행현황(명)</th>
    </tr>
    <tr>
      <th class="title02">완료</th>
      <th class="title02">미완료</th>
      <th class="title02">진행율</th>
      <th class="title02">완료</th>
      <th class="title02">미완료</th>
      <th class="title02">진행율</th>
    </tr>
  </thead>
  <tbody>
    <%-- 데이터를 없을때 화면에 메세지를 출력해준다 --%>
	<c:if test="${fn:length(tab1ResultList) == 0}">
		<tr>
			<td class="lt_text3" colspan="7">
				<spring:message code="common.nodata.msg" />
			</td>
		</tr>
	</c:if>
	<c:forEach items="${tab1ResultList}" var="result" varStatus="status">
		<tr >
			<td ><strong><c:out value="${result.mnuNm}"/></strong></td>
			<td><c:out value="${result.playFinishCnt}"/></td>
			<td><c:out value="${result.playIngCnt}"/></td>			
			<td><c:out value="${result.playProcess}"/> %</td>
			<td><c:out value="${result.quizFinishCnt}"/></td>
			<td><c:out value="${result.quizIngCnt}"/></td>			
			<td><c:out value="${result.quizProcess}"/> %</td>
			
		</tr>
	</c:forEach>
  </tbody>
</table>
          