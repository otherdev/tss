<%--
  Class Name : eduResultList.jsp
  Description : 교육 성과 관리 JSP
  Modification Information
 
      수정일         수정자                   수정내용
    -------    --------    ---------------------------
     2017.03.03                 최초 생성
 
    author   : 공통서비스 개발팀 
    since    : 2017.03.03
--%>
<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>교육 성과 관리</title>
<%@ include file="/WEB-INF/jsp/pms/include-pms.jspf"%>
<script type="text/javaScript" language="javascript" defer="defer">
$(document).ready(function() {
	var sMode  = "<c:out value='${eduVO.mode}'/>";
	var sEduSeq   = "<c:out value='${eduVO.eduSeq}'/>";
	// var sCatCd = "<c:out value='${eduVO.catCd}'/>";
	var sPageIndex = "<c:out value='${eduVO.pageIndex}'/>";

	if( sMode.length == 0 ){
		$('[name="mode"]').val("VIEW");
	} else {
		$('[name="mode"]').val(sMode);
	}
	if( sEduSeq.length == 0) {
		$('[name="eduSeq"]').val("-1");
	} else {
		$('[name="eduSeq"]').val(sEduSeq);
	}

	if( sPageIndex.length == 0  ) {
		$('[name="pageIndex"]').val("1");
	} else {
		$('[name="pageIndex"]').val(sPageIndex);
	}
	
	// 메시지 처리 
	var sResultMsg = '<c:if test="${!empty resultMsg}"><spring:message code="${resultMsg}" /></c:if>';
	if(sResultMsg.length != 0 ) {
		alert(sResultMsg);
	}
});
// 목록화면으로 이동 
function fnLinkPage(pageNo){
    document.eduVO.pageIndex.value = pageNo;
    document.eduVO.action = "<c:url value='/pms/edu/eduResultList.do'/>";
    document.eduVO.submit();
}
// 재조회 
function fnSearch(){
    document.eduVO.pageIndex.value = 1;
    document.eduVO.action = "<c:url value='/pms/edu/eduResultList.do'/>";
    document.eduVO.submit();
}


// 수정화면으로 이동
function fnEduResultInfo(eduSeq){
	// alert("eduSeq=> " + eduSeq);
    document.eduVO.eduSeq.value = eduSeq;
    document.eduVO.mode.value = "VIEW";
    document.eduVO.action = "<c:url value='/pms/edu/eduResultInfo.do'/>";
    document.eduVO.submit();	
}

</script>

</head>
<body>
	<noscript class="noScriptTitle">자바스크립트를 지원하지 않는 브라우저에서는 일부 기능을 사용하실 수 없습니다.</noscript>
	<form name="eduVO" action="<c:url value='/pms/edu/eduSchdulMngList.do'/>" method="post">

	<!-- 현재위치 네비게이션 시작 -->
	<div class="conts" >
		<h1><i class="fa fa-chevron-circle-right  "></i> 교육성과관리</h1>
		<input type="submit" id="invisible" class="invisible"/>
		
		<!-- 검색 필드 박스 시작 -->
		<div class="search-group">
			<input name="mode"  	type="hidden"  /> 
			<input name="eduSeq"   	type="hidden"  />
			<input name="pageIndex" type="hidden"  />
			
			<select name="searchCondition" id="searchCondition" title="검색조건2-검색어구분" class="form-control input-inline" style="width:80px;">
			 <option value="0" <c:if test="${eduVO.searchCondition == '0'}">selected="selected"</c:if> >제목</option>
			 <option value="1" <c:if test="${empty eduVO.searchCondition || eduVO.searchCondition == '1'}">selected="selected"</c:if> >내용</option>
			</select>
					  
			<input type="text" class="form-control" value="" style="width:200px" name="searchKeyword" id="searchKeyword" >
		
			<button id="button" class="btn btn-default " onclick="fnSearch(); return false;" > <i class="fa fa-search " aria-hidden="true"></i> 검색</button>

		</div>
		<!-- //검색 필드 박스 끝 -->
		
		
		<!-- 리스트 -->
	    <div class="of-y-no">
	       <table class="table table-hover table-list center ">
	      
			<colgroup>
              <col width="7%">
              <col width="11%">
              <col width="20%">
              <col width="25%">
              <col width="17%">
              <col width="10%">
              <col width="10%">
			</colgroup>
	        
	        
	        <thead>
	          <tr>
	            <th><strong>번호</strong></th>
	            <th>교육구분</th>
	            <th>시스템</th>
	            <th>교육명</th>
	            <th>교육기간</th>
	            <th>대상자</th>
	            <th>등록일자</th>
	          </tr>
	        </thead>
	        <tbody>
	
				<%-- 데이터를 없을때 화면에 메세지를 출력해준다 --%>
				<c:if test="${fn:length(resultList) == 0}">
					<tr>
						<td class="lt_text3" colspan="7">
							<spring:message code="common.nodata.msg" />
						</td>
					</tr>
				</c:if>
				<c:forEach items="${resultList}" var="result" varStatus="status">
					<tr style="cursor:pointer;" onclick="javascript:fnEduResultInfo('<c:out value="${result.eduSeq}"/>'); return false;">
						<td ><strong><c:out value="${result.rn}"/></strong></td>
						<td><c:out value="${result.eduCatNm}"/></td>
						<td><c:out value="${result.sysNm}"/></td>
						
						<td class="left" >						 
						 	<c:out value="${result.eduNm}"/>
						</td>
						<td><c:out value="${result.eduSchdul}"/></td>
						<td><c:out value="${result.eduTarget}"/></td>
						<td><c:out value="${result.regDt}"/></td>
						
					</tr>
				</c:forEach>
	        </tbody>
	      </table>
	      
      </div>
	      
      <div style="width:100%; text-align:center">
        <ul class="pagination pagination-sm  " >
			<ui:pagination paginationInfo = "${paginationInfo}"  type="image" jsFunction="fnLinkPage" />
        </ul>
      </div>	      
   	</div>
	<!-- //content 끝 -->
    
	</form>
	        
	<!-- footer 시작 
	<div id="footer"><c:import url="/EgovPageLink.do?link=main/inc/EgovIncFooter" /></div>
	-->
</body>
</html>