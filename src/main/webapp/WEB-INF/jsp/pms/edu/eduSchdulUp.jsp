<%--
  Class Name : eduSchdulUp.jsp
  Description : 교육일정 등록 및 변경 JSP
  Modification Information
 
      수정일         수정자                   수정내용
    -------    --------    ---------------------------
     2017.03.03                 최초 생성
 
    author   : 공통서비스 개발팀 
    since    : 2017.03.03
--%>
<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>교육일정 상세정보</title>
<%@ include file="/WEB-INF/jsp/pms/include-pms.jspf"%>
<script type="text/javascript" src="<c:url value='/js/pms/edu/eduSchdulUp.js' />" ></script>
<script type="text/javascript" src="<c:url value='/js/showModalDialog.js' />" ></script>
<script type="text/javaScript" language="javascript" defer="defer">
var nRow = -1;
$(document).ready(function() {
	var nEduSeq = '${eduSchdulModVO.eduSeq}';
	$( "#eduStartDt,#eduEndDt" ).datepicker({
      showOn: "button",
      buttonImage: "/tts/images/calendar.gif",
      buttonImageOnly: true,
      buttonText: "Select date",
      dateFormat: "yy-mm-dd"
    });	
	// 달력 스타일 중앙정렬 
	$(".ui-datepicker-trigger").css("vertical-align","middle");
	$(".ui-datepicker-trigger").css("padding-left","3px");

	var sEduMngYn = "${sessionScope.isAdmYn}"; // "<c:out value='${eduMngYn}'/>";
	// alert($("#mode").val());
	$("#btnDel").hide();
	$("#btnUpdate").hide();
	if(sEduMngYn == 'Y'){
		$("#btnUpdate").show();
		if($("input[name='mode']").val() == 'UPDATE' ){
			$("#btnDel").show();
		}
	}
	// 그룹리스트
	gfn_selectList({sqlId: 'selectEduSchdulDetail', eduSeq: nEduSeq }, function(ret){
		lst = ret.data;
		// 그리드 초기화
		fnObj.pageStart();	
	});	
	
	// 공통코드 가져오기
	gfn_selectList({sqlId: 'selectCmmCd', mstCd:'EDCD'}, function(ret){
		EDCD_LIST = ret.data;
		
		// 과제리스트
		gfn_selectList({sqlId: 'selectEduSchdulDetail2', eduSeq:nEduSeq}, function(ret){
			lst2 = ret.data;
			// 그리드 초기화
			fnObj2.pageStart();	
		});	
	});

	gfn_init();
});
function fnRefresh(){

    document.bizModVO.action = "<c:url value='/pms/edu/eduSchdulUp.do'/>";
    document.bizModVO.submit();	
}

function fnListPage(){
    document.eduSchdulModVO.mode.value = "VIEW";
    document.eduSchdulModVO.action = "<c:url value='/pms/edu/eduSchdulList.do'/>";
    document.eduSchdulModVO.submit();	
}
function fnEduDelete(nEduSeq){

	if(!confirm("삭제하시겠습니까?")) return;
	
    document.eduSchdulModVO.eduSeq.value = nEduSeq;
    document.eduSchdulModVO.mode.value = "DELETE";
    document.eduSchdulModVO.action = "<c:url value='/pms/edu/eduSchdulDelete.do'/>";
    document.eduSchdulModVO.submit();	
	
}
function fnEduUpdate(nEduSeq){

    if(!gfn_formValid())	return;
	
	if( _jsDateCheck($("#eduStartDt").val(), $("#eduEndDt").val()) == false ) {
		return false;
	}
	
	if(!confirm("저장하시겠습니까?")) return;
	
	// ajax 저장 - 공정, 팀원
	var _lst = fnObj.grid.getList();
	var _lst2 = fnObj2.grid.getList();
	var objData = {
    		mode			:$('[name="mode"]').val(), 	// obj.eduSeq,
    		eduSeq			:$('[name="eduSeq"]').val(), 	// obj.eduSeq,
    		eduCatCd		:$('#eduCatCd').val(), 	// obj.catNm,
    		sysCd			:$('#sysCd').val(), 	// obj.ttl,
    		eduNm			:$('#eduNm').val(), 	// obj.contents,
    		eduContents		:$('#eduContents').val(), 	// obj.eduSchdul,
    		eduStartDt		:$('#eduStartDt').val(), 	// obj.startDt,
    		eduEndDt		:$('#eduEndDt').val(), 	// obj.endDt,
    		eduTarget		:$('#eduTarget').val()  // obj.catCd
    };

	//저장처리
	gfn_saveList(
			"/tts/pms/edu/eduSchdulSave.do"
			,{mainData:objData , lst: _lst, sqlId: 'insertEduGrpMap', lst2: _lst2, sqlId2: 'updateEduProject', eduSeq: '${eduSeq}'}
			, function(data){
				if (data.result) {			
					alert("성공적으로 저장되었습니다.");
			        document.eduSchdulModVO.eduSeq.value = -1;
			        document.eduSchdulModVO.mode.value = "VIEW";
			        document.eduSchdulModVO.action = "<c:url value='/pms/edu/eduSchdulList.do'/>";
			        document.eduSchdulModVO.submit();	
					return;
				}
				else{
					alert("저장에 실패하였습니다.","E");
					return;
				}
	});
}

// 그룹선택 팝업에 조회된 값 가져오기 
function fn_GetParam(sType){
	var sParam = ""; // new Array();
	if( sType == "GRP" ){
		var checkedList = myGrid.getList();
		for( var ii = 0 ; ii < checkedList.length ; ii++ ) { 
			if( checkedList[ii].grpId != null && checkedList[ii].grpId != '' ){
				sParam += checkedList[ii].grpId + "|";
			} 
		}
	} 

	return sParam;
}
// 그룹 팝업 리턴된 후 전체 삭제 처리 
function fn_GrpGridRemove(){
	
	myGrid.checkedColSeq(0, true); // 강제로 전체 선택 처리 
	
	var removeList = myGrid.getCheckedListWithIndex(0); // 체크된 리스트 조회 

	myGrid.removeListIndex(removeList);// 삭제 처리 
}

// 그룹 팝업 호출 
function fnGrpAppend(){
	var sGrp = fn_GetParam("GRP"); // '${eduSchdulModVO.eduSeq}'; 
	var retVal;
	var sUrl = "<c:url value='/pms/edu/pmsEduOpenPopup.do?requestUrl=/pms/edu/eduSchdulGrpPopup.do&typeFlag=Y&width=840&height=456&grpId=" + sGrp +"' />";  
	// var targetWin = window.open(arg1, "ShowModalDialog" + arg1, 'toolbar=no, location=no, directories=no, status=' + status + ', menubar=no, scrollbars=' + scroll + ', resizable=' + resizable + ', copyhistory=no, width=' + w	+ ', height=' + h + ', top=' + top + ', left=' + left);
	 var openParam = "dialogWidth: 856px; dialogHeight: 470px; resizable: 0, scroll: 0, center: 1,copyhistory:0,location:0,scrollbars:0,menubar:no,directories:0,toolbar:no";
	//var openParam = "Width=850,Height=464px,resizable=0,scroll=0,center=1,copyhistory=0,location=0,scrollbars=0,menubar=no,directories=0,toolbar=no";
   
    // retVal = fn_egov_popup("p_GrpPopup", sUrl, 850 , 464);
	retVal = window.showModalDialog(sUrl, "p_GrpPopup", openParam);   
	debugger;
	if(retVal != null &&  retVal != '' ) { 
		fn_SetGrp(retVal);
	}
}
var fn_SetGrp = function( retVal  ){
	var aList = new Array();
    var aRtn =  retVal.split("#R#");

    if(retVal != null &&  retVal != '' ) { 
    	// 기존 Data 삭제 처리 
        fn_GrpGridRemove();
    	for( var i = 0 ; i < aRtn.length ; i ++ ) {
    		
    		var aValue = aRtn[i].split("#C#");
    		if(aValue != '' && aValue != null ) {
        		var obj = new Object();
	    		for( var j = 0 ; j < aValue.length ; j ++ ) {
	    			var aVal = aValue[j].split("#S#");
	    			obj[aVal[0]] = aVal[1];
	    		}
	    		aList[i] = obj;
	    		//fnObj.grid.append(obj);
    		}
    	}
    	myGrid.fetchList(aList);
    	myGrid.redrawGrid();
    	
    }
}
//신규 과제 팝업
function fnProjectAppend(){
	var obj = new Object();
	obj.taskSeq = -1;
	obj.eduSeq = -1;
	obj.mode = "ADD";

	fnProjectPopup(obj);
}
// 수정 과제 팝업
function fn_ProjectUpdate(obj){
	obj.mode = 'UPDATE';
	debugger;
	fnProjectPopup(obj);
}
// 과제 팝업 호출 
function fnProjectPopup(obj){
	var retVal ;
	var sUrl = "<c:url value='/pms/edu/pmsEduOpenPopup.do?requestUrl=/pms/edu/eduSchdulProjectPopup.do&typeFlag=Y&width=830&height=400' />";  
	var openParam = "dialogWidth: 840px; dialogHeight: 410px; resizable: 0, scroll: 0, center: 1,copyhistory:0,location:0,scrollbars:0,menubar:no,directories:0,toolbar:no";

	retVal = window.showModalDialog(sUrl, obj, openParam,fn_SetProject);  

    if(retVal != null &&  retVal != '' ) {
    	fn_SetProject(retVal);
    }
    
}
var fn_SetProject = function (retVal){
	var obj = new Object();
	var aCol = retVal.split("#C#");
	for(var i=0;i<aCol.length;i++){
		if(aCol[i] !="" && aCol[i] != null ){
			var aVal = 	aCol[i].split("#S#");
			
			obj[aVal[0]] = aVal[1];
		}
	}
	if(obj.mode == 'ADD') {
		var sTaskSeq = fnObj2.grid.getRowCount()+1;
		obj.taskSeq = sTaskSeq;
		fnObj2.grid.append(obj);
	} else if(obj.mode == 'UPDATE') {
		var objGrd = myGrid2.getList();
		for(var i = 0 ; i < objGrd.length ; i ++ ){

			if( objGrd[i].taskSeq == obj.taskSeq){					
				var objData = {
                		taskSeq		:obj.taskSeq,
                		catNm		:obj.catNm,
                		ttl			:obj.ttl,
                		contents	:obj.contents,
                		startDt		:obj.startDt,
                		endDt		:obj.endDt,
                		eduSchdul	:obj.eduSchdul,
                		catCd		:obj.catCd
                };
				myGrid2.updateList(i,objData);
		    	myGrid2.redrawGrid();
			}
		}
	} else if(obj.mode == 'DELETE') {
		var objData = {};
        objData['taskSeq'] = obj.taskSeq;   
		fnObj2.grid.deleteRow(objData);
    	myGrid2.redrawGrid();
	} else {
		alert('오류발생');
	}
}
// function showModalDialogCallback(retVal) {
//     if(retVal) {
//     	fn_SetProject(retVal);    	
//     }
// }
</script>

</head>
<body>
	<noscript class="noScriptTitle">자바스크립트를 지원하지 않는 브라우저에서는 일부 기능을 사용하실 수 없습니다.</noscript>

	<!-- 현재위치 네비게이션 시작 -->
	<div class="conts" >
		<h1><i class="fa fa-chevron-circle-right  "></i> 교육일정관리</h1>
				
	    <!-- write -->
	    <form:form commandName="eduSchdulModVO" action="${pageContext.request.contextPath}/pms/biz/eduSchdulUp.do" name="eduSchdulModVO" method="post" enctype="multipart/form-data"  >
			<!-- 상세정보 작업 삭제시 prameter 전달용 input -->
			<div class="search-group">
				<!-- 	        검색조건 유지 -->
				<input name="mode"            type="hidden" value="<c:out value='${eduSchdulModVO.mode}'/>"/> 
				<input name="eduSeq"          type="hidden" value="<c:out value='${eduSchdulModVO.eduSeq}'/>" />
				<input name="eduCatCd"        type="hidden" value="<c:out value='${eduSchdulModVO.eduCatCd}'/>"/>
				<input name="pageIndex"       type="hidden" value="<c:out value='${eduSchdulModVO.pageIndex}'/>" />
				<input name="searchCondition" type="hidden" value="<c:out value='${eduSchdulModVO.searchCondition}'/>"/>
		        <input name="searchKeyword"   type="hidden" value="<c:out value='${eduSchdulModVO.searchKeyword}'/>"/>
		        <input name="pageIndex"       type="hidden" value="<c:out value='${eduSchdulModVO.pageIndex}'/>"/>
	        </div>
	        
		<!-- 상세정보 -->
	    		<div class="table-responsive">
		          <table class="table table-write">
		            <caption>
		            	교육일정관리
		            </caption>
		            <colgroup>
		            <col width="20%">
		            <col width="80%">
		            </colgroup>
		            <tbody>
		              <tr>
		                <th class="req">시스템구분</th>
		                <td>
		                    <form:select class="form-control input-inline reqVal" style="width:25%"  path="eduCatCd" id="eduCatCd"  >
								<c:if test="${mode == 'ADD'}">
			                        <form:option value="" label="--선택하세요--"/>
		                    	</c:if>
		                        <form:options items="${EDGB_result}" itemValue="dtlCd" itemLabel="cdNm"/>
		                    </form:select>
		                    <form:errors path="sysCd" cssClass="error"/>
		                    
		                    <form:select class="form-control input-inline reqVal" style="width:25%"  path="sysCd" id="sysCd"  >
								<c:if test="${mode == 'ADD'}">
			                        <form:option value="" label="--선택하세요--"/>
		                    	</c:if>
		                        <form:options items="${SYCD_result}" itemValue="dtlCd" itemLabel="cdNm"/>
		                    </form:select>
		                    <form:errors path="sysCd" cssClass="error"/>
		                  </td>
		              </tr>
		              
		               <tr>
		                 <th class="req">제목</th>
		                 <td>
		                    <form:input path="eduNm" id="eduNm" class="form-control  reqVal"  size="20"  maxlength="60" />
		                    <form:errors path="eduNm" cssClass="error" />
	               		 </td>
		               </tr>
		               
		              <tr>
		                <th class="req">내용</th>
		                <td>
		                    <form:textarea path="eduContents" id="eduContents" class="form-control  reqVal" style="height:200px;" rows="60" maxLength="120"  />
		                    <form:errors path="eduContents" cssClass="error" />
		                </td>
		              </tr>
		              
		               <tr>
		                 <th class="req">교육기간</th>
		                 <td>
		                    <form:input path="eduStartDt" id="eduStartDt" class="form-control  reqVal" style="width:150px;" size="20"  maxlength="8" />
	                        <form:errors path="eduStartDt" cssClass="error" /> ~
	                        <form:input path="eduEndDt" id="eduEndDt" class="form-control  reqVal"  style="width:150px;" size="20"  maxlength="8" />
	                        <form:errors path="eduEndDt" cssClass="error" />
	               		 </td>
		               </tr>
		               
		               <tr>
		                 <th class="req">대상자</th>
		                 <td>
		                    <form:input path="eduTarget" id="eduTarget" class="form-control"  size="20"  maxlength="60" />
		                    <form:errors path="eduTarget" cssClass="error" />
	               		 </td>
		               </tr>
		              <tr>
					  <th>그룹선택
						<p><a style="display:inline-block;" class="btn btn-table btn-xs" onclick="javascript:fnGrpAppend(); return false;">
						그룹지정</a></p>
		              </th>
					  <td>
		                			  	
					  	<div id="grdGrp" style="height:300px;"></div>
					  </td>
					</tr>
					<tr>
					  <th>과제/토론
						<p><a style="display:inline-block;" class="btn btn-table btn-xs" onclick="javascript:fnProjectAppend(); return false;">
						과제지정</a></p>
		              </th>
					  <td>		  	
					  	<div id="grdTask" style="height:300px;"></div>
					  </td>
					</tr>
		            </tbody>
		          </table>
		        </div>

          <div class="btn_group right "  >
            <button id="btnDel" class="btn btn-default" onClick="javascript:fnEduDelete('<c:out value="${eduSchdulModVO.eduSeq}"/>'); return false;"> <i class="fa fa-close" aria-hidden="true"></i> 삭제</button>
            <button id="btnUpdate" class="btn btn-default" onClick="javascript:fnEduUpdate('<c:out value="${eduSchdulModVO.eduSeq}"/>'); return false;"> <i class="fa fa-edit" aria-hidden="true"></i> 저장</button>
            <button id="button" class="btn btn-default" onClick="javascript:fnListPage(); return false;"> <i class="fa fa-bars" aria-hidden="true"></i> 목록</button>
          </div>
    	</form:form>
	      
   	</div>
	<!-- //content 끝 -->
	        
	<!-- footer 시작 
	<div id="footer"><c:import url="/EgovPageLink.do?link=main/inc/EgovIncFooter" /></div>
	-->
</body>
</html>