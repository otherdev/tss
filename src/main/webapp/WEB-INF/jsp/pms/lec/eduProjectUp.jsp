<%--
  Class Name : eduDataList.jsp
  Description : 교육자료(조회,삭제) JSP
  Modification Information
 
      수정일         수정자                   수정내용
    -------    --------    ---------------------------
     2017.03.03                 최초 생성
 
    author   : 공통서비스 개발팀 
    since    : 2017.03.03
--%>
<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>과제 상세</title>
<%@ include file="/WEB-INF/jsp/pms/include-pms.jspf"%>
<script type="text/javascript" src="<c:url value='/js/EgovMultiFile.js'/>"></script> 
<script type="text/javascript" src="<c:url value='/js/jquery-1.4.2.min.js'/>"></script> 
<script type="text/javaScript" language="javascript" defer="defer">
var sEdtYn = "";
$(document).ready(function() {
	var sEduSeq = '${eduSeq}';
	var sTaskSeq = '${taskSeq}';
	sEdtYn = '${edtYn}';
	//------------------------------------------
	//------------------------- 첨부파일 수정 Start
	//-------------------------------------------
	var maxFileNum = document.getElementById('posblAtchFileNumber').value;
	if(maxFileNum==null || maxFileNum==""){ maxFileNum = 5;}
	var multi_selector = new MultiSelector( document.getElementById( 'egovComFileList' ), maxFileNum, 'file_label');
	multi_selector.addElement( document.getElementById( 'egovfile_1' ) );	
	multi_selector.update_count = 0;
	
	
	// 첨부파일 변경 처리 
	$('#egovComFileList').bind("DOMSubtreeModified",function(){
		var objDiv = $("#egovComFileList div:last-child");
		$("#lastFileNm").val(objDiv.text());// 추가된 파일 화면에 표시 
		if($("#egovComFileList div").size() > 1 ){
			$("#egovComFileList div:first-child").remove(); // 이전 파일 삭제 처리 
			multi_selector.count = 1; // 파일갯수 초기화 
		}
	});

	// 정보 없이 조회 불가능 처리 
	if( sEduSeq!=null && sEduSeq != '' && sEduSeq != -1 && sTaskSeq!=null && sTaskSeq != '' && sTaskSeq != -1 ) {
		fn_ReplyAdd(sEduSeq , sTaskSeq);
	}
});
function fnListPage(){
    document.sysEduVO.action = "<c:url value='/pms/lec/eduProjectList.do'/>";
    document.sysEduVO.submit();	
}
function fn_ReplyAdd(sEduSeq , sTaskSeq){
	var sRegId = "${sessionScope.id}";// '${regId}';
	var sCloseYn = '${sysEduVO.closeYn}'; 
	var sEduMngYn = "${sessionScope.isAdmYn}"; // "${eduMngYn}";

	/// 댓글목록 html append
	gfn_selectList({sqlId: 'selectSysEduProjectReply', eduSeq:sEduSeq, taskSeq:sTaskSeq}, function(ret){
		var lst = [];
		lst = ret.data;
		
		// 배열데이터순회
		$("#replList > li").remove();
		
		$.each(lst, function(idx, map){
			var sFileSeq = map['fileSeq'].toString();
			var seq = map['seq'].toString();

			var sHtml = '';
			sHtml += '<li>' ; 
			sHtml += '<div class="comment" id="submit_'+ map['submitSeq'] + '" >' ; 
			sHtml += '<input type="hidden" id="submitSeq" value="'	+  map['submitSeq'] + '" >' ; 
			sHtml += '<input type="hidden" id="fileSeq" value="'	+  	map['fileSeq'] + '" >' ; 
			sHtml += '<input type="hidden" id="seq" value="'		+  	map['seq'] + '" >' ; 
			sHtml += '<p >' ; 
			sHtml += map['regNm'] + " [" +map['regId'] + "] " +  map['regDt']; 
			sHtml += '<span class=" f_r"> <a href="#" id="fileDown" onClick="fn_downFile('+ map['submitSeq'] + ');return false;" ><img src="/tts/images/icon_co_file.png"  alt="파일다운로드 아이콘" >' ; 
			sHtml += map['orignlFileNm'] ; 
			sHtml += '</a>' ; 

			if( sRegId == map['regId'] ){ // 본인만 변경가능 처리 
				sHtml += '<a href="#" disabled onClick="fn_replayUpdate('+ map['submitSeq'] + ');return false;" ><img src="/tts/images/icon_co_edit.png"  alt="수정 아이콘"  > 수정</a>' ;  
			} 
			if( sRegId == map['regId'] || sEduMngYn == 'Y' ){ // 본인만 변경가능 처리 
				sHtml += '<a href="#" disabled onClick="fn_replayDelete('+ map['submitSeq'] + ');return false;" ><img src="/tts/images/icon_co_delete.png"  alt="삭제 아이콘"> 삭제</a>' ; 
			} 

			sHtml += '</span> </p>' ; 
			sHtml += '<p class="cmttxt">'; 
			
			if( sRegId == map['regId'] && sCloseYn == 'ING'){ // 본인만 변경가능 처리 
				sHtml += '<textarea id="contents" name="contents" class="form-control" style="height:80px;" rows="60" >'+ map['contents'] +'</textarea>';
			} else {
				sHtml += map['contents'].toString().replace("\n",'<br>') ; 
			}
			
			sHtml += '</div> </li>'; 
			// console.log("sHtml=> " + sHtml);
			$("#replList").append(sHtml);
		});
	});	

	fn_btn_auth();
}

function fn_btn_auth(){
	var sRegId = "${sessionScope.id}";//'${regId}';
	var sCloseYn = '${sysEduVO.closeYn}'; 
	// alert("sCloseYn=> " + sCloseYn);
	if(sCloseYn == 'ING') {
		$(".comment-write").show();	
		/*
		$("#replList a").each(function(i,e){ 
			//console.log(e.id + " ==== " + $(this).attr('href'));
			$(this).attr("disabled",false); 
			// $(this).attr("href",""); 
		});

		*/
		$(".comment-write a").each(function(i,e){ 
			$(this).attr("disabled",false); 
		});
	} else {
		$(".comment-write").hide();	

		$("#replList a").each(function(i,e){ 
			if(e.id != 'fileDown'){
				$(this).hide();
			}
			
		});
	}
}
function fn_downFile(sSubmitSeq){
	var sFileSeq = $("#submit_"+sSubmitSeq + " input[id='fileSeq']").val();
	var sSeq = $("#submit_"+sSubmitSeq + " input[id='seq']").val();

    window.open("<c:url value='/cmm/fms/FileDown.do?fileSeq="+sFileSeq+"&seq="+sSeq+"'/>");
}   
function fn_replayUpdate(sSubmitSeq){
	var sEduSeq = $("input[name='eduSeq']").val();
	var sTaskSeq = $("input[name='taskSeq']").val() ;

	if(sSubmitSeq==null || sSubmitSeq == '' || sSubmitSeq == undefined ) {return false;}
	
	var sContents = $("#submit_"+sSubmitSeq + " textarea").val();
	
	if(sContents.length > 120){
		alert("제출내용은 120글자를 넘을수 없습니다.");
		return false;
	}
	var objMainData = {
			submitSeq:sSubmitSeq , 
			contents: sContents
	};
	//저장처리
	gfn_saveList(
			"/tts/pms/lec/sysEduProjectReplyUpdate.do"
			,{ mainData : objMainData }
			, function(data){
				if (data.result) {	
					alert("수정 되었습니다.");
					fn_ReplyAdd(sEduSeq , sTaskSeq);
					return;
				}
				else{
					alert("저장에 실패하였습니다.","E");
					return;
				}
	});
}
function fn_replayDelete(sSubmitSeq){
	if(!confirm("삭제하시겠습니까?")) return;
	var sEduSeq = $("input[name='eduSeq']").val();
	var sTaskSeq = $("input[name='taskSeq']").val() ;
	if(sSubmitSeq==null || sSubmitSeq == '' || sSubmitSeq == undefined ) {return false;}
	var objMainData = {
			submitSeq:sSubmitSeq
	};
	//저장처리
	gfn_saveList(
			"/tts/pms/lec/sysEduProjectReplyDelete.do"
			,{ mainData: objMainData }
			, function(data){
				if (data.result) {	
					alert("삭제 되었습니다.");
					fn_ReplyAdd(sEduSeq , sTaskSeq);
					return;
				}
				else{
					alert("저장에 실패하였습니다.","E");
					return;
				}
	});
}
function fn_replayInsert(){
    // if(!gfn_formValid())	return;
    if($("#mainContents").val() == null || $("#mainContents").val() == ''){
    	alert("등록할 내용이 없습니다.");
    	return false;
    } else if($("#mainContents").val().length > 120){
    		alert("제출내용은 120글자를 넘을수 없습니다.");
    		return false;
    	}

	if(!confirm("저장하시겠습니까?")) return;
	
	var sEduSeq = $("input[name='eduSeq']").val();
	var sTaskSeq = $("input[name='taskSeq']").val() ;
	var sContents = $("#mainContents").val();

	// 첨부파일 세팅 단건 최종건에 대하여	
	var fdata = new FormData($("#sysEduVO"));   
	$.each($(".egov_file_box input[type='file']"),function(i,e){
		if( e.files.length > 0 ) {
			fdata.append(e.id, e.files[0]);
		}
	});

	fdata.append("eduSeq", sEduSeq );
	fdata.append("taskSeq", sTaskSeq );
	fdata.append("submitSeq", -1 );
	fdata.append("contents", sContents );

	$.ajax({
		url : "/tts/pms/lec/sysEduProjectReplyCreate.do" ,
		processData: false,
        contentType: false ,
        data: fdata,
        type: 'POST',
        success: function(result){
        		// console.log("result=> " + result);
        		debugger;
				if (result) {
					alert("등록 되었습니다.");
					var sContents = $("#mainContents").val("");
					$("#egovComFileList > div").remove();
					$(".egov_file_box > input").each(function(i,e){
						if(e.id != 'egovfile_1' ){
							$(this).remove();
						}
					});
					fn_ReplyAdd(sEduSeq , sTaskSeq);
					return;
				}
				else{
					alert("등록 실패하였습니다.","E");
					return;
				}
        },
		error : function(xhr, status, error) {
			//			proc = false;
			//			closeModal();
			alert("ERROR : " + error);
		}
	});

}

//강제 Submit 막기 
var fnNoSubmit = function(e){
	e.preventDefault();
	e.stopPropagation();
	/* do something with Error */
};
$("form[name='sysEduVO']").bind("submit",fnNoSubmit);

'<c:if test="${!empty resultMsg}">alert("<spring:message code="${resultMsg}" />");</c:if>'
</script>

</head>
<body>
	<noscript class="noScriptTitle">자바스크립트를 지원하지 않는 브라우저에서는 일부 기능을 사용하실 수 없습니다.</noscript>
	<form name="sysEduVO"  onkeydown="gfn_fireKey(event);" method="post" enctype="multipart/form-data" action="" >

      <div class="contents-box"  > 
	<!-- 현재위치 네비게이션 시작 -->
	<div class="conts" >
		<h1><i class="fa fa-chevron-circle-right  "></i> 토론 및 과제</h1>
          <!-- 상세정보 작업 삭제시 prameter 전달용 input -->
			<div class="search-group">
				<!-- 	        검색조건 유지 -->
				<input name="mode"            type="hidden" value="<c:out value='${sysEduVO.mode}'/>"/> 
				<input name="eduSeq"          type="hidden" value="<c:out value='${sysEduVO.eduSeq}'/>" />
				<input name="taskSeq"         type="hidden" value="<c:out value='${sysEduVO.taskSeq}'/>"/>
				<input name="pageIndex"       type="hidden" value="<c:out value='${sysEduVO.pageIndex}'/>" />
				<input name="searchCondition" type="hidden" value="<c:out value='${sysEduVO.searchCondition}'/>"/>
		        <input name="searchKeyword"   type="hidden" value="<c:out value='${sysEduVO.searchKeyword}'/>"/>		
				<!-- 최대 첨부파일 갯수  -->
	        	<input name="posblAtchFileNumber" type="hidden" id="posblAtchFileNumber" value="5" />        
	        </div>
          <!-- view -->
          
          <div class="of-y-no">
            <table class="table table-hover table-view  ">
              <thead>
                <tr>
                  <th ><c:out value='${sysEduVO.ttl}'/></th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td ><span class="f_l"><b>기간</b> <c:out value='${sysEduVO.projectSchdul}'/></span></td>
                </tr>
                <tr>
                  <td  class="view-top"  style="height:150px"> <c:out value='${sysEduVO.contents}'/> </td>
                </tr>
              </tbody>
            </table>
          </div>
          <div class="btn_group right "  >
            <button id="button" class="btn btn-default" onclick="fnListPage(); return false;" > <i class="fa fa-bars" aria-hidden="true"></i> 목록</button>
          </div>
          
          <!-- //view --> 
          
          <!--comment-->
          <div class="comment-group">
          <ul id="replList" class="comment-group">          
          </ul>
          </div>          
          <!--//comment--> 
          
          <!--comment-write-->
          <div class="comment-write">
            <table class="table table-border-no table-comment" >            
             <colgroup>         
             <col width="80px">
            <col width="*">
            <col width="90px">
            </colgroup>            
            <tbody>
              <tr>
                <td colspan="3" class=" cmtname">${sessionScope.name}[${sessionScope.id}]</td>
              </tr>
              <tr >
                <td colspan="2" > 
                	<textarea id="mainContents" name="mainContents" class="form-control" style="width:98.5%; height:60px;" rows="60"  ></textarea>
                </td>
                <td class="right" ><a href="#" disabled onClick="fn_replayInsert(); return false;" class="btn btn-table " style="height:50px; width:70px; vertical-align:middle; line-height:50px; "> <i class="fa fa-plus "></i> 등록</a></td>
              </tr>
              <tr >
					<!-- attached file Start -->
                <td><span>첨부파일</span></td>
                <td >
                	<input id="lastFileNm" type="text" class="form-control" readonly  value=""  style="background-color:#fff; width:98.5%;">
                		<div id="egovComFileList" style="display:none;"></div>
                </td>
                <td class="right" >
                		<div class="egov_file_box" style="display:none;" >
							<input type="file" name="file_1" id="egovfile_1"> 
						</div>
						<label for="egovfile_1" id="file_label" class="btn btn-table "  >파일불러오기</label > 					
				</td>
					<!-- attached file End -->
              </tr>
              </tbody>
            </table>
          </div>
          
          <!--//comment-write--> 
	</div>
	      
   	</div>
	<!-- //content 끝 -->
	</form>
</body>
</html>