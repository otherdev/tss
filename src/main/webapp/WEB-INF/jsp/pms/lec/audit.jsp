<%--
  Class Name : workUpdt.jsp
  Description : 작업상세조회, 수정 JSP
  Modification Information
 
      수정일         수정자                   수정내용
    -------    --------    ---------------------------
     2017.03.03                 최초 생성
 
    author   : 공통서비스 개발팀
    since    : 2017.03.03
--%>
<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html >
<html>
<head>
<title>작업 상세 및 수정</title>

<%@ include file="/WEB-INF/jsp/pms/include-pms.jspf"%>
<script type="text/javascript" src="<c:url value='/js/showModalDialog.js' />" ></script>
<script type="text/javaScript" language="javascript" defer="defer">
var _fileURL = "";
var _mnuCd = ""; //선택된 교육메뉴
var _recordSeq = ""; //강의이력키

$(document).ready(function() {
	// alert("${sysCd}");
});

// 교육시스템 메뉴동영상 리스트	
function fnRefresh(sysCd){
    // document.eduModVO.mode.value = "UPDATE";
    
    location.href = "<c:url value='/pms/lec/audit.do'/>" + "?sysCd=" + sysCd;
}

// 동영상로드
function fnVideo(fileNm, mnuCd){
	//_fileURL = "${pageContext.request.contextPath}/media/"+fileNm;
	_fileURL = "<c:url value='/lec/FileDown.do?fileNm="+fileNm+"'/>";
	_mnuCd = mnuCd;
	getVideo();
}

//강의시작
function fn_start(){
	console.log("fn_start.....");
	var mnuCd = _mnuCd;
	var userId = "${sessionScope.id}";
	if(gfn_isNull(userId)){
		alert("사용자 세션이 종료되었습니다.");
		return;
	}
	if(gfn_isNull(mnuCd)){
		alert("교육메뉴정보가 없습니다.");
		return;
	}
	
	//저장처리
	gfn_saveList(
			"/tts/pms/insertAudit.do"
			,{sqlId:"insertAudit", data:{mnuCd: mnuCd, userId: userId} }
			, function(data){
				if (data.result) {
					// 저장후 이력키 세팅(강의종료 처리를 위해..)
			        _recordSeq = data.recordSeq;
					return;
				}
				else{
					alert("저장에 실패하였습니다.","E");
					return;
				}
	});
}

//강의중지
function fn_end(){
	var mnuCd = _mnuCd;
	var recordSeq = _recordSeq;
	
	gfn_saveCmm({sqlId:"updateAuditEnd", data:{mnuCd: mnuCd, userId: "${sessionScope.id}", recordSeq: recordSeq}  }
		,function(data){
			if (data.result) {
				console.log("성공적으로 강의종료 처리되었습니다.");
				return;
			}		
			else{
				alert("저장에 실패하였습니다.","E");
				return;
			}
		});
}
function fn_QuizPopup(){
	var sMnuCd = _mnuCd;
	
	if(sMnuCd != "" ) {
		var retVal;
		var sUrl = "<c:url value='/pms/edu/pmsEduOpenPopup.do?requestUrl=/pms/lec/eduQuizPopup.do&typeFlag=Y&width=840&height=416&mnuCd=" + sMnuCd + "' />";
		var openParam = "dialogWidth: 850px; dialogHeight: 424px; resizable: 0; scroll: 0; center: 1; copyhistory:0; location:0; scrollbars:0; menubar:no; directories:0; toolbar:no;";; 		
		
		retVal = window.showModalDialog(sUrl, "p_GrpPopup", openParam);	
	} else {
		alert("강의를 선택하세요.");
		return false;
	}
}

function fn_MapPopup(){
		var retVal;
		var sUrl = "<c:url value='/pms/lec/eduMapPopup.do' />";
		var openParam = "dialogWidth: 850px; dialogHeight: 422px; resizable: 0; scroll: 0; center: 1; copyhistory:0; location:0; scrollbars:0; menubar:no; directories:0; toolbar:no;";; 		
		
		retVal = window.open(sUrl, "p_MapPopup", openParam);	
}
//-->
</script>


</head>
<body>
<noscript>자바스크립트를 지원하지 않는 브라우저에서는 일부 기능을 사용하실 수 없습니다.</noscript>    

<!-- container -->
<div id="container"  class="content" > 
  
  <!-- content -->
  <div id="content" > 
    
    <!--contents-box-->
    
    <div class="contents-box"  > 
      
      <!--conts-->
      <div class="conts"  >
        <h1><i class="fa fa-chevron-circle-right  "></i> 강의동영상
        	<!-- button button id="button" style="float:right; " class="btn btn-default " onclick="fn_MapPopup(); return false;">맵</button -->
        	<button button id="button" style="float:right; " class="btn btn-default " onclick="fn_QuizPopup(); return false;">퀴즈</button>
       	</h1>
        <!-- list -->
      <div style="width:20%;  margin-right:10px ; display:inline-block; position:relative; min-height:400px;">
      
       <div class="search-group "  style=" margin-bottom:3px">
		<span class="f_l"><b>시스템명</b> </span>
        
       	<select class="form-control input-inline" path="sysCd" id="sysCd" style="width:70%" value="${sysCd}" onchange="javascript:fnRefresh(this.options[this.selectedIndex].value);" >
			<c:forEach items="${SYCD_result}" var="result" varStatus="status">
			<c:if test="${sysCd == result.dtlCd}">                
       			<option selected="selected" value="${result.dtlCd}">${result.cdNm}</option>
           	</c:if>
           	<c:if test="${sysCd != result.dtlCd}">                
       			<option value="${result.dtlCd}">${result.cdNm}</option>
           	</c:if>
       		<!-- option value="${result.dtlCd}">${result.cdNm}</option-->
			</c:forEach>
       	</select>

		</div>     
      
      
       <div class="conts-gray of-y-scroll" style="height:460px;">
         <table class="table table-hover table-list center "  >
        
         <colgroup>
           <col width="*">
         
        
         
          </colgroup>
          
          
          <thead>
            <tr>
              <th>대분류</th>
              </tr>
          </thead>
          <tbody>
          
				<c:forEach items="${resultList}" var="result" varStatus="status">
					<tr style="cursor:hand;" >
						<td style="cursor:pointer;cursor:hand" > 
						 	<span class="link left"><a href="#" onclick="javascript:fnVideo('<c:out value="${result.mnuFle}"/>','<c:out value="${result.mnuCd}"/>'); return false;">
						 	<c:out value="${result.mnuNm}"/></a>
					 		</span>
						</td>
					</tr>
				</c:forEach>

          </tbody>
        </table>
	 </div>
	 
	 
	 
	 <div style="margin-top:4px; right:0px; bottom:-60px;">
	    <div id="buttonbar"  style="font-size:larger;">
	    	<div style="display:block; float:left;" >
	        	<button id="btnPlay" title="Play button" class="btn btn-default"> <i class="fa fa-play" aria-hidden="true"></i></button>
	        </div>
	        <div style="display:block; float:right">
		        <button id="btnVolDown" title="volume down" style="background-color:#47C83E;" class="btn btn-default"> <i class="fa fa-volume-down" aria-hidden="true"></i></button>
		        <button id="btnVolUp" title="volume up" style="background-color:#47C83E;" class="btn btn-default"> <i class="fa fa-volume-up" aria-hidden="true"></i></button>
	        </div>
	    </div>   
	    <br/>  
		 
	    <div title="Error message area" id="errorMsg" style="color:Red;"></div>  
		 
	 </div>
	 
	</div>
    <!-- //list --> 
 
 
 
 
     <!-- list2 -->
     <div style="width:77%;  display:inline-block; float:right;">
      
      

	    <video id="Video1" style="border:1px solid blue;" height="526px" width="990px" title="video element"> 
	    </video>
     </div>
        
   	<!-- //list 2-->  
        
      </div>
      <!--conts--> 
    
    
    
      
    </div>
    
    <!--//contents-box--> 
    
  </div>
  <!-- //content --> 
</div>
<!-- //container --> 



<script>
    var video = document.getElementById("Video1");
    var vLength;
    var pgFlag = ""; // used for progress tracking
    var nVolumne = 0.5;
    
    console.log(" video.controls=> " +  video.controls ) ;
    debugger;
    if (video.canPlayType) {   // tests that we have HTML5 video support

      //  video button helper functions
      
      
      //  load video file from input field
      function getVideo() {
          var fileURL = _fileURL;                    
        if (fileURL != "") {
          video.src = fileURL;
          video.load();  // if HTML source element is used
          document.getElementById("btnPlay").click();  // start play
        } else {
          errMessage("Enter a valid video URL");  // fail silently
        }
      }



      //  Play 버튼클릭 
      document.getElementById("btnPlay").addEventListener("click", function vidplay(evt) {
    	  if (video.src == "") {  // inital source load
   		  	alert("강의를 선택하세요.");
  	  		return;
   		  }
   		  if (video.paused) {   // play the file, and display pause symbol
   		  	video.play();
   		  } else {              // pause the file, and display play symbol  
   		  	video.pause();
   		  }
      }, false);

      //  btnVolDown 버튼클릭 
      document.getElementById("btnVolUp").addEventListener("click", function () {    
    	  if(nVolumne + 0.1 > 1 ) nVolumne = 1; 
    	  else nVolumne= nVolumne + 0.1;
    	  //console.log("nVolumne=> " + nVolumne.toFixed(1));
    	  video.volume = nVolumne.toFixed(1);
      });

      //  Play 버튼클릭 
      document.getElementById("btnVolDown").addEventListener("click", function () {    	  
    	  if(nVolumne - 0.1 < 0  ) nVolumne = 0; 
    	  else nVolumne= nVolumne - 0.1;
    	  ///console.log("nVolumne=> " + nVolumne.toFixed(1));
    	  video.volume = nVolumne.toFixed(1);
      });
      //  Restart
      
      //  set src == latest video file URL
      //document.getElementById("loadVideo").addEventListener("click", getVideo, false);



      //  any video error will fail with message 
      video.addEventListener("error", function (err) {
        errMessage(err);
      }, true);
      // content has loaded, display buttons and set up events
      video.addEventListener("canplay", function () {
      }, false);

      //  display video duration when available
      video.addEventListener("loadedmetadata", function () {
      }, false);

      //  display the current and remaining times
      video.addEventListener("timeupdate", function () {
        //  Current time  
      }, false);
      
      
      // 재생중지 이벤트	
      video.addEventListener("pause", function () {
        $("#btnPlay > i").removeClass("fa-pause");
        $("#btnPlay > i").addClass("fa-play");
        try{
        	fn_end();
        }catch(e){}
      }, false);

     // 재생시작 이벤트
      video.addEventListener("playing", function () {
        $("#btnPlay > i").removeClass("fa-play");
        $("#btnPlay > i").addClass("fa-pause");
        //video.style.width = "100%";
        //video.style.height = "100%";
        try{
	        fn_start();
        }catch(e){}
      }, false);

      video.addEventListener("volumechange", function () {
        if (video.muted) {
          // if muted, show mute image
        } else {
          // if not muted, show not muted image
        }
      }, false);
      //  Download and playback status events.
      video.addEventListener("loadstart", function () {
      }, false);
      video.addEventListener("loadeddata", function () {
      }, false);

      video.addEventListener("ended", function () {
      }, false);

      video.addEventListener("emptied", function () {
      }, false);

      video.addEventListener("stalled", function () {
      }, false);
      video.addEventListener("waiting", function () {
      }, false);
      video.addEventListener("progress", function () {
      }, false);
      video.addEventListener("durationchange", function () {
      }, false);
      video.addEventListener("canplaythrough", function () {
      }, false);
    } else {
      errMessage("HTML5 Video is required for this example");
      // end of runtime
    }
    //  display an error message 
    function errMessage(msg) {
      // displays an error message for 5 seconds then clears it
      document.getElementById("errorMsg").textContent = msg;
      setTimeout("document.getElementById('errorMsg').textContent=''", 5000);
    }
</script>

</body>
</html>

