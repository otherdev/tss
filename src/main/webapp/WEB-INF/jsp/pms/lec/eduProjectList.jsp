<%--
  Class Name : eduDataList.jsp
  Description : 교육자료(조회,삭제) JSP
  Modification Information
 
      수정일         수정자                   수정내용
    -------    --------    ---------------------------
     2017.03.03                 최초 생성
 
    author   : 공통서비스 개발팀 
    since    : 2017.03.03
--%>
<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>과제관리</title>
<%@ include file="/WEB-INF/jsp/pms/include-pms.jspf"%>
<script type="text/javaScript" language="javascript" defer="defer">

$(document).ready(function() {
	var sMode  = "<c:out value='${eduVO.mode}'/>";
	var sSeq   = "<c:out value='${eduVO.eduSeq}'/>";
	var sCatCd = "<c:out value='${eduVO.taskSeq}'/>";
	var sPageIndex = "<c:out value='${eduVO.pageIndex}'/>";

	if( sMode.length == 0 ){
		$('[name="mode"]').val("VIEW");
	} else {
		$('[name="mode"]').val(sMode);
	}
	if( sSeq.length == 0) {
		$('[name="eduSeq"]').val(-1);
	} else {
		$('[name="eduSeq"]').val(sSeq);
	}
	if( sCatCd.length == 0) {
		$('[name="taskSeq"]').val(-1);
	} else {
		$('[name="taskSeq"]').val(sCatCd);
	}
	if( sPageIndex.length == 0  ) {
		$('[name="pageIndex"]').val("1");
	} else {
		$('[name="pageIndex"]').val(sPageIndex);
	}
	
	// 메시지 처리 
	var sResultMsg = '<c:if test="${!empty resultMsg}"><spring:message code="${resultMsg}" /></c:if>';
	if(sResultMsg.length != 0 ) {
		alert(sResultMsg);
	}
	gfn_init();
});

//재조회 
function fnSearch(){
	document.eduVO.pageIndex.value = 1;
	document.eduVO.action = "<c:url value='/pms/lec/eduProjectList.do'/>";
	document.eduVO.submit();
}

function fnEduDataInfo(sEduSeq,sTaskSeq,sPressYn){
	var sMode = "VIEW";
// 	if(sPressYn == 'N') {
// 		alert("해당 교육 참석자가 아닙니다.");
// 		// return false;
// 	} else if(sPress == 'Y'){
// 		sMode = 'UPDATE';
// 		// alert(sEduSeq + " = " + sTaskSeq+" = " + sPressYn);
// 	}
	document.eduVO.eduSeq.value = sEduSeq; 
	// document.eduVO.pageIndex.value = 1; 
	document.eduVO.taskSeq.value = sTaskSeq; 
	//		
	document.eduVO.action = "<c:url value='/pms/lec/eduProjectUp.do'/>"; 
    document.eduVO.submit();	
}

</script>

</head>
<body>
	<noscript class="noScriptTitle">자바스크립트를 지원하지 않는 브라우저에서는 일부 기능을 사용하실 수 없습니다.</noscript>
	<form name="eduVO" action="<c:url value='/pms/lec/eduProjectList.do'/>" method="post">

      <div class="contents-box"  > 
	<!-- 현재위치 네비게이션 시작 -->
	<div class="conts" >
		<h1><i class="fa fa-chevron-circle-right  "></i> 토론 및 과제</h1>
          <!-- list -->
    
		<!-- 검색 필드 박스 시작 -->
       <div class="search-group">
			<input name="mode"  	type="hidden"  /> 
			<input name="eduSeq"   	type="hidden"  />
			<input name="taskSeq"   type="hidden"  />
			<input name="pageIndex" type="hidden"  />
			
			<select name="searchCondition" id="searchCondition" title="검색조건2-검색어구분" class="form-control input-inline" style="width:80px;">
			 <option value="0" <c:if test="${eduVO.searchCondition == '0'}">selected="selected"</c:if> >제목</option>
			 <option value="1" <c:if test="${empty eduVO.searchCondition || eduVO.searchCondition == '1'}">selected="selected"</c:if> >내용</option>
			</select>					  
			<input type="text" class="form-control" value="" style="width:200px" name="searchKeyword" id="searchKeyword" >	
				
			<button id="button" class="btn btn-default " onclick="fnSearch(); return false;" > <i class="fa fa-search " aria-hidden="true"></i> 검색</button>
       </div>
		<!-- //검색 필드 박스 끝 -->
        
        <div class="of-y-no">
           <table class="table table-hover table-list center ">          
           <colgroup>
            <col width="8%">           
            <col width="40%">
             <col width="12%">
            <col width="12%">
            <col width="18%">
            </colgroup>
            <thead>
              <tr>
                <th>번호</th>
                <th>제목</th>
                <th>등록일자</th>
                <th>참여율</th>
                <th>기한</th>
              </tr>
            </thead>
            <tbody>
              
				<%-- 데이터를 없을때 화면에 메세지를 출력해준다 --%>
				<c:if test="${fn:length(resultList) == 0}">
					<tr>
						<td class="lt_text3" colspan="7">
							<spring:message code="common.nodata.msg" />
						</td>
					</tr>
				</c:if>
				<c:forEach items="${resultList}" var="result" varStatus="status">
					<tr style="cursor:pointer;" onclick="javascript:fnEduDataInfo('<c:out value="${result.eduSeq}"/>','<c:out value="${result.taskSeq}"/>','<c:out value="${result.pressYn}"/>'); return false;">
						<td><strong><c:out value="${result.rn}"/></strong></td>
						<td class="left" ><c:out value="${result.ttl}"/></td>
						<td><c:out value="${result.regDt}"/></td>						
						<td><c:out value="${result.perUser}"/></td>
						<td><c:out value="${result.projectSchdul}"/></td>						
					</tr>
				</c:forEach>
				
            </tbody>
          </table>
          <div id="paging_div" style="width:100%; text-align:center">
	        <ul class="pagination pagination-sm  " >
				<ui:pagination paginationInfo = "${paginationInfo}"  type="image" jsFunction="fnLinkPage" />
	        </ul>	    
	      </div>
        </div>
      <!-- //list --> 
	      </div>
   	</div>
	<!-- //content 끝 -->
    
	</form>
</body>
</html>