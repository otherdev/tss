<%--
  Class Name : eduSchdulProjectPopup.jsp
  Description : 과제선택 JSP
  Modification Information
 
      수정일         수정자                   수정내용
    -------    --------    ---------------------------
     2017.03.03                 최초 생성
 
    author   : 공통서비스 개발팀 
    since    : 2017.03.03
--%>
<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>과제 등록</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<!-- jstl태그-->
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<link href="<c:url value='/css/style_popup.css' />" rel="stylesheet" type="text/css">
<link href="<c:url value='/css/date-style.css' />" rel="stylesheet" type="text/css">
<link href="<c:url value='/css/jquery-ui.css' />" rel="stylesheet" type="text/css">
<link href="<c:url value='/fonts/font-awesome/css/font-awesome.min.css' />" rel="stylesheet" />

<script type="text/javascript" src="<c:url value="/validator.do"/>"></script>
<script type="text/javascript" src="<c:url value='/js/lib/jquery-1.12.4.js' />" ></script>
<script type="text/javascript" src="<c:url value='/js/lib/jquery-ui.js' />" ></script>
<script type="text/javascript" src="<c:url value='/js/pms/pms.js' />" ></script>

<script type="text/javaScript" language="javascript" defer="defer">
var fnTotQuizCnt= 0;
var fnNowCnt= 1;
var FsSubmitYn = "";
var sEduYn = "";

$(document).ready(function() {
	FsSubmitYn = $("#submitYn").val() ;
	sEduYn = $("#eduYn").val() ;
	
	if( sEduYn ==  "N" ){
		alert("접속자는 교육기간이 아닙니다.");
		parent.self.close();
	}
	
	if( FsSubmitYn !=  "N" ){
		$("#btnSubmit").attr("disabled",true);
	}
	// 기본적으로 비활성화
	$("#btnSubmit").hide();
	
	var sMnuCd = $("#mnuCd").val() ;
	var sMnuNm = $("#mnuNm").val() ;

	/// Quiz 정보 가져오기 
	gfn_selectList({sqlId: 'sysEdu.selectQuizPopupList', mnuCd:sMnuCd}, function(ret){
		objQuiz = ret.data;
		var sQuestionSeq= "";
		var sDivInfo = "";
		$(".conts-gray div").remove();
		fnTotQuizCnt = 1;
		// 배열데이터순회
		$.each(objQuiz, function(idx, map){
			var no = idx+1;
			if(sQuestionSeq != map.questionSeq){

				if(sDivInfo!="") {			
					sDivInfo+="</ul></p></div>"
					$(".conts-gray").append(sDivInfo);
					sDivInfo= "";
					fnTotQuizCnt++;
				}
				sDivInfo+="<div id='QuizArea_"+fnTotQuizCnt+"' class='quiz-group ' style='display:none;'>";
				sDivInfo+="<input id='seq' type='hidden' value='" +no+ "'>";
				sDivInfo+="<input id='questionSeq' type='hidden' value='" +map.questionSeq+ "'>";
				sDivInfo+="<input id='mnuCd' type='hidden' value='" +map.mnuCd+ "'>";
				sQuestionSeq = map.questionSeq;
			}
			if( sQuestionSeq == map.questionSeq ){
				if(map.optSeq == "QUIZ"){
					sDivInfo+="<p class='q'>";
					sDivInfo+=fnTotQuizCnt + ". " + map.contents;
					sDivInfo+="</p>";
					sDivInfo+="<p class='example'><ul>";
				} else {

					var sStyle = ""; 
					sDivInfo+="<li>";
					if(FsSubmitYn !=  "N") {
						// 정답 은 레드 칼라 
						if( map.rightYn == 'Y') {
							sStyle = "color:red;";
						}
						// 유저가 체크된 내용 표시 
						if( map.userSubmit == 'Y') {
							sDivInfo+="<input name='answer_" +fnTotQuizCnt + "' type='radio' class='i_radio ' id='answer_" +fnTotQuizCnt + "_"+ no + "' value='"+map.optSeq+"' checked >";
						} else {
							sDivInfo+="<input name='answer_" +fnTotQuizCnt + "' type='radio' class='i_radio ' id='answer_" +fnTotQuizCnt + "_"+ no + "' value='"+map.optSeq+"' >";
						}
					} else {
						sDivInfo+="<input name='answer_" +fnTotQuizCnt + "' type='radio' class='i_radio ' id='answer_" +fnTotQuizCnt + "_"+ no + "' value='"+map.optSeq+"' >";
					}

					
					
					sDivInfo+="<label for='c1' style='" + sStyle + "' > "+map.contents+"</label>";
					sDivInfo+="</li>";
				}
			}
		});		
		if( fnTotQuizCnt == 1 ){
			alert("해당 분류는 퀴즈가 존재하지 않습니다.");
			parent.self.close();
		}
		if(sDivInfo!="") {			
			sDivInfo+="</ul></p></div>"
			$(".conts-gray").append(sDivInfo);
			sDivInfo= "";
			// fnTotQuizCnt++;
		}
	});	
	fn_QuizMove(0);
});

function fn_QuizMove(nVal){
	var nPrevVal = fnNowCnt;
	var nNextVal = nVal + fnNowCnt;
	// 
	if(nNextVal < 1 ) {
		alert("첫번째 퀴즈 입니다.");
		return false;
	} else if(nNextVal > fnTotQuizCnt ) {
		alert("마지막 퀴즈 입니다.");
		return false;
	}
	
	fnNowCnt = nNextVal;
	
	var sMnuCd = $("#mnuNm").val(); 
	var sTitle = " " + sMnuCd + " (" + fnNowCnt +  "/" + fnTotQuizCnt + ")";
	$("#quizTitle").text(sTitle);
	if(nVal == 0 ){
		$("#sb-slider div:first-child").show();
	} else {
		$("#sb-slider div[id='QuizArea_" + nPrevVal +"']").hide(); // 이전 화면 
		$("#sb-slider div[id='QuizArea_" + fnNowCnt +"']").show();  // 보여줄 화면 
		if(fnNowCnt == fnTotQuizCnt){
			$("#btnSubmit").show();
		} else {
			$("#btnSubmit").hide();
		}
	}
}
function fnNext(){
	fn_QuizMove(1);
}
function fnPrev(){
	fn_QuizMove(-1);
}
function fnSubmit(){
	if( fn_BrforeSave() == false ) return false;
	var arrAnswerData = [];
	
	for( var i = 0 ; i < fnTotQuizCnt ; i ++ ) {
		var objAnswer = {};
		var no = i +1;
		$("div[id='QuizArea_" + no +"'] input").each(function(){
			var sId = $(this).attr("id");
			var sVal = $(this).val();

			if( sId == "questionSeq"  || sId == "mnuCd" ) {
				objAnswer[sId] = sVal;
			} 
		});

		var sRightYnSeq =  $("#sb-slider :input:radio[name='answer_" + no + "']:checked").val();
		objAnswer["optSeq"] = sRightYnSeq;		
		arrAnswerData.push( objAnswer);
		
	}
	//저장처리
	gfn_saveList(
			"/tts/pms/lec/eduQuizSubmit.do"
			,{ objData: arrAnswerData ,userId : $("#userId").val() }
			, function(data){
				if (data.result) {	
					alert("저장되었습니다.");
					FsSubmitYn = 'Y';
					parent.self.close();
					return;
				}
				else{
					alert("저장에 실패하였습니다.","E");
					return;
				}
	});
}

function fn_BrforeSave(){
	var nCnt = 0;
	/// 질문 체크 
	for( var i = 0 ; i < fnTotQuizCnt ; i ++ ) {
		var no = i +1;
		var sRightYnSeq =  $("#sb-slider :input:radio[name='answer_" + no + "']:checked").val();
		if(sRightYnSeq==null || sRightYnSeq==""){
			nCnt++;
			alert(no + "번 문항의 답을 입력하세요." );
			var nVal = no - fnTotQuizCnt;
			fn_QuizMove(nVal);
			return false;
		}
	}	

	if( nCnt > 0 ){
		return false;
	}
	return true;
}
function fn_close(){
	if(FsSubmitYn == 'Y'){
		return true ;
	} else {
		event.returnValue = "그래도 닫으시겠습니까?";
	}
}
</script>
</head>
<body onbeforeunload="fn_close();">
	<noscript class="noScriptTitle">자바스크립트를 지원하지 않는 브라우저에서는 일부 기능을 사용하실 수 없습니다.</noscript>
	<form name="listform" method="post" >

	<input id="eduYn" name="eduYn" type="hidden" value="<c:out value='${eduYn}'/>" >
	<input id="submitYn" name="submitYn" type="hidden" value="<c:out value='${submitYn}'/>" >
	<input id="userId" name="userId" type="hidden" value="<c:out value='${regId}'/>" >
	<input id="mnuCd" name="mnuCd" type="hidden" value="<c:out value='${mnuCd}'/>" >
	<input id="mnuNm" name="mnuNm" type="hidden" value="<c:out value='${mnuNm}'/>" >
    <!-- UI Object -->
	<div id="wrap"> 
		<h1><i class="fa fa-chevron-right   "></i> 퀴즈현황</h1>
		<div class="conts"  >
	        
    	<h2 id="h2" > <i  class="fa fa-arrow-right   "></i><span id="quizTitle"> 퀴즈현황 </span>
			<span class="f_r">
            	<button id="button" class="btn btn-default" onclick="javascript:fnPrev(); return false;" ><i class="fa fa-caret-left fa-lg" aria-hidden="true"></i></button>
            	<button id="button" class="btn btn-default" onclick="javascript:fnNext(); return false;" ><i class="fa fa-caret-right fa-lg" aria-hidden="true"></i></button>
            </span>
    	</h2>
    	<!-- info -->
    <div id="sb-slider" class="conts-gray sb-slider" style="height:238px; padding:0px 15px; ">
      
      <!-- //qui -->
      <div class="quiz-group">
      
      </div>
    </div>
    <!-- //info -->     
			<div class="btn_group center m-t-15  "  >
				<button id="btnSubmit" class="btn btn-default"  onclick="javascript:fnSubmit(); return false;" > <i class="fa fa-check" aria-hidden="true"></i> 제출하기</button>
				<button id="button" class="btn btn-default " onclick="JavaScript:parent.self.close();" > <i class="fa fa-close" aria-hidden="true"></i> 닫기</button>
			</div>
    	</div>      
	<!--conts--> 
	</div>
	<!-- //UI Object -->
	</form>
</body>
</html>