<%--
  Class Name : eduDataInfo.jsp
  Description : 교육자료 상세정보 JSP
  Modification Information
 
      수정일         수정자                   수정내용
    -------    --------    ---------------------------
     2017.03.03                 최초 생성
 
    author   : 공통서비스 개발팀
    since    : 2017.03.03
--%>
<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>교육자료 상세</title>
<%@ include file="/WEB-INF/jsp/pms/include-pms.jspf"%>
<script type="text/javaScript" language="javascript" defer="defer">
<!--
$(document).ready(function() {
	var sEduMngYn = "${sessionScope.isAdmYn}";
	var sUserId = "${sessionScope.id}"; // "<c:out value='${userId}'/>";
	var sRegId = "<c:out value='${eduDataModVO.regId}'/>";
	var sLvl = "<c:out value='${eduDataModVO.lvl}'/>";
	var sChildCnt = "<c:out value='${eduDataModVO.childCnt}'/>";

	$("#btnUpdate").hide();
	$("#btnDelete").hide();
	$("#btnReply").hide();
	
	if( sChildCnt == '0' )	{
		if( sUserId == sRegId || sEduMngYn == 'Y' )  {
			$("#btnUpdate").show();
			$("#btnDelete").show();
		} 
	}
	if(sUserId == sRegId ) {
		$("#btnDelete").show();
	}

	if( sChildCnt == '0' && sLvl == '1' && sEduMngYn == 'Y' ){
		$("#btnReply").show();
	}

	var objTextarea = $("textarea");
	
	$.each(objTextarea , function(i,e){
		fn_TextareaResize(this);
	});
});
function fnRefresh(){

    document.eduDataModVO.action = "<c:url value='/pms/data/eduQnaInfo.do'/>";
    document.eduDataModVO.submit();	
}
function fnListPage(){
    document.eduDataModVO.seq.value = -1;
    document.eduDataModVO.mode.value = "VIEW";
    document.eduDataModVO.action = "<c:url value='/pms/data/eduQnaList.do'/>";
    document.eduDataModVO.submit();	
}
function fnEduQnaDelete(nSeq){
    document.eduDataModVO.seq.value = nSeq;
    document.eduDataModVO.mode.value = "DELETE";
    document.eduDataModVO.action = "<c:url value='/pms/data/eduQnaDelete.do'/>";
    document.eduDataModVO.submit();	
	
}
function fnEduQnaUpdate(nSeq){
    document.eduDataModVO.seq.value = nSeq;
    document.eduDataModVO.mode.value = "UPDATE";
    document.eduDataModVO.action = "<c:url value='/pms/data/eduQnaUp.do'/>";
    document.eduDataModVO.submit();		
}

function fnEduQnaReply(nSeq){
    document.eduDataModVO.seq.value = nSeq;
    document.eduDataModVO.mode.value = "REPLY";
    document.eduDataModVO.action = "<c:url value='/pms/data/eduQnaUp.do'/>";
    document.eduDataModVO.submit();		
}
function fn_TextareaResize(obj){
	obj.style.height = "1px";
	console.log(obj.id + " = " + (12+obj.scrollHeight));
	obj.style.height = (12+obj.scrollHeight)+"px";
}
//-->
</script>

</head>
<body>
	<noscript class="noScriptTitle">자바스크립트를 지원하지 않는 브라우저에서는 일부 기능을 사용하실 수 없습니다.</noscript>

<!-- container -->
<div id="container"  class="content" > 
  
  <!-- content -->
  <div id="content" > 
    
    <!--cont-box-->
    
    <div class="contents-box"  > 
	<!-- 현재위치 네비게이션 시작 -->
	<div class="conts" >
		<h1><i class="fa fa-chevron-circle-right  "></i> 질의/응답</h1>
				
	    <!-- write -->
	    <form:form commandName="eduDataModVO" action="${pageContext.request.contextPath}/pms/data/eduDataInfo.do" name="eduDataModVO" method="post" >
			<!-- 상세정보 작업 삭제시 prameter 전달용 input -->
			<div class="search-group">
				<!-- 	        검색조건 유지 -->
				<input name="mode"            type="hidden" value="<c:out value='${eduDataModVO.mode}'/>"/> 
				<input name="seq"             type="hidden" value="<c:out value='${eduDataModVO.seq}'/>" />
				<input name="pageIndex"       type="hidden" value="<c:out value='${eduDataModVO.pageIndex}'/>" />
				<input name="searchCondition" type="hidden" value="<c:out value='${eduDataModVO.searchCondition}'/>"/>
		        <input name="searchKeyword"   type="hidden" value="<c:out value='${eduDataModVO.searchKeyword}'/>"/>
		        <input name="pageIndex"       type="hidden" value="<c:out value='${eduDataModVO.pageIndex}'/>"/>
	        </div>
			<!-- 상세정보 -->
	    		<div class="of-y-no">
		            <table class="table table-hover table-view  ">
		              <thead>
		                <tr>
		                  <th ><c:out value="${eduDataModVO.ttl}"/></th>
		                </tr>
		              </thead>
		              <tbody>
		                <tr>
		                  <td  class="view-top" > <textarea readonly style="overflow:hidden; border:0; min-height:50px; width:100%;" ><c:out value='${eduDataModVO.contents}'/></textarea> </td>
		                </tr>		                
		                <!-- 첨부파일 -->
						<tr>
							<td >
								<c:import charEncoding="utf-8" url="/cmm/fms/selectFileInfsFileSeq.do" >
								<c:param name="param_fileSeq" value="${eduDataModVO.fileSeq}"/>
								<c:param name="returnUrl" value="" />
								</c:import>
							</td>							
						</tr>
		              </tbody>
		            </table>
          		</div>
          <div class="btn_group right "  >
            <button id="btnDelete" class="btn btn-default" onClick="javascript:fnEduQnaDelete('<c:out value="${eduDataModVO.seq}"/>'); return false;"> <i class="fa fa-close" aria-hidden="true"></i> 삭제</button>
            <button id="btnUpdate" class="btn btn-default" onClick="javascript:fnEduQnaUpdate('<c:out value="${eduDataModVO.seq}"/>'); return false;"> <i class="fa fa-edit" aria-hidden="true"></i> 수정</button>
            <button id="btnReply" class="btn btn-default" onClick="javascript:fnEduQnaReply('<c:out value="${eduDataModVO.seq}"/>'); return false;"> <i class="fa fa-edit" aria-hidden="true"></i> 답변</button>
            <button id="button" class="btn btn-default" onClick="javascript:fnListPage(); return false;"> <i class="fa fa-bars" aria-hidden="true"></i> 목록</button>
          </div>
    	</form:form>
	      
   	</div>
	<!-- //content 끝 -->
	        
    </div>
    <!--//cont-box--> 
    
  </div>
  <!-- //content --> 
</div>
<!-- //container --> 
</body>
</html>