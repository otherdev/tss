<%--
  Class Name : eduDataUp.jsp
  Description : 교육자료 등록 및 삭제 JSP
  Modification Information
 
      수정일         수정자                   수정내용
    -------    --------    ---------------------------
     2017.03.03                 최초 생성
 
    author   : 공통서비스 개발팀 
    since    : 2017.03.03
--%>
<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>교육자료 상세</title>
<%@ include file="/WEB-INF/jsp/pms/include-pms.jspf"%>
<link href="<c:url value='/css/fileStyle.css' />" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<c:url value='/js/EgovMultiFile.js'/>"></script> 
<script type="text/javaScript" language="javascript" defer="defer">
<!--
$(document).ready(function() {

	var sMode = "<c:out value='${mode}'/>";
	var sEduMngYn = "${sessionScope.isAdmYn}"; // "<c:out value='${eduMngYn}'/>";
	var sUserId = "${sessionScope.id}"; // "<c:out value='${userId}'/>";
	var sRegId = "<c:out value='${eduDataModVO.regId}'/>";
	
	$("#btnUpdate").hide();
	$("#btnDelete").hide();
	
	if( sEduMngYn == 'Y' || (sUserId == sRegId) ) {
		$("#btnUpdate").show();
		if(sMode == 'UPDATE' ) {
			$("#btnDelete").show();
		}
	} 
	
	if(sMode == 'ADD' ) {
		$("#btnUpdate").show();
	} 
	
	gfn_init();
	// 첨부 가능여부 처리 
	fn_egov_check_file("Y");
	//------------------------------------------
	//------------------------- 첨부파일 수정 Start
	//-------------------------------------------
	var maxFileNum = document.getElementById('posblAtchFileNumber').value;
	if(maxFileNum==null || maxFileNum==""){ maxFileNum = 3;}
	var multi_selector = new MultiSelector( document.getElementById( 'egovComFileList' ), maxFileNum, 'file_label');
	multi_selector.addElement( document.getElementById( 'egovfile_1' ) );	
	fn_egov_multi_selector_update_setting(multi_selector);
	//------------------------- 첨부파일 수정 End
});
function fnRefresh(){

    document.bizModVO.action = "<c:url value='/pms/data/eduFaqInfo.do'/>";
    document.bizModVO.submit();	
}

function fnListPage(){
    document.eduDataModVO.mode.value = "VIEW";
    document.eduDataModVO.action = "<c:url value='/pms/data/eduQnaList.do'/>";
    document.eduDataModVO.submit();	
}
function fnEduDelete(nSeq){

	if(!confirm("삭제하시겠습니까?")) return;
	
    document.eduDataModVO.seq.value = nSeq;
    document.eduDataModVO.mode.value = "DELETE";
    document.eduDataModVO.action = "<c:url value='/pms/data/eduQnaDelete.do'/>";
    document.eduDataModVO.submit();	
	
}
function fnEduUpdate(nSeq){

    if(!gfn_formValid())	return;

	if(!confirm("저장하시겠습니까?")) return;
	
    document.eduDataModVO.seq.value = nSeq;
    document.eduDataModVO.mode.value = $("input[name='mode']").val(); //  "UPDATE";
    document.eduDataModVO.action = "<c:url value='/pms/data/eduQnaUpdate.do'/>";
    document.eduDataModVO.submit();		
}

function fn_egov_check_file(flag) {
    if (flag=="Y") {
        document.getElementById('file_upload_posbl').style.display = "block";
        document.getElementById('file_upload_imposbl').style.display = "none";          
    } else {
        document.getElementById('file_upload_posbl').style.display = "none";
        document.getElementById('file_upload_imposbl').style.display = "block";
    }
}
//-->
'<c:if test="${!empty resultMsg}">alert("<spring:message code="${resultMsg}" />");</c:if>'
</script>

</head>
<body>
	<noscript class="noScriptTitle">자바스크립트를 지원하지 않는 브라우저에서는 일부 기능을 사용하실 수 없습니다.</noscript>

	<!-- 현재위치 네비게이션 시작 -->
	<div class="conts" >
		<h1><i class="fa fa-chevron-circle-right  "></i> 질의/응답</h1>
				
	    <!-- write -->
	    <form:form commandName="eduDataModVO" action="${pageContext.request.contextPath}/pms/data/eduDataDataUp.do" name="eduDataModVO" method="post" enctype="multipart/form-data"  >
			<!-- 상세정보 작업 삭제시 prameter 전달용 input -->
			<div class="search-group">
				<!-- 	        검색조건 유지 -->
				<input name="mode"            type="hidden" value="<c:out value='${eduDataModVO.mode}'/>" /> 
				<input name="seq"             type="hidden" value="<c:out value='${eduDataModVO.seq}'/>" />
				<input name="questionSeq"     type="hidden" value="<c:out value='${eduDataModVO.questionSeq}'/>" />
				<input name="pageIndex"       type="hidden" value="<c:out value='${eduDataModVO.pageIndex}'/>" />
				<input name="searchCondition" type="hidden" value="<c:out value='${eduDataModVO.searchCondition}'/>"/>
		        <input name="searchKeyword"   type="hidden" value="<c:out value='${eduDataModVO.searchKeyword}'/>"/>
		        <input name="pageIndex"       type="hidden" value="<c:out value='${eduDataModVO.pageIndex}'/>"/>
	        	<input name="posblAtchFileNumber" type="hidden" id="posblAtchFileNumber" value="3" />
	        </div>
	        
		<!-- 상세정보 -->
	    		<div class="table-responsive">
		          <table class="table table-write">
		            <caption>
		            	질의/응답
		            </caption>
		            <colgroup>
		            <col width="20%">
		            <col width="80%">
		            </colgroup>
		            <tbody>
		               <tr>
		                 <th class="req">제목</th>
		                 <td>
		                    <form:input path="ttl" id="ttl" class="form-control  reqVal"  size="20"  maxlength="60" />
		                    <form:errors path="ttl" cssClass="error" />
	               		 </td>
		               </tr>
		              <tr>
		                <th class="req">내용</th>
		                <td>
		                    <form:textarea path="contents" id="contents" class="form-control  reqVal" style="height:200px;" rows="60" maxlength="500" />
		                    <form:errors path="contents" cssClass="error" />
		                </td>
		              </tr>
		              
				      <tr> 
				        <th>첨부파일</th>
				        <td class="nopd">
				        	 <!--첨부목록을 보여주기 위한 -->
								<c:if test="${eduDataModVO.fileSeq ne null && eduDataModVO.fileSeq ne ''}">
								<c:import charEncoding="utf-8" url="/cmm/fms/selectFileInfsForUpdateFileSeq.do" >
								<c:param name="param_fileSeq" value="${eduDataModVO.fileSeq}" />
								<c:param name="returnUrl" value="" />
								</c:import>
								</c:if>
					  			
					  			 <!-- 첨부화일 업로드를 위한 Start -->
								<c:if test="${eduDataModVO.fileSeq eq null || eduDataModVO.fileSeq eq ''}">
								<input type="hidden" name="fileListCnt" value="0" />
								<input name="atchFileAt" type="hidden" value="N">
								</c:if>
								<c:if test="${eduDataModVO.fileSeq ne null && eduDataModVO.fileSeq ne ''}">
								<input name="atchFileAt" type="hidden" value="Y">
								</c:if>
								
							    <div id="file_upload_posbl"  style="display:none;" >
								<!-- attached file Start -->
								<div>
									<div class="egov_file_box">
									<label for="egovfile_1" id="file_label" style="background-color:#4688d2;coloe:#ffffff;padding:.45em .75em;" >파일선택</label> 
									<input type="file" name="file_1" id="egovfile_1"> 
									</div>
									<div id="egovComFileList"></div>
								</div>
								<!-- attached file End -->
								</div>
								<div id="file_upload_imposbl"  style="display:none;" >
						            <table width="100%" cellspacing="0" cellpadding="0" border="0" align="left" class="UseTable">
									    <tr>
									        <td style="padding:0px 0px 0px 0px;margin:0px 0px 0px 0px;">파일선택</td>
									    </tr>
						   	        </table>
								</div>
							</td>
				      </tr>
		            </tbody>
		          </table>
		        </div>

          <div class="btn_group right "  >
            <button id="btnDelete" class="btn btn-default" onClick="javascript:fnEduDelete('<c:out value="${eduDataModVO.seq}"/>'); return false;"> <i class="fa fa-close" aria-hidden="true"></i> 삭제</button>
            <button id="btnUpdate" class="btn btn-default" onClick="javascript:fnEduUpdate('<c:out value="${eduDataModVO.seq}"/>'); return false;"> <i class="fa fa-edit" aria-hidden="true"></i> 저장</button>
            <button id="button" class="btn btn-default" onClick="javascript:fnListPage(); return false;"> <i class="fa fa-bars" aria-hidden="true"></i> 목록</button>
          </div>
    	</form:form>
	      
   	</div>
	<!-- //content 끝 -->
	        
	<!-- footer 시작 
	<div id="footer"><c:import url="/EgovPageLink.do?link=main/inc/EgovIncFooter" /></div>
	-->
</body>
</html>