<%--
  Class Name : eduDataUp.jsp
  Description : 교육자료 등록 및 삭제 JSP
  Modification Information
 
      수정일         수정자                   수정내용
    -------    --------    ---------------------------
     2017.03.03                 최초 생성
 
    author   : 공통서비스 개발팀 
    since    : 2017.03.03
--%>
<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>교육자료 상세</title>
<%@ include file="/WEB-INF/jsp/pms/include-pms.jspf"%>
<link href="<c:url value='/css/fileStyle.css' />" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<c:url value='/js/EgovMultiFile.js'/>"></script> 
<script type="text/javaScript" language="javascript" defer="defer">
<!--
$(document).ready(function() {
	var sEduMngYn = "${sessionScope.isAdmYn}";
	
	$("#btnUpdate").hide();
	$("#btnDelete").hide();
	$("#replArea").hide();
	
	if( sEduMngYn == 'Y' ) {
		$("#btnUpdate").show();
		if($("input[name='mode']").val() == 'UPDATE' ){
			$("#btnDelete").show();
		}
		if(sEduMngYn == 'Y'){
			$("#replArea").show();
		} 	
	} 
	gfn_init();
});
function fnRefresh(){

    document.bizModVO.action = "<c:url value='/pms/data/eduFaqInfo.do'/>";
    document.bizModVO.submit();	
}

function fnListPage(){
    document.eduDataModVO.mode.value = "VIEW";
    document.eduDataModVO.action = "<c:url value='/pms/data/eduFaqList.do'/>";
    document.eduDataModVO.submit();	
}
function fnEduDelete(nSeq){

	if(!confirm("삭제하시겠습니까?")) return;
	
    document.eduDataModVO.seq.value = nSeq;
    document.eduDataModVO.mode.value = "DELETE";
    document.eduDataModVO.action = "<c:url value='/pms/data/eduFaqDelete.do'/>";
    document.eduDataModVO.submit();	
	
}
function fnEduUpdate(nSeq){
    if(!gfn_formValid())	return;

	if(!confirm("저장하시겠습니까?")) return;
	
    document.eduDataModVO.seq.value = nSeq;
    document.eduDataModVO.mode.value = $("input[name='mode']").val(); // "UPDATE";
    document.eduDataModVO.action = "<c:url value='/pms/data/eduFaqUpdate.do'/>";
    document.eduDataModVO.submit();		
}

//-->
'<c:if test="${!empty resultMsg}">alert("<spring:message code="${resultMsg}" />");</c:if>'
</script>

</head>
<body>
	<noscript class="noScriptTitle">자바스크립트를 지원하지 않는 브라우저에서는 일부 기능을 사용하실 수 없습니다.</noscript>

	<!-- 현재위치 네비게이션 시작 -->
	<div class="conts" >
		<h1><i class="fa fa-chevron-circle-right  "></i> FAQ</h1>
				
	    <!-- write -->
	    <form:form commandName="eduDataModVO" action="${pageContext.request.contextPath}/pms/data/eduDataDataUp.do" name="eduDataModVO" method="post" enctype="multipart/form-data"  >
			<!-- 상세정보 작업 삭제시 prameter 전달용 input -->
			<div class="search-group">
				<!-- 	        검색조건 유지 -->
				<input name="mode"            type="hidden" value="<c:out value='${eduDataModVO.mode}'/>" /> 
				<input name="seq"             type="hidden" value="<c:out value='${eduDataModVO.seq}'/>" />
				<input name="pageIndex"       type="hidden" value="<c:out value='${eduDataModVO.pageIndex}'/>" />
				<input name="searchCondition" type="hidden" value="<c:out value='${eduDataModVO.searchCondition}'/>"/>
		        <input name="searchKeyword"   type="hidden" value="<c:out value='${eduDataModVO.searchKeyword}'/>"/>
		        <input name="pageIndex"       type="hidden" value="<c:out value='${eduDataModVO.pageIndex}'/>"/>
	        </div>
	        
		<!-- 상세정보 -->
	    		<div class="table-responsive">
		          <table class="table table-write">
		            <caption>
		            	자료실
		            </caption>
		            <colgroup>
		            <col width="20%">
		            <col width="80%">
		            </colgroup>
		            <tbody>
		              <tr>
		                <th class="req">시스템구분</th>
		                <td>
		                    <form:select class="form-control input-inline" style="width:25%"  path="catCd" id="catCd" onchange="javascript:fnRefresh();"     >
								<c:if test="${mode == 'ADD'}">
			                        <form:option value="" label="--선택하세요--"/>
		                    	</c:if>
		                        <form:options items="${SYCD_result}" itemValue="dtlCd" itemLabel="cdNm"/>
		                    </form:select>
		                    <form:errors path="catCd" cssClass="error"/>
		                  </td>
		              </tr>
		               <tr>
		                 <th class="req">제목</th>
		                 <td>
		                    <form:input path="ttl" id="ttl" class="form-control"  size="20"  maxlength="60" />
		                    <form:errors path="ttl" cssClass="error" />
	               		 </td>
		               </tr>
		              <tr>
		                <th>내용</th>
		                <td>
		                    <form:textarea path="question" id="question" class="form-control" style="height:200px;" rows="60" maxLength="500" />
		                    <form:errors path="question" cssClass="error" />
		                </td>
		              </tr>
		              <tr id="replArea" >
		                <th>답변</th>
		                <td>
		                    <form:textarea path="repl" id="repl" class="form-control" style="height:200px;" rows="60" maxLength="500" />
		                    <form:errors path="repl" cssClass="error" />
		                </td>
		              </tr>
		            </tbody>
		          </table>
		        </div>

          <div class="btn_group right "  >
            <button id="btnDelete" class="btn btn-default" onClick="javascript:fnEduDelete('<c:out value="${eduDataModVO.seq}"/>'); return false;"> <i class="fa fa-close" aria-hidden="true"></i> 삭제</button>
            <button id="btnUpdate" class="btn btn-default" onClick="javascript:fnEduUpdate('<c:out value="${eduDataModVO.seq}"/>'); return false;"> <i class="fa fa-edit" aria-hidden="true"></i> 저장</button>
            <button id="button" class="btn btn-default" onClick="javascript:fnListPage(); return false;"> <i class="fa fa-bars" aria-hidden="true"></i> 목록</button>
          </div>
    	</form:form>
	      
   	</div>
	<!-- //content 끝 -->
	        
	<!-- footer 시작 
	<div id="footer"><c:import url="/EgovPageLink.do?link=main/inc/EgovIncFooter" /></div>
	-->
</body>
</html>