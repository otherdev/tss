<%--
  Class Name : eduDataList.jsp
  Description : 교육자료(조회,삭제) JSP
  Modification Information
 
      수정일         수정자                   수정내용
    -------    --------    ---------------------------
     2017.03.03                 최초 생성
 
    author   : 공통서비스 개발팀 
    since    : 2017.03.03
--%>
<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>그룹 관리</title>
<%@ include file="/WEB-INF/jsp/pms/include-pms.jspf"%>
<script type="text/javascript" src="<c:url value='/js/pms/edu/eduGrpList.js' />" ></script>
<script type="text/javascript" src="<c:url value='/js/showModalDialog.js' />" ></script>
<script type="text/javaScript" language="javascript" defer="defer">
var fs_grpId = ""; 
$(document).ready(function() {
	var sEduMngYn = "${sessionScope.isAdmYn}";
	fn_Search();
});
// 그룹 조회 
function fn_Search(){
	gfn_selectList({sqlId: 'selectEduGrpList_S' }, function(ret){
		lst = ret.data;
		try{
			_grpId = lst[0].grpId;
			fn_DetailSearch(_grpId); //  
			
		}catch(e){}
		// 그리드 초기화
		fnObj.pageStart();
	});
}
//그룹 사용자 목록 조회
function fn_DetailSearch(sGrpId){
	fs_grpId = sGrpId;
	// 상세코드리스트
	gfn_selectList({sqlId: 'selectEduGrpUserList_S', grpId:sGrpId}, function(ret){
		lst2 = ret.data;
		
		// 그리드 초기화
		fnObj2.pageStart();	
	});	
	fn_DetailSearch2();	
}
// 사용자 목록 조회
function fn_DetailSearch2(){
	var sSelectBox = $("#searchCondition").val();
	var sInputBox = $("#searchKeyword").val();
	// 상세코드리스트
	gfn_selectList({sqlId: 'selectEduUserList_S', grpId:fs_grpId , searchCondition:sSelectBox , searchKeyword:sInputBox}, function(ret){
		lst3 = ret.data;
		
		// 그리드 초기화
		fnObj3.pageStart();	
	});	
}
// 그룹 등록팝업
function fnGrpPopup(sGrpId,sMode){

	var retVal;
	var sUrl = "<c:url value='/pms/edu/pmsEduOpenPopup.do?requestUrl=/pms/adm/eduGrpPopup.do&typeFlag=Y&width=440&height=332&grpId=" + sGrpId + "&mode=" + sMode + "' />";
	var openParam = "dialogWidth: 450px; dialogHeight: 340px; resizable: 0; scroll: 0; center: 1; copyhistory:0; location:0; scrollbars:0; menubar:no; directories:0; toolbar:no;";; 		
	
	retVal = window.showModalDialog(sUrl, "p_GrpPopup", openParam);	
	
	fn_Search();
}
function fn_SetGrp(rtnVal){
	fn_Search();
}
//사용자 삭제 처리 후 그룹 사용자  목록에 추가 
function fnGrpUserAdd(){
	var checkedList = myGrid3.getCheckedListWithIndex(0);
	var listAll = fnObj2.grid.getList();
	// 사용자 삭제 처리
	fnObj3.grid.remove();
	// 그룹 사용자  목록에 추가 
	$.each(checkedList, function(i,e){
		var nCnt = 0;
		$.each(listAll,function(idx,element){
			if( e.item.userId == element.userId){
				nCnt++;
			}
		});
		if( nCnt == 0 ) {
			fnObj2.grid.append(e.item);
		} 
	});
	myGrid2.redrawGrid();
	myGrid3.redrawGrid();
}
// 그룹 사용자 삭제 처리 후 사용자 목록에 추가 
function fnGrpUserDel(){
	var checkedList = myGrid2.getCheckedListWithIndex(0);
	// 그룹 사용자 삭제 처리
	fnObj2.grid.remove();
	// 사용자 목록에 추가 
	$.each(checkedList, function(i,e){
		fnObj3.grid.append(e.item);
	});
	myGrid2.redrawGrid();
	myGrid3.redrawGrid();
}
function fnSave(){

	if( myGrid2.validateCheck('C') == false ) {
		return false;
	}
	
	if( myGrid2.validateCheck('U') == false ) {
		return false;
	}
	
	if(!confirm("저장하시겠습니까?")) return;	
	
	// ajax 저장 
	var _lst = fnObj2.grid.getList();
	//저장처리
	gfn_saveList(
			"/tts/pms/adm/eduUserGrpSave.do"
			,{ lst: _lst, grpId: fs_grpId}
			, function(data){
				if (data.result) {			
					alert("성공적으로 저장되었습니다.");
					fn_DetailSearch (fs_grpId);// fn_Search();
					return;
				}
				else{
					alert("저장에 실패하였습니다.","E");
					return;
				}
	});
	// myGrid.selectClear();
}
// 강제 Submit 막기 
var fnNoSubmit = function(e){
	e.preventDefault();
	e.stopPropagation();
	/* do something with Error */
};
$("form[name='grpForm']").bind("submit",fnNoSubmit);
</script>

</head>
<body>
	<noscript class="noScriptTitle">자바스크립트를 지원하지 않는 브라우저에서는 일부 기능을 사용하실 수 없습니다.</noscript>
	<form name="grpForm" onkeydown="if(event.keyCode==13) return false;" >

      <div class="contents-box"  > 
	<!-- 현재위치 네비게이션 시작 -->
	<div class="conts" >
		<h1><i class="fa fa-chevron-circle-right  "></i> 그룹관리</h1>
          
          <!-- list -->
          <div style="width:40%; height:100%;  margin-right:3%;  display:inline-block; ">
            <h2>그룹 <span class="f_r" >
              <button id="button" class="btn btn-default " onclick="javascript:fnGrpPopup('','ADD'); return false;"> <i class="fa fa-plus "></i> 등록</button>
              </span></h2>

            <div id="grdGrp" class="conts-gray of-y-auto" style="margin-top:15px; border-top:2px solid #005ba6; height:500px; width:100%;">

            </div>
          </div>
          
          <!-- //list --> 
          <!-- list -->
          
          <div  style="width:57%;  display:inline-block; float:right">
            <h2 >그룹 사용자 <span class="f_r">
              <button id="button"  class="btn btn-default" onclick="javascript:fnSave(); return false;"> <i class="fa fa-check" aria-hidden="true"></i> 저장</button>
              </span></h2>
            <div class="conts-gray" style="margin-top:15px;" >
              <div id="grdGrpUser"  class=" of-y-scroll " style="border-top:2px solid #005ba6; height:200px; width:100%">
              </div>
              <div class="btn_group_gray center "  >
                <button id="btnAdd" class="btn btn-default " onclick="javascript:fnGrpUserAdd(); return false;" > <i class="fa fa-caret-up fa-lg" aria-hidden="true"></i> </button>
                <button id="btnDel" class="btn btn-default" onclick="javascript:fnGrpUserDel(); return false;"> <i class="fa fa-caret-down fa-lg" aria-hidden="true"></i> </button>
              </div>
              <div class="search-group m-t-5 m-r-10  " >
                <select name="searchCondition" id="searchCondition" class="form-control input-inline" >
                  <option value="0">사업소</option>
                  <option value="1">이름</option>
                </select>
                <input name="searchKeyword" id="searchKeyword" type="text" class="form-control" value="" style="width:150px">
                <button id="button" class="btn btn-default " onclick="javascript:fn_DetailSearch2(); return false;"> <i class="fa fa-search " aria-hidden="true"></i> 검색</button>
              </div>
              <div id="grdUser"   class="of-y-scroll " style="border-top:2px solid #005ba6; height:200px;">
                
              </div>
              
            </div>
            
           
          </div>
          
          <!-- //list --> 
   	</div>
	<!-- //content 끝 -->
	</div>
    
	</form>
	        
	<!-- footer 시작 
	<div id="footer"><c:import url="/EgovPageLink.do?link=main/inc/EgovIncFooter" /></div>
	-->
</body>
</html>