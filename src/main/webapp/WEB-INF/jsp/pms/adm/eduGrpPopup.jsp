<%--
  Class Name : eduDataList.jsp
  Description : 교육자료(조회,삭제) JSP
  Modification Information
 
      수정일         수정자                   수정내용
    -------    --------    ---------------------------
     2017.03.03                 최초 생성
 
    author   : 공통서비스 개발팀 
    since    : 2017.03.03
--%>
<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>그룹 상세</title>
<%@ include file="/WEB-INF/jsp/pms/include-pms_popup.jspf"%>
<script type="text/javascript" src="<c:url value='/js/showModalDialogCallee.js'/>" ></script>
<script type="text/javaScript" language="javascript" defer="defer">
<!--
$(document).ready(function() {
	var sMode = "<c:out value='${mode}'/>";
	var sGrpId = "<c:out value='${grpId}'/>";
	// alert("sMode=> " + sMode);
	if(sMode!= "ADD") {		
		fnSearch(sGrpId);
	} else {
		$("#btnDelete").hide();
		$("#grpId").attr("disabled",false);
	}
	gfn_init();
});
function fnSearch(sGrpId){
	gfn_selectList({sqlId: 'selectEduGrpList_S',grpId:sGrpId }, function(ret){
		// debugger;
		lst = ret.data;
		try{
			$("#grpId").val( lst[0].grpId);
			$("#grpNm").val( lst[0].grpNm);
			$("#grpDesc").val( lst[0].grpDesc);
			$("#ordSort").val( lst[0].ord);
		} catch(e){}
	});
}
function fnListPage(){
    document.listForm.action = "<c:url value='/pms/edu/eduSchdulList.do'/>";
    document.listForm.submit();	
}

function fnSave(){

    if(!gfn_formValid())	return;

	if(!confirm("저장하시겠습니까?")) return;	
	
	var sGrpId = $("#grpId").val();
	var sGrpNm = $("#grpNm").val();
	var sGrpDesc = $("#grpDesc").val();
	var sOrd = $("#ordSort").val();
	var sMode = $("#mode").val();
	
	var arrData = [];
	
	var objData = new Object();
	objData.grpId = sGrpId;
	objData.grpNm = sGrpNm;
	objData.grpDesc = sGrpDesc;
	objData.ord = sOrd;
	objData.mode = sMode;

	// arrData.push(objData);
	
	//저장처리
	gfn_saveList(
			"/tts/pms/adm/eduGrpSave.do"
			,{ mainData:objData }
			, function(data){
				if (data.result) {	

					var objData = {"rtnVal":"Y"};
					// window.returnValue = "Y"; // parent.fn_egov_returnValue("Y");
					gfn_alert("저장되었습니다.");
					// parent.self.close("Y");
					fn_setArguments(objData);
					return;
				}
				else{
					gfn_alert("저장에 실패하였습니다.","E");
					return;
				}
	});
}
function fn_close(){
	parent.fn_egov_returnValue("Y");
}
function fnDelete(){
	if(!confirm("삭제하시겠습니까?")) return;	

	var objData = {
			grpId   : $("#grpId").val()
	      , grpNm   : $("#grpNm").val()
	      , grpDesc : $("#grpDesc").val()
	      , ord     : $("#ordSort").val()
	      , mode    : "DELETE"
	};
	
	//저장처리
	gfn_saveList(
			"/tts/pms/adm/eduGrpSave.do"
			,{ mainData:objData }
			, function(data){
				if (data.result) {	
					gfn_alert("삭제되었습니다.");
					var objData = {"rtnVal":"Y"};
					fn_setArguments(objData);
					return;
				}
				else{
					gfn_alert("저장에 실패하였습니다.","E");
					return;
				}
	});
}

function fn_setArguments(retVal){

	if (opener != null && !opener.closed) {
		try {
			// return opener.dialogArguments;
			
			opener.fn_SetGrp(retVal);
			opener.returnValue = retVal;
			debugger;
			opener.close();
		} catch (err) {
			alert('팝업 처리 시 오류가 발생하였습니다. \n오류내용 : ' + err);
		}
	} else if (parent.opener != null && !parent.opener.closed) {
		try {
			parent.opener.fn_SetGrp(retVal);
			parent.opener.returnValue = retVal;
			parent.self.close();
		} catch (err) {
			alert('팝업 처리 시 오류가 발생하였습니다. \n오류내용 : ' + err);
		}
	} else {
		parent.fn_egov_returnValue(retVal);	
	}
}
//-->
'<c:if test="${!empty resultMsg}">alert("<spring:message code="${resultMsg}" />");</c:if>'
</script>

</head>
<body>
	<noscript class="noScriptTitle">자바스크립트를 지원하지 않는 브라우저에서는 일부 기능을 사용하실 수 없습니다.</noscript>
	<form name="listForm" method="post">
    <!-- UI Object -->
	<div id="wrap"> 
		<h1><i class="fa fa-chevron-right  "></i> 그룹정보</h1>
		<div class="conts"  >
    	  <!-- write -->          
          <div class="table-responsive">
            <input id="mode" type="hidden" value="${mode}">
            <table class="table table-write">
              <caption>
              		입력양식
              </caption>
              <colgroup>
              <col width="30%">
              <col width="70%">
              </colgroup>
              <tbody>
                <tr>
                  <th class="req">그룹ID</th>
                  <td><input id="grpId" disabled="true" type="text" class="form-control reqVal" maxLength="8" value="" style="width:98.5%;"  ></td>
                </tr>
                <tr>
                  <th class="req">그룹명</th>
                  <td><input id="grpNm" type="text" class="form-control reqVal" value="" maxLength="16"style="width:98.5%"  ></td>
                </tr>
                <tr>
                  <th class="req">설명</th>
                  <td><input id="grpDesc" type="text" class="form-control reqVal" value="" maxLength="64"style="width:98.5%"  ></td>
                </tr>
                <tr>
                  <th class="req">정렬순서</th>
                  <td><input id="ordSort" type="text" class="form-control reqVal" value="" maxLength="3"style="width:98.5%"  ></td>
                </tr>
              </tbody>
            </table>
          </div>
    	  <div class="btn_group center m-t-15  "  >
	        	<button id="btnDelete" class="btn btn-default " onclick="JavaScript:fnDelete();return false;"> <i class="fa fa-close" aria-hidden="true"></i> 삭제</button>       
	        	<button id="btnSave" class="btn btn-default" onclick="JavaScript:fnSave();return false;"> <i class="fa fa-check" aria-hidden="true"></i> 저장</button>
	         	<button id="button" class="btn btn-default " onclick="JavaScript:parent.self.close();"> <i class="fa fa-close" aria-hidden="true"></i> 닫기</button>
          </div>          
            <!-- //write --> 
  		</div>
        <!--conts-->   
   </div>
<!-- //UI Object -->
	<!-- //content 끝 -->
	</form>
</body>
</html>