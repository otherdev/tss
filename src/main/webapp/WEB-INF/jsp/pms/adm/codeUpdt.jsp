<%--
  Class Name : workUpdt.jsp
  Description : 작업상세조회, 수정 JSP
  Modification Information
 
      수정일         수정자                   수정내용
    -------    --------    ---------------------------
     2017.03.03                 최초 생성
 
    author   : 공통서비스 개발팀
    since    : 2017.03.03
--%>
<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>코드 상세 및 수정</title>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<link href="<c:url value='/css/style_popup.css' />" rel="stylesheet" type="text/css">
<link href="<c:url value='/fonts/font-awesome/css/font-awesome.min.css' />" rel="stylesheet" />

<script type="text/javaScript" language="javascript" defer="defer">
<!--
$(document).ready(function() {
	if('${mode}' == 'MOD'){
		$("select").attr("disabled",true);
	}
	
	$( "#workStartDt,#workEndDt" ).datepicker({
      showOn: "button",
      buttonImage: "/tts/images/calendar.gif",
      buttonImageOnly: true,
      buttonText: "Select date",
      dateFormat: "yy-mm-dd"
    });	
});

function fnListPage(){
	location.href = '/tts/pms/adm/codeMngList.do';
    //document.bizModVO.action = "<c:url value='/pms/adm/codeMngList.do'/>";
    //document.bizModVO.submit();
}
function fnDeleteWork(checkedIds) {
    if(confirm("<spring:message code="common.delete.msg" />")){
        document.bizModVO.checkedIdForDel.value=checkedIds;
        document.bizModVO.action = "<c:url value='/pms/biz/EgovUserDelete.do'/>";
        document.bizModVO.submit(); 
    }
}
function fnRefresh(){
    document.bizModVO.action = "<c:url value='/pms/biz/workUpdt.do'/>";
    document.bizModVO.submit();
}
function fnUpdate(){
    //if(validateUserManageVO(document.bizModVO)){
    //}
    document.bizModVO.submit();
}
//-->
</script>

</head>
<body>
<noscript>자바스크립트를 지원하지 않는 브라우저에서는 일부 기능을 사용하실 수 없습니다.</noscript>    

<!-- UI Object -->
<div id="wrap"> 

<h1><i class="fa fa-chevron-right  "></i> 코드정보</h1>

   <div class="conts"  >
          
    <!-- write -->
          
          <div class="table-responsive">
            <table class="table table-write">
              <caption>
              입력양식
              </caption>
              <colgroup>
              <col width="30%">
              <col width="70%">
              </colgroup>
              <tbody>
                <tr>
                  <th>코드명</th>
                  <td><input type="text" class="form-control" value="" style="width:98.5%"  ></td>
                </tr>
                <tr>
                  <th>설명</th>
                  <td><input type="text" class="form-control" value="" style="width:98.5%"  ></td>
                </tr>
                <tr>
                  <th>정렬순서</th>
                  <td><input type="text" class="form-control" value="" style="width:98.5%"  ></td>
                </tr>
                <tr>
                  <th>사용여부</th>
                  <td>  <input name="" type="radio" class="i_radio" id="c1" value="" checked>
                  <label for="c1"> 사용</label>
                  <input name="" type="radio" value="" id="c2" >
                  <label for="c2"> 미사용</label></td>
                </tr>
              </tbody>
            </table>
          </div>
    <div class="btn_group center m-t-15  "  >
      <button id="button" class="btn btn-default " > <i class="fa fa-close" aria-hidden="true"></i> 삭제</button>
       
      <button id="button" class="btn btn-default" > <i class="fa fa-check" aria-hidden="true"></i> 저장</button>
       <button id="button" class="btn btn-default "  onclick="JavaScript:self.close();"> <i class="fa fa-close" aria-hidden="true"></i> 닫기</button>
          </div>
          
          <!-- //write --> 
    
          
  </div>
        
</div>

</body>
</html>

