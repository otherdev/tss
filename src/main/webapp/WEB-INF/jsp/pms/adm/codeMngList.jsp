<%--
  Class Name : codeMngList.jsp
  Description : 코드관리(조회,삭제) JSP
  Modification Information
 
      수정일         수정자                   수정내용
    -------    --------    ---------------------------
     2017.03.03                 최초 생성
 
    author   : 공통서비스 개발팀 
    since    : 2017.03.03
--%>
<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>코드관리</title>
<%@ include file="/WEB-INF/jsp/pms/include-pms.jspf"%>


<script type="text/javascript" src="<c:url value='/js/pms/adm/codeMngList.js' />" ></script>
<script type="text/javaScript" language="javascript" defer="defer">
<!--
$(document).ready(function() {
	/*
	var sEduMngYn = "${sessionScope.isAdmYn}";
	var objButton = $('button');
	if( sEduMngYn != 'Y'){
		$.each(objButton,function(i,e){
			$(this).attr("disabled",true);
		});
	}
	*/
	fnSearch();	

});

//코드마스터 조회
function fnSearch(){
	 // 코드마스터 리스트
	gfn_selectList({sqlId: 'selectMstCdList', }, function(ret){
		debugger;
		lst = ret.data;
		try{
			_mstCd = lst[0].mstCd;
		}catch(e){}
		// 그리드 초기화
		fnObj.pageStart();
	});
	 
	// 상세코드리스트
	gfn_selectList({sqlId: 'selectDtlCdList', mstCd:_mstCd}, function(ret){
		lst2 = ret.data;
		
		// 그리드 초기화
		fnObj2.pageStart();	
	});	
	
 }


//코드상세 조회
function fnSearch2(mstCd){
	// 상세코드리스트
	gfn_selectList({sqlId: 'selectDtlCdList', mstCd:mstCd}, function(ret){
		lst2 = ret.data;
		
		// 그리드 초기화
		fnObj2.pageStart();	
	});	
}

//코드마스터 저장 
function fnSave(){
	myGrid.editCellClear(0,0,0);
	myGrid.selectClear();

	if( myGrid.validateCheck("U") == false ) {
		return false;
	}
	if( myGrid.validateCheck("C") == false ) {
		return false;
	}
	// ajax 저장 - 공정, 팀원
	var _lst = fnObj.grid.getList();
	var ll = fnObj.grid.getSelectedItem();	
	var rl = fnObj.grid.removedList ;	

	if(!confirm("저장하시겠습니까?")) return;
 	
	//저장처리
	gfn_saveList(
			"/tts/pms/adm/saveMstCdList.do"
			,{lst: _lst, sqlId: 'saveMstCd',}
			, function(data){
				if (data.result) {
					alert("성공적으로 저장되었습니다.");
					fnSearch();
					return;
				}
				else{
					alert("저장에 실패하였습니다 : " + data.error);
					return;
				}
	});	
}

//코드t상세 저장 
function fnSave2(mstCd){
	myGrid2.editCellClear(0,0,0);
	myGrid2.selectClear();

	if( myGrid2.validateCheck('U') == false ) {
		return false;
	}
	
	// ajax 저장 
	if( myGrid2.validateCheck('C') == false ) {
		return false;
	}
	
	if(!confirm("저장하시겠습니까?")) return;
	
	var _lst = fnObj2.grid.getList();
	var ll = fnObj2.grid.getSelectedItem();	
	var rl = fnObj2.grid.removedList ;	
	
	//저장처리
	gfn_saveList(
			"/tts/pms/adm/saveDtlCdList.do"
			,{lst: _lst, sqlId: 'saveDtlCd',}
			, function(data){
				if (data.result) {
					alert("성공적으로 저장되었습니다.");
					fnSearch2(_mstCd);
					return;
				}
				else{
					alert("저장에 실패하였습니다.","E");
					return;
				}
	});	
};

'<c:if test="${!empty resultMsg}">alert("<spring:message code="${resultMsg}" />");</c:if>'
//-->
</script>

</head>
<body>
	<noscript class="noScriptTitle">자바스크립트를 지원하지 않는 브라우저에서는 일부 기능을 사용하실 수 없습니다.</noscript>
	<form name="listForm" action="<c:url value='/pms/adm/codeMngList.do'/>" method="post">

    <div class="contents-box"  > 

	<!-- 현재위치 네비게이션 시작 -->
	<div class="conts"  >
          <h1><i class="fa fa-chevron-circle-right  "></i> 코드관리</h1>
         
         
          
        <!-- list -->
       
        <div style="width:40%;  margin-right:3% ; display:inline-block; ">
         <h2>상위코드 <span class="f_r"></span></h2>
         
			<div style="padding:10px;" >
			    <input id="btnAdd"  type="button" value="추가" class="AXButton Blue" onclick="fnObj.grid.append();"/>
			    <input id="btnDel"  type="button" value="삭제" class="AXButton Blue" onclick="fnObj.grid.remove();"/>
                <input id="btnSave" type="button" value="저장" class="AXButton Green" style="float:right;" onclick="fnSave();return false;"/>
			</div>					  	
			<div id="grdMstCd" style="border-top:2px solid #005ba6; height:300px;"></div>
        
        </div>
        
            
          
          
    
      <!-- //list --> 
        <!-- list -->
    
      
       
        <div style="width:57%;  display:inline-block; float:right">
         <h2>하위코드 <span class="f_r"> </span></h2>
            <div style="padding:10px;" >
                <input id="btnAdd2"  type="button" value="추가" class="AXButton Blue" onclick="fnObj2.grid.append();"/>
                <input id="btnDel2"  type="button" value="삭제" class="AXButton Blue" onclick="fnObj2.grid.remove();"/>
                <input id="btnSave2" type="button" value="저장" class="AXButton Green" style="float:right;" onclick="fnSave2();"/>
            </div>					  	
		  	<div id="grdDtlCd" style="border-top:2px solid #005ba6; height:300px;"></div>
         
        </div>
        
            
          
          
      <!-- //list --> 
      
      
      
          
    </div>
	<!-- //content 끝 -->
    </div>
    
	</form>
	        
	<!-- footer 시작 
	<div id="footer"><c:import url="/EgovPageLink.do?link=main/inc/EgovIncFooter" /></div>
	-->
</body>
</html>