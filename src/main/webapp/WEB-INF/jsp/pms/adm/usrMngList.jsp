<%--
  Class Name : docMngList.jsp
  Description : 사용자관리(조회) JSP
  Modification Information
 
      수정일         수정자                   수정내용
    -------    --------    ---------------------------
     2017.03.03                 최초 생성
 
    author   : 공통서비스 개발팀 
    since    : 2017.03.03
--%>
<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>사용자관리</title>

<%@ include file="/WEB-INF/jsp/pms/include-pms.jspf"%>
<script type="text/javaScript" language="javascript" defer="defer">
<!--


// 신규
function fnAdd(){
	/* 	
	    document.listForm.mode.value = "ADD";
	    document.listForm.action = "<c:url value='/pms/biz/chgUpdt.do'/>";
	    document.listForm.submit();
	 */    
    window.open("<c:url value='/pms/adm/usrUpdt.do'/>"+"?mode=ADD","usr"
    		,"height=622, width=600, top=50, left=20, scrollbars=no, resizable=no");
}

// 수정 href 호출하면서, vo로도 키 전달하기 위해 폼에 세팅해줌
function fnView(userId) {
    window.open("<c:url value='/pms/adm/usrUpdt.do?userId='/>"+userId+"&mode=MOD","usr"
    		,"height=622, width=600, top=50, left=20, scrollbars=no, resizable=no");
}


function fnLinkPage(pageNo){
    document.listForm.pageIndex.value = pageNo;
    document.listForm.action = "<c:url value='/pms/adm/usrMngList.do'/>";
    document.listForm.submit();
}
function fnSearch(){
    document.listForm.pageIndex.value = 1;
    document.listForm.action = "<c:url value='/pms/adm/usrMngList.do'/>";
    document.listForm.submit();
}


//-->
</script>

</head>
<body>
	<noscript class="noScriptTitle">자바스크립트를 지원하지 않는 브라우저에서는 일부 기능을 사용하실 수 없습니다.</noscript>
	<form name="listForm" action="<c:url value='/pms/adm/usrMngList.do'/>" method="post">

	<!-- 현재위치 네비게이션 시작 -->
	<div class="conts" >
		<h1><i class="fa fa-chevron-circle-right  "></i> 사용자관리</h1>
		<input type="submit" id="invisible" class="invisible"/>
		
		<!-- 검색 필드 박스 시작 -->
		<div class="search-group">
		
			<input name="pageIndex" type="hidden" value="<c:out value='${admVO.pageIndex}'/>"/>
			
			<select name="searchCondition" id="searchCondition" title="검색조건2-검색어구분" class="form-control input-inline" >
			 <option value="0" <c:if test="${empty admVO.searchCondition || admVO.searchCondition == '0'}">selected="selected"</c:if> >사용자</option>
			</select>
					  
			<input type="text" class="form-control" value="" style="width:200px" name="searchKeyword" id="searchKeyword" >
		
			<button id="button" class="btn btn-default " onclick="fnSearch(); return false;" > <i class="fa fa-search " aria-hidden="true"></i> 검색</button>

		</div>
		<!-- //검색 필드 박스 끝 -->
		
		
		<!-- 리스트 -->
	    <div class="of-y-no">
	       <table class="table table-hover table-list center ">
	      
			<colgroup>
				<col width="60px">
				<col width="130px">
				<col width="130px">
				<col width="130px">
				<col width="150px">
				<col width="200px">
				<col width="150px">
				<col width="130px">
				<col width="130px">
				<col width="100px">
			</colgroup>
	        
	        
	        <thead>
	          <tr>
	            <th rowspan="2">번호</th>
	            <th rowspan="2">이름</th>
	            <th rowspan="2">아이디</th>
	            <th rowspan="2">연락처</th>
	            <th colspan="3">소속</th>
	            <th rowspan="2">비고</th>
	            <th rowspan="2">사용여부</th>
	          </tr>
	          <tr>
	            <th>사업소</th>
	            <th>부서</th>
	            <th>직급</th>
	          </tr>
	        </thead>
	        <tbody>
	
				<%-- 데이터를 없을때 화면에 메세지를 출력해준다 --%>
				<c:if test="${fn:length(resultList) == 0}">
					<tr>
						<td class="lt_text3" colspan="10">
							<spring:message code="common.nodata.msg" />
						</td>
					</tr>
				</c:if>
				<c:forEach items="${resultList}" var="result" varStatus="status">
					<tr style="cursor:pointer;" onclick="javascript:fnView('${result.userId}'); return false;">
						<td ><c:out value="${paginationInfo.totalRecordCount - result.rn + 1}"/></td>
						<td ><c:out value="${result.userId}"/></td>
						<td ><c:out value="${result.userNm}"/></td>
						<td ><c:out value="${result.mbtlnum}"/> </td>
						<td ><c:out value="${result.ofcNm}"/></td>
						<td ><c:out value="${result.deptNm}"/></td>
						<td ><c:out value="${result.posNm}"/></td>
						<td ><c:out value="${result.etc}"/></td>
						<td ><c:out value="${result.useYn}"/></td>
					</tr>
				</c:forEach>
	        </tbody>
	      </table>
	      
      </div>
	      
      <div style="width:100%; text-align:center">
        <ul class="pagination pagination-sm  " >
			<ui:pagination paginationInfo = "${paginationInfo}"  type="image" jsFunction="fnLinkPage" />
        </ul>
      </div>
      
	    <div class="btn_group right"  >
	      <button id="button" class="btn btn-default " onclick="fnAdd();"> <i class="fa fa-plus "></i> 등록</button>
	    </div>
	      
   	</div>
	<!-- //content 끝 -->
    
	</form>
	        
</body>
</html>