<%--
  Class Name : docMngList.jsp
  Description : 사용자로그관리(조회) JSP
  Modification Information
 
      수정일         수정자                   수정내용
    -------    --------    ---------------------------
     2017.03.03                 최초 생성
 
    author   : 공통서비스 개발팀 
    since    : 2017.03.03
--%>
<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>사용자로그관리</title>

<%@ include file="/WEB-INF/jsp/pms/include-pms.jspf"%>
<script type="text/javaScript" language="javascript" defer="defer">
<!--
$(document).ready(function() {
	gfn_init();
	
	$( "#startDt,#endDt" ).datepicker({
	      showOn: "button",
	      buttonImage: "/tts/images/calendar.gif",
	      buttonImageOnly: true,
	      buttonText: "Select date",
	      dateFormat: "yy-mm-dd"
	    });
	
});




function fnLinkPage(pageNo){
    document.listForm.pageIndex.value = pageNo;
    document.listForm.action = "<c:url value='/pms/adm/usrLogList.do'/>";
    document.listForm.submit();
}
function fnSearch(){
    document.listForm.pageIndex.value = 1;
    document.listForm.action = "<c:url value='/pms/adm/usrLogList.do'/>";
    document.listForm.submit();
}


//-->
</script>

</head>
<body>
	<noscript class="noScriptTitle">자바스크립트를 지원하지 않는 브라우저에서는 일부 기능을 사용하실 수 없습니다.</noscript>
	<form name="listForm" action="<c:url value='/pms/adm/usrLogList.do'/>" method="post">

	<!-- 현재위치 네비게이션 시작 -->
	<div class="conts" >
		<h1><i class="fa fa-chevron-circle-right  "></i> 사용자로그관리</h1>
		<input type="submit" id="invisible" class="invisible"/>
		
		<!-- 검색 필드 박스 시작 -->
		<div class="search-group">
			<span><b>검색기간</b></span>
            <input name="startDt" id="startDt" class="form-control date" style="width:150px;" size="20"  maxlength="60" value="${admVO.startDt}"/> &nbsp;~&nbsp; 
            <input name="endDt" id="endDt" class="form-control date"  style="width:150px;" size="20"  maxlength="60" value="${admVO.endDt}"/>

			<div style="width:100px;display:inline-block;"></div>
		
			<input name="mode" type="hidden" value="<c:out value='${admVO.mode}'/>"/>
			<input name="pageIndex" type="hidden" value="<c:out value='${admVO.pageIndex}'/>"/>
			
			<select name="searchCondition" id="searchCondition" title="검색조건2-검색어구분" class="form-control input-inline" >
			 <option value="0" <c:if test="${empty admVO.searchCondition || admVO.searchCondition == '0'}">selected="selected"</c:if> >사용자</option>
			</select>
					  
			<input type="text" class="form-control" value="" style="width:200px" name="searchKeyword" id="searchKeyword" >
		
			<button id="button" class="btn btn-default " onclick="fnSearch(); return false;" > <i class="fa fa-search " aria-hidden="true"></i> 검색</button>

		</div>
		<!-- //검색 필드 박스 끝 -->
		
		
		<!-- 리스트 -->
	    <div class="of-y-no">
	       <table class="table table-hover table-list center ">
	      
			<colgroup>
				<col width="60px">
				<col width="130px">
				<col width="200px">
				<col width="130px">
				<col width="130px">
				<col width="200px">
				<col width="150px">
				<col width="130px">
			</colgroup>
	        
	        
	        <thead>
	          <tr>
	            <th rowspan="2">번호</th>
	            <th rowspan="2">접속구분</th>
	            <th rowspan="2">접속시간</th>
	            <th rowspan="2">아이디</th>
	            <th rowspan="2">이름</th>
	            <th colspan="3">소속</th>
	          </tr>
	          <tr>
	            <th>사업소</th>
	            <th>부서</th>
	            <th>직급</th>
	          </tr>
	        </thead>
	        <tbody>
	
				<%-- 데이터를 없을때 화면에 메세지를 출력해준다 --%>
				<c:if test="${fn:length(resultList) == 0}">
					<tr>
						<td class="lt_text3" colspan="8">
							<spring:message code="common.nodata.msg" />
						</td>
					</tr>
				</c:if>
				<c:forEach items="${resultList}" var="result" varStatus="status">
					<tr>
						<td ><c:out value="${paginationInfo.totalRecordCount - result.rn + 1}"/></td>
						<td ><c:out value="${result.loginGb}"/></td>
						<td ><c:out value="${result.creatDt}"/> </td>
						<td ><c:out value="${result.conectId}"/> </td>
						<td ><c:out value="${result.userNm}"/></td>
						<td ><c:out value="${result.ofcNm}"/></td>
						<td ><c:out value="${result.deptNm}"/></td>
						<td ><c:out value="${result.posNm}"/></td>
					</tr>
				</c:forEach>
	        </tbody>
	      </table>
	      
      </div>
	      
      <div style="width:100%; text-align:center">
        <ul class="pagination pagination-sm  " >
			<ui:pagination paginationInfo = "${paginationInfo}"  type="image" jsFunction="fnLinkPage" />
        </ul>
    
      </div>
	      
   	</div>
	<!-- //content 끝 -->
    
	</form>
	        
</body>
</html>