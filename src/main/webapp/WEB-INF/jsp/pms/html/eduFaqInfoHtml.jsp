<%--
  Class Name : eduDataList.jsp
  Description : 교육자료(조회,삭제) JSP
  Modification Information
 
      수정일         수정자                   수정내용
    -------    --------    ---------------------------
     2017.03.03                 최초 생성
 
    author   : 공통서비스 개발팀 
    since    : 2017.03.03
--%>
<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>FAQ 상세정보</title>
<%@ include file="/WEB-INF/jsp/pms/include-pms.jspf"%>
<script type="text/javaScript" language="javascript" defer="defer">
<!--
$(document).ready(function() {
});
function fnListPage(){
    document.listForm.action = "<c:url value='/pms/html/eduFaqListHtml.do'/>";
    document.listForm.submit();	
}
function fnUpPage(){
    document.listForm.action = "<c:url value='/pms/html/eduFaqUpHtml.do'/>";
    document.listForm.submit();	
}
function fnSave(){
	
}
//-->
'<c:if test="${!empty resultMsg}">alert("<spring:message code="${resultMsg}" />");</c:if>'
</script>

</head>
<body>
	<noscript class="noScriptTitle">자바스크립트를 지원하지 않는 브라우저에서는 일부 기능을 사용하실 수 없습니다.</noscript>
	<form name="listForm" action="<c:url value='/pms/edu/eduSchdulList.do'/>" method="post">

      <div class="contents-box"  > 
	<!-- 현재위치 네비게이션 시작 -->
	<div class="conts" >
		<h1><i class="fa fa-chevron-circle-right  "></i> FAQ</h1>
         
         
          
       <!-- view -->
          
          <div class="of-y-no">
            <table class="table table-hover table-view  ">
              <thead>
                <tr>
                  <th >[계정] 권한 없다고 나올 경우</th>
                </tr>
              </thead>
              <tbody>
              <!--  <tr>
                  <td ><span class="f_l"><b>작성자</b> 관리자</span> <span class="f_r"><b>조회수</b> 56478</span> <span class="f_r"><b>등록일자</b> 2017.08.01</span></td>
                </tr> -->
                <tr>
                  <td class="view-q">[질문] 로그인후 시스템 교육에서 관망관리시스템을 클릭시 권한이 없거나 접속이 되지 않을 경우</td>
                </tr>
                <tr>
                  <td  class="view-top" >관리자에서 연락후 본인 계정을 알려주고, 접속할려는 시스템에 대하여 접속권한 여부를 문의</td>
                </tr>
                
              </tbody>
            </table>
          </div>
          <div class="btn_group right "  >
            <button id="button" class="btn btn-default " > <i class="fa fa-close" aria-hidden="true"></i> 삭제</button>
            <button id="button" class="btn btn-default " onclick="fnUpPage(); return false;"> <i class="fa fa-edit" aria-hidden="true"></i> 수정</button>
            <button id="button" class="btn btn-default" onclick="fnListPage(); return false;"> <i class="fa fa-bars" aria-hidden="true"></i> 목록</button>
          </div>
          
          <!-- //view --> 

	      </div>
   	</div>
	<!-- //content 끝 -->
	</form>
</body>
</html>