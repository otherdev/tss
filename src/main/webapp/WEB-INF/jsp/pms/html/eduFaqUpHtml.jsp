<%--
  Class Name : eduDataList.jsp
  Description : 교육자료(조회,삭제) JSP
  Modification Information
 
      수정일         수정자                   수정내용
    -------    --------    ---------------------------
     2017.03.03                 최초 생성
 
    author   : 공통서비스 개발팀 
    since    : 2017.03.03
--%>
<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>FAQ 상세정보</title>
<%@ include file="/WEB-INF/jsp/pms/include-pms.jspf"%>
<script type="text/javaScript" language="javascript" defer="defer">
<!--
$(document).ready(function() {
});
function fnListPage(){
    document.listForm.action = "<c:url value='/pms/html/eduFaqListHtml.do'/>";
    document.listForm.submit();	
}
function fnSave(){
	
}
//-->
'<c:if test="${!empty resultMsg}">alert("<spring:message code="${resultMsg}" />");</c:if>'
</script>

</head>
<body>
	<noscript class="noScriptTitle">자바스크립트를 지원하지 않는 브라우저에서는 일부 기능을 사용하실 수 없습니다.</noscript>
	<form name="listForm" action="<c:url value='/pms/html/eduFaqUpHtml.do'/>" method="post">

      <div class="contents-box"  > 
	<!-- 현재위치 네비게이션 시작 -->
	<div class="conts" >
		<h1><i class="fa fa-chevron-circle-right  "></i> FAQ</h1>
         
         
          
         <!-- write -->
          
          <div class="table-responsive">
            <table class="table table-write">
              <caption>
              입력양식
              </caption>
              <colgroup>
              <col width="20%">
              <col width="80%">
              </colgroup>
              <tbody>
                <tr>
                  <th>분류</th>
                  <td><select  class="form-control input-inline" style="width:25%"  name="region3">
        <option value="관망관리시스템">관망관리시스템</option>
                    </select></td>
                </tr>
                <tr>
                  <th>제목</th>
                  <td><input type="text" class="form-control" value="" ></td>
                </tr>
                <tr>
                  <th>질문</th>
                  <td><textarea class="form-control" id="textarea" name="textarea" rows="60" style="resize: none; width:100%; height:100px"></textarea></td>
                </tr>
                <tr>
                  <th>답변</th>
                  <td><textarea class="form-control" id="" name="" rows="60" style="resize: none; width:100%; height:250px"></textarea></td>
                </tr>
              </tbody>
            </table>
          </div>
          <div class="btn_group right "  >
            <button id="button" class="btn btn-default " onclick="fnListPage(); return false;"> <i class="fa fa-reply" aria-hidden="true"></i> 취소</button>
            <button id="button" class="btn btn-default" > <i class="fa fa-check" aria-hidden="true"></i> 저장</button>
          </div>
          
          <!-- //write --> 

	      </div>
   	</div>
	<!-- //content 끝 -->
	</form>
</body>
</html>