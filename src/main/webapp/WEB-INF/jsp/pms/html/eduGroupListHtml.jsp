<%--
  Class Name : eduDataList.jsp
  Description : 교육자료(조회,삭제) JSP
  Modification Information
 
      수정일         수정자                   수정내용
    -------    --------    ---------------------------
     2017.03.03                 최초 생성
 
    author   : 공통서비스 개발팀 
    since    : 2017.03.03
--%>
<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>그룹 관리</title>
<%@ include file="/WEB-INF/jsp/pms/include-pms.jspf"%>
<script type="text/javaScript" language="javascript" defer="defer">

$(document).ready(function() {
});


// 신규등록모드로 상세페이지 호출
function fnEduAdd() {
    document.listForm.action = "<c:url value='/pms/html/eduProjectUpHtml.do'/>";
    document.listForm.submit();	
}
function fnLinkPage(pageNo){
}
function fnSearch(){
}


function fnEduDataInfo(eduSeq){
    document.listForm.action = "<c:url value='/pms/html/eduProjectUpHtml.do'/>";
    document.listForm.submit();	
}

function fnEduDataCreate(eduSeq){
}

function fnPopup(sType){
	var sUrl = ""; 
		window.open("<c:url value='/pms/html/eduGroupHtmlPopup.do'/>","code","height=280, width=750, top=50, left=20, scrollbars=no, resizable=no");
		// sUrl = "/pms/html/eduSchdulHtmlGrpPopup.do";
	
}
</script>

</head>
<body>
	<noscript class="noScriptTitle">자바스크립트를 지원하지 않는 브라우저에서는 일부 기능을 사용하실 수 없습니다.</noscript>
	<form name="listForm" action="<c:url value='/pms/edu/eduSchdulMngList.do'/>" method="post">

      <div class="contents-box"  > 
	<!-- 현재위치 네비게이션 시작 -->
	<div class="conts" >
		<h1><i class="fa fa-chevron-circle-right  "></i> 그룹관리</h1>
          
          <!-- list -->
          
          <div style="width:40%; height:100%;  margin-right:3%;  display:inline-block; ">
            <h2>그룹 <span class="f_r">
              <button id="button" class="btn btn-default " onclick="javascript:fnPopup('process'); return false;"> <i class="fa fa-plus "></i> 등록</button>
              </span></h2>

            <div class="conts-gray of-y-auto" style="height:540px;width:100%;">
              <table class="table table-hover table-list center "  >
                <colgroup>
                <col width="15%">
                <col width="20%">
                <col width="*">
                </colgroup>
                <thead>
                  <tr>
                    <th>번호</th>
                    <th>그룹명</th>
                    <th>그룹설명</th>
                  </tr>
                </thead>
                <tbody>
                  <tr style="cursor:hand;" onclick="javascript:fnPopup('process'); return false;">
                    <td>13</td>
                    <td>
						 <span class="link"><a 
						 	onclick="javascript:fnPopup('process'); return false;">admin</a></span></td>
                    <td>
						 <span class="link"><a 
						 	onclick="javascript:fnPopup('process'); return false;">관리자</a></span></td>
                  </tr>
                  <tr style="cursor:hand;" onclick="javascript:fnPopup('process'); return false;">
                    <td>12</td>
                    <td>
						 <span class="link"><a 
						 	onclick="javascript:fnPopup('process'); return false;">admin</a></span></td>
                    <td>
						 <span class="link"><a 
						 	onclick="javascript:fnPopup('process'); return false;">관리자</a></span></td>
                  </tr>
                  <tr>
                    <td>11</td>
                    <td>
						 <span class="link"><a 
						 	onclick="javascript:fnPopup('process'); return false;">admin</a></span></td>
                    <td>
						 <span class="link"><a 
						 	onclick="javascript:fnPopup('process'); return false;">관리자</a></span></td>
                  </tr>
                  <tr style="cursor:hand;" onclick="javascript:fnPopup('process'); return false;">
                    <td>10</td>
                    <td>
						 <span class="link"><a 
						 	onclick="javascript:fnPopup('process'); return false;">admin</a></span></td>
                    <td>
						 <span class="link"><a 
						 	onclick="javascript:fnPopup('process'); return false;">관리자</a></span></td>
                  </tr>
                  <tr style="cursor:hand;" onclick="javascript:fnPopup('process'); return false;">
                    <td>9</td>
                    <td>admin</td>
                    <td>관리자</td>
                  </tr>
                  <tr style="cursor:hand;" onclick="javascript:fnPopup('process'); return false;">
                    <td>8</td>
                    <td>admin</td>
                    <td>관리자</td>
                  </tr>
                  <tr style="cursor:hand;" onclick="javascript:fnPopup('process'); return false;">
                    <td>7</td>
                    <td>admin</td>
                    <td>관리자</td>
                  </tr>
                  <tr style="cursor:hand;" onclick="javascript:fnPopup('process'); return false;">
                    <td>6</td>
                    <td>admin</td>
                    <td>관리자</td>
                  </tr>
                  <tr style="cursor:hand;" onclick="javascript:fnPopup('process'); return false;">
                    <td>5</td>
                    <td>admin</td>
                    <td>관리자</td>
                  </tr>
                  <tr style="cursor:hand;" onclick="javascript:fnPopup('process'); return false;">
                    <td>4</td>
                    <td>admin</td>
                    <td>관리자</td>
                  </tr>
                  <tr>
                    <td>3</td>
                    <td>admin</td>
                    <td>관리자</td>
                  </tr>
                  <tr>
                    <td>2</td>
                    <td>admin</td>
                    <td>관리자</td>
                  </tr>
                  <tr >
                    <td>1</td>
                    <td>admin</td>
                    <td>관리자</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
          
          <!-- //list --> 
          <!-- list -->
          
          <div  style="width:57%;  display:inline-block; float:right">
            <h2>그룹 사용자 <span class="f_r">
              <button id="button" class="btn btn-default" > <i class="fa fa-check" aria-hidden="true"></i> 저장</button>
              </span></h2>
            <div class="conts-gray ">
              <div class=" of-y-scroll " style="width:100%">
                <table class="table table-hover table-list center  " >
                  <colgroup>
                  <col width="15%">
                  <col width="20%">
                  <col width="20%">
                  <col width="*">
                  </colgroup>
                  <thead>
                    <tr>
                      <th>선택</th>
                      <th>이름</th>
                      <th>사업소</th>
                      <th>부서</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td class="check"><input name="a" type="checkbox" id="b0" value="" checked ></td>
                      <td>admin</td>
                      <td>관리자</td>
                      <td>사용</td>
                    </tr>
                    <tr>
                      <td class="check"><input name="a" type="checkbox" id="b0" value="" checked ></td>
                      <td>admin</td>
                      <td>관리자</td>
                      <td>미사용</td>
                    </tr>
                    <tr>
                      <td class="check"><input name="a" type="checkbox" id="b0" value="" ></td>
                      <td>admin</td>
                      <td>관리자</td>
                      <td>미사용</td>
                    </tr>
                    <tr>
                      <td class="check"><input name="a" type="checkbox" id="b0" value="" ></td>
                      <td>admin</td>
                      <td>관리자</td>
                      <td>미사용</td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <div class="btn_group_gray center "  >
                <button id="button" class="btn btn-default " > <i class="fa fa-caret-up fa-lg" aria-hidden="true"></i> </button>
                <button id="button" class="btn btn-default" > <i class="fa fa-caret-down fa-lg" aria-hidden="true"></i> </button>
              </div>
              <div class="search-group m-t-5 m-r-10  " >
                <select  class="form-control input-inline" name="region3">
                  <option value="사업소">사업소</option>
                  <option value="이름">이름</option>
                </select>
                <input type="text" class="form-control" value="" style="width:150px">
                <button id="button" class="btn btn-default " > <i class="fa fa-search " aria-hidden="true"></i> 검색</button>
              </div>
              <div class="of-y-scroll ">
                <table class="table table-hover table-list center  " >
                  <colgroup>
                  <col width="15%">
                  <col width="20%">
                  <col width="20%">
                  <col width="*">
                  </colgroup>
                  <thead>
                    <tr>
                      <th>선택</th>
                      <th>이름</th>
                      <th>사업소</th>
                      <th>부서</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td class="check"><input name="a" type="checkbox" id="b0" value="" checked ></td>
                      <td>admin</td>
                      <td>관리자</td>
                      <td>사용</td>
                    </tr>
                    <tr>
                      <td class="check"><input name="a" type="checkbox" id="b0" value="" checked ></td>
                      <td>admin</td>
                      <td>관리자</td>
                      <td>미사용</td>
                    </tr>
                    <tr>
                      <td class="check"><input name="a" type="checkbox" id="b0" value="" ></td>
                      <td>admin</td>
                      <td>관리자</td>
                      <td>미사용</td>
                    </tr>
                    <tr>
                      <td class="check"><input name="a" type="checkbox" id="b0" value="" ></td>
                      <td>admin</td>
                      <td>관리자</td>
                      <td>미사용</td>
                    </tr>
                  </tbody>
                </table>
              </div>
              
            </div>
            
           
          </div>
          
          <!-- //list --> 
	      
   	</div>
	<!-- //content 끝 -->
	</div>
    
	</form>
	        
	<!-- footer 시작 
	<div id="footer"><c:import url="/EgovPageLink.do?link=main/inc/EgovIncFooter" /></div>
	-->
</body>
</html>