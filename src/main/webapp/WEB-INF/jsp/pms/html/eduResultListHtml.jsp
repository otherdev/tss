<%--
  Class Name : eduDataList.jsp
  Description : 교육자료(조회,삭제) JSP
  Modification Information
 
      수정일         수정자                   수정내용
    -------    --------    ---------------------------
     2017.03.03                 최초 생성
 
    author   : 공통서비스 개발팀 
    since    : 2017.03.03
--%>
<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>교육 성과 관리</title>
<%@ include file="/WEB-INF/jsp/pms/include-pms.jspf"%>
<script type="text/javaScript" language="javascript" defer="defer">

$(document).ready(function() {
});

// 신규등록모드로 상세페이지 호출
function fnEduAdd() {
    document.listForm.action = "<c:url value='/pms/html/eduResultUpHtml.do'/>";
    document.listForm.submit();	
}
function fnLinkPage(pageNo){
}
function fnSearch(){
}


function fnEduDataInfo(eduSeq){
}

function fnEduDataCreate(eduSeq){
}

</script>

</head>
<body>
	<noscript class="noScriptTitle">자바스크립트를 지원하지 않는 브라우저에서는 일부 기능을 사용하실 수 없습니다.</noscript>
	<form name="listForm" action="<c:url value='/pms/html/eduResultListHtml.do'/>" method="post">

      <div class="contents-box"  > 
	<!-- 현재위치 네비게이션 시작 -->
	<div class="conts" >
	      <h1><i class="fa fa-chevron-circle-right  "></i> 교육성과관리</h1>
          
          <!-- list -->
          
          <div class="search-group">
            <select  class="form-control input-inline" name="region3">
              <option value="제목">제목</option>
              <option value="시스템명">시스템명</option>
            </select>
            <input type="text" class="form-control" value="" style="width:200px">
            <button id="button" class="btn btn-default " > <i class="fa fa-search " aria-hidden="true"></i> 검색</button>
          </div>
          <div class="of-y-no">
            <table class="table table-hover table-list center ">
              <colgroup>
              <col width="7%">
              <col width="11%">
              <col width="20%">
              <col width="25%">
              <col width="17%">
              <col width="10%">
              <col width="10%">
              </colgroup>
              <thead>
                <tr>
                  <th>번호</th>
                  <th>교육구분</th>
                  <th>시스템명</th>
                  <th>교육명</th>
                  <th>교육기간</th>
                  <th>대상자</th>
                  <th>등록일자</th>
                </tr>
              </thead>
              <tbody>
                <tr onclick="javascript:fnEduAdd(); return false;">
                  <td>10</td>
                  <td>상시구분</td>
                  <td class="left">
						 <span class="link"><a 
						 	onclick="javascript:fnEduAdd(); return false;">
						 	상수 최적 관망관리시스템
						 	</a></span></td>
                  <td class="left">
						 <span class="link"><a 
						 	onclick="javascript:fnEduAdd(); return false;">
						 	사용자 메뉴얼 v3.1</td>
                  <td>2017-09-01~2017-09-01
						 	</a></span></td>
                  <td>관리자</td>
                  <td>2017-09-01</td>
                </tr>
                <tr onclick="javascript:fnEduAdd(); return false;">
                  <td>9</td>
                  <td>분기구분</td>
                  <td class="left">
						 <span class="link"><a 
						 	onclick="javascript:fnEduAdd(); return false;">
						 	관망관리시스템
						 	</a></span></td>
                  <td class="left">
						 <span class="link"><a 
						 	onclick="javascript:fnEduAdd(); return false;">
						 	관망관리시스템 설치 매뉴얼
						 	</a></span></td>
                  <td>2017-09-01</td>
                  <td>관리자</td>
                  <td>2017-09-01</td>
                </tr>
                <tr onclick="javascript:fnEduAdd(); return false;">
                  <td>8</td>
                  <td>년간교육</td>
                  <td class="left">
						 <span class="link"><a 
						 	onclick="javascript:fnEduAdd(); return false;">
						 	상수 최적 관망관리시스템
						 	</a></span></td>
                  <td class="left">
						 <span class="link"><a 
						 	onclick="javascript:fnEduAdd(); return false;">
						 	사용자 메뉴얼 v3.1
						 	</a></span></td>
                  <td>2017-09-01</td>
                  <td>관리자</td>
                  <td>2017-09-01</td>
                </tr>
                <tr onclick="javascript:fnEduAdd(); return false;">
                  <td>7</td>
                  <td>상시구분</td>
                  <td class="left">
						 <span class="link"><a 
						 	onclick="javascript:fnEduAdd(); return false;">
						 	관망관리시스템
						 	</a></span></td>
                  <td class="left">
						 <span class="link"><a 
						 	onclick="javascript:fnEduAdd(); return false;">
						 	관망관리시스템 설치 매뉴얼
						 	</a></span></td>
                  <td>2017-09-01</td>
                  <td>관리자</td>
                  <td>2017-09-01</td>
                </tr>
                <tr>
                  <td>6</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr >
                  <td>5</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td>4</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td>3</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td>2</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr >
                  <td>1</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
              </tbody>
            </table>
            <div style="width:100%; text-align:center">
              <ul class="pagination pagination-sm  " >
                <li> <a href="#" aria-label="Previous"> <span aria-hidden="true"> &lt; </span> </a> </li>
                <li><a href="#">1</a></li>
                <li class="active"><a href="#"  >2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">4</a></li>
                <li><a href="#">5</a></li>
                <li> <a href="#" aria-label="Next"> <span aria-hidden="true">&gt;</span> </a> </li>
              </ul>
            </div>
          </div>
          <div class="btn_group right"  >
            <button id="button" class="btn btn-default " onclick="fnEduAdd(); return false;" > <i class="fa fa-plus "></i> 등록</button>
          </div>
          </div>
          <!-- //list --> 
   	</div>
	<!-- //content 끝 -->
    
	</form>
	        
	<!-- footer 시작 
	<div id="footer"><c:import url="/EgovPageLink.do?link=main/inc/EgovIncFooter" /></div>
	-->
</body>
</html>