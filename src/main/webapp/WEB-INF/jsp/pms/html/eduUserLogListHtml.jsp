<%--
  Class Name : eduDataList.jsp
  Description : 교육자료(조회,삭제) JSP
  Modification Information
 
      수정일         수정자                   수정내용
    -------    --------    ---------------------------
     2017.03.03                 최초 생성
 
    author   : 공통서비스 개발팀 
    since    : 2017.03.03
--%>
<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>사용자로그</title>
<%@ include file="/WEB-INF/jsp/pms/include-pms.jspf"%>
<script type="text/javaScript" language="javascript" defer="defer">

$(document).ready(function() {
});


// 신규등록모드로 상세페이지 호출
function fnEduAdd() {
    document.listForm.action = "<c:url value='/pms/html/eduProjectUpHtml.do'/>";
    document.listForm.submit();	
}
function fnLinkPage(pageNo){
}
function fnSearch(){
}


function fnEduDataInfo(eduSeq){
    document.listForm.action = "<c:url value='/pms/html/eduProjectUpHtml.do'/>";
    document.listForm.submit();	
}

function fnEduDataCreate(eduSeq){
}

</script>

</head>
<body>
	<noscript class="noScriptTitle">자바스크립트를 지원하지 않는 브라우저에서는 일부 기능을 사용하실 수 없습니다.</noscript>
	<form name="listForm" action="<c:url value='/pms/edu/eduSchdulMngList.do'/>" method="post">

      <div class="contents-box"  > 
	<!-- 현재위치 네비게이션 시작 -->
	<div class="conts" >
		<h1><i class="fa fa-chevron-circle-right  "></i> 사용자로그정보</h1>
         
         
          
            <!-- list -->
    
       <div class="search-group">
       <span><b>검색기간</b>
       <input type="text" class="form-control  " style="width:120px; " value="">
                    <a href="#"> <i class="fa fa-calendar fa-lg" aria-hidden="true" ></i></a> ~
                    <input type="text" class="form-control  " style="width:120px" value="">
                    <a href="#"> <i class="fa fa-calendar fa-lg" aria-hidden="true" style="margin: 0 20px 0 5px; "></i></a>
                    </span>
                    
       <select  class="form-control input-inline" name="region3">
       <option value="사용자">사용자</option>
       <option value="시스템명">시스템명</option>
       </select>
                  
       <input type="text" class="form-control" value="" style="width:200px">
       
        <button id="button" class="btn btn-default " > <i class="fa fa-search " aria-hidden="true"></i> 검색</button>
       </div>
        
        <div class="of-y-no">
           <table class="table table-hover table-list center ">
          
           <colgroup>
            <col width="5%">
             <col width="10%">
            <col width="15">
             <col width="10%">
            <col width="10%">
            <col width="19%">
            <col width="19%">
            <col width="12%">
            </colgroup>
            
            
            <thead>
              <tr>
                <th rowspan="2">번호</th>
                <th rowspan="2">구분</th>
                <th rowspan="2">접속시간</th>
                <th rowspan="2">아이디</th>
                <th rowspan="2">이름</th>
                <th colspan="3" class="title01">소속</th>
                </tr>
              <tr>
                <th class="title02">사업소</th>
                <th class="title02">부서</th>
                <th class="title02">직급</th>
                </tr>
            </thead>
            <tbody>
              <tr>
                <td>10</td>
                <td>로그아웃</td>
                <td>2017-12-01 14:00:30</td>
                <td>admin</td>
                <td>홍길동</td>
                <td>상수도사업본부</td>
                <td>누수방지팀</td>
                <td>주무관</td>
                </tr>
              <tr>
                <td>9</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                </tr>
              <tr>
                <td>8</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                </tr>
             
              <tr>
                <td>7</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                </tr>
              <tr>
                <td>6</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                </tr>
              <tr >
                <td>5</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                </tr>
                <tr>
                <td>4</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                </tr>
              <tr>
                <td>3</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                </tr>
              <tr>
                <td>2</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                </tr>
              <tr >
                <td>1</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                </tr>
            </tbody>
          </table>
          <div style="width:100%; text-align:center">
            <ul class="pagination pagination-sm  " >
            
              <li> <a href="#" aria-label="Previous"> <span aria-hidden="true">	&lt; </span> </a> </li>
               
              <li><a href="#">1</a></li>
              <li class="active"><a href="#"  >2</a></li>
              <li><a href="#">3</a></li>
              <li><a href="#">4</a></li>
              <li><a href="#">5</a></li>
             
               <li> <a href="#" aria-label="Next"> <span aria-hidden="true">&gt;</span> </a> </li>
              
            </ul>
          </div>
        </div>
        
            <!-- 
            
            <div class="btn_group right"  >
            <button id="button" class="btn btn-default " > <i class="fa fa-plus "></i> 등록</button>
          
          </div>
          -->
          
          
    
      <!-- //list --> 
      </div>
	      
   	</div>
	<!-- //content 끝 -->
    
	</form>
	        
	<!-- footer 시작 
	<div id="footer"><c:import url="/EgovPageLink.do?link=main/inc/EgovIncFooter" /></div>
	-->
</body>
</html>