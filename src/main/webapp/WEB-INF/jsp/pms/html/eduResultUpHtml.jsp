<%--
  Class Name : eduDataList.jsp
  Description : 교육자료(조회,삭제) JSP
  Modification Information
 
      수정일         수정자                   수정내용
    -------    --------    ---------------------------
     2017.03.03                 최초 생성
 
    author   : 공통서비스 개발팀 
    since    : 2017.03.03
--%>
<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>교육 성과 상세</title>
<%@ include file="/WEB-INF/jsp/pms/include-pms.jspf"%>
<script type="text/javaScript" language="javascript" defer="defer">
<!--
$(document).ready(function() {
	$("#tab1_info").show();
	$("#tab2_info").hide();
	$("#tab3_info").hide();
});
function fnListPage(){
    document.listForm.action = "<c:url value='/pms/edu/eduSchdulList.do'/>";
    document.listForm.submit();	
}
function fnSave(){
	
}
function fnTabChange(sTab){
	if(sTab=='2') {
		$("#li_tab1").removeClass("active");
		$("#tab1").removeClass("active");
		$("#tab1_info").hide();

		$("#li_tab2").addClass("active");
		$("#tab2").addClass("active");
		$("#tab2_info").show();

		$("#li_tab3").removeClass("active");
		$("#tab3").removeClass("active");
		$("#tab3_info").hide();
	} else if(sTab=='3') {
		$("#li_tab1").removeClass("active");
		$("#tab1").removeClass("active");
		$("#tab1_info").hide();

		$("#li_tab2").removeClass("active");
		$("#tab2").removeClass("active");
		$("#tab2_info").hide();

		$("#li_tab3").addClass("active");
		$("#tab3").addClass("active");
		$("#tab3_info").show();
	} else {
		$("#li_tab1").addClass("active");
		$("#tab1").addClass("active");
		$("#tab1_info").show();

		$("#li_tab2").removeClass("active");
		$("#tab2").removeClass("active");
		$("#tab2_info").hide();

		$("#li_tab3").removeClass("active");
		$("#tab3").removeClass("active");
		$("#tab3_info").hide();
	}
}
//-->
'<c:if test="${!empty resultMsg}">alert("<spring:message code="${resultMsg}" />");</c:if>'
</script>

</head>
<body>
	<noscript class="noScriptTitle">자바스크립트를 지원하지 않는 브라우저에서는 일부 기능을 사용하실 수 없습니다.</noscript>
	<form name="listForm" action="<c:url value='/pms/edu/eduSchdulList.do'/>" method="post">

      <div class="contents-box"  > 
	<!-- 현재위치 네비게이션 시작 -->
	<div class="conts" >
		<h1><i class="fa fa-chevron-circle-right  "></i> 교육성과관리</h1>
          
          <!-- info -->
          <div class="conts-gray" style="padding:15px 20px">
            <table class="table table-border-no "  >
              <colgroup>
              <col width="15%">
              <col width="35%">
              <col width="15%">
              <col width="35%">
              </colgroup>
              <tbody>
                <tr>
                  <th > <p>교육구분</p></th>
                  <td>[상시교육] 관망관리시스템 </td>
                  <th><p>교육명</p></th>
                  <td>상수도 사업본부 상시 교육</td>
                </tr>
                <tr>
                  <th><p>교육기간</p></th>
                  <td>2017-01-01 ~2018-12-30</td>
                  <th><p>대상자</p></th>
                  <td> 상수도 사업본부 전직원</td>
                </tr>
              </tbody>
            </table>
          </div>
          
          <!-- //info --> 
          
          <!-- tab -->
          
          <div class="content-tab">
            <ul class="content-tab">
              <li id="li_tab1" class="active"><a href="#"  id="tab1" class="active" onclick="fnTabChange('1'); return false;">교육진행 현황</a></li>
              <li id="li_tab2" ><a href="#" id="tab2" onclick="fnTabChange('2'); return false;" >과제제출 현황</a></li>
              <li id="li_tab3" ><a href="#" id="tab3" onclick="fnTabChange('3'); return false;" >대상자별 진도 현황</a></li>
            </ul>
          </div>
          <!-- //tab --> 
          
          <!-- list -->
          
          <div id="tab1_info" class="conts-gray of-y-auto" style="border-top:none">
            <table class="table table-list center ">
              <colgroup>
              <col width="22%">
              <col width="13">
              <col width="13%">
              <col width="13%">
              <col width="13%">
              <col width="13%">
              <col width="13%">
              </colgroup>
              <thead>
                <tr>
                  <th rowspan="2">대분류</th>
                  <th colspan="3" class="title01">교육진행현황(명)</th>
                  <th colspan="3" class="title01">퀴즈진행현황(명)</th>
                </tr>
                <tr>
                  <th class="title02">완료</th>
                  <th class="title02">미완료</th>
                  <th class="title02">진행율</th>
                  <th class="title02">완료</th>
                  <th class="title02">미완료</th>
                  <th class="title02">진행율</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>계측기블록현황</td>
                  <td>7</td>
                  <td>5</td>
                  <td>10%</td>
                  <td>7</td>
                  <td>5</td>
                  <td>10%</td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr >
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr >
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
              </tbody>
            </table>
          </div>
          <!-- //list --> 
          
          <!-- list -->
          
          <div id="tab2_info" class="conts-gray of-y-auto" style="border-top:none;">
            <table class="table table-hover table-list center ">
              <colgroup>
              <col width="7%">
             
              <col width="26%">
              <col width="17%">
              <col width="15%">
              <col width="15%">
              </colgroup>
              <thead>
                <tr>
                  <th>번호</th>
                  <th>과제명</th>
                  <th>제출기한</th>
                  <th>제출인원</th>
                  <th>전체인원</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>10</td>
                  <td class="left">상수 최적 관망관리시스템</td>
                  <td>2017-09-01~2017-09-01</td>
                  <td>100</td>
                  <td>200</td>
                </tr>
                <tr>
                  <td>9</td>
                  <td class="left">관망관리시스템</td>
                  <td>2017-09-01</td>
                  <td>100</td>
                  <td>200</td>
                </tr>
                <tr>
                  <td>8</td>
                  <td class="left">상수 최적 관망관리시스템</td>
                  <td>2017-09-01</td>
                  <td>100</td>
                  <td>200</td>
                </tr>
                <tr>
                  <td>7</td>
                  <td class="left">관망관리시스템</td>
                  <td>2017-09-01</td>
                  <td>100</td>
                  <td>200</td>
                </tr>
                <tr>
                  <td>6</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr >
                  <td>5</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td>4</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td>3</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td>2</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr >
                  <td>1</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
              </tbody>
            </table>
          </div>
          <!-- //list --> 
          
          <!-- list -->
          
          <div id="tab3_info" class="conts-gray of-y-auto" style="border-top:none;">
            <table class="table table-list center ">
              <colgroup>
              <col width="6%">
              <col width="15%">
              <col width="15%">
              <col width="10%">
              <col width="10%">
              <col width="8%">
              <col width="10%">
              <col width="8%">
              <col width="10%">
              <col width="8%">

              </colgroup>
              <thead>
                <tr>
                  <th rowspan="2">번호</th>
                  <th rowspan="2">소속</th>
                  <th rowspan="2">부서</th>
                  <th rowspan="2">이름</th>
                  <th colspan="2" class="title01">강의</th>
                  <th colspan="2" class="title01">과제</th>
                  <th colspan="2" class="title01">퀴즈</th>
                </tr>
                <tr>
                  <th class="title02">진행/전체시간</th>
                  <th class="title02">진행율</th>
                  <th class="title02">진행/전체건수</th>
                  <th class="title02">진행율</th>
                  <th class="title02">진행/전체건수</th>
                  <th class="title02">진행율</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>10</td>
                  <td>시설관리부</td>
                  <td>누수방지팀</td>
                  <td>홍길동</td>
                  <td>5</td>
                  <td>10%</td>
                  <td>5</td>
                  <td>10%</td>
                  <td>5</td>
                  <td>10%</td>
                </tr>
                <tr>
                  <td >9</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td >8</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td >7</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td >6</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr >
                  <td >5</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td >4</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td >3</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td >2</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr >
                  <td >1</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
              </tbody>
            </table>
          </div>
          <!-- //list --> 

	      
   	</div>
   	</div>
	<!-- //content 끝 -->
	</form>
</body>
</html>