<%--
  Class Name : eduDataList.jsp
  Description : 교육자료(조회,삭제) JSP
  Modification Information
 
      수정일         수정자                   수정내용
    -------    --------    ---------------------------
     2017.03.03                 최초 생성
 
    author   : 공통서비스 개발팀 
    since    : 2017.03.03
--%>
<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>그룹 선택</title>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<link href="<c:url value='/css/style_popup.css' />" rel="stylesheet" type="text/css">
<link href="<c:url value='/fonts/font-awesome/css/font-awesome.min.css' />" rel="stylesheet" />
<script type="text/javaScript" language="javascript" defer="defer">
<!--
$(document).ready(function() {
});
function fnListPage(){
    document.listForm.action = "<c:url value='/pms/edu/eduSchdulList.do'/>";
    document.listForm.submit();	
}
function fnSave(){
	
}
//-->
'<c:if test="${!empty resultMsg}">alert("<spring:message code="${resultMsg}" />");</c:if>'
</script>

</head>
<body>
	<noscript class="noScriptTitle">자바스크립트를 지원하지 않는 브라우저에서는 일부 기능을 사용하실 수 없습니다.</noscript>
	<form name="listForm" action="<c:url value='/pms/html/eduSchdulHtmlGrpPopup.do'/>" method="post">

    <!-- UI Object -->
<div id="wrap"> 
  
 
  
<h1><i class="fa fa-chevron-right   "></i> 그룹선택</h1>
<div class="conts"  >
          
          
          <!-- write -->
          
          <div class="table-responsive of-y-auto ">
           <table class="table table-in">
                <colgroup>
                <col width="150px"> <col width="200px">
                <col width="*">
               
                </colgroup>
                <thead>
                  <tr>
                    <th>선택</th>
                    <th>그룹명</th>
                    <th>구성원</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td class="check"><input name="a" type="checkbox" id="b0" value="" checked ></td>
                    <td class="left">시설관리부</td>
                    <td class="left">홍길동, 김삿갓, …<br></td>
                  </tr>
                  <tr>
                    <td class="check"><input name="a" type="checkbox" id="b0" value="" checked ></td>
                    <td class="left">시설관리부</td>
                    <td class="left">홍길동, 김삿갓, …<br></td>
                  </tr>
                  <tr>
                    <td class="check"><input name="b" type="checkbox" id="b" value="" ></td>
                    <td class="left">시설관리부</td>
                    <td class="left">홍길동, 김삿갓, …<br></td>
                  </tr>
                  <tr>
                    <td class="check"><input name="b2" type="checkbox" id="b2" value="" ></td>
                    <td class="left">시설관리부</td>
                    <td class="left">홍길동, 김삿갓, …<br></td>
                  </tr>
                  <tr>
                    <td class="check"><input name="a" type="checkbox" id="b0" value="" ></td>
                    <td class="left">시설관리부</td>
                    <td class="left">홍길동, 김삿갓, …<br></td>
                  </tr>
                </tbody>
              </table>
          </div>
    <div class="btn_group center m-t-15  "  > 
           
            <button id="button" class="btn btn-default"  > <i class="fa fa-check" aria-hidden="true"></i> 저장</button>
            <button id="button" class="btn btn-default " onclick="JavaScript:self.close();"  > <i class="fa fa-close" aria-hidden="true"></i> 닫기</button>
          </div>
          
          <!-- //write --> 
          
  </div>
        
        <!--conts--> 
  
   </div>
<!-- //UI Object -->
	<!-- //content 끝 -->
	</form>
</body>
</html>