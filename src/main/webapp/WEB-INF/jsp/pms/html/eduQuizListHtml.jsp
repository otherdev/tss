<%--
  Class Name : eduDataList.jsp
  Description : 교육자료(조회,삭제) JSP
  Modification Information
 
      수정일         수정자                   수정내용
    -------    --------    ---------------------------
     2017.03.03                 최초 생성
 
    author   : 공통서비스 개발팀 
    since    : 2017.03.03
--%>
<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>퀴즈관리</title>
<%@ include file="/WEB-INF/jsp/pms/include-pms.jspf"%>
<script type="text/javaScript" language="javascript" defer="defer">

$(document).ready(function() {
});

$(document).ready(function() {
});

// 신규등록모드로 상세페이지 호출
function fnEduAdd() {
}
function fnLinkPage(pageNo){
}
function fnSearch(){
}


function fnEduDataInfo(eduSeq){
}

function fnEduDataCreate(eduSeq){
}

</script>

</head>
<body>
	<noscript class="noScriptTitle">자바스크립트를 지원하지 않는 브라우저에서는 일부 기능을 사용하실 수 없습니다.</noscript>
	<form name="listForm" action="<c:url value='/pms/edu/eduSchdulMngList.do'/>" method="post">

      <div class="contents-box"  > 
	<!-- 현재위치 네비게이션 시작 -->
	<div class="conts" >
		<h1><i class="fa fa-chevron-circle-right  "></i> 퀴즈관리</h1>
          
           
          
            <!-- list -->
    
      
       
        <div style="width:27%;  margin-right:3% ; display:inline-block; ">
         <div class="search-group "  style=" margin-bottom:3px">
       
        <span><b>시스템명</b> </span>
          
          <select  class="form-control input-inline" name="region3" style="width:71%">
            <option value="관망관리시스템">관망관리시스템 </option>
            <option value="관망관리시스템">관망관리시스템 01 </option>
            <option value="관망관리시스템">관망관리시스템 00000001 </option>
            
          </select>
          
         
       </div>     
        
         <div class="conts-gray of-y-scroll">
           <table class="table table-hover table-list center "  >
          
           <colgroup>
             <col width="*">
           
          
           
            </colgroup>
            
            
            <thead>
              <tr>
                <th>대분류</th>
                </tr>
            </thead>
            <tbody>
              <tr>
                <td class="left">계측기 블록현황</td>
                </tr>
             <tr>
                <td class="left">블록계통도</td>
                </tr>
              <tr>
                <td class="left">유량분석</td>
                </tr>
             
              <tr>
                <td class="left">&nbsp;</td>
                </tr>
              <tr>
                <td class="left">&nbsp;</td>
                </tr>
              <tr >
                <td class="left">&nbsp;</td>
                </tr>
                <tr>
                <td class="left">&nbsp;</td>
                </tr>
              <tr>
                <td class="left">&nbsp;</td>
                </tr>
              <tr>
                <td class="left">&nbsp;</td>
                </tr>
              <tr >
                <td class="left">&nbsp;</td>
                </tr>
            </tbody>
          </table>
        </div>
        </div>
        
            
          
          
    
      <!-- //list --> 
        <!-- list2 -->
    
      
       
        <div style="width:70%;  display:inline-block; float:right;">
         <h2>퀴즈항목
         <span class="f_r"  > 
         <b>문제수</b> 
         
          
         <input type="text" class="form-control" value="" style="width:100px">
         <button id="button" class="btn btn-default" > <i class="fa fa-check" aria-hidden="true"></i> 저장</button>
         </span></h2>
         
         
         <div class="conts-gray of-y-scroll " style="padding:0px 15px; height:480px;width:97%;margin-top:15px;">
         <!-- //qui -->   
        <div class="quiz-group">
        <p class="q">1. <input type="text" class="form-control" style="width:94%" value="블록 계측데이터는 몇 분 간격으로 수집이 되는가?"></p>
        <p class="example-n">보기수  <input type="text" class="form-control" value="4" style="width:30px"></p>
        <p class="example">
       
       <input name="" type="radio" class="i_radio" id="c1" value="" checked>
                   <label for="c1"><input type="text" class="form-control" value="10분" style="width:50px"></label>
                  <input name="" type="radio" value="" id="c2" >
                   <label for="c1"><input type="text" class="form-control" value="30분" style="width:50px"></label>
                  <input name="" type="radio" value="" id="c3" >
                   <label for="c1"><input type="text" class="form-control" value="60분" style="width:50px"></label>
                  <input name="" type="radio" value="" id="c4">
                   <label for="c1"><input type="text" class="form-control" value="90분" style="width:50px"></label>
      
        
        </div>
        
         <!-- //qui -->  
         
           <!-- //qui -->   
        <div class="quiz-group">
        <p class="q">2. <input type="text" class="form-control" style="width:94%" value="블록 계측데이터는 몇 분 간격으로 수집이 되는가?"></p>
        <p class="example-n">보기수  <input type="text" class="form-control" value="3" style="width:30px"></p>
        <p class="example">
       
       <input name="" type="radio" class="i_radio" id="c1" value="" checked>
                   <label for="c1"><input type="text" class="form-control" value="10분" style="width:50px"></label>
                  <input name="" type="radio" value="" id="c2" >
                   <label for="c1"><input type="text" class="form-control" value="30분" style="width:50px"></label>
                  <input name="" type="radio" value="" id="c3" >
                   <label for="c1"><input type="text" class="form-control" value="60분" style="width:50px"></label>
                
      
        
        </div>
        
         <!-- //qui --> 
         
             <!-- //qui -->   
        <div class="quiz-group">
        <p class="q">3. <input type="text" class="form-control" style="width:94%" value="블록 계측데이터는 몇 분 간격으로 수집이 되는가?"></p>
        <p class="example-n">보기수  <input type="text" class="form-control" value="4" style="width:30px"></p>
        <p class="example">
       
       <input name="" type="radio" class="i_radio" id="c1" value="" checked>
                   <label for="c1"><input type="text" class="form-control" value="10분" style="width:50px"></label>
                  <input name="" type="radio" value="" id="c2" >
                   <label for="c1"><input type="text" class="form-control" value="30분" style="width:50px"></label>
                  <input name="" type="radio" value="" id="c3" >
                   <label for="c1"><input type="text" class="form-control" value="60분" style="width:50px"></label>
                  <input name="" type="radio" value="" id="c4">
                   <label for="c1"><input type="text" class="form-control" value="90분" style="width:50px"></label>
      
        
        </div>
        
         <!-- //qui -->  
         
             <!-- //qui -->   
        <div class="quiz-group">
        <p class="q">1. 블록 계측데이터는 몇 분 간격으로 수집이 되는가?</p>
        <p class="example-n">보기수  <input type="text" class="form-control" value="" style="width:50px"></p>
        <p class="example">
       
       <input name="" type="radio" class="i_radio" id="c1" value="" checked>
                  <label for="c1"> 10분</label>
                  <input name="" type="radio" value="" id="c2" >
                  <label for="c2"> 30분</label>
                  <input name="" type="radio" value="" id="c3" >
                  <label for="c3"> 60분</label>
                  <input name="" type="radio" value="" id="c4">
                  <label for="c4"> 90분</label> </p>
      
        
        </div>
        
         <!-- //qui -->  
         
         
             <!-- //qui -->   
        <div class="quiz-group">
        <p class="q">1. 블록 계측데이터는 몇 분 간격으로 수집이 되는가?</p>
        <p class="example-n">보기수  <input type="text" class="form-control" value="" style="width:50px"></p>
        <p class="example">
       
       <input name="" type="radio" class="i_radio" id="c1" value="" checked>
                  <label for="c1"> 10분</label>
                  <input name="" type="radio" value="" id="c2" >
                  <label for="c2"> 30분</label>
                  <input name="" type="radio" value="" id="c3" >
                  <label for="c3"> 60분</label>
                  <input name="" type="radio" value="" id="c4">
                  <label for="c4"> 90분</label> </p>
      
        
        </div>
        
         <!-- //qui -->   
        </div>
        </div>
        
            
          
          
      <!-- //list 2-->  
	      
   	</div>
   	</div>
	<!-- //content 끝 -->
    
	</form>
	        
	<!-- footer 시작 
	<div id="footer"><c:import url="/EgovPageLink.do?link=main/inc/EgovIncFooter" /></div>
	-->
</body>
</html>