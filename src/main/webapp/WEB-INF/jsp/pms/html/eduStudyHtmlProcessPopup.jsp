<%--
  Class Name : eduDataList.jsp
  Description : 교육자료(조회,삭제) JSP
  Modification Information
 
      수정일         수정자                   수정내용
    -------    --------    ---------------------------
     2017.03.03                 최초 생성
 
    author   : 공통서비스 개발팀 
    since    : 2017.03.03
--%>
<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>강의 상세</title>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<link href="<c:url value='/css/style_popup.css' />" rel="stylesheet" type="text/css">
<link href="<c:url value='/fonts/font-awesome/css/font-awesome.min.css' />" rel="stylesheet" />
<script type="text/javaScript" language="javascript" defer="defer">
<!--
$(document).ready(function() {
});
function fnListPage(){
    document.listForm.action = "<c:url value='/pms/edu/eduSchdulList.do'/>";
    document.listForm.submit();	
}
function fnSave(){
	
}
//-->
'<c:if test="${!empty resultMsg}">alert("<spring:message code="${resultMsg}" />");</c:if>'
</script>

</head>
<body>
	<noscript class="noScriptTitle">자바스크립트를 지원하지 않는 브라우저에서는 일부 기능을 사용하실 수 없습니다.</noscript>
	<form name="listForm" action="<c:url value='/pms/html/eduSchdulHtmlGrpPopup.do'/>" method="post">

    <!-- UI Object -->
<div id="wrap"> 
  
 
  
<h1><i class="fa fa-chevron-right   "></i> 강의진행현황</h1>

<div class="conts" style="margin-top:0;" >

<h2> <i class="fa fa-arrow-right   "></i> 시설관리부 누수방지팀 홍길동</h2>
          
            <!-- info -->
          <div class="conts-gray" style="padding:15px 20px">
            <table class="table table-border-no "  >
              <colgroup>
              <col width="20%">
              <col width="80%">
           
              </colgroup>
              <tbody>
                <tr>
                  <th > <p>교육구분</p></th>
                  <td>[상시교육] 관망관리시스템 </td>
                </tr>
                <tr>
                  <th><p>교육명</p></th>
                  <td>상수도 사업본부 상시 교육</td>
                </tr>
                <tr>
                  <th><p>교육기간</p></th>
                  <td>2017-01-01 ~2018-12-30</td>
                </tr>
              </tbody>
            </table>
          </div>
          
          <!-- //info --> 
    <!-- write -->
          
          <div class="table-responsive of-y-auto ">
           <table class="table table-in">
                <colgroup>
                <col width="30%"> 
                <col width="15%">
                <col width="15%">
                <col width="15%">
                <col width="25%"> 
               
                </colgroup>
                <thead>
                  <tr>
                    <th>대분류</th>
                    <th>총교육시간</th>
                    <th>학습시간</th>
                    <th>진행율</th>
                    <th>최근 학습시간</th>
                  </tr>
                </thead>
                <tbody>
                  <tr class="total ">
                    <td>전체</td>
                    <td>70분</td>
                    <td>20분</td>
                    <td> 50% </td>
                    <td>2017-12-30 15:20 </td>
                  </tr>
                  <tr>
                    <td>계측기블록현황</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                  </tr>
                  <tr>
                    <td>블록계통</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                  </tr>
                  <tr>
                    <td>유량분석</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                  </tr>
                  <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                  </tr>
                </tbody>
              </table>
          </div>
    <div class="btn_group center m-t-15  "  >
           
           
            <button id="button" class="btn btn-default "  onclick="JavaScript:self.close();" > <i class="fa fa-close" aria-hidden="true"></i> 닫기</button>
          </div>
          
          <!-- //write --> 
          
  </div>
        
        <!--conts--> 
  
   </div>
<!-- //UI Object -->
	<!-- //content 끝 -->
	</form>
</body>
</html>