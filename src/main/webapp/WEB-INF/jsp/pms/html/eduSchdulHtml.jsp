<%--
  Class Name : eduDataList.jsp
  Description : 교육자료(조회,삭제) JSP
  Modification Information
 
      수정일         수정자                   수정내용
    -------    --------    ---------------------------
     2017.03.03                 최초 생성
 
    author   : 공통서비스 개발팀 
    since    : 2017.03.03
--%>
<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>교육 일정 상세</title>
<%@ include file="/WEB-INF/jsp/pms/include-pms.jspf"%>
<script type="text/javaScript" language="javascript" defer="defer">
<!--
$(document).ready(function() {
});
function fnListPage(){
    document.listForm.action = "<c:url value='/pms/edu/eduSchdulList.do'/>";
    document.listForm.submit();	
}
function fnSave(){
	
}
function fnPopup(sType){
	var sUrl = ""; 
	if(sType == 'group') {// height=540, width=750
		window.open("<c:url value='/pms/html/eduSchdulHtmlGrpPopup.do'/>","code","height=360, width=750, top=50, left=20, scrollbars=no, resizable=no");
		// sUrl = "/pms/html/eduSchdulHtmlGrpPopup.do";
	} else if(sType == 'project'){
		window.open("<c:url value='/pms/html/eduSchdulHtmlProjectPopup.do'/>","code","height=400, width=750, top=50, left=20, scrollbars=no, resizable=no");
		
		// sUrl = "/pms/html/eduSchdulHtmlProjectPopup.do";
	}
}
//-->
'<c:if test="${!empty resultMsg}">alert("<spring:message code="${resultMsg}" />");</c:if>'
</script>

</head>
<body>
	<noscript class="noScriptTitle">자바스크립트를 지원하지 않는 브라우저에서는 일부 기능을 사용하실 수 없습니다.</noscript>
	<form name="listForm" action="<c:url value='/pms/edu/eduSchdulList.do'/>" method="post">

      <div class="contents-box"  > 
	<!-- 현재위치 네비게이션 시작 -->
	<div class="conts" >
		<h1><i class="fa fa-chevron-circle-right  "></i> 교육일정관리</h1>
          
          <!-- write -->
          
          <div class="table-responsive">
            <table class="table table-write">
              <caption>
              입력양식
              </caption>
              <colgroup>
              <col width="20%">
              <col width="80%">
              </colgroup>
              <tbody>
                <tr>
                  <th>교육구분</th>
                  <td><select  class="form-control input-inline" style="width:25%"  name="region3">
                      <option value="관망관리시스템">관망관리시스템</option>
                    </select>
                    <select  class="form-control input-inline" style="width:25%"  name="region3">
                      <option value="관망관리시스템">관망관리시스템</option>
                    </select></td>
                </tr>
                <tr>
                  <th>교육명</th>
                  <td><input type="text" class="form-control" value="" ></td>
                </tr>
                <tr>
                  <th>내용</th>
                  <td><textarea class="form-control" id="" name="" rows="60" style="resize: none; width:100%; height:100px"></textarea></td>
                </tr>
                <tr>
                  <th>교육기간</th>
                  <td><input type="text" class="form-control  " style="width:150px" value="">
                    <a href="#"> <i class="fa fa-calendar fa-lg" aria-hidden="true"></i></a> ~
                    <input type="text" class="form-control  " style="width:150px" value="">
                    <a href="#"> <i class="fa fa-calendar fa-lg" aria-hidden="true"></i></a></td>
                </tr>
                <tr>
                  <th>대상자 </th>
                  <td><input type="text" class="form-control" value="" ></td>
                </tr>
                <tr>
                  <th>그룹선택
                    <p> <a href="#" class="btn btn-table btn-xs " onClick="javascript:fnPopup('group'); return false;"  >그룹지정</a></p></th>
                 <td style="padding-right:0" ><!--table list -->
                   <table class="table table-in">
                <colgroup>
                <col width="150px">
                <col width="*">
                <col width="200px">
                </colgroup>
                <thead>
                  <tr>
                    <th>그룹명</th>
                    <th>구성원</th>
                    </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>과제</td>
                    <td class="left">과제제목입니다.</td>
                    </tr>
                  <tr>
                    <td>과제</td>
                    <td class="left">과제제목입니다.</td>
                    </tr>
                  <tr>
                    <td>과제</td>
                    <td class="left">과제제목입니다.</td>
                    </tr>
                </tbody>
              </table>
                  <!--//table list --></td>
                </tr>
                <tr>
                  <th>과제/토론
                   <p> <a href="#" class="btn btn-table btn-xs " onClick="javascript:fnPopup('project'); return false;">과제지정</a></p></th>
                  <td style="padding-right:0">
                  <!--table list -->
                   <table class="table table-in">
                <colgroup>
                <col width="150px">
                <col width="*">
                <col width="200px">
                </colgroup>
                <thead>
                  <tr>
                    <th>구분</th>
                    <th>제목</th>
                    <th>기간</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>과제</td>
                    <td class="left">과제제목입니다.</td>
                    <td >2017-12-01 ~ 2017-12-01</td>
                  </tr>
                  <tr>
                    <td>과제</td>
                    <td class="left">과제제목입니다.</td>
                    <td >2017-12-01 ~ 2017-12-01</td>
                  </tr>
                  <tr>
                    <td>과제</td>
                    <td class="left">과제제목입니다.</td>
                    <td >2017-12-01 ~ 2017-12-01</td>
                  </tr>
                </tbody>
              </table>
                  <!--//table list --></td>
                </tr>
              </tbody>
            </table>
          </div>
          <div class="btn_group right "  >
            <button id="button" class="btn btn-default " onClick="javascript:fnListPage(); return false;" > <i class="fa fa-reply" aria-hidden="true"></i> 취소</button>
            <button id="button" class="btn btn-default" onClick="javascript:fnSave(); return false;" > <i class="fa fa-check" aria-hidden="true"></i> 저장</button>
          </div>
          
          <!-- //write --> 
          

	      </div>
   	</div>
	<!-- //content 끝 -->
	</form>
</body>
</html>