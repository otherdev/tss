<%--
  Class Name : eduDataList.jsp
  Description : 교육자료(조회,삭제) JSP
  Modification Information
 
      수정일         수정자                   수정내용
    -------    --------    ---------------------------
     2017.03.03                 최초 생성
 
    author   : 공통서비스 개발팀 
    since    : 2017.03.03
--%>
<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>나의 학습현황</title>
<%@ include file="/WEB-INF/jsp/pms/include-pms.jspf"%>
<script type="text/javaScript" language="javascript" defer="defer">

$(document).ready(function() {
});


// 신규등록모드로 상세페이지 호출
function fnEduAdd() {
    document.listForm.action = "<c:url value='/pms/html/eduProjectUpHtml.do'/>";
    document.listForm.submit();	
}
function fnLinkPage(pageNo){
}
function fnSearch(){
}


function fnEduDataInfo(eduSeq){
    document.listForm.action = "<c:url value='/pms/html/eduProjectUpHtml.do'/>";
    document.listForm.submit();	
}

function fnEduDataCreate(eduSeq){
}

function fnPopup(sType){
	var sUrl = ""; 
	if(sType == 'process') {
		window.open("<c:url value='/pms/html/eduStudyHtmlProcessPopup.do'/>","code","height=540, width=750, top=50, left=20, scrollbars=no");
		// sUrl = "/pms/html/eduSchdulHtmlGrpPopup.do";
	} else if(sType == 'project'){
		window.open("<c:url value='/pms/html/eduStudyHtmlProjectPopup.do'/>","code","height=540, width=750, top=50, left=20, scrollbars=no");
		
		// sUrl = "/pms/html/eduSchdulHtmlProjectPopup.do";
	} else if(sType == 'quiz'){
		window.open("<c:url value='/pms/html/eduStudyHtmlQuizPopup.do'/>","code","height=350, width=750, top=50, left=20, scrollbars=no");
		
		// sUrl = "/pms/html/eduSchdulHtmlProjectPopup.do";
	}
}
</script>

</head>
<body>
	<noscript class="noScriptTitle">자바스크립트를 지원하지 않는 브라우저에서는 일부 기능을 사용하실 수 없습니다.</noscript>
	<form name="listForm" action="<c:url value='/pms/edu/eduSchdulMngList.do'/>" method="post">

      <div class="contents-box"  > 
	<!-- 현재위치 네비게이션 시작 -->
	<div class="conts" >
		<h1><i class="fa fa-chevron-circle-right  "></i> 교육성과관리</h1>
          
       
          
         
          
         
          
          <!-- list -->
           <div class="search-group">
            <select  class="form-control input-inline" name="region3">
              <option value="제목">제목</option>
              <option value="시스템명">시스템명</option>
            </select>
            <input type="text" class="form-control" value="" style="width:200px">
            <button id="button" class="btn btn-default " > <i class="fa fa-search " aria-hidden="true"></i> 검색</button>
          </div>
          <div class="of-y-no">
            <table class="table table-list center ">
              <colgroup>
              <col width="6%">
              <col width="12%">
              <col width="12%">
              <col width="*">
              <col width="*">
              <col width="6%">
              <col width="6%">
              <col width="7%">
              <col width="6%">
              <col width="7%">
              <col width="6%">

              </colgroup>
              <thead>
                <tr>
                  <th rowspan="2">번호</th>
                  <th rowspan="2">교육구분</th>
                  <th rowspan="2">시스템명</th>
                  <th rowspan="2">교육명</th>
                  <th rowspan="2">교육기간</th>
                  <th colspan="2" class="title01">강의</th>
                  <th colspan="2" class="title01">과제</th>
                  <th colspan="2" class="title01">퀴즈</th>
                </tr>
                <tr>
                  <th class="title02">진행/<br>총시간</th>
                  <th class="title02">진행율</th>
                  <th class="title02">진행/<br>전체건수</th>
                  <th class="title02">진행율</th>
                  <th class="title02">진행/<br>전체건수</th>
                  <th class="title02">진행율</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>10</td>
                  <td>상시교육</td>
                  <td>관망관리</td>
                  <td>-</td>
                  <td>2017-12-12~2018-12-12</td>
                  <td>
						 <span class="link"><a 
						 	onclick="javascript:fnPopup('process'); return false;">
						 	5
						 	</a></span></td>
                  <td>
						 <span class="link"><a 
						 	onclick="javascript:fnPopup('process'); return false;">
						 	10%
						 	</a></span></td>
                  <td>
						 <span class="link"><a 
						 	onclick="javascript:fnPopup('project'); return false;">
						 	5
						 	</a></span></td>
                  <td>
						 <span class="link"><a 
						 	onclick="javascript:fnPopup('project'); return false;">
						 	10%
						 	</a></span></td>
                  <td>
						 <span class="link"><a 
						 	onclick="javascript:fnPopup('quiz'); return false;">
						 	5
						 	</a></span></td>
                  <td>
						 <span class="link"><a 
						 	onclick="javascript:fnPopup('quiz'); return false;">
						 	10%
						 	</a></span></td>
                </tr>
                <tr>
                  <td >9</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td >8</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td >7</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td >6</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr >
                  <td >5</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td >4</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td >3</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td >2</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr >
                  <td >1</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
              </tbody>
            </table>
          </div>
          <!-- //list --> 
	      
   	</div>
	<!-- //content 끝 -->
	</div>
    
	</form>
	        
	<!-- footer 시작 
	<div id="footer"><c:import url="/EgovPageLink.do?link=main/inc/EgovIncFooter" /></div>
	-->
</body>
</html>