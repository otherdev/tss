<%--
  Class Name : eduDataList.jsp
  Description : 교육자료(조회,삭제) JSP
  Modification Information
 
      수정일         수정자                   수정내용
    -------    --------    ---------------------------
     2017.03.03                 최초 생성
 
    author   : 공통서비스 개발팀 
    since    : 2017.03.03
--%>
<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>과제 상세</title>
<%@ include file="/WEB-INF/jsp/pms/include-pms.jspf"%>
<script type="text/javaScript" language="javascript" defer="defer">
<!--
$(document).ready(function() {
});
function fnListPage(){
    document.listForm.action = "<c:url value='/pms/html/eduProjectListHtml.do'/>";
    document.listForm.submit();	
}
function fnSave(){
	
}
//-->
'<c:if test="${!empty resultMsg}">alert("<spring:message code="${resultMsg}" />");</c:if>'
</script>

</head>
<body>
	<noscript class="noScriptTitle">자바스크립트를 지원하지 않는 브라우저에서는 일부 기능을 사용하실 수 없습니다.</noscript>
	<form name="listForm" action="<c:url value='/pms/html/eduProjectListHtml.do'/>" method="post">

      <div class="contents-box"  > 
	<!-- 현재위치 네비게이션 시작 -->
	<div class="conts" >
		<h1><i class="fa fa-chevron-circle-right  "></i> 토론 및 과제</h1>
          
          <!-- view -->
          
          <div class="of-y-no">
            <table class="table table-hover table-view  ">
              <thead>
                <tr>
                  <th >관망관리시스템 활용방안</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td ><span class="f_l"><b>기간</b> 2017-12-01 ~ 2017-12-31</span></td>
                </tr>
                <tr>
                  <td  class="view-top"  style="height:150px"> 관망관리시스템 활용방안 제출 </td>
                </tr>
              </tbody>
            </table>
          </div>
          <div class="btn_group right "  >
            <button id="button" class="btn btn-default" onclick="fnListPage(); return false;" > <i class="fa fa-bars" aria-hidden="true"></i> 목록</button>
          </div>
          
          <!-- //view --> 
          
          <!--comment-->
          <div class="comment-group">
          <ul class="comment-group">
          <li>
          <div class="comment" >
            <p >김삿갓 [kim]  2017-12-01 17:00:49 <span class=" f_r"> <a href="#"><img src="/tts/images/icon_co_file.png"  alt="파일다운로드 아이콘"> 사용자 메뉴얼 v3.1 .txt</a> <a href="#"><img src="/tts/images/icon_co_edit.png"  alt="수정 아이콘"> 수정</a> <a href="#"><img src="/tts/images/icon_co_delete.png"  alt="삭제 아이콘"> 삭제</a> </span> </p>
            <p class="cmttxt">과제 올립니다.<br>
              관망관리시스템 사용자 매뉴얼입니다.<br>
              잘숙지하시기 바랍니다 </p>
          </div>
          </li>
          
          <li>
          <div class="comment">
            <p >김삿갓 [kim]  2017-12-01 17:00:49 <span class=" f_r"> <a href="#"><img src="/tts/images/icon_co_file.png"  alt="파일다운로드 아이콘"> 사용자 메뉴얼 v3.1 .txt</a> <a href="#"><img src="/tts/images/icon_co_edit.png"  alt="수정 아이콘"> 수정</a> <a href="#"><img src="/tts/images/icon_co_delete.png"  alt="삭제 아이콘"> 삭제</a> </span> </p>
            <p class="cmttxt">과제 올립니다.<br>
              관망관리시스템 사용자 매뉴얼입니다.<br>
              잘숙지하시기 바랍니다 </p>
          </div>
          </li>
          
          </ul>
          </div>
          
          <!--//comment--> 
          
          <!--comment-write-->
          <div class="comment-write">
           
           
            
          
            <table class="table table-border-no table-comment" >
            
             <colgroup>
         
             <col width="80px">
            <col width="*">
            <col width="90px">
            </colgroup>
            
            <tbody>
              <tr>
                <td colspan="3" class=" cmtname">김선달[water]</td>
              </tr>
              <tr>
                <td colspan="2"> <textarea class="form-control" id="textarea" name="textarea" rows="60" style=" width:98.5%; height:60px"></textarea></td>
                <td class="right" ><a href="#" class="btn btn-table " style="height:50px; width:70px; vertical-align:middle; line-height:50px; "> <i class="fa fa-plus "></i> 등록</a></td>
              </tr>
              <tr>
                <td><span>첨부파일</span></td>
                <td ><input type="text" class="form-control  "  value=""  style=" width:98.5%;"></td>
                <td class="right" ><a href="#" class="btn btn-table ">파일불러오기</a></td>
              </tr>
              </tbody>
            </table>
          </div>
          
          <!--//comment-write--> 
	</div>
	      
   	</div>
	<!-- //content 끝 -->
	</form>
</body>
</html>