<%--
  Class Name : eduDataList.jsp
  Description : 교육자료(조회,삭제) JSP
  Modification Information
 
      수정일         수정자                   수정내용
    -------    --------    ---------------------------
     2017.03.03                 최초 생성
 
    author   : 공통서비스 개발팀 
    since    : 2017.03.03
--%>
<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<title>퀴즈 상세</title>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<link href="<c:url value='/css/style_popup.css' />" rel="stylesheet" type="text/css">
<link href="<c:url value='/fonts/font-awesome/css/font-awesome.min.css' />" rel="stylesheet" />
<script type="text/javaScript" language="javascript" defer="defer">
<!--
$(document).ready(function() {
});
function fnListPage(){
    document.listForm.action = "<c:url value='/pms/edu/eduSchdulList.do'/>";
    document.listForm.submit();	
}
function fnSave(){
	
}
//-->
'<c:if test="${!empty resultMsg}">alert("<spring:message code="${resultMsg}" />");</c:if>'
</script>

</head>
<body>
	<noscript class="noScriptTitle">자바스크립트를 지원하지 않는 브라우저에서는 일부 기능을 사용하실 수 없습니다.</noscript>
	<form name="listForm" action="<c:url value='/pms/html/eduSchdulHtmlGrpPopup.do'/>" method="post">

    <!-- UI Object -->
<div id="wrap">
  <h1><i class="fa fa-chevron-right   "></i> 퀴즈현황</h1>
  <div class="conts"  >
    <h2> <i class="fa fa-arrow-right   "></i> 계측기블록현황(1/5)
    
     <span class="f_r">
              <button id="button" class="btn btn-default" ><i class="fa fa-caret-left fa-lg" aria-hidden="true"></i></button>
                <button id="button" class="btn btn-default" ><i class="fa fa-caret-right fa-lg" aria-hidden="true"></i></button>
              </span></h2>
    
    <!-- info -->
    <div class="conts-gray " style=" padding:0px 15px; "> 
      <!-- //qui -->
      <div class="quiz-group">
        <p class="q">1. 블록 계측데이터는 몇 분 간격으로 수집이 되는가?</p>
        <p class="example">
        <ul>
          <li>
            <input name="" type="radio" class="i_radio " id="c1" value="" checked>
            <label for="c1"> 10분</label>
          </li>
         <li> <input name="" type="radio" value="" id="c2" >
          
            <label for="c2"> 30분</label> </li>
            <li>  <input name="" type="radio" value="" id="c3" >
            <label for="c3"> 60분</label> </li>
         
          <li> 
            <input name="" type="radio" value="" id="c4">
            <label for="c4"> 90분</label></li>
         
        </ul>
        </p>
      </div>
    </div>
    
    <!-- //info --> 
    
      <div class="btn_group center m-t-15  "  >
     
       
      <button id="button" class="btn btn-default" > <i class="fa fa-check" aria-hidden="true"></i> 제출하기</button>
       <button id="button" class="btn btn-default "  onclick="JavaScript:self.close();"> <i class="fa fa-close" aria-hidden="true"></i> 닫기</button>
          </div>
    
  </div>
  
  <!--conts--> 
  
</div>
<!-- //UI Object -->
	<!-- //content 끝 -->
	</form>
</body>
</html>