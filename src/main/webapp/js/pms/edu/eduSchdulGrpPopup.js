
//var pageID = "inline-edit";


// 그룹선택 객체
var fnObj = {
        pageStart: function () {
            fnObj.grid.bind();
        },
        grid: {
            target: new AXGrid(),
            bind: function () {
                window.myGrid = fnObj.grid.target;

                myGrid.setConfig({
                            targetID: "grdGrpPopup",
                            //fitToWidth: true,
                            sort: false,
                            //fixedColSeq: 3,
                            colGroup: [
                                {	key: "grpNm"		, label: "그룹명"	, width: "150"	, align:"center"	},
                                {	key: "grpDesc"		, label: "구성원"	, width: "530"	, align:"center"	},
                                {	key: "grpId"		, label: "그룹"	, width: "0"	, align:"center"	,display:false	},
                                {	key: "eduSeq"		, label: "그룹"	, width: "0"	, align:"center"	,display:false	},
                            ],
                            colHeadAlign: "center", // 헤드의 기본 정렬 값 ( colHeadAlign 을 지정하면 colGroup 에서 정의한 정렬이 무시되고 colHeadAlign : false 이거나 없으면 colGroup 에서 정의한 속성이 적용됩니다.
                            body: {
                                onclick: function(){
                                    //toast.push(Object.toJSON({index:this.index, r:this.r, c:this.c, item:this.item}));
                                },
                                addClass: function(){
                                },
                                onchangeScroll: function(){
                                }
                            }
                            ,
                            page: {
                                paging: false
                            },
                            onkeyup: function(event, element){
                                //this.list
                                //this.item
                                //this.element
                            },
							/*                                 
							                                request: {
							                                    //ajaxUrl :"saveGrid.php",
							                                    //ajaxPars:"param1=1¶m2=2"
							                                },
							                                response: function(){ // ajax 응답에 대해 예외 처리 필요시 response 구현
							                                    // response에서 처리 할 수 있는 객체 들
							                                    //console.log({res:this.res, index:this.index, insertIndex:this.insertIndex, list:this.list, page:this.page});
							                                    if(this.error){
							                                        console.log(this);
							                                        return;
							                                    }
							                                },
							 */                                
                        }
                );

                myGrid.setList(lst);
                console.log(myGrid.getSortParam());

            },
            
            getList: function () {
                console.log(this.target.getList());
                return this.target.getList();
            },
            getSelectedItem: function () {
                console.log(this.target.getSelectedItem());
                //toast.push('콘솔창에 데이터를 출력하였습니다.');
            }
            ,
            append: function (map) {
            	this.target.pushList(
                      {
                          grpNm: map.grpNm, // '${grpNm}',
                          grpDesc: map.grpDesc, // '${grpDesc}',
                          grpId: map.grpId, // '${grpId}',
                          eduSeq: map.eduSeq // '${eduSeq}'
                      }
              );
              // this.target.setFocus(this.target.list.length - 1);
            }
            ,
            remove: function () {
                var checkedList = myGrid.getCheckedListWithIndex(0);// colSeq
                if (checkedList.length == 0) {
                    alert("선택된 목록이 없습니다. 삭제하시려는 목록을 체크하세요");
                    return;
                }
                this.target.removeListIndex(checkedList);
                // 전달한 개체와 비교하여 일치하는 대상을 제거 합니다. 이때 고유한 값이 아닌 항목을 전달 할 때에는 에러가 발생 할 수 있습니다.
            }
        }
    };
