
var _grpId = ""; //선택된 코드마스터
var lst = [];
var lst2 = [];
var lst3 = [];




// 그룹 그리드 객체
var fnObj = {
        pageStart: function () {
            fnObj.grid.bind();
        },
        grid: {
            target: new AXGrid(),
            bind: function () {
                window.myGrid = fnObj.grid.target;

                myGrid.setConfig({
                            targetID: "grdGrp",
                            fitToWidth: true,
                            sort: false,
                            //fixedColSeq: 3,
                            passiveMode:true,	//그리드에 데이터 그대로 남겨놓기
                            passiveRemoveHide:false,                            
                            colGroup: [
                                {	key: "rowId", 	label: "선택", 		width: "60", 	align: "center"},
                                {	key: "grpId", 	label: "그룹코드", 	width: "0", 	align: "left" ,		display:false},
                                {	key: "grpNm", 	label: "그룹코드명", 	width: "120", 	align: "left"	},
                                {   key: "grpDesc", label: "설명", 		width: "250", 	align: "left" },
                                {   key: "ord", 	label: "순번", 		width: "60", 	align: "center", 	formatter: "money"},
                            ],
                            colHeadAlign: "center", // 헤드의 기본 정렬 값 ( colHeadAlign 을 지정하면 colGroup 에서 정의한 정렬이 무시되고 colHeadAlign : false 이거나 없으면 colGroup 에서 정의한 속성이 적용됩니다.
                            body: {
                                onclick: function(){
                                    //alert("onclick - " + JSON.stringify(Object.toJSON({index:this.index, r:this.r, c:this.c, item:this.item})));
                                	_grpId = this.item.grpId;
                                    fn_DetailSearch(this.item.grpId);
                                },
                                addClass: function(){
                                },
                                onchangeScroll: function(){
                                    //console.log(this);
                                },
                                ondblclick:function(){
                                	fnGrpPopup(this.item.grpId,'UPDATE');
                                  }
                            }
                            ,
                            page: {
                                paging: false
                            },
                            onkeyup: function(event, element){
                                //this.list
                                //this.item
                                //this.element
                            },
							/*                                 
                            request: {
                                //ajaxUrl :"saveGrid.php",
                                //ajaxPars:"param1=1¶m2=2"
                            },
                            response: function(){ // ajax 응답에 대해 예외 처리 필요시 response 구현
                                // response에서 처리 할 수 있는 객체 들
                                //console.log({res:this.res, index:this.index, insertIndex:this.insertIndex, list:this.list, page:this.page});
                                if(this.error){
                                    console.log(this);
                                    return;
                                }
                            },
							 */                                
                        }
                );

                myGrid.setList(lst);
                //console.log(myGrid.getSortParam());

            },
            
            getList: function () {
                //console.log(this.target.getList());
                return this.target.getList();
            },
            getSelectedItem: function () {
                //alert("getSelectedItem - " + this.target.getSelectedItem());
                //toast.push('콘솔창에 데이터를 출력하였습니다.');
            }
            ,
            append: function (map) {
                this.target.pushList(
                        {
                        	rowId: this.target.list.length,
                            grpId: 		map.grpId,
                            grpNm: 		map.grpNm,
                            grpDesc: 	map.grpDesc,
                            ord: 		map.ord,
                        }
                );
                this.target.setFocus(this.target.list.length - 1);
            }
            ,
            remove: function () {
                var checkedList = myGrid.getCheckedListWithIndex(0);// colSeq
                if (checkedList.length == 0) {
                    alert("선택된 목록이 없습니다. 삭제하시려는 목록을 체크하세요");
                    return;
                }
                this.target.removeListIndex(checkedList);
                // 전달한 개체와 비교하여 일치하는 대상을 제거 합니다. 이때 고유한 값이 아닌 항목을 전달 할 때에는 에러가 발생 할 수 있습니다.
            }
        }
    };






//그룹사용자 그리드 객체
var fnObj2 = {
        pageStart: function () {
            fnObj2.grid.bind();
        },
        grid: {
            target: new AXGrid(),
            bind: function () {
                window.myGrid2 = fnObj2.grid.target;

                myGrid2.setConfig({
                            targetID: "grdGrpUser",
                            fitToWidth: true,
                            sort: false,
                            //fixedColSeq: 3,
                            passiveMode:true,	//그리드에 데이터 그대로 남겨놓기
                            passiveRemoveHide:true,                            
                            colGroup: [
									{	key: "no", 		label: "선택", 	width: "60", 	align: "center", formatter: "checkbox" , formatterLabel: " 전체"},
									{   key: "userNm", 	label: "이름", 	width: "160", 	align: "center" },
									{   key: "ofcNm", 	label: "사업소", 	width: "228", 	align: "left" },
									{   key: "deptNm", 	label: "부서", 	width: "228", 	align: "left" },
									{	key: "grpId", 	label: "그룹코드",	width: "0", 	align: "left"  ,display:false},
									{	key: "userId", 	label: "이름ID", 	width: "0", 	align: "left"  ,display:false},
                            ],
                            colHeadAlign: "center", // 헤드의 기본 정렬 값 ( colHeadAlign 을 지정하면 colGroup 에서 정의한 정렬이 무시되고 colHeadAlign : false 이거나 없으면 colGroup 에서 정의한 속성이 적용됩니다.
                            body: {
                                onclick: function(){
                                    //toast.push(Object.toJSON({index:this.index, r:this.r, c:this.c, item:this.item}));
                                },
                                addClass: function(){
                                },
                                onchangeScroll: function(){
                                }
                            }
                            ,
                            page: {
                                paging: false
                            },
                            onkeyup: function(event, element){
                                //this.list
                                //this.item
                                //this.element
                            },
                        }
                );

                myGrid2.setList(lst2);
                //console.log(myGrid2.getSortParam());

            },
            
            getList: function () {
                //console.log(this.target.getList());
                return this.target.getList();
            },
            getSelectedItem: function () {
                console.log(this.target.getSelectedItem());
                //toast.push('콘솔창에 데이터를 출력하였습니다.');
            }
            ,
            append: function (map) {
                this.target.pushList(
                        {
                            no: this.target.list.length,
                            grpId	:	map.grpId , 	
                            userId	:	map.userId , 	
                            userNm	:	map.userNm , 	
                            ofcNm	:	map.ofcNm , 	
                            deptNm	:	map.deptNm , 
                        }
                );
                this.target.setFocus(this.target.list.length - 1);
            }
            ,
            remove: function () {
                var checkedList = myGrid2.getCheckedListWithIndex(0);// colSeq
                debugger;
                if (checkedList.length == 0) {
                    alert("선택된 목록이 없습니다. 삭제하시려는 목록을 체크하세요");
                    return;
                }
                this.target.removeListIndex(checkedList);
                // 전달한 개체와 비교하여 일치하는 대상을 제거 합니다. 이때 고유한 값이 아닌 항목을 전달 할 때에는 에러가 발생 할 수 있습니다.
            }, deleteRow: function(objData){
            	debugger; 
            	this.target.removeList([objData]);
            }
        }
    };


//그룹사용자 그리드 객체
var fnObj3 = {
      pageStart: function () {
          fnObj3.grid.bind();
      },
      grid: {
          target: new AXGrid(),
          bind: function () {
              window.myGrid3 = fnObj3.grid.target;

              myGrid3.setConfig({
                          targetID: "grdUser",
                          fitToWidth: true,
                          sort: false,
                          //fixedColSeq: 3,
                          passiveMode:true,	//그리드에 데이터 그대로 남겨놓기
                          passiveRemoveHide:true,                            
                          colGroup: [
									{	key: "no", 		label: "선택", 	width: "60", 	align: "center", formatter: "checkbox" , formatterLabel: " 전체"},
									{   key: "userNm", 	label: "이름", 	width: "160", 	align: "center" },
									{   key: "ofcNm", 	label: "사업소", 	width: "228", 	align: "left" },
									{   key: "deptNm", 	label: "부서", 	width: "228", 	align: "left" },
									{	key: "grpId", 	label: "그룹코드",	width: "0", 	align: "left"  ,display:false},
									{	key: "userId", 	label: "이름ID", 	width: "0", 	align: "left"  ,display:false},
                          ],
                          colHeadAlign: "center", // 헤드의 기본 정렬 값 ( colHeadAlign 을 지정하면 colGroup 에서 정의한 정렬이 무시되고 colHeadAlign : false 이거나 없으면 colGroup 에서 정의한 속성이 적용됩니다.
                          body: {
                              onclick: function(){
                                  //toast.push(Object.toJSON({index:this.index, r:this.r, c:this.c, item:this.item}));
                              },
                              addClass: function(){
                              },
                              onchangeScroll: function(){
                              }
                          }
                          ,
                          page: {
                              paging: false
                          },
                          onkeyup: function(event, element){
                              //this.list
                              //this.item
                              //this.element
                          },
                      }
              );

              myGrid3.setList(lst3);

          },
          
          getList: function () {
              return this.target.getList();
          },
          getSelectedItem: function () {
              console.log(this.target.getSelectedItem());
              //toast.push('콘솔창에 데이터를 출력하였습니다.');
          }
          ,
          append: function (map) {
              this.target.pushList(
                      {
                          no: this.target.list.length,
                          grpId		:	map.grpId , 	
                          userId	:	map.userId , 	
                          userNm	:	map.userNm , 	
                          ofcNm		:	map.ofcNm , 	
                          deptNm	:	map.deptNm , 
                      }
              );
              this.target.setFocus(this.target.list.length - 1);
          }
          ,
          remove: function () {
              var checkedList = myGrid3.getCheckedListWithIndex(0);// colSeq
              if (checkedList.length == 0) {
                  alert("선택된 목록이 없습니다. 삭제하시려는 목록을 체크하세요");
                  return;
              }
              this.target.removeListIndex(checkedList);
              // 전달한 개체와 비교하여 일치하는 대상을 제거 합니다. 이때 고유한 값이 아닌 항목을 전달 할 때에는 에러가 발생 할 수 있습니다.
          }, deleteRow: function(objData){
          	debugger;
        	this.target.removeList([objData]);
        }
      }
  };
