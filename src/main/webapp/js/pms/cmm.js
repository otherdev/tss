/*****************************************************************************************/
// 전역변수 처리 관련
/*****************************************************************************************/
/**
 * 세션정보
 */

var sessionInfo = {};
























/*****************************************************************************************/
// 공통 기능 관련
/*****************************************************************************************/

Date.prototype.format = function(f) {
	if (!this.valueOf()) return " ";

	var weekName = ["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa"];
	var d = this;

	return f.replace(/(yyyy|yy|MM|dd|E|hh|mm|ss|a\/p)/gi, function($1) {
		switch ($1) {
			case "yyyy": return d.getFullYear();
			case "yy": return (d.getFullYear() % 1000).zf(2);
			case "MM": return (d.getMonth() + 1).zf(2);
			case "dd": return d.getDate().zf(2);
			case "E": return weekName[d.getDay()];
			case "HH": return d.getHours().zf(2);
			case "hh": return ((h = d.getHours() % 12) ? h : 12).zf(2);
			case "mm": return d.getMinutes().zf(2);
			case "ss": return d.getSeconds().zf(2);
			case "a/p": return d.getHours() < 12 ? "am" : "pm";
			default: return $1;
		}
	});
};



if (!String.prototype.startsWith) {
	String.prototype.startsWith = function(searchString, position) {
		position = position || 0;
		return this.indexOf(searchString, position) === position;
	};
}

if (!String.prototype.endsWith) {
	String.prototype.endsWith = function(searchString, position) {
		var subjectString = this.toString();
		if (typeof position !== 'number' || !isFinite(position) || Math.floor(position) !== position || position > subjectString.length) {
			position = subjectString.length;
		}
		position -= searchString.length;
		var lastIndex = subjectString.indexOf(searchString, position);
		return lastIndex !== -1 && lastIndex === position;
	};
}

String.prototype.string = function(len){var s = '', i = 0; while (i++ < len) { s += this; } return s;};
String.prototype.zf = function(len){return "0".string(len - this.length) + this;};
Number.prototype.zf = function(len){return this.toString().zf(len);};






/**
 * 공통 - 함수 존재여부 
 * @param 함수ID(String)
 * @return	true/false
 */
function gfn_isFunction(func){
	
	if(gfn_isNull(func)){
		return false;	
	}
	
	if(typeof window[func] == "function"){
		//함수존재
		return true;
	}
	
	return false;
	
}

/**
 * 그리드 데이터를 json array 로 포맷팅
 */
var gfn_itemToJson = function(item_data){
	
	var list = [];
	try{
		$.each(item_data,function(idx, data){
			var row = data.item;
			row.rowIndex = data.rowIndex; 
			list.push(row);
		});
	}catch(e){
		list = item_data;
	}
	
	return list;
};

/**
 * JSON 데이터 상태플래그 세팅 
 */
var gfn_toStatusList = function(item_data, status){
	
	if(status != "I" && status != "U" && status != "D"){
		return item_data;
	}	

	$.each(item_data,function(idx, data){
		data.STAT = status;
	});
	
	return item_data;
};







/**
 * 현재날짜 형식으로 표시
 * @param f
 * @returns
 */
function gfn_today(f) {
	return new Date().format(f);
}



/**
 * 날짜에 구분자 형식으로 변환
 * @param date_str
 * @param gubun
 * @returns
 */
function gfn_toDate(date_str, gubun)
{
	if(gfn_isNull(gubun))	gubun = "-";
	
    var yyyyMMdd = String(date_str);
    var sYear = yyyyMMdd.substring(0,4);
    var sMonth = yyyyMMdd.substring(4,6);
    var sDate = yyyyMMdd.substring(6,8);

    return sYear + gubun + sMonth + gubun + sDate;
}


/**
 * 숫자여부 체크
 */
var gfn_isNum = function(s){
	//숫자여부
	var regNum = /^[0-9]*$/;//숫자 정규식
	return regNum.test(s);
};

/**
 * 전화번호 형식 체크
 */
var gfn_isTel = function(s){
	var regExp = /^\d{3}-\d{3,4}-\d{4}$/;
	var regExp2 = /^\d{2,3}-\d{3,4}-\d{4}$/;	
	
	if(regExp.test(s) || regExp2.test(s) ){
		return true;
	}
	
	return false;
};



/**
 * json배열엥서 특정컬럼값이 특정값인 map을 리턴
 * param : colNm - 컬럼명
 * param : value - 특정값
 */
var gfn_findData = function(data, param){
	var result = null;
	
	$.each(data, function(idx, map){
		if(map[param.colNm] == param.value){
			result = map;
			return false;
		}
	});
	
	return result;
};



/**
 * json array 컬럼 특정값으로 세팅
 * list - 대상 josn배열
 * param - {컬럼:변경할값}
 */
var gfn_chgColValue = function(list, param){

	$.each(param, function(key, value){
		
		$.each(list, function(idx, map){
			$.each(map, function(k, v){
				if(k == key){
					map[key] = value;
				}
			});
		});
	});
	
	return list;
};






/**
 * 공통 alert
 */
var ___bAlertDupChk = false;
function gfn_alert(sMsg, type, callback){	
	
	var bReturn = false;
	var title = "알림";
	
	//중복방지
	if(___bAlertDupChk) return;
	___bAlertDupChk = true;
	
	if(typeof type === "function"){
		callback = type;
		type = "";
		title = "알림";
	}
	else if(gfn_isNull(type)){
		type = "";
		title = "알림";
	}else if(type.toUpperCase() == "W"){
		type = "Warning";
		title = "경고";
	}else if(type.toUpperCase() == "E"){
		type = "Caution";
		title = "에러";
	}
	
	//"<b>Alert</b>\n Application Call dialog push"
	if(gfn_isNull(sMsg)) sMsg = "";
	
	dialog.push({
		type : type
		,title : title
		,body : sMsg
		,buttons : [
		             {buttonValue : "확인"
		             ,buttonClass : "" //"Blue"
		             ,onClick : function (){
		            	 ___bAlertDupChk = false;
		            	 bReturn = true;
		            	 
		            	if(typeof callback === "function"){
			     				callback(bReturn);
		     			}
		             }}
	            ]
	});
	
	return bReturn;
}

/**
 * 공통 confirm
 */
var ___bConfirmDupChk = false;
function gfn_confirm(sMsg,callback){	
	
	var bReturn = false;
	var title = "확인";
	var type = "";
	
	//중복방지
	if(___bConfirmDupChk) return;	
	___bConfirmDupChk = true;
				
	//"<b>Alert</b>\n Application Call dialog push"
	if(gfn_isNull(sMsg)) sMsg = "";
	
	dialog.push({
		type : type
		,title : title
		,body : sMsg
		,buttons : [
		             {buttonValue : "Yes"
		             ,buttonClass : "Blue"
		             ,onClick : function (){
		            	 ___bConfirmDupChk = false;
		            	 bReturn = true;
		            	 
		            	 if(typeof callback === "function"){
			     				callback(bReturn);
			     		}
		             }},
		             {buttonValue : "No"
		            	 ,buttonClass : "" //"Blue"
			             ,onClick : function (){
			            	 ___bConfirmDupChk = false;
			            	 bReturn = false;
			            	 
			            	 if(typeof callback === "function"){
				     				callback(bReturn);
				     		}
			          }}
	            ]
	});
	
	return bReturn;
}


/**
 * 토스트 알람처리
 */
var gfn_toast = function(_body, _type){
	var param = {};
	param.body = gfn_isNull(_body)? "" : _body;
	param.type = _type == "W" ? "Warning" : (_type == "E" ? "Caution" : null) ;
	
	toast.push(param);
};



function gfn_isNull(str) {
	if (str == null)
		return true;
	if (str == "NaN")
		return true;
	if (new String(str).valueOf().replace(/-/g,"").search("undef") > -1)
		return true;
	var chkStr = new String(str);
	if (chkStr.valueOf().replace(/-/g,"").search("undef") > -1)
		return true;
	if (chkStr == null)
		return true;
	if (chkStr.toString().length == 0)
		return true;
	return false;
}


function gfn_fireKey(e) {
    if(e.keyCode==13 && e.srcElement.type != 'textarea')
    return false;
}
























