/**
 * Convert a single file-input element into a 'multiple' input list
 * Usage:
 *
 *   1. Create a file input element (no name)
 *      eg. <input type="file" id="first_file_element">
 *
 *   2. Create a DIV for the output to be written to
 *      eg. <div id="files_list"></div>
 *
 *   3. Instantiate a MultiSelector object, passing in the DIV and an (optional) maximum number of files
 *      eg. var multi_selector = new MultiSelector( document.getElementById( 'files_list' ), 3 );
 *
 *   4. Add the first element
 *      eg. multi_selector.addElement( document.getElementById( 'first_file_element' ) );
 */

function MultiSelector( list_target, max , file_label ){

	// Where to write the list
	this.list_target = list_target;
	// How many elements?
	this.count = 0;
	// How many elements?
	this.id = 0;

	this.update_count = 0;
	// Is there a maximum?
	if( max ){
		this.max = max;
	} else {
		this.max = -1;
	};
	this.file_label = file_label
	this.current_count = 0;

	/**
	 * Add a new file input element
	 */
	_base = this;
	this.addElement = function( element ){

		// Make sure it's a file input element
		if( element.tagName == 'INPUT' && element.type == 'file' ){
			debugger;
			
			// Element name -- what number am I?
			element.name = 'file_' + this.id;
			element.id =  'egovfile_' + this.id;
			element.class = "";

			document.getElementById(this.file_label).setAttribute("for","egovfile_"+this.id);

			this.id++;

			// Add reference to this object
			element.multi_selector = this;

			// What to do when a file is selected
			element.onchange = function(){
				var sErrMsg = "첨부파일 갯수는 ["+_base.max+"]이상 첨부할 수 없습니다!";
				if( _base.update_count > 0 ){
					if( _base.count > (_base.max-_base.update_count) ){
						element.value = "";
						alert(sErrMsg);	return;
					}
				}
				
				if( _base.max > 0  && _base.count > _base.max ){
					element.value = "";
					alert(sErrMsg); return;
				}
				var aFiles = element.files;
				var objFile = this;
				$.each(aFiles,function(i,e){
					// New file input
					var new_element = document.createElement( 'input' );
					new_element.type = 'file';
					new_element.multiple = 'multiple';
					new_element = e;
					// Hide this: we can't use display:none because Safari doesn't like it
					new_element.style.position = 'absolute';
					new_element.style.left = '-1000px';
					new_element.style.top = '-1000px';
					new_element.style.display = 'none';
					new_element.style.visibility = 'hidden';
					new_element.style.width = '0';
					new_element.style.height = '0';
					new_element.style.overflow = 'hidden';
	
					new_element.onkeypress = function(){
						return false;
					};
	
					// Add new element
					objFile.parentNode.insertBefore( new_element, objFile );
	
					// Apply 'update' to element
					objFile.multi_selector.addElement( new_element );
	
					// Update list
					objFile.multi_selector.addListRow( e );
				});		
			};
			// If we've reached maximum number, disable input element
			if( this.max != -1 && this.count >= this.max ){
				element.disabled = true;
			};

			// File element counter
			this.count++;
			// Most recent element
			this.current_element = element;
			
		} else {
			// This can only be applied to file input elements!
			alert( 'Error: not a file input element' );
		};

	};


	/**
	 * Add a new row to the list of files
	 */
	this.addListRow = function( element ){
			
		// Row div
		var new_row = document.createElement( 'div' );
		new_row.className = 'fileApend';
		

		var new_row_span = document.createElement( 'span' );
		new_row_span.className = "cursor";
		
		var new_row_img = document.createElement( 'img' );
		new_row_img.src = '/tts/images/btn/icon_file_delete.png';
		new_row_img.className = "cursor";
		new_row_img.style.margin = "0px 0px 2px 0px"; 
		// new_row_img.style.padding = "0px 0px 0px 0px";
		
		// References
		new_row.element = element;

		// Delete function
		new_row_img.onclick= function(){

			// Remove element from form
			this.parentNode.element.parentNode.removeChild( this.parentNode.element );

			// Remove this row from the list
			this.parentNode.parentNode.removeChild( this.parentNode );

			// Decrement counter
			this.parentNode.element.multi_selector.count--;

			// Re-enable input element (if it's disabled)
			this.parentNode.element.multi_selector.current_element.disabled = false;

			//    which nixes your already queued uploads
			return false;
		};

		// Set row value
		new_row.innerHTML =  element.name; // element.value;
		
		// Add span
		new_row.appendChild( new_row_span );
		
		// Add image
		new_row.appendChild( new_row_img );

		// Add it to the list
		this.list_target.appendChild( new_row );
	};
};