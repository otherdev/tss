package egovframework.pms.adm.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.json.simple.JSONObject;
import org.springframework.stereotype.Service;

import egovframework.com.cmm.SessionUtil;
import egovframework.pms.adm.service.AdmService;
import egovframework.pms.cmm.service.CmmService;
import egovframework.pms.cmm.service.impl.CmmDAO;
import egovframework.rte.fdl.cmmn.EgovAbstractServiceImpl;

/**
 * 사용자관리에 관한 비지니스 클래스를 정의한다.
 * @author 공통서비스 개발팀 
 * @version 1.0
 * @see
 *
 */
@Service("admService")
public class AdmServiceImpl extends EgovAbstractServiceImpl implements AdmService {

	/** cmmService */
	@Resource(name = "cmmService")
	private CmmService cmmService;
	
	

	/** cmmDAO */
	@Resource(name="cmmDAO")
	private CmmDAO cmmDAO;



	
	@Override
	public String saveMstCdList(Map<String,Object> param) throws Exception {
		
		

		
		// 1.공정스케줄  저장
		String sqlId = (String)param.get("sqlId");
		List<Map<String, Object>> lst = (List<Map<String, Object>>) param.get("lst");
		
		int cnt = 0;
		for (Map<String, Object> map : lst) {
			map.put("id", SessionUtil.id);
			try{
				int r = cmmDAO.update(sqlId, map);
				if(r>0)	cnt++;
				
			}catch(Exception e){
				throw e;
			}
		}
		
		

		
		JSONObject result = new JSONObject();
		result.put("result", true);
		result.put("cnt", cnt);
		return result.toJSONString();
	}
	
	


}