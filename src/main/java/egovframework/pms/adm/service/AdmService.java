package egovframework.pms.adm.service;
import java.util.Map;

/**
 * 설정관리에 관한 인터페이스클래스를 정의한다.
 * @author 공통서비스 개발팀 
 * @since 2017.04.10
 * @version 1.0
 * @see
 *
 *
 * </pre>
 */
public interface AdmService  {

	

	String saveMstCdList(Map<String,Object> param)throws Exception;



}