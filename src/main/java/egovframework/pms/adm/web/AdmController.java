package egovframework.pms.adm.web;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springmodules.validation.commons.DefaultBeanValidator;

import egovframework.com.cmm.EgovMessageSource;
import egovframework.let.utl.sim.service.EgovFileScrty;
import egovframework.pms.adm.service.AdmModVO;
import egovframework.pms.adm.service.AdmVO;
import egovframework.pms.cmm.service.CmmService;
import egovframework.pms.cmm.web.CommandMap;
import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.fdl.security.userdetails.util.EgovUserDetailsHelper;
import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;

/**
 * 사업관리 
 * @author 공통서비스 개발팀 
 * @see
 *
 */
@Controller
public class AdmController {

	/** cmmService */
	@Resource(name = "cmmService")
	private CmmService cmmService;

	/** EgovMessageSource */
	@Resource(name = "egovMessageSource")
	EgovMessageSource egovMessageSource;

	/** EgovPropertyService */
	@Resource(name = "propertiesService")
	protected EgovPropertyService propertiesService;

	/** DefaultBeanValidator beanValidator */
	@Autowired
	private DefaultBeanValidator beanValidator;	
	
	/**
	 * 작업목록을 조회한다. (pageing)
	 * @param bizVO 검색조건정보
	 * @param model 화면모델
	 * @throws Exception
	 */
	@RequestMapping(value = "/pms/adm/codeMngList.do")
	public String codeMngList(HttpSession session, ModelMap model, @RequestParam(value = "baseMenuNo", required = false) String baseMenuNo 
			) throws Exception {

		// 미인증 작업에 대한 보안처리
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
    	if(!isAuthenticated) {
    		model.addAttribute("message", egovMessageSource.getMessage("fail.common.login"));
        	return "uat/uia/EgovLoginUsr";
    	}




		// 선택된 메뉴정보를 세션으로 등록한다.
		if (baseMenuNo != null && !baseMenuNo.equals("") && !baseMenuNo.equals("null")) { 
			session.setAttribute("baseMenuNo", baseMenuNo);
		}

		return "pms/adm/codeMngList";
	}
	
	
	
	/**
	 * 작업정보 수정을 위해 작업정보를 상세조회한다.
	 * @param bizVO 검색조건
	 * @param model 화면모델
	 * @throws Exception
	 */
	@RequestMapping("/pms/adm/codeUpdt.do")
	public String codeUpdt(
				@ModelAttribute("searchVO") AdmVO admVO, ModelMap model) throws Exception {

		// 미인증 작업에 대한 보안처리
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
    	if(!isAuthenticated) {
    		model.addAttribute("message", egovMessageSource.getMessage("fail.common.login"));
        	return "uat/uia/EgovLoginUsr";
    	}


		return "pms/adm/codeUpdt";
	}
		
	
	
	
	
	
	/**
	 * 로그정보목록을 조회한다. (pageing)
	 * @param admVO 검색조건정보
	 * @param model 화면모델
	 * @throws Exception
	 */
	@RequestMapping(value = "/pms/adm/usrLogList.do")
	public String usrLogList(@ModelAttribute("admVO") AdmVO admVO, ModelMap model
			) throws Exception {

		// 미인증 작업에 대한 보안처리
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
    	if(!isAuthenticated) {
    		model.addAttribute("message", egovMessageSource.getMessage("fail.common.login"));
        	return "uat/uia/EgovLoginUsr";
    	}

    	Map<String, Object> map = new HashMap<String, Object>();
		/** EgovPropertyService */
    	admVO.setPageUnit(propertiesService.getInt("pageUnit"));
    	admVO.setPageSize(propertiesService.getInt("pageSize"));
    	map.put("pageUnit", propertiesService.getInt("pageUnit"));
    	map.put("pageSize", propertiesService.getInt("pageSize"));

		/** pageing */
		PaginationInfo paginationInfo = new PaginationInfo();
		paginationInfo.setCurrentPageNo(admVO.getPageIndex());
		paginationInfo.setRecordCountPerPage(admVO.getPageUnit());
		paginationInfo.setPageSize(admVO.getPageSize());

    	map.put("firstIndex", paginationInfo.getFirstRecordIndex());
    	map.put("lastIndex", paginationInfo.getLastRecordIndex());
    	map.put("recordCountPerPage", paginationInfo.getRecordCountPerPage());
    	map.put("searchCondition", admVO.getSearchCondition());
    	map.put("searchKeyword", admVO.getSearchKeyword());
    	
    	map.put("startDt", admVO.getStartDt());
    	map.put("endDt", admVO.getEndDt());

    	// 조회
		List<Map<String, Object>> list = (List<Map<String, Object>>) cmmService.selectList("selectLogList", map);
		model.addAttribute("resultList", list);

		// 카운트
		List<Map<String, Object>> listCnt = (List<Map<String, Object>>) cmmService.selectList("selectLogListCnt", map);
		int totCnt = 0;
		try{
			totCnt = ((BigDecimal) listCnt.get(0).get("totCnt")).intValue();
		}catch(Exception e){
			System.out.println("..." + e.getMessage());
		};
		
		
		paginationInfo.setTotalRecordCount(totCnt);
		model.addAttribute("paginationInfo", paginationInfo);
		model.addAttribute("admVO", admVO);



		return "pms/adm/usrLogList";
	}
			
	
	
	/**
	 * 사용자목록을 조회한다. (pageing)
	 * @param admVO 검색조건정보
	 * @param model 화면모델
	 * @throws Exception
	 */
	@RequestMapping(value = "/pms/adm/usrMngList.do")
	public String usrMngList(@ModelAttribute("admVO") AdmVO admVO, ModelMap model
			) throws Exception {

		// 미인증 작업에 대한 보안처리
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
    	if(!isAuthenticated) {
    		model.addAttribute("message", egovMessageSource.getMessage("fail.common.login"));
        	return "uat/uia/EgovLoginUsr";
    	}

    	Map<String, Object> map = new HashMap<String, Object>();
		/** EgovPropertyService */
    	admVO.setPageUnit(propertiesService.getInt("pageUnit"));
    	admVO.setPageSize(propertiesService.getInt("pageSize"));
    	map.put("pageUnit", propertiesService.getInt("pageUnit"));
    	map.put("pageSize", propertiesService.getInt("pageSize"));

		/** pageing */
		PaginationInfo paginationInfo = new PaginationInfo();
		paginationInfo.setCurrentPageNo(admVO.getPageIndex());
		paginationInfo.setRecordCountPerPage(admVO.getPageUnit());
		paginationInfo.setPageSize(admVO.getPageSize());

    	map.put("firstIndex", paginationInfo.getFirstRecordIndex());
    	map.put("lastIndex", paginationInfo.getLastRecordIndex());
    	map.put("recordCountPerPage", paginationInfo.getRecordCountPerPage());
    	map.put("searchCondition", admVO.getSearchCondition());
    	map.put("searchKeyword", admVO.getSearchKeyword());
    	

    	// 조회
		List<Map<String, Object>> list = (List<Map<String, Object>>) cmmService.selectList("selectUsrList", map);
		model.addAttribute("resultList", list);

		// 카운트
		List<Map<String, Object>> listCnt = (List<Map<String, Object>>) cmmService.selectList("selectUsrListCnt", map);
		int totCnt = 0;
		try{
			totCnt = ((BigDecimal) listCnt.get(0).get("totCnt")).intValue();
		}catch(Exception e){
			System.out.println("..." + e.getMessage());
		};
		
		
		paginationInfo.setTotalRecordCount(totCnt);
		model.addAttribute("paginationInfo", paginationInfo);
		model.addAttribute("admVO", admVO);



		return "pms/adm/usrMngList";
	}
			
	
		
	/**
	 * 작업정보 수정을 위해 작업정보를 상세조회한다.
	 * @param bizVO 검색조건
	 * @param model 화면모델
	 * @throws Exception
	 */
	@RequestMapping("/pms/adm/usrUpdt.do")
	public String usrUpdt(
				@ModelAttribute("searchVO") AdmModVO admModVO, ModelMap model) throws Exception {

		String isAdmYn = "N"; //관리자여부
		
		// 미인증 작업에 대한 보안처리
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
    	if(!isAuthenticated) {
    		model.addAttribute("message", egovMessageSource.getMessage("fail.common.login"));
        	return "uat/uia/EgovLoginUsr";
    	}

    	model.addAttribute("mode", admModVO.getMode()); //수정모드

    	if("MOD".equals(admModVO.getMode())){
    		Map<String, Object> map = new HashMap<String, Object>();
    		
    		// 공정변경마스터조회
    		map.put("userId", admModVO.getUserId());
    		List<Map<String, Object>> lst = (List<Map<String, Object>>) cmmService.selectList("selectUserDtl", map);
    		model.addAttribute("usrDtl", lst.get(0));
    		
    		//isAdmYn = cmmService.isBizMngYn(map);
    		isAdmYn = "Y";
    	}
    	
    	//해당사업관리자 여부
    	model.addAttribute("isAdmYn", isAdmYn);

		return "pms/adm/usrUpdt";
	}
		
	
	
	/**
	 * 사용자 폼데이터 저장
	 * @param commandMap
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/pms/adm/saveUser.do")
	public ModelAndView saveChgMst(CommandMap commandMap, HttpServletRequest request) throws Exception {
		ModelAndView mv = new ModelAndView("/proc/proc");

		// 0.비밀번호 암호화
		String mode = "MOD";
		String CHG_PWD = "N";
		Map<String,Object> data = commandMap.getMap();
		mode = data.get("mode").toString();
		CHG_PWD = data.get("CHG_PWD").toString();

		if("ADD".equals(mode) ){
			String enpassword = EgovFileScrty.encryptPassword(data.get("password").toString(), data.get("userId").toString());
			data.put("password", enpassword);
		}
		else{
			if("Y".equals(CHG_PWD)){ //수정모드에서 비번변경인경우
				String enpassword = EgovFileScrty.encryptPassword(data.get("password").toString(), data.get("userId").toString());
				data.put("password", enpassword);
			}
		}
		
		// 1.사용자정보 저장
		Map<String,Object> param = new HashMap<String, Object>();
		param.put("sqlId", "updateUser");
		param.put("data", data);

		String result = cmmService.saveData(param);
		mv.addObject("result", result);

		return mv;
	}

		
	/**
	 * 코드마스터 저장처리
	 * @param param
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/pms/adm/saveMstCdList.do")
	@ResponseBody
	public ModelAndView saveMstCdList(@RequestBody Map<String,Object> param) throws Exception {
		ModelAndView mv = new ModelAndView("/proc/proc");

		// 미인증 사업에 대한 보안처리
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
    	if(!isAuthenticated) {
    		JSONObject result = new JSONObject();
    		result.put("result", false);
    		mv.addObject("result", result.toJSONString());
    		return mv;
    	}
		

		// 공정리스트, 팀원리스트  상세를 먼저저장
		String result = cmmService.saveCmmList(param);
		
		mv.addObject("result", result);
		return mv;
	}
		
	/**
	 * 코드상세 저장처리
	 * @param param
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/pms/adm/saveDtlCdList.do")
	@ResponseBody
	public ModelAndView saveDtlCdList(@RequestBody Map<String,Object> param) throws Exception {
		ModelAndView mv = new ModelAndView("/proc/proc");
		
		// 미인증에 대한 보안처리
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
		if(!isAuthenticated) {
			JSONObject result = new JSONObject();
			result.put("result", false);
			mv.addObject("result", result.toJSONString());
			return mv;
		}
		
		
		// 공정리스트, 팀원리스트  상세를 먼저저장
		String result = cmmService.saveCmmList(param);
		
		mv.addObject("result", result);
		return mv;
	}
	
	
}
