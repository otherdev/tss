package egovframework.pms.biz.web;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springmodules.validation.commons.DefaultBeanValidator;

import egovframework.com.cmm.EgovMessageSource;
import egovframework.pms.biz.service.LecService;
import egovframework.pms.cmm.service.CmmService;
import egovframework.pms.cmm.service.ComDtlVO;
import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.fdl.security.userdetails.util.EgovUserDetailsHelper;

/**
 * 사업관리 
 * @author 공통서비스 개발팀 
 * @see
 *
 */
@Controller
public class LecController {


	/** cmmService */
	@Resource(name = "cmmService")
	private CmmService cmmService;

	@Resource(name = "lecService")
	private LecService lecService;
	
	/** EgovMessageSource */
	@Resource(name = "egovMessageSource")
	EgovMessageSource egovMessageSource;

	/** EgovPropertyService */
	@Resource(name = "propertiesService")
	protected EgovPropertyService propertiesService;

	/** DefaultBeanValidator beanValidator */
	@Autowired
	private DefaultBeanValidator beanValidator;

	
	
	/**
	 * 사업정보 수정을 위해 사업정보를 상세조회한다.
	 * @param bizVO 검색조건
	 * @param model 화면모델
	 * @throws Exception
	 */
	@RequestMapping("/pms/lec/audit.do")
	public String bizUpdt(
				HttpSession session
				, @RequestParam(value = "sysCd", required = false) String sysCd
				, @RequestParam(value = "baseMenuNo", required = false) String baseMenuNo
				, ModelMap model) throws Exception {

		// 미인증 사업에 대한 보안처리
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
    	if(!isAuthenticated) {
    		model.addAttribute("message", egovMessageSource.getMessage("fail.common.login"));
        	return "uat/uia/EgovLoginUsr";
    	}

		
		ComDtlVO vo = new ComDtlVO();
		//공통코드 : 교육시스템구분
		vo.setMstCd("SYCD");
		List<ComDtlVO> lst = cmmService.selectCmmCodeDetail(vo); 
		model.addAttribute("SYCD_result", lst);

		
		//시스템동영상목록
    	Map<String, Object> map = new HashMap<String, Object>();
		if( (sysCd == null || "".equals(sysCd))){
			if(lst != null){
				map.put("sysCd", lst.get(0).getDtlCd()); //선택한 분류가 없으면  첫 분류 
				model.addAttribute("sysCd", lst.get(0).getDtlCd());
			}
			else{
				map.put("sysCd", ""); //첫 분류가 없으면 전체 
				model.addAttribute("sysCd", "");
			}
		}
		else{
			map.put("sysCd", sysCd); 
			model.addAttribute("sysCd", sysCd);
		}
		List<?> lst2 = cmmService.selectList("selectAuditList", map);
		model.addAttribute("resultList", lst2);
		

		return "pms/lec/audit";
	}
	
	
	
	
	/**
	 * 강의시작 처리
	 * @param param
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/pms/insertAudit.do")
	@ResponseBody
	public ModelAndView insertAudit(@RequestBody Map<String,Object> param) throws Exception {
		ModelAndView mv = new ModelAndView("/proc/proc");


		// 공정리스트, 팀원리스트  상세를 먼저저장
		String result = lecService.insertAudit(param);
		
		mv.addObject("result", result);
		return mv;
	}
	
	
	

	
	
	
}
