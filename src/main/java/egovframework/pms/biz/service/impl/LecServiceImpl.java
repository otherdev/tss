package egovframework.pms.biz.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.json.simple.JSONObject;
import org.springframework.stereotype.Service;

import egovframework.com.cmm.SessionUtil;
import egovframework.pms.biz.service.LecService;
import egovframework.pms.cmm.service.CmmService;
import egovframework.pms.cmm.service.impl.CmmDAO;
import egovframework.rte.fdl.cmmn.EgovAbstractServiceImpl;

/**
 * 강의에 관한 비지니스 클래스를 정의한다.
 * @author 공통서비스 개발팀 
 * @version 1.0
 * @see
 *
 */
@Service("lecService")
public class LecServiceImpl extends EgovAbstractServiceImpl implements LecService {

	@Resource(name="cmmDAO")
	private CmmDAO cmmDAO;

	@Resource(name="cmmService")
	private CmmService cmmService;


	
	
	/**
	 * 강의시작 신규등록
	 * @param param
	 * @return
	 * @throws Exception
	 */
	@Override
	public String insertAudit(Map<String,Object> param) throws Exception {
		
		// 0.채번
		Map<String, Object> data = (Map<String, Object>) param.get("data");
		List<Map<String,Object>> lst = (List<Map<String, Object>>) cmmService.selectList("selectLecSeq", data);
		
		long seq = 0;
		if(lst != null && !lst.isEmpty()){
			seq = Long.parseLong(lst.get(0).get("recordSeq").toString()) ;
		}
		
		
		// 1.단건데이터  저장
		String sqlId = (String)param.get("sqlId");
		data.put("recordSeq", seq);
		data.put("id", SessionUtil.id);
		
		int cnt = 0;
		try{
			int r = cmmDAO.update(sqlId, data);
			if(r>0)	cnt++;
			
		}catch(Exception e){
			throw e;
		}
		
		
		
		JSONObject result = new JSONObject();
		result.put("result", true);
		result.put("cnt", cnt);
		result.put("recordSeq", seq);
		return result.toJSONString();
	}	
	
	
	
	

}