package egovframework.pms.biz.service;
import java.util.Map;

/**
 * 강의관한 인터페이스클래스를 정의한다.
 * @author 공통서비스 개발팀 
 * @since 2017.04.10
 * @version 1.0
 * @see
 *
 *
 * </pre>
 */
public interface LecService  {

	String insertAudit(Map<String, Object> param) throws Exception;

	



}