package egovframework.pms.cmm.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Component;

@Component("jsonUtils")
public class JsonUtils {

	public Map<String, Object> JSONObj2Map(JSONObject obj) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		if (obj == null)
			return map;
		try {

			for (Object key : obj.keySet()) {
				String k = key.toString();
				Object v = obj.get(k);

				map.put(k, v);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return map;
	}

	@SuppressWarnings("unchecked")
	public JSONObject Map2JSONObj(Map<String, Object> map) throws Exception {

		JSONObject obj = new JSONObject();
		if (map == null)
			return obj;
		try {

			for (String key : map.keySet()) {
				String k = key.toString();
				Object v = map.get(k) == null ? "" : map.get(k);

				obj.put(k, v);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return obj;
	}

	@SuppressWarnings("unchecked")
	public JSONArray List2JSONArr(List<Map<String, Object>> list) throws Exception {

		JSONArray arr = new JSONArray();
		if (list == null)
			return arr;
		try {

			for (Map<String, Object> map : list) {

				JSONObject obj = Map2JSONObj(map);
				arr.add(obj);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return arr;
	}

	public List<Map<String, Object>> JSONArr2List(JSONArray arr) throws Exception {

		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		if (arr == null)
			return list;
		try {

			for (Object obj : arr) {

				Map<String, Object> map = JSONObj2Map((JSONObject) obj);
				list.add(map);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return list;
	}

	@SuppressWarnings("unchecked")
	public JSONArray List2JSONArr2(List<Map<String, String>> list) throws Exception {

		JSONArray arr = new JSONArray();
		if (list == null)
			return arr;
		try {

			for (Map<String, String> map : list) {

				JSONObject obj = Map2JSONObj2(map);
				arr.add(obj);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return arr;
	}

	@SuppressWarnings("unchecked")
	public JSONObject Map2JSONObj2(Map<String, String> map) throws Exception {

		JSONObject obj = new JSONObject();
		if (map == null)
			return obj;
		try {

			for (String key : map.keySet()) {
				String k = key.toString();
				Object v = map.get(k) == null ? "" : map.get(k);

				obj.put(k, v);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return obj;
	}

	public void mapPrint(Map<String, Object> map) throws Exception {

		if (map == null)
			return;
		try {

			for (String key : map.keySet()) {
				String k = key.toString();
				Object v = map.get(k) == null ? "" : map.get(k);
				System.out.println(k + " : " + v.toString());
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return;
	}

	public void mapStringPrint(Map<String, String> map) throws Exception {

		if (map == null)
			return;
		try {

			for (String key : map.keySet()) {
				String k = key.toString();
				String v = map.get(k) == null ? "" : map.get(k);
				System.out.println(k + " : " + v);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return;
	}

	
}
