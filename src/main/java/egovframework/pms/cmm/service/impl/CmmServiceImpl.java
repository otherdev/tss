package egovframework.pms.cmm.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.json.simple.JSONObject;
import org.springframework.stereotype.Service;

import egovframework.com.cmm.LoginVO;
import egovframework.com.cmm.SessionUtil;
import egovframework.pms.cmm.service.CmmService;
import egovframework.pms.cmm.service.ComDtlVO;
import egovframework.rte.fdl.cmmn.EgovAbstractServiceImpl;

/**
 * @Class Name : EgovCmmUseServiceImpl.java
 * @Description : 공통코드등 전체 업무에서 공용해서 사용해야 하는 서비스를 정의하기위한 서비스 구현 클래스
 * @Modification Information
 *
 *    수정일       수정자         수정내용
 *    -------        -------     -------------------
 *    2009. 3. 11.     이삼섭
 *
 * @author 공통 서비스 개발팀 이삼섭
 * @since 2009. 3. 11.
 * @version
 * @see
 *
 */
@Service("cmmService")
public class CmmServiceImpl extends EgovAbstractServiceImpl implements CmmService {

	@Resource(name = "cmmDAO")
	private CmmDAO cmmDAO;

	/**
	 * 공통코드를 조회한다.
	 *
	 * @param vo
	 * @return
	 * @throws Exception
	 */
	@Override
	public List<ComDtlVO> selectCmmCodeDetail(ComDtlVO vo) throws Exception {
		return cmmDAO.selectCmmCodeDetail(vo);
	}


	/**
	 * 목록을 조회
	 * @param sqlId - 쿼리아이디, map - 파라미터 정보
	 * 
	 */
	public List<?> selectList(String sqlId, Map<String, Object> map) throws Exception {
		return cmmDAO.selectList(sqlId, map);
	}
	
	
	/**
	 * ajax 단건데이터 저정 공통처리
	 * @param param
	 * @return
	 * @throws Exception
	 */
	@Override
	public String saveData(Map<String,Object> param) throws Exception {
		
		// 1.단건데이터  저장
		String sqlId = (String)param.get("sqlId");
		Map<String, Object> data = (Map<String, Object>) param.get("data");
		data.put("id", SessionUtil.id);
		
		int cnt = 0;
		try{
			int r = cmmDAO.update(sqlId, data);
			if(r>0)	cnt++;
			
		}catch(Exception e){
			throw e;
		}
		
		// 2 쿼리있으면수행
		if( param.get("sqlId2")!= null && !"".equals(param.get("sqlId2"))){
			String sqlId2 = (String)param.get("sqlId");
			Map<String, Object> data2 = (Map<String, Object>) param.get("data2");
			data2.put("id", SessionUtil.id);

			try{
				int r = cmmDAO.update(sqlId2, data2);
				if(r>0)	cnt++;
				
			}catch(Exception e){
				throw e;
			}
		}
		
		
		
		JSONObject result = new JSONObject();
		result.put("result", true);
		result.put("cnt", cnt);
		return result.toJSONString();
	}	
	
	
	/**
	 * ajax 리스트 데이터 저정 공통처리
	 */
	@Override
	public String saveCmmList(Map<String,Object> param) throws Exception {
		
		
		// 1.리스트  저장
		String sqlId = (String)param.get("sqlId");
		List<Map<String, Object>> lst = (List<Map<String, Object>>) param.get("lst");
		
		int cnt = 0;
		for (Map<String, Object> map : lst) {
			map.put("id", SessionUtil.id);
			try{
				int r = cmmDAO.update(sqlId, map);
				if(r>0)	cnt++;
				
			}catch(Exception e){
				throw e;
			}
		}
		
		
		// 2.팀원저장
		if( param.get("sqlId2")!= null && !"".equals(param.get("sqlId2"))){
			String sqlId2 = (String)param.get("sqlId2");
			List<Map<String, Object>> lst2 = (List<Map<String, Object>>) param.get("lst2");
			
			for (Map<String, Object> map : lst2) {
				map.put("id", SessionUtil.id);
				try{
					int r = cmmDAO.update(sqlId2, map);
					if(r>0)	cnt++;
					
				}catch(Exception e){
					throw e;
				}
			}
		}
		
		
		JSONObject result = new JSONObject();
		result.put("result", true);
		result.put("cnt", cnt);
		return result.toJSONString();
	}
	
	
	
	@Override
	public void insertLoginLog(LoginVO vo) throws Exception {
		
	}
	@Override
	public void insertLogoutLog() throws Exception {
		
	}
	
	
	
	
	
	
	
	
		
}
