package egovframework.pms.cmm.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import egovframework.com.cmm.service.impl.EgovComAbstractDAO;
import egovframework.pms.cmm.service.ComDtlVO;

/**
 * @Class Name : CmmUseDAO.java
 * @Description : 공통코드등 전체 업무에서 공용해서 사용해야 하는 서비스를 정의하기위한 데이터 접근 클래스
 * @Modification Information
 *
 *    수정일       수정자         수정내용
 *    -------        -------     -------------------
 *    2009. 3. 11.     
 *
 * @author 공통 서비스 개발팀 이삼섭
 * @since 2009. 3. 11.
 * @version
 * @see
 *
 */
@Repository("cmmDAO")
public class CmmDAO extends EgovComAbstractDAO {

	/**
	 * 주어진 조건에 따른 공통코드를 불러온다.
	 *
	 * @param vo
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public List<ComDtlVO> selectCmmCodeDetail(ComDtlVO vo) throws Exception {
		return (List<ComDtlVO>) list("selectCmmCodeDetail", vo);
	}

	
	/**
	 * 목록을 조회
	 * @return list
	 * @exception Exception
	 */
	public List<Map<String, Object>> selectList(String sqlId, Map<String, Object> map) throws Exception{
		return (List<Map<String, Object>>) list(sqlId, map);
	}
	

}
