package egovframework.pms.cmm.service;

import java.util.List;
import java.util.Map;

import egovframework.com.cmm.LoginVO;



/**
 *
 * 공통코드등 전체 업무에서 공용해서 사용해야 하는 서비스를 정의하기 위한 서비스 인터페이스
 * @author 공통서비스 개발팀 이삼섭
 * @since 2017.04.01
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일      수정자           수정내용
 *  -------    --------    ---------------------------
 *   2017.03.11   	최초 생성
 *
 * </pre>
 */
public interface CmmService {

    /**
     * 공통코드를 조회한다.
     *
     * @param vo
     * @return List(코드)
     * @throws Exception
     */
    public List<ComDtlVO> selectCmmCodeDetail(ComDtlVO vo) throws Exception;

    public List<?> selectList(String sqlId, Map<String, Object> map) throws Exception;

	String saveData(Map<String, Object> param) throws Exception;

	String saveCmmList(Map<String, Object> param) throws Exception;

	void insertLoginLog(LoginVO vo) throws Exception;

	void insertLogoutLog() throws Exception;    

}
