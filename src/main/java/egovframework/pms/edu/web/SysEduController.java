package egovframework.pms.edu.web;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.json.simple.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.SessionUtil;
import egovframework.com.cmm.service.EgovFileMngService;
import egovframework.com.cmm.service.EgovFileMngUtil;
import egovframework.com.cmm.service.FileVO;
import egovframework.pms.cmm.service.CmmService;
import egovframework.pms.edu.service.EduVO;
import egovframework.pms.edu.service.SysEduService;
import egovframework.pms.edu.service.SysEduVO;
import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.fdl.security.userdetails.util.EgovUserDetailsHelper;
import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;

/**
 * 교육일정관리 
 * @author 공통서비스 개발팀 
 * @see
 *
 */
@Controller
public class SysEduController {
	
	/** sysEduService */
	@Resource(name = "sysEduService")
	private SysEduService sysEduService;

	/** cmmService */
	@Resource(name = "cmmService")
	private CmmService cmmService;

	/** EgovMessageSource */
	@Resource(name = "egovMessageSource")
	EgovMessageSource egovMessageSource;

	/** EgovPropertyService */
	@Resource(name = "propertiesService")
	protected EgovPropertyService propertiesService;
	
	// 첨부파일 관련
	@Resource(name="EgovFileMngService")
	private EgovFileMngService fileMngService;
	
	@Resource(name="EgovFileMngUtil")
	private EgovFileMngUtil fileUtil;

	
	
	/**
	 * 교육자료실 목록을 조회한다. (pageing)
	 * @param bizVO 검색조건정보
	 * @param model 화면모델
	 * @throws Exception
	 */
	@RequestMapping(value = "/pms/lec/eduProjectList.do")
	public String eduProjectList(@ModelAttribute("eduVO") EduVO eduVO, ModelMap model
    		,HttpSession session, @RequestParam(value = "baseMenuNo", required = false) String baseMenuNo 
			) throws Exception {

		// 미인증 작업에 대한 보안처리
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
    	if(!isAuthenticated) {
    		model.addAttribute("message", egovMessageSource.getMessage("fail.common.login"));
        	return "uat/uia/EgovLoginUsr";
    	}

		String isEduMngYn = SessionUtil.isAdmYn; //관리자 여부
		model.addAttribute("userId", SessionUtil.id);
		model.addAttribute("eduMngYn", isEduMngYn);
		
		eduVO.setMode("VIEW");
		eduVO.setRegId(SessionUtil.id); 
    	
		/** EgovPropertyService */
    	eduVO.setPageUnit(propertiesService.getInt("pageUnit"));
    	eduVO.setPageSize(propertiesService.getInt("pageSize"));

		/** pageing */
		PaginationInfo paginationInfo = new PaginationInfo();
		paginationInfo.setCurrentPageNo(eduVO.getPageIndex());
		paginationInfo.setRecordCountPerPage(eduVO.getPageUnit());
		paginationInfo.setPageSize(eduVO.getPageSize());

		eduVO.setFirstIndex(paginationInfo.getFirstRecordIndex());
		eduVO.setLastIndex(paginationInfo.getLastRecordIndex());
		eduVO.setRecordCountPerPage(paginationInfo.getRecordCountPerPage());

		// List<?> lst2 = cmmService.selectList("selectSysEduList", map);
		model.addAttribute("resultList", sysEduService.selectEduProjectList(eduVO));

		int totCnt = sysEduService.selectEduProjectListTotCnt(eduVO);
		paginationInfo.setTotalRecordCount(totCnt);
		model.addAttribute("paginationInfo", paginationInfo);


		// 선택된 메뉴정보를 세션으로 등록한다.
		if (baseMenuNo != null && !baseMenuNo.equals("") && !baseMenuNo.equals("null")) {
			session.setAttribute("baseMenuNo", baseMenuNo);
		}

		return "pms/lec/eduProjectList";
	}

	/**
	 * 수정을 위해 교육자료정보를 상세조회한다.
	 * @param eduVO 검색조건
	 * @param model 화면모델
	 * @throws Exception
	 */
	@RequestMapping("/pms/lec/eduProjectUp.do")
	public String eduProjectUp(
				@RequestParam("mode") String mode, @ModelAttribute("eduVO") EduVO eduVO, ModelMap model) throws Exception {

		// 미인증 작업에 대한 보안처리
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
    	if(!isAuthenticated) {
    		model.addAttribute("message", egovMessageSource.getMessage("fail.common.login"));
        	return "uat/uia/EgovLoginUsr";
    	}		
    	
    	eduVO.setRegId(SessionUtil.id);

		String isEduMngYn = SessionUtil.isAdmYn; //관리자 여부
		model.addAttribute("userId", SessionUtil.id);
		model.addAttribute("eduMngYn", isEduMngYn);
		
		
		model.addAttribute("mode", mode);
		model.addAttribute("eduSeq", eduVO.getEduSeq());
		model.addAttribute("taskSeq", eduVO.getTaskSeq());
		model.addAttribute("taskSeq", eduVO.getTaskSeq());
		model.addAttribute("pageIndex", eduVO.getPageIndex());
		model.addAttribute("regId", SessionUtil.id);
		model.addAttribute("regNm", SessionUtil.name );

    	SysEduVO sysEduVO = sysEduService.selectSysEduProjectInfo(eduVO); 
    	/*
    	if( sysEduVO == null ){
    		sysEduVO = new SysEduVO();
    		// sysEduVO.setCatCd(eduVO.getCatCd());
    		// sysEduVO.setSeq(eduVO.getSeq());
    	}
		*/
    	sysEduVO.setPageIndex(eduVO.getPageIndex());
    	sysEduVO.setPageSize(eduVO.getPageSize());
    	sysEduVO.setPageUnit(eduVO.getPageUnit());
    	sysEduVO.setSearchCondition(eduVO.getSearchCondition());
    	sysEduVO.setSearchKeyword(eduVO.getSearchKeyword());
    	sysEduVO.setMode(mode);
		model.addAttribute("sysEduVO", sysEduVO);
		

		return  "pms/lec/eduProjectUp";
	}	

	/**
	 * 교육일정을 저장처리한다. 
	 * @param bizVO 검색조건정보
	 * @param model 화면모델
	 * @throws Exception
	 */
	@RequestMapping(value = "/pms/lec/sysEduProjectReplyUpdate.do")
	@ResponseBody
	public ModelAndView sysEduProjectReplyUpdate(@RequestBody Map<String,Object> param) throws Exception {
		ModelAndView mv = new ModelAndView("/proc/proc");

		// 미인증 사업에 대한 보안처리
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
    	if(!isAuthenticated) {
    		JSONObject result = new JSONObject();
    		result.put("result", false);
    		mv.addObject("result", result.toJSONString());
    		return mv;
    	}

		String result = sysEduService.sysEduProjectReplyUpdate(param); 
		
		mv.addObject("result", result);
		return mv;
	}
	

	/**
	 * 교육일정을 저장처리한다. 
	 * @param bizVO 검색조건정보
	 * @param model 화면모델
	 * @throws Exception
	 */
	@RequestMapping(value = "/pms/lec/sysEduProjectReplyDelete.do")
	@ResponseBody
	public ModelAndView sysEduProjectReplyDelete(@RequestBody Map<String,Object> param) throws Exception {
		ModelAndView mv = new ModelAndView("/proc/proc");

		// 미인증 사업에 대한 보안처리
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
    	if(!isAuthenticated) {
    		JSONObject result = new JSONObject();
    		result.put("result", false);
    		mv.addObject("result", result.toJSONString());
    		return mv;
    	}

		String result = sysEduService.sysEduProjectReplyDelete(param); 
		
		mv.addObject("result", result);
		return mv;
	}

	/**
	 * 교육일정을 저장처리한다. 
	 * @param bizVO 검색조건정보
	 * @param model 화면모델
	 * @throws Exception
	 */
	@RequestMapping(value = "/pms/lec/sysEduProjectReplyInsert.do")
	@ResponseBody
	public ModelAndView sysEduProjectReplyInsert(@RequestBody Map<String,Object> param) throws Exception {
		ModelAndView mv = new ModelAndView("/proc/proc");

		// 미인증 사업에 대한 보안처리
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
    	if(!isAuthenticated) {
    		JSONObject result = new JSONObject();
    		result.put("result", false);
    		mv.addObject("result", result.toJSONString());
    		return mv;
    	}

		Map<String,Object> map = (Map<String,Object>) param.get("mainData");
		Map<String, MultipartFile> fileMap = (Map<String, MultipartFile>) param.get("oFile");
		// 첨부파일 관련 첨부파일ID 생성
		List<FileVO> _result = null;
		String _fileSeq = "";

		if(!fileMap.isEmpty()){
    		 // 파일전송 
    		 _result = fileUtil.parseFileInf(fileMap, "DSCH_", 0, _fileSeq, "sysEdu");
    		 if( _fileSeq == "" || _fileSeq.isEmpty() || _fileSeq == null ){
	    		 // 키 생성 및 첨부테이블 등록 처리 
	    		 _fileSeq = fileMngService.insertFileInfs(_result);  //파일이 생성되고나면 생성된 첨부파일 ID를 리턴한다.
    		 }
    		 /*
    		 else {
    			// 첨부테이블 수정 처리 
	    		fileMngService.updateFileInfs(_result);  //파일이 생성되고나면 생성된 첨부파일 ID를 리턴한다.
    		 }
    		 */
		}
		map.put("regId", SessionUtil.id);
		map.put("fileSeq", _fileSeq);
		
		
		String result = sysEduService.sysEduProjectReplyInsert(map); 
		
		mv.addObject("result", result);
		return mv;
	}

	/**
	 * 교육일정을 저장처리한다. 
	 * @param bizVO 검색조건정보
	 * @param model 화면모델
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/pms/lec/sysEduProjectReplyCreate.do")
	@ResponseBody
	public ModelAndView sysEduProjectReplyCreate(
			final MultipartHttpServletRequest multiRequest) throws Exception {
		ModelAndView mv = new ModelAndView("/proc/proc");

		// 미인증 사업에 대한 보안처리
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
    	if(!isAuthenticated) {
    		JSONObject result = new JSONObject();
    		result.put("result", false);
    		mv.addObject("result", result.toJSONString());
    		return mv;
    	}

		Map<String,Object> map = new HashMap<String,Object>(); 
		Map<String, MultipartFile> fileMap = multiRequest.getFileMap();
		
		String sEduSeq = multiRequest.getParameter("eduSeq");
		String sTaskSeq = multiRequest.getParameter("taskSeq");
		String sContents = multiRequest.getParameter("contents");
		
		map.put("eduSeq", sEduSeq);
		map.put("taskSeq", sTaskSeq);
		map.put("contents", sContents);
		
		// 첨부파일 관련 첨부파일ID 생성
		List<FileVO> _result = null;
		String _fileSeq = "";
		try{
			if(!fileMap.isEmpty()){
	    		 // 파일전송 
	    		 _result = fileUtil.parseFileInf(fileMap, "DSCH_", 0, _fileSeq, "sysEdu");
	    		 if( _fileSeq == "" || _fileSeq.isEmpty() || _fileSeq == null ){
		    		 // 키 생성 및 첨부테이블 등록 처리 
		    		 _fileSeq = fileMngService.insertFileInfs(_result);  //파일이 생성되고나면 생성된 첨부파일 ID를 리턴한다.
	    		 }
	    		 /*
	    		 else {
	    			// 첨부테이블 수정 처리 
		    		fileMngService.updateFileInfs(_result);  //파일이 생성되고나면 생성된 첨부파일 ID를 리턴한다.
	    		 }
	    		 */
			}
			if( map.get("eduSeq") != null ){
				map.put("regId", SessionUtil.id);
				map.put("fileSeq", _fileSeq);			
				
				String result = sysEduService.sysEduProjectReplyInsert(map); 
			}
			mv.addObject("result", true);
		} catch(SQLException se ) {
			mv.addObject("result", false);
		} catch(Exception e ) {
			mv.addObject("result", false);
		}
		return mv;
	}
	
	@RequestMapping("/pms/lec/eduQuizPopup.do")
	public String eduQuizPopup(@RequestParam Map<String, Object> commandMap,ModelMap model) throws Exception {

		// 미인증 작업에 대한 보안처리
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
    	if(!isAuthenticated) {
    		model.addAttribute("message", egovMessageSource.getMessage("fail.common.login"));
        	return "uat/uia/EgovLoginUsr";
    	}
		model.addAttribute("userId", SessionUtil.id);
    	EduVO eduVO = new EduVO();
		if( commandMap.get("mnuCd") != null && !"".equals((String) commandMap.get("mnuCd"))) {
			eduVO.setMnuCd( (String) commandMap.get("mnuCd") );
			eduVO.setRegId( SessionUtil.id );
			Map<String, Object> map = sysEduService.eduQuizPopup(eduVO);
			model.addAttribute("mnuCd"   , commandMap.get("mnuCd"));
			model.addAttribute("mnuNm"   , map.get("mnuNm"));
			model.addAttribute("eduYn"   , map.get("eduYn"));
			model.addAttribute("submitYn", map.get("submitYn"));
		}
		
		return  "pms/lec/eduQuizPopup";
	}	

	@RequestMapping("/pms/lec/eduQuizSubmit.do")
	public ModelAndView eduQuizSubmit(@RequestBody Map<String,Object> param) throws Exception {
		ModelAndView mv = new ModelAndView("/proc/proc");

		// 미인증 작업에 대한 보안처리
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
    	if(!isAuthenticated) {
    		JSONObject result = new JSONObject();
    		result.put("result", false);
    		mv.addObject("result", result.toJSONString());
    		return mv;
    	}

		String result = sysEduService.eduQuizSubmit(param); 

		mv.addObject("result", result);
		return mv;
	}	
}
