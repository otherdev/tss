package egovframework.pms.edu.web;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.json.simple.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.SessionUtil;
import egovframework.pms.cmm.service.CmmService;
import egovframework.pms.cmm.service.ComDtlVO;
import egovframework.pms.cmm.util.JsonUtils;
import egovframework.pms.edu.service.EduSchdulModVO;
import egovframework.pms.edu.service.EduSchdulService;
import egovframework.pms.edu.service.EduService;
import egovframework.pms.edu.service.EduVO;
import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.fdl.security.userdetails.util.EgovUserDetailsHelper;
import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;

/**
 * 교육일정관리 
 * @author 공통서비스 개발팀 
 * @see
 *
 */
@Controller
public class EduSchdulController {

	/** eduService */
	@Resource(name = "eduService")
	private EduService eduService;
	
	/** eduSchdulService */
	@Resource(name = "eduSchdulService")
	private EduSchdulService eduSchdulService;

	/** cmmService */
	@Resource(name = "cmmService")
	private CmmService cmmService;

	/** EgovMessageSource */
	@Resource(name = "egovMessageSource")
	EgovMessageSource egovMessageSource;

	/** EgovPropertyService */
	@Resource(name = "propertiesService")
	protected EgovPropertyService propertiesService;
	
	@Resource(name = "jsonUtils")
	private JsonUtils jsonUtils;

	
	
	/**
	 * 교육자료실 목록을 조회한다. (pageing)
	 * @param bizVO 검색조건정보
	 * @param model 화면모델
	 * @throws Exception
	 */
	@RequestMapping(value = "/pms/edu/eduSchdulList.do")
	public String eduSchdulList(@ModelAttribute("eduVO") EduVO eduVO, ModelMap model
    		,HttpSession session, @RequestParam(value = "baseMenuNo", required = false) String baseMenuNo 
			) throws Exception {

		// 미인증 작업에 대한 보안처리
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
    	if(!isAuthenticated) {
    		model.addAttribute("message", egovMessageSource.getMessage("fail.common.login"));
        	return "uat/uia/EgovLoginUsr";
    	}

		String isEduMngYn = SessionUtil.isAdmYn; //관리자 여부
		model.addAttribute("userId", SessionUtil.id);
		model.addAttribute("eduMngYn", isEduMngYn);
		
		eduVO.setMode("VIEW");
    	
		/** EgovPropertyService */
    	eduVO.setPageUnit(propertiesService.getInt("pageUnit"));
    	eduVO.setPageSize(propertiesService.getInt("pageSize"));

		/** pageing */
		PaginationInfo paginationInfo = new PaginationInfo();
		paginationInfo.setCurrentPageNo(eduVO.getPageIndex());
		paginationInfo.setRecordCountPerPage(eduVO.getPageUnit());
		paginationInfo.setPageSize(eduVO.getPageSize());

		eduVO.setFirstIndex(paginationInfo.getFirstRecordIndex());
		eduVO.setLastIndex(paginationInfo.getLastRecordIndex());
		eduVO.setRecordCountPerPage(paginationInfo.getRecordCountPerPage());

		model.addAttribute("resultList", eduSchdulService.eduSchdulList(eduVO));

		int totCnt = eduSchdulService.eduSchdulListTotCnt(eduVO);
		paginationInfo.setTotalRecordCount(totCnt);
		model.addAttribute("paginationInfo", paginationInfo);


		// 선택된 메뉴정보를 세션으로 등록한다.
		if (baseMenuNo != null && !baseMenuNo.equals("") && !baseMenuNo.equals("null")) {
			session.setAttribute("baseMenuNo", baseMenuNo);
		}

		return "pms/edu/eduSchdulMngList";
	}

	/**
	 * 수정을 위해 교육자료정보를 상세조회한다.
	 * @param eduVO 검색조건
	 * @param model 화면모델
	 * @throws Exception
	 */
	@RequestMapping("/pms/edu/eduSchdulUp.do")
	public String eduSchdulUp(
				@RequestParam("mode") String mode,  
				@ModelAttribute("eduVO") EduVO eduVO, ModelMap model) throws Exception {

		// 미인증 작업에 대한 보안처리
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
    	if(!isAuthenticated) {
    		model.addAttribute("message", egovMessageSource.getMessage("fail.common.login"));
        	return "uat/uia/EgovLoginUsr";
    	}

		String isEduMngYn = SessionUtil.isAdmYn; //관리자 여부
		model.addAttribute("userId", SessionUtil.id);
		model.addAttribute("eduMngYn", isEduMngYn);
		
    	ComDtlVO vo = new ComDtlVO();
		//공통코드 : 교육구분
		vo.setMstCd("EDGB");
		List<ComDtlVO> lst = cmmService.selectCmmCodeDetail(vo); 
		model.addAttribute("EDGB_result", lst);
		//공통코드 : 시스템
		vo.setMstCd("SYCD");
		List<ComDtlVO> lst2 = cmmService.selectCmmCodeDetail(vo); 
		model.addAttribute("SYCD_result", lst2);
		// 마스터 조회 
    	EduSchdulModVO eduSchdulModVO = eduSchdulService.selectEduSchdulInfo(eduVO); 
    	
    	if( eduSchdulModVO == null ){
    		eduSchdulModVO = new EduSchdulModVO();
    		eduSchdulModVO.setCatCd(eduVO.getCatCd());
    		eduSchdulModVO.setMode(eduVO.getMode());
    		eduSchdulModVO.setEduSeq(eduVO.getEduSeq());
    	} 
    	/*
    	else {
	    	//그룹 매핑정보 조회 
			model.addAttribute("resultGrpList", eduSchdulService.selectEduSchdulDetail(eduVO));
			// 과제정보 조회
			model.addAttribute("resultTaskList", eduSchdulService.selectEduSchdulDetail2(eduVO));
    	}
    	*/
		
		eduSchdulModVO.setPageIndex(eduVO.getPageIndex());
		eduSchdulModVO.setPageSize(eduVO.getPageSize());
		eduSchdulModVO.setPageUnit(eduVO.getPageUnit());
		eduSchdulModVO.setSearchCondition(eduVO.getSearchCondition());
		eduSchdulModVO.setSearchKeyword(eduVO.getSearchKeyword());
		eduSchdulModVO.setMode(mode);
		model.addAttribute("eduSchdulModVO", eduSchdulModVO);

		return  "pms/edu/eduSchdulUp";
	}	


	@RequestMapping("/pms/edu/eduSchdulGrpPopup.do")
	public String eduSchdulGrpPopup(@RequestParam Map<String, Object> commandMap,
				ModelMap model) throws Exception {

		// 미인증 작업에 대한 보안처리
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
    	if(!isAuthenticated) {
    		model.addAttribute("message", egovMessageSource.getMessage("fail.common.login"));
        	return "uat/uia/EgovLoginUsr";
    	}

		String isEduMngYn = SessionUtil.isAdmYn; //관리자 여부
		model.addAttribute("userId", SessionUtil.id);
		model.addAttribute("eduMngYn", isEduMngYn);
		
    	EduVO eduVO = new EduVO();

		model.addAttribute("grpId", (String)commandMap.get("grpId"));
    	//그룹 매핑정보 조회 
		model.addAttribute("grpResultList", eduService.selectEduSchdulGrpPopupList(eduVO));
		
		return  "pms/edu/eduSchdulGrpPopup";
	}	

	@RequestMapping("/pms/edu/eduSchdulProjectPopup.do")
	public String eduSchdulProjectPopup(@RequestParam Map<String, Object> commandMap,
				ModelMap model) throws Exception {

		// 미인증 작업에 대한 보안처리
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
    	if(!isAuthenticated) {
    		model.addAttribute("message", egovMessageSource.getMessage("fail.common.login"));
        	return "uat/uia/EgovLoginUsr";
    	}


		String isEduMngYn = SessionUtil.isAdmYn; //관리자 여부
		model.addAttribute("userId", SessionUtil.id);
		model.addAttribute("eduMngYn", isEduMngYn);
		
    	ComDtlVO vo = new ComDtlVO();
		//공통코드 : 교육구분
		vo.setMstCd("EDCD");
		List<ComDtlVO> lst = cmmService.selectCmmCodeDetail(vo); 
		model.addAttribute("EDCD_result", lst);
		
    	EduVO eduVO = new EduVO();
    	if( commandMap.get("taskSeq") != null && Long.parseLong((String) commandMap.get("taskSeq")) > 0 && !"".equals((String) commandMap.get("taskSeq"))) {
        	eduVO.setEduSeq(Long.parseLong((String) commandMap.get("eduSeq")));
        	eduVO.setTaskSeq(Long.parseLong((String) commandMap.get("taskSeq")));
			model.addAttribute("taskSeq", (String)commandMap.get("taskSeq"));
			model.addAttribute("eduSeq", (String)commandMap.get("eduSeq"));
			List<?> projectList = eduService.selectEduSchdulProjectPopupList(eduVO);
			Map<String , Object> objMap = (Map<String, Object>) projectList.get(0);
			model.addAttribute("ttl", objMap.get("ttl"));
			model.addAttribute("contents", objMap.get("contents"));
			model.addAttribute("startDt", objMap.get("startDt"));
			model.addAttribute("endDt", objMap.get("endDt"));
    	}
    	//그룹 매핑정보 조회 
		// model.addAttribute("grpResultList", eduService.selectEduSchdulProjectPopupList(eduVO));
		
		return  "pms/edu/eduSchdulProjectPopup";
	}	

	/**
	 * 교육일정을 저장처리한다. 
	 * @param bizVO 검색조건정보
	 * @param model 화면모델
	 * @throws Exception
	 */
	@RequestMapping(value = "/pms/edu/eduSchdulSave.do")
	@ResponseBody
	public ModelAndView eduSchdulSave(@RequestBody Map<String,Object> param) throws Exception {
		ModelAndView mv = new ModelAndView("/proc/proc");

		// 미인증 사업에 대한 보안처리
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
    	if(!isAuthenticated) {
    		JSONObject result = new JSONObject();
    		result.put("result", false);
    		mv.addObject("result", result.toJSONString());
    		return mv;
    	}
		
		// 교육일정
		String result = eduSchdulService.saveSchdul(param);
		
		mv.addObject("result", result);
		return mv;
	}

	/**
	 * 교육자료정보를 삭제 후 상세조회한다.
	 * @param eduVO 검색조건
	 * @param model 화면모델
	 * @throws Exception
	 */
	@RequestMapping("/pms/edu/eduSchdulDelete.do")
	public String eduSchdulDelete(
				@RequestParam("mode") String mode,  
				@ModelAttribute("eduSchdulModVO") EduSchdulModVO eduSchdulModVO, 
				ModelMap model,HttpSession session) throws Exception {

		// 미인증 작업에 대한 보안처리
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
    	if(!isAuthenticated) {
    		model.addAttribute("message", egovMessageSource.getMessage("fail.common.login"));
        	return "uat/uia/EgovLoginUsr";
    	}
    	
    	// 삭제 처리 
    	eduSchdulService.eduSchdulDelete(eduSchdulModVO); 
    	
    	Map<String, Object> map = new HashMap<String, Object>();

		String isEduMngYn = SessionUtil.isAdmYn; //관리자 여부
		model.addAttribute("userId", SessionUtil.id);
		model.addAttribute("eduMngYn", isEduMngYn);
		
		EduVO eduVO = new EduVO();
		// eduVO.setSeq(eduSchdulModVO.getEduSeq());
		eduVO.setPageIndex(eduSchdulModVO.getPageIndex());
		eduVO.setPageSize(eduSchdulModVO.getPageSize());
		eduVO.setPageUnit(eduSchdulModVO.getPageUnit());
		eduVO.setSearchCondition(eduSchdulModVO.getSearchCondition());
		eduVO.setSearchKeyword(eduSchdulModVO.getSearchKeyword());
		
		// eduModVO = eduService.selectEduDataList(eduVo); 

		/** pageing */
		PaginationInfo paginationInfo = new PaginationInfo();
		paginationInfo.setCurrentPageNo(eduVO.getPageIndex());
		paginationInfo.setRecordCountPerPage(eduVO.getPageUnit());
		paginationInfo.setPageSize(eduVO.getPageSize());

		model.addAttribute("resultList", eduSchdulService.eduSchdulList(eduVO));

		int totCnt = eduSchdulService.eduSchdulListTotCnt(eduVO);
		paginationInfo.setTotalRecordCount(totCnt);
		model.addAttribute("paginationInfo", paginationInfo);

    	model.addAttribute("mode", "VIEW"); 
    	model.addAttribute("pageIndex", eduVO.getPageIndex()); 
    	model.addAttribute("resultMsg", "success.common.delete");

		return  "pms/data/eduDataList";
	}	

	/**
	 * 퀴즈관리 목록을 조회한다. (pageing)
	 * @param bizVO 검색조건정보
	 * @param model 화면모델
	 * @throws Exception
	 */
	@RequestMapping(value = "/pms/edu/eduQuizList.do")
	public String eduQuizList(
			HttpSession session
			, @RequestParam(value = "sysCd", required = false) String sysCd
			, @RequestParam(value = "baseMenuNo", required = false) String baseMenuNo
			, ModelMap model) throws Exception {

		// 미인증 사업에 대한 보안처리
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
		if(!isAuthenticated) {
			model.addAttribute("message", egovMessageSource.getMessage("fail.common.login"));
	    	return "uat/uia/EgovLoginUsr";
		}

		String isEduMngYn = SessionUtil.isAdmYn; //관리자 여부
		model.addAttribute("userId", SessionUtil.id);
		model.addAttribute("eduMngYn", isEduMngYn);
				
		ComDtlVO vo = new ComDtlVO();
		//공통코드 : 교육시스템구분
		vo.setMstCd("SYCD");
		List<ComDtlVO> lst = cmmService.selectCmmCodeDetail(vo); 
		model.addAttribute("SYCD_result", lst);
	
		
		//시스템동영상목록
		Map<String, Object> map = new HashMap<String, Object>();
		if( (sysCd == null || "".equals(sysCd))){
			if(lst != null){
				map.put("sysCd", lst.get(0).getDtlCd()); //선택한 분류가 없으면  첫 분류 
				model.addAttribute("sysCd", lst.get(0).getDtlCd());
			}
			else{
				map.put("sysCd", ""); //첫 분류가 없으면 전체 
				model.addAttribute("sysCd", "");
			}
		}
		else{
			map.put("sysCd", sysCd); 
			model.addAttribute("sysCd", sysCd);
		}
		List<?> lst2 = cmmService.selectList("selectQuizList", map);
		model.addAttribute("resultList", lst2);
	

		return "pms/edu/eduQuizList";
	}
	

	/**
	 * 교육일정을 저장처리한다. 
	 * @param bizVO 검색조건정보
	 * @param model 화면모델
	 * @throws Exception
	 */
	@RequestMapping(value = "/pms/edu/eduQuizSave.do")
	@ResponseBody
	public ModelAndView eduQuizSave(@RequestBody Map<String,Object> param) throws Exception {
		ModelAndView mv = new ModelAndView("/proc/proc");

		// 미인증 사업에 대한 보안처리
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
    	if(!isAuthenticated) {
    		JSONObject result = new JSONObject();
    		result.put("result", false);
    		mv.addObject("result", result.toJSONString());
    		return mv;
    	}

		// 교육일정
		String result = eduSchdulService.eduQuizSave(param); 
		
		mv.addObject("result", result);
		return mv;
	}

	/**
	 * 교육자료실 목록을 조회한다. (pageing)
	 * @param bizVO 검색조건정보
	 * @param model 화면모델
	 * @throws Exception
	 */
	@RequestMapping(value = "/pms/edu/eduResultList.do")
	public String eduResultList(@ModelAttribute("eduVO") EduVO eduVO, ModelMap model
    		,HttpSession session, @RequestParam(value = "baseMenuNo", required = false) String baseMenuNo 
			) throws Exception {

		// 미인증 작업에 대한 보안처리
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
    	if(!isAuthenticated) {
    		model.addAttribute("message", egovMessageSource.getMessage("fail.common.login"));
        	return "uat/uia/EgovLoginUsr";
    	}

		String isEduMngYn = SessionUtil.isAdmYn; //관리자 여부
		model.addAttribute("userId", SessionUtil.id);
		model.addAttribute("eduMngYn", isEduMngYn);
		
		eduVO.setMode("VIEW");
    	
		/** EgovPropertyService */
    	eduVO.setPageUnit(propertiesService.getInt("pageUnit"));
    	eduVO.setPageSize(propertiesService.getInt("pageSize"));

		/** pageing */
		PaginationInfo paginationInfo = new PaginationInfo();
		paginationInfo.setCurrentPageNo(eduVO.getPageIndex());
		paginationInfo.setRecordCountPerPage(eduVO.getPageUnit());
		paginationInfo.setPageSize(eduVO.getPageSize());

		eduVO.setFirstIndex(paginationInfo.getFirstRecordIndex());
		eduVO.setLastIndex(paginationInfo.getLastRecordIndex());
		eduVO.setRecordCountPerPage(paginationInfo.getRecordCountPerPage());

		model.addAttribute("resultList", eduSchdulService.eduSchdulList(eduVO));

		int totCnt = eduSchdulService.eduSchdulListTotCnt(eduVO);
		paginationInfo.setTotalRecordCount(totCnt);
		model.addAttribute("paginationInfo", paginationInfo);


		// 선택된 메뉴정보를 세션으로 등록한다.
		if (baseMenuNo != null && !baseMenuNo.equals("") && !baseMenuNo.equals("null")) {
			session.setAttribute("baseMenuNo", baseMenuNo);
		}

		return "pms/edu/eduResultList";
	}

	/**
	 * 수정을 위해 교육자료정보를 상세조회한다.
	 * @param eduVO 검색조건
	 * @param model 화면모델
	 * @throws Exception
	 */
	@RequestMapping("/pms/edu/eduResultInfo.do")
	public String eduResultInfo(
				@RequestParam("mode") String mode,  
				@ModelAttribute("eduVO") EduVO eduVO, ModelMap model) throws Exception {

		// 미인증 작업에 대한 보안처리
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
    	if(!isAuthenticated) {
    		model.addAttribute("message", egovMessageSource.getMessage("fail.common.login"));
        	return "uat/uia/EgovLoginUsr";
    	}

		String isEduMngYn = SessionUtil.isAdmYn; //관리자 여부
		model.addAttribute("userId", SessionUtil.id);
		model.addAttribute("eduMngYn", isEduMngYn);
		
		// 마스터 조회 
    	EduSchdulModVO eduSchdulModVO = eduSchdulService.selectEduResultInfo(eduVO); 
    	
    	if( eduSchdulModVO == null ){
    		eduSchdulModVO = new EduSchdulModVO();
    		eduSchdulModVO.setCatCd(eduVO.getCatCd());
    		eduSchdulModVO.setMode(eduVO.getMode());
    		eduSchdulModVO.setEduSeq(eduVO.getEduSeq());
    	} 
		
		eduSchdulModVO.setPageIndex(eduVO.getPageIndex());
		eduSchdulModVO.setPageSize(eduVO.getPageSize());
		eduSchdulModVO.setPageUnit(eduVO.getPageUnit());
		eduSchdulModVO.setSearchCondition(eduVO.getSearchCondition());
		eduSchdulModVO.setSearchKeyword(eduVO.getSearchKeyword());
		eduSchdulModVO.setMode(mode);
		model.addAttribute("eduSchdulModVO", eduSchdulModVO);

		return  "pms/edu/eduResultInfo";
	}	


	/**
	 * 수정을 위해 교육자료정보를 상세조회한다.
	 * @param eduVO 검색조건
	 * @param model 화면모델
	 * @throws Exception
	 */
	@RequestMapping("/pms/edu/eduResultInfoTab1.do")
	public String eduResultInfoTab1(
				@ModelAttribute("eduVO") EduVO eduVO
			  , @RequestParam Map<String, Object> commandMap
			  , ModelMap model) throws Exception {
			
			
		// 미인증 작업에 대한 보안처리
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
    	if(!isAuthenticated) {
    		model.addAttribute("message", egovMessageSource.getMessage("fail.common.login"));
        	return "uat/uia/EgovLoginUsr";
    	}

		String isEduMngYn = SessionUtil.isAdmYn; //관리자 여부
		model.addAttribute("userId", SessionUtil.id);
		model.addAttribute("eduMngYn", isEduMngYn);
		
		String sEduSeq = (String) commandMap.get("param_eduSeq");
		eduVO.setEduSeq( Long.parseLong(sEduSeq) );

		model.addAttribute("tab1ResultList", eduSchdulService.selectEduResultInfoTab1(eduVO));

		return  "pms/edu/eduResultInfoTab1";
	}	

	/**
	 * 수정을 위해 교육자료정보를 상세조회한다.
	 * @param eduVO 검색조건
	 * @param model 화면모델
	 * @throws Exception
	 */
	@RequestMapping("/pms/edu/eduResultInfoTab2.do")
	public String eduResultInfoTab2(
			@ModelAttribute("eduVO") EduVO eduVO
			  , @RequestParam Map<String, Object> commandMap
			  , ModelMap model) throws Exception {
			
			
		// 미인증 작업에 대한 보안처리
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
	  	if(!isAuthenticated) {
	  		model.addAttribute("message", egovMessageSource.getMessage("fail.common.login"));
	      	return "uat/uia/EgovLoginUsr";
	  	}

		String isEduMngYn = SessionUtil.isAdmYn; //관리자 여부
		model.addAttribute("userId", SessionUtil.id);
		model.addAttribute("eduMngYn", isEduMngYn);
		
		String sEduSeq = (String) commandMap.get("param_eduSeq");
		eduVO.setEduSeq( Long.parseLong(sEduSeq) );

		model.addAttribute("tab2ResultList", eduSchdulService.selectEduResultInfoTab2(eduVO));

		return  "pms/edu/eduResultInfoTab2";
	}	

	/**
	 * 수정을 위해 교육자료정보를 상세조회한다.
	 * @param eduVO 검색조건
	 * @param model 화면모델
	 * @throws Exception
	 */
	@RequestMapping("/pms/edu/eduResultInfoTab3.do")
	public String eduResultInfoTab3(
			@ModelAttribute("eduVO") EduVO eduVO
			  , @RequestParam Map<String, Object> commandMap
			  , ModelMap model) throws Exception {
			
			
		// 미인증 작업에 대한 보안처리
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
	  	if(!isAuthenticated) {
	  		model.addAttribute("message", egovMessageSource.getMessage("fail.common.login"));
	      	return "uat/uia/EgovLoginUsr";
	  	}

		String isEduMngYn = SessionUtil.isAdmYn; //관리자 여부
		model.addAttribute("userId", SessionUtil.id);
		model.addAttribute("eduMngYn", isEduMngYn);
		
		String sEduSeq = (String) commandMap.get("param_eduSeq");
		eduVO.setEduSeq( Long.parseLong(sEduSeq) );

		model.addAttribute("tab3ResultList", eduSchdulService.selectEduResultInfoTab3(eduVO));

		return  "pms/edu/eduResultInfoTab3";
	}	

	@RequestMapping("/pms/edu/eduResultPlayPopup.do")
	public String eduResultPlayPopup(@RequestParam Map<String, Object> commandMap,
				ModelMap model) throws Exception {

		// 미인증 작업에 대한 보안처리
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
    	if(!isAuthenticated) {
    		model.addAttribute("message", egovMessageSource.getMessage("fail.common.login"));
        	return "uat/uia/EgovLoginUsr";
    	}

		String isEduMngYn = SessionUtil.isAdmYn; //관리자 여부
		model.addAttribute("userId", SessionUtil.id);
		model.addAttribute("eduMngYn", isEduMngYn);
				
    	EduVO eduVO = new EduVO();
    	if( commandMap.get("eduSeq") != null && !"".equals((String) commandMap.get("eduSeq"))&& Long.parseLong((String) commandMap.get("eduSeq")) > 0 ) {
        	eduVO.setEduSeq(Long.parseLong((String) commandMap.get("eduSeq")));
        	eduVO.setRegId( (String) commandMap.get("userId") );
    		// 마스터 조회 
        	EduSchdulModVO eduSchdulModVO = eduSchdulService.selectEduResultPopup(eduVO);
			model.addAttribute("eduSeq", (String)commandMap.get("eduSeq"));
			model.addAttribute("eduSchdulModVO", eduSchdulModVO);
			model.addAttribute("resultList", eduSchdulService.selectEduResultPlayPopup(eduVO));
    	}
		
		return  "pms/edu/eduResultPlayPopup";
	}	

	@RequestMapping("/pms/edu/eduResultQuizPopup.do")
	public String eduResultQuizPopup(@RequestParam Map<String, Object> commandMap,
				ModelMap model) throws Exception {

		// 미인증 작업에 대한 보안처리
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
    	if(!isAuthenticated) {
    		model.addAttribute("message", egovMessageSource.getMessage("fail.common.login"));
        	return "uat/uia/EgovLoginUsr";
    	}

		String isEduMngYn = SessionUtil.isAdmYn; //관리자 여부
		model.addAttribute("userId", SessionUtil.id);
		model.addAttribute("eduMngYn", isEduMngYn);
				
    	EduVO eduVO = new EduVO();
    	if( commandMap.get("eduSeq") != null && !"".equals((String) commandMap.get("eduSeq"))&& Long.parseLong((String) commandMap.get("eduSeq")) > 0 ) {
        	eduVO.setEduSeq(Long.parseLong((String) commandMap.get("eduSeq")));
        	eduVO.setRegId( (String) commandMap.get("userId") );
    		// 마스터 조회 
        	EduSchdulModVO eduSchdulModVO = eduSchdulService.selectEduResultPopup(eduVO);
			model.addAttribute("eduSeq", (String)commandMap.get("eduSeq"));
			model.addAttribute("eduSchdulModVO", eduSchdulModVO);
			model.addAttribute("resultList", eduSchdulService.selectEduResultQuizPopup(eduVO));
    	}
		
		return  "pms/edu/eduResultQuizPopup";
	}	

	@RequestMapping("/pms/edu/eduResultTaskPopup.do")
	public String eduResultTaskPopup(@RequestParam Map<String, Object> commandMap,
				ModelMap model) throws Exception {

		// 미인증 작업에 대한 보안처리
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
    	if(!isAuthenticated) {
    		model.addAttribute("message", egovMessageSource.getMessage("fail.common.login"));
        	return "uat/uia/EgovLoginUsr";
    	}

		String isEduMngYn = SessionUtil.isAdmYn; //관리자 여부
		model.addAttribute("userId", SessionUtil.id);
		model.addAttribute("eduMngYn", isEduMngYn);
				
    	EduVO eduVO = new EduVO();
    	if( commandMap.get("eduSeq") != null && !"".equals((String) commandMap.get("eduSeq"))&& Long.parseLong((String) commandMap.get("eduSeq")) > 0 ) {
        	eduVO.setEduSeq(Long.parseLong((String) commandMap.get("eduSeq")));
        	eduVO.setRegId( (String) commandMap.get("userId") );
    		// 마스터 조회 
        	EduSchdulModVO eduSchdulModVO = eduSchdulService.selectEduResultPopup(eduVO);
			model.addAttribute("eduSeq", (String)commandMap.get("eduSeq"));
			model.addAttribute("eduSchdulModVO", eduSchdulModVO);
			model.addAttribute("resultList", eduSchdulService.selectEduResultTaskPopup(eduVO));
    	}
		
		return  "pms/edu/eduResultTaskPopup";
	}	

	@RequestMapping("/pms/edu/eduMyResultList.do")
	public String eduMyResultList(@ModelAttribute("eduVO") EduVO eduVO, ModelMap model
    		,HttpSession session, @RequestParam(value = "baseMenuNo", required = false) String baseMenuNo ) throws Exception {

		// 미인증 작업에 대한 보안처리
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
    	if(!isAuthenticated) {
    		model.addAttribute("message", egovMessageSource.getMessage("fail.common.login"));
        	return "uat/uia/EgovLoginUsr";
    	}

		String isEduMngYn = SessionUtil.isAdmYn; //관리자 여부
		model.addAttribute("userId", SessionUtil.id);
		model.addAttribute("eduMngYn", isEduMngYn);

    	
		/** EgovPropertyService */
    	eduVO.setPageUnit(propertiesService.getInt("pageUnit"));
    	eduVO.setPageSize(propertiesService.getInt("pageSize"));

		/** pageing */
		PaginationInfo paginationInfo = new PaginationInfo();
		paginationInfo.setCurrentPageNo(eduVO.getPageIndex());
		paginationInfo.setRecordCountPerPage(eduVO.getPageUnit());
		paginationInfo.setPageSize(eduVO.getPageSize());

		eduVO.setFirstIndex(paginationInfo.getFirstRecordIndex());
		eduVO.setLastIndex(paginationInfo.getLastRecordIndex());
		eduVO.setRecordCountPerPage(paginationInfo.getRecordCountPerPage());
		
    	eduVO.setRegId( SessionUtil.id );

		model.addAttribute("resultList", eduSchdulService.eduMyResultList(eduVO));


		int totCnt = eduSchdulService.eduMyResultListTotCnt(eduVO);
		paginationInfo.setTotalRecordCount(totCnt);
		model.addAttribute("paginationInfo", paginationInfo);
		
		return  "pms/edu/eduMyResultList";
	}	
}
