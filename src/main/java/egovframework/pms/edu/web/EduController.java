package egovframework.pms.edu.web;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springmodules.validation.commons.DefaultBeanValidator;

import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.SessionUtil;
import egovframework.com.cmm.service.EgovFileMngService;
import egovframework.com.cmm.service.EgovFileMngUtil;
import egovframework.pms.cmm.service.CmmService;
import egovframework.pms.cmm.service.impl.CmmDAO;
import egovframework.pms.edu.service.EduService;
import egovframework.pms.edu.service.EduVO;
import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.fdl.security.userdetails.util.EgovUserDetailsHelper;

/**
 * 교육관리 
 * @author 공통서비스 개발팀 
 * @see
 *
 */
@Controller
public class EduController {

	/** eduService */
	@Resource(name = "eduService")
	private EduService eduService;

	/** cmmService */
	@Resource(name = "cmmService")
	private CmmService cmmService;

	/** EgovMessageSource */
	@Resource(name = "egovMessageSource")
	EgovMessageSource egovMessageSource;
	
	/** cmmDAO */
	@Resource(name="cmmDAO")
	private CmmDAO cmmDAO;

	/**
	 * 팝업 페이지를 호출한다.
	 *
	 * @param userVO
	 * @param sessionVO
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/pms/edu/pmsEduOpenPopup.do")
	public String pmsEduOpenPopup(@RequestParam Map<String, Object> commandMap, ModelMap model) throws Exception {

		String requestUrl = ""; // (String) commandMap.get("requestUrl"); 
		String width 	  = ""; //= (String) commandMap.get("width");
		String height 	  = ""; //= (String) commandMap.get("height");
		//String trgetId = (String) commandMap.get("trgetId");
		//String typeFlag = (String) commandMap.get("typeFlag");
		//String grpId = (String) commandMap.get("grpId");
		String addParam = "";
		if( commandMap.size() > 0) {
			Iterator<Entry<String, Object>> itr = commandMap.entrySet().iterator();
			while(itr.hasNext()){
				Entry<String, Object> entry = itr.next();
				String key = entry.getKey();
				String value = (String) entry.getValue();
				if("requestUrl".equals(key)){
					requestUrl = value;
				} else if("width".equals(key)) {
					width = value;
				} else if("height".equals(key)) {
					height = value;
				} else {
					addParam += "&amp;"+key+"="+value;
				}
			}
		}

		model.addAttribute("requestUrl", requestUrl + "?PopFlag=Y"+ addParam);

		model.addAttribute("width", width);
		model.addAttribute("height", height);

		return "/cop/com/EgovModalPopupFrame";
	}

	/**
	 * 팝업 페이지를 호출한다.
	 *
	 * @param userVO
	 * @param sessionVO
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/pms/adm/eduGrpList.do")
	public String pmsEduGrpList(@RequestParam Map<String, Object> commandMap, ModelMap model) throws Exception {

		// 미인증 작업에 대한 보안처리
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
    	if(!isAuthenticated) {
    		model.addAttribute("message", egovMessageSource.getMessage("fail.common.login"));
        	return "uat/uia/EgovLoginUsr";
    	}

		String isEduMngYn = SessionUtil.isAdmYn; //관리자 여부
		model.addAttribute("userId", SessionUtil.id);
		model.addAttribute("eduMngYn", isEduMngYn);
		
		return "/pms/adm/eduGrpList";
	}

	@RequestMapping("/pms/adm/eduGrpPopup.do")
	public String eduGrpPopup(@RequestParam Map<String, Object> commandMap,ModelMap model) throws Exception {

		// 미인증 작업에 대한 보안처리
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
    	if(!isAuthenticated) {
    		model.addAttribute("message", egovMessageSource.getMessage("fail.common.login"));
        	return "uat/uia/EgovLoginUsr";
    	}
		model.addAttribute("userId", SessionUtil.id);
		model.addAttribute("grpId", commandMap.get("grpId"));
		model.addAttribute("mode", commandMap.get("mode"));

		return  "/pms/adm/eduGrpPopup";
	}	
	/**
	 * 교육일정을 저장처리한다. 
	 * @param bizVO 검색조건정보
	 * @param model 화면모델
	 * @throws Exception
	 */
	@RequestMapping(value = "/pms/adm/eduGrpSave.do")
	@ResponseBody
	public ModelAndView eduGrpSave(@RequestBody Map<String,Object> param) throws Exception {
		ModelAndView mv = new ModelAndView("/proc/proc");

		// 미인증 사업에 대한 보안처리
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
    	if(!isAuthenticated) {
    		JSONObject result = new JSONObject();
    		result.put("result", false);
    		mv.addObject("result", result.toJSONString());
    		return mv;
    	}
    	Map<String,Object> mainData = (Map<String,Object>) param.get("mainData");
    	int in = 0 ;
		mainData.put("regId", SessionUtil.id);
		
    	if("DELETE".equals( mainData.get("mode"))){
    		//mainData.put("sqlId", "deleteGrpInfo");
    		in = cmmDAO.update("deleteGrpInfo", mainData);  // cmmDAO.delete("deleteEduGrpMap", sEduSeq);cmmDAO.update(queryId); // cmmService.saveData(mainData);
    	} else {
    		//mainData.put("sqlId", "updateGrpInfo");
    		in = cmmDAO.update("updateGrpInfo", mainData); // cmmService.saveData(mainData);
    	}

		JSONObject result = new JSONObject();
		result.put("cnt", in);
		result.put("result", true);
		mv.addObject("result", result.toJSONString());
		return mv;
	}

	/**
	 * 교육일정을 저장처리한다. 
	 * @param bizVO 검색조건정보
	 * @param model 화면모델
	 * @throws Exception
	 */
	@RequestMapping(value = "/pms/adm/eduUserGrpSave.do")
	@ResponseBody
	public ModelAndView eduUserGrpSave(@RequestBody Map<String,Object> param) throws Exception {
		ModelAndView mv = new ModelAndView("/proc/proc");

		// 미인증 사업에 대한 보안처리
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
    	if(!isAuthenticated) {
    		JSONObject result = new JSONObject();
    		result.put("result", false);
    		mv.addObject("result", result.toJSONString());
    		return mv;
    	}
    	List<Map<String,Object>> lst = (List<Map<String,Object>>) param.get("lst");
    	String _grpId = (String)param.get("grpId");
    	
    	int in = 0 ;
    	in = (int) cmmDAO.delete("deleteGrpUserMap", _grpId);
    	for( Map map : lst){
    		map.put("regId", SessionUtil.id);
    		cmmDAO.insert("insertGrpUserMap", map);
    		in++;
    	}

		JSONObject result = new JSONObject();
		result.put("cnt", in);
		result.put("result", true);
		mv.addObject("result", result.toJSONString());
		return mv;
	}
}
