package egovframework.pms.edu.web;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springmodules.validation.commons.DefaultBeanValidator;

import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.SessionUtil;
import egovframework.com.cmm.service.EgovFileMngService;
import egovframework.com.cmm.service.EgovFileMngUtil;
import egovframework.com.cmm.service.FileVO;
import egovframework.pms.cmm.service.CmmService;
import egovframework.pms.cmm.service.ComDtlVO;
import egovframework.pms.edu.service.EduDataModVO;
import egovframework.pms.edu.service.EduDataService;
import egovframework.pms.edu.service.EduService;
import egovframework.pms.edu.service.EduVO;
import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.fdl.security.userdetails.util.EgovUserDetailsHelper;
import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;

/**
 * 교육자료관리 
 * @author 공통서비스 개발팀 
 * @see
 *
 */
@Controller
public class EduDataController {

	/** eduService */
	@Resource(name = "eduService")
	private EduService eduService;
	
	/** eduDataService */
	@Resource(name = "eduDataService")
	private EduDataService eduDataService;

	/** cmmService */
	@Resource(name = "cmmService")
	private CmmService cmmService;

	/** EgovMessageSource */
	@Resource(name = "egovMessageSource")
	EgovMessageSource egovMessageSource;

	/** EgovPropertyService */
	@Resource(name = "propertiesService")
	protected EgovPropertyService propertiesService;

	/** DefaultBeanValidator beanValidator */
	@Autowired
	private DefaultBeanValidator beanValidator;

	// 첨부파일 관련
	@Resource(name="EgovFileMngService")
	private EgovFileMngService fileMngService;
	
	@Resource(name="EgovFileMngUtil")
	private EgovFileMngUtil fileUtil;


	/**
	 * 교육자료실 목록을 조회한다. (pageing)
	 * @param bizVO 검색조건정보
	 * @param model 화면모델
	 * @throws Exception
	 */
	@RequestMapping(value = "/pms/data/eduDataList.do")
	public String eduDataList(@ModelAttribute("eduVO") EduVO eduVO, ModelMap model
    		,HttpSession session
    		, @RequestParam(value = "baseMenuNo", required = false) String baseMenuNo
			) throws Exception {

		
		// 미인증 작업에 대한 보안처리
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
    	if(!isAuthenticated) {
    		model.addAttribute("message", egovMessageSource.getMessage("fail.common.login"));
        	return "uat/uia/EgovLoginUsr";
    	}

		String isEduMngYn = SessionUtil.isAdmYn; //관리자 여부
		model.addAttribute("userId", SessionUtil.id);
		model.addAttribute("eduMngYn", isEduMngYn);
		eduVO.setSeq(-1);
		eduVO.setMode("VIEW");
    	
		/** EgovPropertyService */
    	eduVO.setPageUnit(propertiesService.getInt("pageUnit"));
    	eduVO.setPageSize(propertiesService.getInt("pageSize"));

		/** pageing */
		PaginationInfo paginationInfo = new PaginationInfo();
		paginationInfo.setCurrentPageNo(eduVO.getPageIndex());
		paginationInfo.setRecordCountPerPage(eduVO.getPageUnit());
		paginationInfo.setPageSize(eduVO.getPageSize());

		eduVO.setFirstIndex(paginationInfo.getFirstRecordIndex());
		eduVO.setLastIndex(paginationInfo.getLastRecordIndex());
		eduVO.setRecordCountPerPage(paginationInfo.getRecordCountPerPage());
		
		eduVO.setCatCd("0001");
		/*
		// 메뉴명 구하기
		eduVO.setId(baseMenuNo);
		String sMenuNm = eduService.selectMenuNm(eduVO); 
		// 메뉴 세팅
		model.addAttribute("eduTitle", sMenuNm); 
		*/
		
    	model.addAttribute("catCd", eduVO.getCatCd()); 
		model.addAttribute("resultList", eduDataService.selectEduDataList(eduVO));
 
		int totCnt = eduDataService.selectEduListTotCnt(eduVO);
		paginationInfo.setTotalRecordCount(totCnt);
		model.addAttribute("paginationInfo", paginationInfo);
		model.addAttribute("eduVO", eduVO);


		// 선택된 메뉴정보를 세션으로 등록한다.
		if (baseMenuNo != null && !baseMenuNo.equals("") && !baseMenuNo.equals("null")) {
			session.setAttribute("baseMenuNo", baseMenuNo);
		}

		return "pms/data/eduDataList";

	}
	

	/**
	 * 교육자료정보를 상세조회한다.
	 * @param eduVO 검색조건
	 * @param model 화면모델
	 * @throws Exception
	 */
	@RequestMapping("/pms/data/eduDataInfo.do")
	public String eduDataInfo(
				@RequestParam("mode") String mode,  
				@ModelAttribute("eduVO") EduVO eduVO, ModelMap model) throws Exception {

		// 미인증 작업에 대한 보안처리
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
    	if(!isAuthenticated) {
    		model.addAttribute("message", egovMessageSource.getMessage("fail.common.login"));
        	return "uat/uia/EgovLoginUsr";
    	}

		String isEduMngYn = SessionUtil.isAdmYn; //관리자 여부
		model.addAttribute("userId", SessionUtil.id);
		model.addAttribute("eduMngYn", isEduMngYn);
    	model.addAttribute("mode", mode); 

		// 조회수 증가 처리 
    	EduDataModVO eduDataModVO = new EduDataModVO();
    	eduDataModVO.setSeq(eduVO.getSeq());
		eduDataService.updateEduReadCnt(eduDataModVO); 
		// 조회 
		//eduVo.setSeq(eduVO.getSeq());
		//eduVo.setCatCd(eduVO.getCatCd());
		eduDataModVO = eduDataService.selectEduDataInfo(eduVO); 
		
		eduDataModVO.setPageIndex(eduVO.getPageIndex());
		eduDataModVO.setPageSize(eduVO.getPageSize());
		eduDataModVO.setPageUnit(eduVO.getPageUnit());
		eduDataModVO.setSearchCondition(eduVO.getSearchCondition());
		eduDataModVO.setSearchKeyword(eduVO.getSearchKeyword());
		eduDataModVO.setMode(mode);
		model.addAttribute("eduDataModVO", eduDataModVO);

		return  "pms/data/eduDataInfo";
	}


	/**
	 * 수정을 위해 교육자료정보를 상세조회한다.
	 * @param eduVO 검색조건
	 * @param model 화면모델
	 * @throws Exception
	 */
	@RequestMapping("/pms/data/eduDataUp.do")
	public String eduDataUp(
				@RequestParam("mode") String mode,  
				@ModelAttribute("eduVO") EduVO eduVO, ModelMap model) throws Exception {

		// 미인증 작업에 대한 보안처리
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
    	if(!isAuthenticated) {
    		model.addAttribute("message", egovMessageSource.getMessage("fail.common.login"));
        	return "uat/uia/EgovLoginUsr";
    	}

		String isEduMngYn = SessionUtil.isAdmYn; //관리자 여부
		model.addAttribute("userId", SessionUtil.id);
		model.addAttribute("eduMngYn", isEduMngYn);
		
    	ComDtlVO vo = new ComDtlVO();
		//공통코드 : 시스템
		vo.setMstCd("SYCD");
		List<ComDtlVO> lst = cmmService.selectCmmCodeDetail(vo); 
		model.addAttribute("SYCD_result", lst);
		
		// 상세조회
		//eduVo.setSeq(eduModVO.getSeq());
		//eduVo.setCatCd(eduModVO.getCatCd());

    	EduDataModVO eduDataModVO = eduDataService.selectEduDataInfo(eduVO); 
    	if( eduDataModVO == null ){
    		eduDataModVO = new EduDataModVO();
    		eduDataModVO.setCatCd(eduVO.getCatCd());
    		eduDataModVO.setSeq(eduVO.getSeq());
    	}
		
		eduDataModVO.setPageIndex(eduVO.getPageIndex());
		eduDataModVO.setPageSize(eduVO.getPageSize());
		eduDataModVO.setPageUnit(eduVO.getPageUnit());
		eduDataModVO.setSearchCondition(eduVO.getSearchCondition());
		eduDataModVO.setSearchKeyword(eduVO.getSearchKeyword());
		eduDataModVO.setMode(mode);

    	model.addAttribute("mode", mode); 
		model.addAttribute("eduDataModVO", eduDataModVO);

		return  "pms/data/eduDataUp";
	}	

	/**
	 * 교육자료정보를 저장후 상세조회한다.
	 * @param eduVO 검색조건
	 * @param model 화면모델
	 * @throws Exception
	 */
	@RequestMapping("/pms/data/eduDataUpdate.do")
	public String eduDataUpdate(
				final MultipartHttpServletRequest multiRequest,
				@RequestParam("mode") String mode,  
				@ModelAttribute("eduDataModVO") EduDataModVO eduDataModVO, ModelMap model) throws Exception {

		// 미인증 작업에 대한 보안처리
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
    	if(!isAuthenticated) {
    		model.addAttribute("message", egovMessageSource.getMessage("fail.common.login"));
        	return "uat/uia/EgovLoginUsr";
    	}

        if(mode.equals("ADD") || mode.equals("UPDATE") ){
        	
        	// 첨부파일 관련 첨부파일ID 생성
    		List<FileVO> _result = null;
    		String _fileSeq = eduDataModVO.getFileSeq();

    		final Map<String, MultipartFile> files = multiRequest.getFileMap();

    		if(!files.isEmpty()){
	    		 // 파일전송 
	    		 _result = fileUtil.parseFileInf(files, "DSCH_", 0, _fileSeq, "DATA");
	    		 if( _fileSeq == "" || _fileSeq.isEmpty() || _fileSeq == null ){
		    		 // 키 생성 및 첨부테이블 등록 처리 
		    		 _fileSeq = fileMngService.insertFileInfs(_result);  //파일이 생성되고나면 생성된 첨부파일 ID를 리턴한다.
	    		 } else {
	    			// 첨부테이블 수정 처리 
		    		fileMngService.updateFileInfs(_result);  //파일이 생성되고나면 생성된 첨부파일 ID를 리턴한다.
	    		 }
    		}
    		eduDataModVO.setRegId(SessionUtil.id);
    		eduDataModVO.setFileSeq(_fileSeq);
	    	// 저장 처리 
	    	eduDataService.updateEdu(eduDataModVO); 
        }

		String isEduMngYn = SessionUtil.isAdmYn; //관리자 여부
		model.addAttribute("userId", SessionUtil.id);
		model.addAttribute("eduMngYn", isEduMngYn);
		
		EduVO eduVo = new EduVO();
		eduVo.setSeq(eduDataModVO.getSeq());
		eduVo.setCatCd(eduDataModVO.getCatCd());
		eduVo.setPageIndex(eduDataModVO.getPageIndex());
		eduVo.setPageSize(eduDataModVO.getPageSize());
		eduVo.setPageUnit(eduDataModVO.getPageUnit());
		eduVo.setSearchCondition(eduDataModVO.getSearchCondition());
		eduVo.setSearchKeyword(eduDataModVO.getSearchKeyword());
		
		// eduModVO = eduService.selectEduDataList(eduVo); 

		/** pageing */
		PaginationInfo paginationInfo = new PaginationInfo();
		paginationInfo.setCurrentPageNo(eduVo.getPageIndex());
		paginationInfo.setRecordCountPerPage(eduVo.getPageUnit());
		paginationInfo.setPageSize(eduVo.getPageSize());

		model.addAttribute("resultList", eduDataService.selectEduDataList(eduVo));

		int totCnt = eduDataService.selectEduListTotCnt(eduVo);
		paginationInfo.setTotalRecordCount(totCnt);
		model.addAttribute("paginationInfo", paginationInfo);
		model.addAttribute("eduVO", eduVo);

		//Exception 없이 진행시 수정성공메시지
        if(mode.equals("ADD")){ //  || mode.equals("UPDATE") ){
        	model.addAttribute("resultMsg", "success.common.insert");
        } else {
        	model.addAttribute("resultMsg", "success.common.update");
        }
		return  "pms/data/eduDataList";
	}	

	/**
	 * 교육자료정보를 삭제 후 상세조회한다.
	 * @param eduVO 검색조건
	 * @param model 화면모델
	 * @throws Exception
	 */
	@RequestMapping("/pms/data/eduDataDelete.do")
	public String eduDataDelete(
				@RequestParam("mode") String mode,  
				@ModelAttribute("eduDataModVO") EduDataModVO eduDataModVO, ModelMap model,HttpSession session) throws Exception {

		// 미인증 작업에 대한 보안처리
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
    	if(!isAuthenticated) {
    		model.addAttribute("message", egovMessageSource.getMessage("fail.common.login"));
        	return "uat/uia/EgovLoginUsr";
    	}
    	
    	// 삭제 처리 
    	eduDataService.deleteEdu(eduDataModVO); 
    	
    	Map<String, Object> map = new HashMap<String, Object>();

		String isEduMngYn = SessionUtil.isAdmYn; //관리자 여부
		model.addAttribute("userId", SessionUtil.id);
		model.addAttribute("eduMngYn", isEduMngYn);
		
		EduVO eduVO = new EduVO();
		eduVO.setSeq(eduDataModVO.getSeq());
		eduVO.setCatCd(eduDataModVO.getCatCd());
		eduVO.setPageIndex(eduDataModVO.getPageIndex());
		eduVO.setPageSize(eduDataModVO.getPageSize());
		eduVO.setPageUnit(eduDataModVO.getPageUnit());
		eduVO.setSearchCondition(eduDataModVO.getSearchCondition());
		eduVO.setSearchKeyword(eduDataModVO.getSearchKeyword());
		
		// eduModVO = eduService.selectEduDataList(eduVo); 

		/** pageing */
		PaginationInfo paginationInfo = new PaginationInfo();
		paginationInfo.setCurrentPageNo(eduVO.getPageIndex());
		paginationInfo.setRecordCountPerPage(eduVO.getPageUnit());
		paginationInfo.setPageSize(eduVO.getPageSize());

		model.addAttribute("resultList", eduDataService.selectEduDataList(eduVO));

		int totCnt = eduDataService.selectEduListTotCnt(eduVO);
		paginationInfo.setTotalRecordCount(totCnt);
		model.addAttribute("paginationInfo", paginationInfo);

    	model.addAttribute("mode", "VIEW"); 
    	model.addAttribute("catCd", eduVO.getCatCd()); 
    	model.addAttribute("pageIndex", eduVO.getPageIndex()); 
		// model.addAttribute("eduModVO", eduModVO);
    	model.addAttribute("resultMsg", "success.common.delete");

		return  "pms/data/eduDataList";
	}	
	

	/**
	 * 교육자료실 목록을 조회한다. (pageing)
	 * @param bizVO 검색조건정보
	 * @param model 화면모델
	 * @throws Exception
	 */
	@RequestMapping(value = "/pms/data/eduFaqList.do")
	public String eduFaqList(@ModelAttribute("eduVO") EduVO eduVO, ModelMap model
    		,HttpSession session
    		, @RequestParam(value = "baseMenuNo", required = false) String baseMenuNo
			) throws Exception {

		// 미인증 작업에 대한 보안처리
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
    	if(!isAuthenticated) {
    		model.addAttribute("message", egovMessageSource.getMessage("fail.common.login"));
        	return "uat/uia/EgovLoginUsr";
    	}

		String isEduMngYn = SessionUtil.isAdmYn; //관리자 여부
		model.addAttribute("userId", SessionUtil.id);
		model.addAttribute("eduMngYn", isEduMngYn);
		
		eduVO.setSeq(-1);
		eduVO.setMode("VIEW");
    	
		/** EgovPropertyService */
    	eduVO.setPageUnit(propertiesService.getInt("pageUnit"));
    	eduVO.setPageSize(propertiesService.getInt("pageSize"));

		/** pageing */
		PaginationInfo paginationInfo = new PaginationInfo();
		paginationInfo.setCurrentPageNo(eduVO.getPageIndex());
		paginationInfo.setRecordCountPerPage(eduVO.getPageUnit());
		paginationInfo.setPageSize(eduVO.getPageSize());

		eduVO.setFirstIndex(paginationInfo.getFirstRecordIndex());
		eduVO.setLastIndex(paginationInfo.getLastRecordIndex());
		eduVO.setRecordCountPerPage(paginationInfo.getRecordCountPerPage());
				
    	model.addAttribute("catCd", eduVO.getCatCd()); 
		model.addAttribute("resultList", eduDataService.selectEduFaqList(eduVO));
 
		int totCnt = eduDataService.selectEduFaqListTotCnt(eduVO);
		paginationInfo.setTotalRecordCount(totCnt);
		model.addAttribute("paginationInfo", paginationInfo);
		model.addAttribute("eduVO", eduVO);


		// 선택된 메뉴정보를 세션으로 등록한다.
		if (baseMenuNo != null && !baseMenuNo.equals("") && !baseMenuNo.equals("null")) {
			session.setAttribute("baseMenuNo", baseMenuNo);
		}
		return "pms/data/eduFaqList";
	}
	

	/**
	 * 교육자료정보를 상세조회한다.
	 * @param eduVO 검색조건
	 * @param model 화면모델
	 * @throws Exception
	 */
	@RequestMapping("/pms/data/eduFaqInfo.do")
	public String eduFaqInfo(
				@RequestParam("mode") String mode,  
				@ModelAttribute("eduVO") EduVO eduVO, ModelMap model) throws Exception {

		// 미인증 작업에 대한 보안처리
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
    	if(!isAuthenticated) {
    		model.addAttribute("message", egovMessageSource.getMessage("fail.common.login"));
        	return "uat/uia/EgovLoginUsr";
    	}

		String isEduMngYn = SessionUtil.isAdmYn; //관리자 여부
		model.addAttribute("userId", SessionUtil.id);
		model.addAttribute("eduMngYn", isEduMngYn);
		
    	model.addAttribute("mode", mode); 

		// 조회수 증가 처리 
    	EduDataModVO eduDataModVO = new EduDataModVO();
    	eduDataModVO.setSeq(eduVO.getSeq());
		eduDataService.updateEduFaqReadCnt(eduDataModVO); 
		// 조회 
		eduDataModVO = eduDataService.selectEduFaqInfo(eduVO); 
		
		eduDataModVO.setPageIndex(eduVO.getPageIndex());
		eduDataModVO.setPageSize(eduVO.getPageSize());
		eduDataModVO.setPageUnit(eduVO.getPageUnit());
		eduDataModVO.setSearchCondition(eduVO.getSearchCondition());
		eduDataModVO.setSearchKeyword(eduVO.getSearchKeyword());
		eduDataModVO.setMode(mode);
		model.addAttribute("eduDataModVO", eduDataModVO);

		return  "pms/data/eduFaqInfo";
	}


	/**
	 * 수정을 위해 교육자료정보를 상세조회한다.
	 * @param eduVO 검색조건
	 * @param model 화면모델
	 * @throws Exception
	 */
	@RequestMapping("/pms/data/eduFaqUp.do")
	public String eduFaqUp(
				@RequestParam("mode") String mode,  
				@ModelAttribute("eduVO") EduVO eduVO, ModelMap model) throws Exception {

		// 미인증 작업에 대한 보안처리
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
    	if(!isAuthenticated) {
    		model.addAttribute("message", egovMessageSource.getMessage("fail.common.login"));
        	return "uat/uia/EgovLoginUsr";
    	}
		String isEduMngYn = SessionUtil.isAdmYn; //관리자 여부
		model.addAttribute("userId", SessionUtil.id);
		model.addAttribute("eduMngYn", isEduMngYn);
		
    	ComDtlVO vo = new ComDtlVO();
		//공통코드 : 시스템
		vo.setMstCd("SYCD");
		List<ComDtlVO> lst = cmmService.selectCmmCodeDetail(vo); 
		
		// 상세조회
		//eduVo.setSeq(eduModVO.getSeq());
		//eduVo.setCatCd(eduModVO.getCatCd());

    	EduDataModVO eduDataModVO = eduDataService.selectEduFaqInfo(eduVO); 
    	if( eduDataModVO == null ){
    		eduDataModVO = new EduDataModVO();
    		eduDataModVO.setCatCd(eduVO.getCatCd());
    		eduDataModVO.setSeq(eduVO.getSeq());
    	}
		
		eduDataModVO.setPageIndex(eduVO.getPageIndex());
		eduDataModVO.setPageSize(eduVO.getPageSize());
		eduDataModVO.setPageUnit(eduVO.getPageUnit());
		eduDataModVO.setSearchCondition(eduVO.getSearchCondition());
		eduDataModVO.setSearchKeyword(eduVO.getSearchKeyword());
		eduDataModVO.setMode(mode);


		model.addAttribute("SYCD_result", lst);
    	model.addAttribute("mode", mode); 
		model.addAttribute("eduDataModVO", eduDataModVO);

		return  "pms/data/eduFaqUp";
	}	

	/**
	 * 교육자료정보를 저장후 상세조회한다.
	 * @param eduVO 검색조건
	 * @param model 화면모델
	 * @throws Exception
	 */
	@RequestMapping("/pms/data/eduFaqUpdate.do")
	public String eduFaqUpdate(
				@RequestParam("mode") String mode,  
				@ModelAttribute("eduDataModVO") EduDataModVO eduDataModVO, ModelMap model) throws Exception {

		// 미인증 작업에 대한 보안처리
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
    	if(!isAuthenticated) {
    		model.addAttribute("message", egovMessageSource.getMessage("fail.common.login"));
        	return "uat/uia/EgovLoginUsr";
    	}

		String isEduMngYn = SessionUtil.isAdmYn; //관리자 여부
		model.addAttribute("userId", SessionUtil.id);
		model.addAttribute("eduMngYn", isEduMngYn);
		
        if(mode.equals("ADD") || mode.equals("UPDATE") ){
    		eduDataModVO.setRegId(SessionUtil.id);
	    	// 저장 처리 
	    	eduDataService.updateEduFaq(eduDataModVO); 
        }
    	    	
		EduVO eduVo = new EduVO();
		eduVo.setSeq(eduDataModVO.getSeq());
		eduVo.setCatCd(eduDataModVO.getCatCd());
		eduVo.setPageIndex(eduDataModVO.getPageIndex());
		eduVo.setPageSize(eduDataModVO.getPageSize());
		eduVo.setPageUnit(eduDataModVO.getPageUnit());
		eduVo.setSearchCondition(eduDataModVO.getSearchCondition());
		eduVo.setSearchKeyword(eduDataModVO.getSearchKeyword());
		
		// eduModVO = eduService.selectEduDataList(eduVo); 

		/** pageing */
		PaginationInfo paginationInfo = new PaginationInfo();
		paginationInfo.setCurrentPageNo(eduVo.getPageIndex());
		paginationInfo.setRecordCountPerPage(eduVo.getPageUnit());
		paginationInfo.setPageSize(eduVo.getPageSize());

		model.addAttribute("resultList", eduDataService.selectEduFaqList(eduVo));

		int totCnt = eduDataService.selectEduFaqListTotCnt(eduVo);
		paginationInfo.setTotalRecordCount(totCnt);
		model.addAttribute("paginationInfo", paginationInfo);
		model.addAttribute("eduVO", eduVo);

		//Exception 없이 진행시 수정성공메시지
        if(mode.equals("ADD")){ //  || mode.equals("UPDATE") ){
        	model.addAttribute("resultMsg", "success.common.insert");
        } else {
        	model.addAttribute("resultMsg", "success.common.update");
        }
		return  "pms/data/eduFaqList";
	}	

	/**
	 * 교육자료정보를 삭제 후 상세조회한다.
	 * @param eduVO 검색조건
	 * @param model 화면모델
	 * @throws Exception
	 */
	@RequestMapping("/pms/data/eduFaqDelete.do")
	public String eduFaqDelete(
				@RequestParam("mode") String mode,  
				@ModelAttribute("eduDataModVO") EduDataModVO eduDataModVO, ModelMap model,HttpSession session) throws Exception {

		// 미인증 작업에 대한 보안처리
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
    	if(!isAuthenticated) {
    		model.addAttribute("message", egovMessageSource.getMessage("fail.common.login"));
        	return "uat/uia/EgovLoginUsr";
    	}
    	
    	// 삭제 처리 
    	eduDataService.deleteEduFaq(eduDataModVO); 

		String isEduMngYn = SessionUtil.isAdmYn; //관리자 여부
		model.addAttribute("userId", SessionUtil.id);
		model.addAttribute("eduMngYn", isEduMngYn);
		
    	Map<String, Object> map = new HashMap<String, Object>();
    	
		EduVO eduVO = new EduVO();
		eduVO.setSeq(eduDataModVO.getSeq());
		eduVO.setCatCd(eduDataModVO.getCatCd());
		eduVO.setPageIndex(eduDataModVO.getPageIndex());
		eduVO.setPageSize(eduDataModVO.getPageSize());
		eduVO.setPageUnit(eduDataModVO.getPageUnit());
		eduVO.setSearchCondition(eduDataModVO.getSearchCondition());
		eduVO.setSearchKeyword(eduDataModVO.getSearchKeyword());
		
		// eduModVO = eduService.selectEduDataList(eduVo); 

		/** pageing */
		PaginationInfo paginationInfo = new PaginationInfo();
		paginationInfo.setCurrentPageNo(eduVO.getPageIndex());
		paginationInfo.setRecordCountPerPage(eduVO.getPageUnit());
		paginationInfo.setPageSize(eduVO.getPageSize());

		model.addAttribute("resultList", eduDataService.selectEduFaqList(eduVO));

		int totCnt = eduDataService.selectEduFaqListTotCnt(eduVO);
		paginationInfo.setTotalRecordCount(totCnt);
		model.addAttribute("paginationInfo", paginationInfo);

    	model.addAttribute("mode", "VIEW"); 
    	model.addAttribute("catCd", eduVO.getCatCd()); 
    	model.addAttribute("pageIndex", eduVO.getPageIndex()); 
		// model.addAttribute("eduModVO", eduModVO);
    	
    	model.addAttribute("resultMsg", "success.common.delete");

		return  "pms/data/eduFaqList";
	}	
	

	/**
	 * 교육자료실 목록을 조회한다. (pageing)
	 * @param bizVO 검색조건정보
	 * @param model 화면모델
	 * @throws Exception
	 */
	@RequestMapping(value = "/pms/data/eduQnaList.do")
	public String eduQnaList(@ModelAttribute("eduVO") EduVO eduVO, ModelMap model
    		,HttpSession session
    		, @RequestParam(value = "baseMenuNo", required = false) String baseMenuNo
			) throws Exception {

		// 미인증 작업에 대한 보안처리
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
    	if(!isAuthenticated) {
    		model.addAttribute("message", egovMessageSource.getMessage("fail.common.login"));
        	return "uat/uia/EgovLoginUsr";
    	}

		String isEduMngYn = SessionUtil.isAdmYn; //관리자 여부
		model.addAttribute("userId", SessionUtil.id);
		model.addAttribute("eduMngYn", isEduMngYn);
		
		eduVO.setSeq(-1);
		eduVO.setMode("VIEW");
    	
		/** EgovPropertyService */
    	eduVO.setPageUnit(propertiesService.getInt("pageUnit"));
    	eduVO.setPageSize(propertiesService.getInt("pageSize"));

		/** pageing */
		PaginationInfo paginationInfo = new PaginationInfo();
		paginationInfo.setCurrentPageNo(eduVO.getPageIndex());
		paginationInfo.setRecordCountPerPage(eduVO.getPageUnit());
		paginationInfo.setPageSize(eduVO.getPageSize());

		eduVO.setFirstIndex(paginationInfo.getFirstRecordIndex());
		eduVO.setLastIndex(paginationInfo.getLastRecordIndex());
		eduVO.setRecordCountPerPage(paginationInfo.getRecordCountPerPage());
				
		model.addAttribute("resultList", eduDataService.selectEduQnaList(eduVO));
 
		int totCnt = eduDataService.selectEduQnaListTotCnt(eduVO);
		paginationInfo.setTotalRecordCount(totCnt);
		model.addAttribute("paginationInfo", paginationInfo);
		model.addAttribute("eduVO", eduVO);


		// 선택된 메뉴정보를 세션으로 등록한다.
		if (baseMenuNo != null && !baseMenuNo.equals("") && !baseMenuNo.equals("null")) {
			session.setAttribute("baseMenuNo", baseMenuNo);
		}
		return "pms/data/eduQnaList";
	}
	

	/**
	 * 교육자료정보를 상세조회한다.
	 * @param eduVO 검색조건
	 * @param model 화면모델
	 * @throws Exception
	 */
	@RequestMapping("/pms/data/eduQnaInfo.do")
	public String eduQnaInfo(
				@RequestParam("mode") String mode,  
				@ModelAttribute("eduVO") EduVO eduVO, ModelMap model) throws Exception {

		// 미인증 작업에 대한 보안처리
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
    	if(!isAuthenticated) {
    		model.addAttribute("message", egovMessageSource.getMessage("fail.common.login"));
        	return "uat/uia/EgovLoginUsr";
    	}

		String isEduMngYn = SessionUtil.isAdmYn; //관리자 여부
		model.addAttribute("userId", SessionUtil.id);
		model.addAttribute("eduMngYn", isEduMngYn);
		
    	model.addAttribute("mode", mode); 

		// 조회수 증가 처리 
    	EduDataModVO eduDataModVO = new EduDataModVO();
    	eduDataModVO.setSeq(eduVO.getSeq());
		eduDataService.updateEduQnaReadCnt(eduDataModVO); 
		// 조회 
		eduDataModVO = eduDataService.selectEduQnaInfo(eduVO); 
		
		eduDataModVO.setPageIndex(eduVO.getPageIndex());
		eduDataModVO.setPageSize(eduVO.getPageSize());
		eduDataModVO.setPageUnit(eduVO.getPageUnit());
		eduDataModVO.setSearchCondition(eduVO.getSearchCondition());
		eduDataModVO.setSearchKeyword(eduVO.getSearchKeyword());
		eduDataModVO.setMode(mode);
		model.addAttribute("eduDataModVO", eduDataModVO);

		return  "pms/data/eduQnaInfo";
	}


	/**
	 * 수정을 위해 교육자료정보를 상세조회한다.
	 * @param eduVO 검색조건
	 * @param model 화면모델
	 * @throws Exception
	 */
	@RequestMapping("/pms/data/eduQnaUp.do")
	public String eduQnaUp(
				@RequestParam("mode") String mode,  
				@ModelAttribute("eduVO") EduVO eduVO, ModelMap model) throws Exception {

		// 미인증 작업에 대한 보안처리
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
    	if(!isAuthenticated) {
    		model.addAttribute("message", egovMessageSource.getMessage("fail.common.login"));
        	return "uat/uia/EgovLoginUsr";
    	}
		String isEduMngYn = SessionUtil.isAdmYn; //관리자 여부
		model.addAttribute("userId", SessionUtil.id);
		model.addAttribute("eduMngYn", isEduMngYn);
		EduDataModVO eduDataModVO = new EduDataModVO();
		if("REPLY".equals(mode)){
			eduDataModVO= eduDataService.selectEduQnaReplyInfo(eduVO); 			
		} else {
			eduDataModVO= eduDataService.selectEduQnaInfo(eduVO); 			
		}
		
    	if( eduDataModVO == null ){
    		eduDataModVO = new EduDataModVO();
    		eduDataModVO.setSeq(eduVO.getSeq());
    	}
		
		eduDataModVO.setPageIndex(eduVO.getPageIndex());
		eduDataModVO.setPageSize(eduVO.getPageSize());
		eduDataModVO.setPageUnit(eduVO.getPageUnit());
		eduDataModVO.setSearchCondition(eduVO.getSearchCondition());
		eduDataModVO.setSearchKeyword(eduVO.getSearchKeyword());
		eduDataModVO.setMode(mode);

    	model.addAttribute("mode", mode); 
		model.addAttribute("eduDataModVO", eduDataModVO);

		return  "pms/data/eduQnaUp";
	}	

	/**
	 * 교육자료정보를 저장후 상세조회한다.
	 * @param eduVO 검색조건
	 * @param model 화면모델
	 * @throws Exception
	 */
	@RequestMapping("/pms/data/eduQnaUpdate.do")
	public String eduQnaUpdate(
			final MultipartHttpServletRequest multiRequest,
				@RequestParam("mode") String mode,  
				@ModelAttribute("eduDataModVO") EduDataModVO eduDataModVO, ModelMap model) throws Exception {

		// 미인증 작업에 대한 보안처리
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
    	if(!isAuthenticated) {
    		model.addAttribute("message", egovMessageSource.getMessage("fail.common.login"));
        	return "uat/uia/EgovLoginUsr";
    	}

		String isEduMngYn = SessionUtil.isAdmYn; //관리자 여부
		model.addAttribute("userId", SessionUtil.id);
		model.addAttribute("eduMngYn", isEduMngYn);
		
        if(mode.equals("ADD") || mode.equals("UPDATE") || mode.equals("REPLY")){
        	// 첨부파일 관련 첨부파일ID 생성
    		List<FileVO> _result = null;
    		String _fileSeq = eduDataModVO.getFileSeq();

    		final Map<String, MultipartFile> files = multiRequest.getFileMap();

    		if(!files.isEmpty()){
	    		 // 파일전송 
	    		 _result = fileUtil.parseFileInf(files, "DSCH_", 0, _fileSeq, "QNA");
	    		 if( _fileSeq == "" || _fileSeq.isEmpty() || _fileSeq == null ){
		    		 // 키 생성 및 첨부테이블 등록 처리 
		    		 _fileSeq = fileMngService.insertFileInfs(_result);  //파일이 생성되고나면 생성된 첨부파일 ID를 리턴한다.
	    		 } else {
	    			// 첨부테이블 수정 처리 
		    		fileMngService.updateFileInfs(_result);  //파일이 생성되고나면 생성된 첨부파일 ID를 리턴한다.
	    		 }
    		}
    		eduDataModVO.setRegId(SessionUtil.id);
    		eduDataModVO.setFileSeq(_fileSeq);
	    	// 저장 처리 
	    	eduDataService.updateEduQna(eduDataModVO); 
        }
    	    	
		EduVO eduVo = new EduVO();
		eduVo.setSeq(eduDataModVO.getSeq());
		eduVo.setPageIndex(eduDataModVO.getPageIndex());
		eduVo.setPageSize(eduDataModVO.getPageSize());
		eduVo.setPageUnit(eduDataModVO.getPageUnit());
		eduVo.setSearchCondition(eduDataModVO.getSearchCondition());
		eduVo.setSearchKeyword(eduDataModVO.getSearchKeyword());

		/** pageing */
		PaginationInfo paginationInfo = new PaginationInfo();
		paginationInfo.setCurrentPageNo(eduVo.getPageIndex());
		paginationInfo.setRecordCountPerPage(eduVo.getPageUnit());
		paginationInfo.setPageSize(eduVo.getPageSize());

		model.addAttribute("resultList", eduDataService.selectEduQnaList(eduVo));

		int totCnt = eduDataService.selectEduQnaListTotCnt(eduVo);
		paginationInfo.setTotalRecordCount(totCnt);
		model.addAttribute("paginationInfo", paginationInfo);
		model.addAttribute("eduVO", eduVo);

		//Exception 없이 진행시 수정성공메시지
        if(mode.equals("ADD")){ //  || mode.equals("UPDATE") ){
        	model.addAttribute("resultMsg", "success.common.insert");
        } else {
        	model.addAttribute("resultMsg", "success.common.update");
        }
		return  "pms/data/eduQnaList";
	}	

	/**
	 * 교육자료정보를 삭제 후 상세조회한다.
	 * @param eduVO 검색조건
	 * @param model 화면모델
	 * @throws Exception
	 */
	@RequestMapping("/pms/data/eduQnaDelete.do")
	public String eduQnaDelete(
				@RequestParam("mode") String mode,  
				@ModelAttribute("eduDataModVO") EduDataModVO eduDataModVO, ModelMap model,HttpSession session) throws Exception {

		// 미인증 작업에 대한 보안처리
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
    	if(!isAuthenticated) {
    		model.addAttribute("message", egovMessageSource.getMessage("fail.common.login"));
        	return "uat/uia/EgovLoginUsr";
    	}
    	
    	// 삭제 처리 
    	eduDataService.deleteEduQna(eduDataModVO);

		String isEduMngYn = SessionUtil.isAdmYn; //관리자 여부
		model.addAttribute("userId", SessionUtil.id);
		model.addAttribute("eduMngYn", isEduMngYn);
		    	
		EduVO eduVO = new EduVO();
		eduVO.setSeq(eduDataModVO.getSeq());
		eduVO.setPageIndex(eduDataModVO.getPageIndex());
		eduVO.setPageSize(eduDataModVO.getPageSize());
		eduVO.setPageUnit(eduDataModVO.getPageUnit());
		eduVO.setSearchCondition(eduDataModVO.getSearchCondition());
		eduVO.setSearchKeyword(eduDataModVO.getSearchKeyword());

		/** pageing */
		PaginationInfo paginationInfo = new PaginationInfo();
		paginationInfo.setCurrentPageNo(eduVO.getPageIndex());
		paginationInfo.setRecordCountPerPage(eduVO.getPageUnit());
		paginationInfo.setPageSize(eduVO.getPageSize());

		model.addAttribute("resultList", eduDataService.selectEduQnaList(eduVO));

		int totCnt = eduDataService.selectEduQnaListTotCnt(eduVO);
		paginationInfo.setTotalRecordCount(totCnt);
		model.addAttribute("paginationInfo", paginationInfo);

    	model.addAttribute("mode", "VIEW"); 
    	model.addAttribute("pageIndex", eduVO.getPageIndex()); 
    	
    	model.addAttribute("resultMsg", "success.common.delete");

		return  "pms/data/eduQnaList";
	}	
}
