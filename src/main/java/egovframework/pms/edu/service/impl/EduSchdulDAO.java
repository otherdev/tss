package egovframework.pms.edu.service.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import egovframework.pms.edu.service.EduSchdulModVO;
import egovframework.pms.edu.service.EduVO;
import egovframework.rte.psl.dataaccess.EgovAbstractDAO;

/**
 * 교육일정에 관한 데이터 접근 클래스를 정의한다.
 * @author 공통서비스 개발팀 
 * @since 2017.04.10
 * @version 1.0
 * @see
 *
 */
@Repository("eduSchdulDAO")
public class EduSchdulDAO extends EgovAbstractDAO{

    /**
     * 교육자료 정보를 데이터베이스에서 읽어와 화면에 출력
     * @param bizVO 검색조건
     * @return List 업무사용자 목록정보
     */
	public List<?> eduSchdulList(EduVO eduVO){
        return list("eduSchdulList", eduVO);
    }
    /**
     * 총 갯수를 조회한다.
     * @param bizVO 검색조건
     * @return int 업무사용자 총갯수
     */
    public int eduSchdulListTotCnt(EduVO eduVO) {
        return (Integer)select("eduSchdulListTotCnt", eduVO);
    }

    /**
     * 교육자료 정보를 데이터베이스에서 읽어와 화면에 출력
     * @param bizVO 검색조건
     * @return List 업무사용자 목록정보
     */
	public EduSchdulModVO selectEduSchdulInfo(EduVO eduVO){
        return (EduSchdulModVO)select("selectEduSchdulInfo", eduVO); 
    }

    /**
     * 교육자료 정보를 데이터베이스에서 읽어와 화면에 출력
     * @param bizVO 검색조건
     * @return List 업무사용자 목록정보
     */
	public List<?> selectEduSchdulDetail(EduVO eduVO){
        return list("selectEduSchdulDetail", eduVO);
    }

    /**
     * 교육자료 정보를 데이터베이스에서 읽어와 화면에 출력
     * @param bizVO 검색조건
     * @return List 업무사용자 목록정보
     */
	public List<?> selectEduSchdulDetail2(EduVO eduVO){
        return list("selectEduSchdulDetail2", eduVO);
    }

    /**
     * 총 갯수를 조회한다.
     * @param bizVO 검색조건
     * @return int 업무사용자 총갯수
     */
    public String selectNewEduSeq( ) {
        return (String)select("selectNewEduSeq");
    }
    
	/**
     * 교육일정 정보를 저장
     * @param bizVO 검색조건
     * @return List 업무사용자 목록정보
     */
	public void updateEduSchdul(EduSchdulModVO eduSchdulModVO){ 
		update("updateEduSchdul", eduSchdulModVO);
    }

	/**
     * 교육일정 정보를 저장
     * @param bizVO 검색조건
     * @return List 업무사용자 목록정보
     */
	public void deleteEduSchdul(EduSchdulModVO eduSchdulModVO){ 
		delete("deleteEduSchdul", eduSchdulModVO);
    }

	/**
     * 교육일정 정보를 저장
     * @param bizVO 검색조건
     * @return List 업무사용자 목록정보
     */
	public void deleteEduSchdulGrp(EduSchdulModVO eduSchdulModVO){ 
		delete("deleteEduSchdulGrp", eduSchdulModVO);
    }
	/**
     * 교육일정 정보를 저장
     * @param bizVO 검색조건
     * @return List 업무사용자 목록정보
     */
	public void deleteEduSchdulProject(EduSchdulModVO eduSchdulModVO){ 
		delete("deleteEduSchdulProject", eduSchdulModVO);
    }

    /**
     * 총 갯수를 조회한다.
     * @param bizVO 검색조건
     * @return int 업무사용자 총갯수
     */
    public String selectMaxQuestionSeq( ) {
        return (String)select("selectMaxQuestionSeq");
    }


    /**
     * 교육자료 정보를 데이터베이스에서 읽어와 화면에 출력
     * @param bizVO 검색조건
     * @return List 업무사용자 목록정보
     */
	public EduSchdulModVO selectEduResultInfo(EduVO eduVO){
        return (EduSchdulModVO)select("selectEduResultInfo", eduVO); 
    }

    /**
     * 교육자료 정보를 데이터베이스에서 읽어와 화면에 출력
     * @param bizVO 검색조건
     * @return List 업무사용자 목록정보
     */
	public List<?> selectEduResultInfoTab1(EduVO eduVO){
        return list("selectEduResultInfoTab1", eduVO);
    }

    /**
     * 교육자료 정보를 데이터베이스에서 읽어와 화면에 출력
     * @param bizVO 검색조건
     * @return List 업무사용자 목록정보
     */
	public List<?> selectEduResultInfoTab2(EduVO eduVO){
        return list("selectEduResultInfoTab2", eduVO);
    }

    /**
     * 교육자료 정보를 데이터베이스에서 읽어와 화면에 출력
     * @param bizVO 검색조건
     * @return List 업무사용자 목록정보
     */
	public List<?> selectEduResultInfoTab3(EduVO eduVO){
        return list("selectEduResultInfoTab3", eduVO);
    }

    /**
     * 교육자료 정보를 데이터베이스에서 읽어와 화면에 출력
     * @param bizVO 검색조건
     * @return List 업무사용자 목록정보
     */
	public EduSchdulModVO selectEduResultPopup(EduVO eduVO){
        return (EduSchdulModVO)select("selectEduResultPopup", eduVO); 
    }
    /**
     * 교육자료 정보를 데이터베이스에서 읽어와 화면에 출력
     * @param bizVO 검색조건
     * @return List 업무사용자 목록정보
     */
	public List<?> selectEduResultPlayPopup(EduVO eduVO){
        return list("selectEduResultPlayPopup", eduVO);
    }

    /**
     * 교육자료 정보를 데이터베이스에서 읽어와 화면에 출력
     * @param bizVO 검색조건
     * @return List 업무사용자 목록정보
     */
	public List<?> selectEduResultQuizPopup(EduVO eduVO){
        return list("selectEduResultQuizPopup", eduVO);
    }

    /**
     * 교육자료 정보를 데이터베이스에서 읽어와 화면에 출력
     * @param bizVO 검색조건
     * @return List 업무사용자 목록정보
     */
	public List<?> selectEduResultTaskPopup(EduVO eduVO){
        return list("selectEduResultTaskPopup", eduVO);
    }

    /**
     * 교육자료 정보를 데이터베이스에서 읽어와 화면에 출력
     * @param bizVO 검색조건
     * @return List 업무사용자 목록정보
     */
	public List<?> eduMyResultList(EduVO eduVO){
        return list("selectMyResultList", eduVO);
    }

    /**
     * 총 갯수를 조회한다.
     * @param bizVO 검색조건
     * @return int 업무사용자 총갯수
     */
    public int selectMyResultListTotCnt(EduVO eduVO) {
        return (Integer)select("selectMyResultListTotCnt", eduVO);
    }
}