package egovframework.pms.edu.service.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import egovframework.pms.edu.service.EduDataModVO;
import egovframework.pms.edu.service.EduVO;
import egovframework.rte.psl.dataaccess.EgovAbstractDAO;

/**
 * 교육자료에 관한 데이터 접근 클래스를 정의한다.
 * @author 공통서비스 개발팀 
 * @since 2017.04.10
 * @version 1.0
 * @see
 *
 */
@Repository("eduDataDAO")
public class EduDataDAO extends EgovAbstractDAO{


    /**
     * 교육자료 정보를 데이터베이스에서 읽어와 화면에 출력
     * @param bizVO 검색조건
     * @return List 업무사용자 목록정보
     */
	public List<?> selectEduDataList(EduVO eduVO){
        return list("selectEduDataList_S", eduVO);
    }

    /**
     * 총 갯수를 조회한다.
     * @param bizVO 검색조건
     * @return int 업무사용자 총갯수
     */
    public int selectEduListTotCnt(EduVO eduVO) {
        return (Integer)select("selectEduDataListTotCnt", eduVO);
    }


    /**
     * 교육자료 정보를 데이터베이스에서 읽어와 화면에 출력
     * @param bizVO 검색조건
     * @return List 업무사용자 목록정보
     */
	public EduDataModVO selectEduDataInfo(EduVO eduVO){
        return (EduDataModVO) select("selectEduDataInfo", eduVO);
    }

    /**
     * 교육자료 정보를 저장
     * @param bizVO 검색조건
     * @return List 업무사용자 목록정보
     */
	public void updateEdu(EduDataModVO eduDataModVO){
		update("updateEdu", eduDataModVO);
    }

    /**
     * 교육자료 정보를 저장
     * @param bizVO 검색조건
     * @return List 업무사용자 목록정보
     */
	public void updateEduReadCnt(EduDataModVO eduDataModVO){
		update("updateEduReadCnt", eduDataModVO);
    }

    /**
     * 교육자료 정보를 저장
     * @param bizVO 검색조건
     * @return List 업무사용자 목록정보
     */
	public void deleteEdu(EduDataModVO eduDataModVO){
		update("deleteEdu", eduDataModVO);
    }

    /**
     * 교육자료 정보를 데이터베이스에서 읽어와 화면에 출력
     * @param bizVO 검색조건
     * @return List 업무사용자 목록정보
     */
	public List<?> selectEduFaqList(EduVO eduVO){
        return list("selectEduFaqList", eduVO);
    }

    /**
     * 총 갯수를 조회한다.
     * @param bizVO 검색조건
     * @return int 업무사용자 총갯수
     */
    public int selectEduFaqListTotCnt(EduVO eduVO) {
        return (Integer)select("selectEduFaqListTotCnt", eduVO);
    }


    /**
     * 교육자료 정보를 데이터베이스에서 읽어와 화면에 출력
     * @param bizVO 검색조건
     * @return List 업무사용자 목록정보
     */
	public EduDataModVO selectEduFaqInfo(EduVO eduVO){
        return (EduDataModVO) select("selectEduFaqInfo", eduVO);
    }

    /**
     * 교육자료 정보를 저장
     * @param bizVO 검색조건
     * @return List 업무사용자 목록정보
     */
	public void updateEduFaq(EduDataModVO eduDataModVO){
		update("updateEduFaq", eduDataModVO);
    }

    /**
     * 교육자료 정보를 저장
     * @param bizVO 검색조건
     * @return List 업무사용자 목록정보
     */
	public void deleteEduFaq(EduDataModVO eduDataModVO){
		update("deleteEduFaq", eduDataModVO);
    }
    /**
     * 교육자료 정보를 저장
     * @param bizVO 검색조건
     * @return List 업무사용자 목록정보
     */
	public void updateEduFaqReadCnt(EduDataModVO eduDataModVO){
		update("updateEduFaqReadCnt", eduDataModVO);
    }

    /**
     * 교육자료 정보를 데이터베이스에서 읽어와 화면에 출력
     * @param bizVO 검색조건
     * @return List 업무사용자 목록정보
     */
	public List<?> selectEduQnaList(EduVO eduVO){
        return list("selectEduQnaList", eduVO);
    }

    /**
     * 총 갯수를 조회한다.
     * @param bizVO 검색조건
     * @return int 업무사용자 총갯수
     */
    public int selectEduQnaListTotCnt(EduVO eduVO) {
        return (Integer)select("selectEduQnaListTotCnt", eduVO);
    }


    /**
     * 교육자료 정보를 데이터베이스에서 읽어와 화면에 출력
     * @param bizVO 검색조건
     * @return List 업무사용자 목록정보
     */
	public EduDataModVO selectEduQnaInfo(EduVO eduVO){
        return (EduDataModVO) select("selectEduQnaInfo", eduVO);
    }

    /**
     * 교육자료 정보를 데이터베이스에서 읽어와 화면에 출력
     * @param bizVO 검색조건
     * @return List 업무사용자 목록정보
     */
	public EduDataModVO selectEduQnaReplyInfo(EduVO eduVO){
        return (EduDataModVO) select("selectEduQnaReplyInfo", eduVO);
    }

    /**
     * 교육자료 정보를 저장
     * @param bizVO 검색조건
     * @return List 업무사용자 목록정보
     */
	public void updateEduQna(EduDataModVO eduDataModVO){
		update("updateEduQna", eduDataModVO);
    }

    /**
     * 교육자료 정보를 저장
     * @param bizVO 검색조건
     * @return List 업무사용자 목록정보
     */
	public void deleteEduQna(EduDataModVO eduDataModVO){
		update("deleteEduQna", eduDataModVO);
    }
    /**
     * 교육자료 정보를 저장
     * @param bizVO 검색조건
     * @return List 업무사용자 목록정보
     */
	public void updateEduQnaReadCnt(EduDataModVO eduDataModVO){
		update("updateEduQnaReadCnt", eduDataModVO);
    }
}