package egovframework.pms.edu.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import egovframework.pms.edu.service.EduModVO;
import egovframework.pms.edu.service.EduService;
import egovframework.pms.edu.service.EduVO;
import egovframework.rte.fdl.cmmn.EgovAbstractServiceImpl;

/**
 * 교육에 관한 비지니스 클래스를 정의한다.
 * @author 공통서비스 개발팀 
 * @version 1.0
 * @see
 *
 */
@Service("eduService")
public class EduServiceImpl extends EgovAbstractServiceImpl implements EduService {

	/** eduDAO */
	@Resource(name="eduDAO")
	private EduDAO eduDAO;

	/**
	 * 메뉴명 가져오기
	 * @param eduVO 검색조건
	 * @return 메뉴명
	 * @throws Exception
	 */
	@Override
	public String selectMenuNm	(EduVO eduVO) {
		return eduDAO.selectMenuNm(eduVO); 
	}	

	/**
	 * 교육일정의 그룹선택의 정보를 데이터베이스에서 읽어와 화면에 출력
	 * @param eduVO 검색조건
	 * @return List<EduModVO> 업무사용자 목록정보
	 * @throws Exception
	 */
	@Override
	public List<?> selectEduSchdulGrpPopupList(EduVO eduVO) {
		List<?> result = eduDAO.selectEduSchdulGrpPopupList(eduVO);
		return result; 
	}
	/**
	 * 교육일정의 그룹선택의 정보를 데이터베이스에서 읽어와 화면에 출력
	 * @param eduVO 검색조건
	 * @return List<EduModVO> 업무사용자 목록정보
	 * @throws Exception
	 */
	@Override
	public List<?> selectEduSchdulProjectPopupList(EduVO eduVO) {
		List<?> result = eduDAO.selectEduSchdulProjectPopupList(eduVO);
		return result; 
	}
}