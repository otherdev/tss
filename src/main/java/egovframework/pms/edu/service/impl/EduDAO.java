package egovframework.pms.edu.service.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import egovframework.pms.edu.service.EduModVO;
import egovframework.pms.edu.service.EduVO;
import egovframework.rte.psl.dataaccess.EgovAbstractDAO;

/**
 * 교육에 관한 데이터 접근 클래스를 정의한다.
 * @author 공통서비스 개발팀 
 * @since 2017.04.10
 * @version 1.0
 * @see
 *
 */
@Repository("eduDAO")
public class EduDAO extends EgovAbstractDAO{


    /**
     * 메뉴명을 조회한다.
     * @param bizVO 검색조건
     * @return int 업무사용자 총갯수
     */
    public String selectMenuNm(EduVO eduVO) {
        return (String)select("selectMenuNm", eduVO);
    }


    /**
     * 교육자료 정보를 데이터베이스에서 읽어와 화면에 출력
     * @param bizVO 검색조건
     * @return List 업무사용자 목록정보
     */
	public List<?> selectEduSchdulGrpPopupList(EduVO eduVO){
        return list("selectEduSchdulGrpPopupList", eduVO);
    }
	/**
     * 교육자료 정보를 데이터베이스에서 읽어와 화면에 출력
     * @param bizVO 검색조건
     * @return List 업무사용자 목록정보
     */
	public List<?> selectEduSchdulProjectPopupList(EduVO eduVO){
        return list("selectEduSchdulProjectPopupList", eduVO);
    }
}