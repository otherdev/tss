package egovframework.pms.edu.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.json.simple.JSONObject;
import org.springframework.stereotype.Service;

import egovframework.com.cmm.SessionUtil;
import egovframework.pms.cmm.service.CmmService;
import egovframework.pms.cmm.service.impl.CmmDAO;
import egovframework.pms.edu.service.EduSchdulModVO;
import egovframework.pms.edu.service.EduSchdulService;
import egovframework.pms.edu.service.EduVO;
import egovframework.rte.fdl.cmmn.EgovAbstractServiceImpl;

/**
 * 교육일정에 관한 비지니스 클래스를 정의한다.
 * @author 공통서비스 개발팀 
 * @version 1.0
 * @see
 *
 */
@Service("eduSchdulService")
public class EduSchdulServiceImpl extends EgovAbstractServiceImpl implements EduSchdulService {

	/** eduDAO */
	@Resource(name="eduSchdulDAO")
	private EduSchdulDAO eduSchdulDAO;

	/** cmmDAO */
	@Resource(name="cmmDAO")
	private CmmDAO cmmDAO;

	/** cmmService */
	@Resource(name = "cmmService")
	private CmmService cmmService;
	
	/**
	 * 교육일정의 정보를 데이터베이스에서 읽어와 화면에 출력
	 * @param eduVO 검색조건
	 * @return List<EduModVO> 업무사용자 목록정보
	 * @throws Exception
	 */
	@Override
	public List<?> eduSchdulList(EduVO eduVO) {
		List<?> result = eduSchdulDAO.eduSchdulList(eduVO);
		return result;
	}
	/**
	 * 교육일정목록의 전체수를 확인
	 * @param eduVO 검색조건
	 * @return 총사용자갯수(int)
	 * @throws Exception
	 */
	@Override
	public int eduSchdulListTotCnt(EduVO eduVO) {
		return eduSchdulDAO.eduSchdulListTotCnt(eduVO);
	}	
	/**
	 * 사업관리의 정보를 데이터베이스에서 읽어와 화면에 출력
	 * @param eduVO 검색조건
	 * @return List<EduModVO> 업무사용자 목록정보
	 * @throws Exception
	 */
	@Override
	public EduSchdulModVO selectEduSchdulInfo(EduVO eduVO) { 
		EduSchdulModVO eduSchdulModVo = eduSchdulDAO.selectEduSchdulInfo(eduVO);
		return eduSchdulModVo;
	}

	/**
	 * 교육일정의 정보를 데이터베이스에서 읽어와 화면에 출력
	 * @param eduVO 검색조건
	 * @return List<EduModVO> 업무사용자 목록정보
	 * @throws Exception
	 */
	@Override
	public List<?> selectEduSchdulDetail(EduVO eduVO) {
		List<?> result = eduSchdulDAO.selectEduSchdulDetail(eduVO);
		return result;
	}
	/**
	 * 교육일정의 정보를 데이터베이스에서 읽어와 화면에 출력
	 * @param eduVO 검색조건
	 * @return List<EduModVO> 업무사용자 목록정보
	 * @throws Exception
	 */
	@Override
	public List<?> selectEduSchdulDetail2(EduVO eduVO) {
		List<?> result = eduSchdulDAO.selectEduSchdulDetail2(eduVO);
		return result;
	}


	/**
	 * 교육 일정 저장
	 * @param eduVO 검색조건
	 * @return List<EduModVO> 업무사용자 목록정보
	 * @throws Exception
	 */
	@Override
	public String saveSchdul(Map<String,Object> param) throws Exception {
			
		// 0.신듀등록이면 채번하고, 사업마스터 등록

		Map<String, Object> mainData = (Map<String, Object>) param.get("mainData");
		Long lEduSeq ;
		String sEduSeq ; 
		String sRegId = ""; 
		
		if("ADD".equals((String) mainData.get( "mode") ) || "-1".equals((String) mainData.get( "eduSeq") )){
			lEduSeq =  Long.parseLong((String) eduSchdulDAO.selectNewEduSeq());  
		} else {
			lEduSeq = Long.parseLong((String) mainData.get( "eduSeq"));
		}
		sEduSeq = String.valueOf(lEduSeq);
		if(SessionUtil.id != null && !(SessionUtil.id).isEmpty() && !"".equals( SessionUtil.id )   ) {
			sRegId = SessionUtil.id;
		} else {
			sRegId = " ";
		}
		
		EduSchdulModVO eduSchdulModVO = new EduSchdulModVO();
		eduSchdulModVO.setEduSeq(lEduSeq);
		eduSchdulModVO.setCatEduCd((String) mainData.get( "eduCatCd") );
		eduSchdulModVO.setSysCd((String) mainData.get( "sysCd") );
		eduSchdulModVO.setEduNm((String) mainData.get( "eduNm") );
		eduSchdulModVO.setEduContents((String) mainData.get( "eduContents") );
		eduSchdulModVO.setEduStartDt((String) mainData.get( "eduStartDt") );
		eduSchdulModVO.setEduEndDt((String) mainData.get( "eduEndDt") );
		eduSchdulModVO.setEduTarget((String) mainData.get( "eduTarget") );
		eduSchdulModVO.setRegId(sRegId);

		try{			
			eduSchdulDAO.updateEduSchdul(eduSchdulModVO); 
		}catch(Exception e){
			throw e;
		}
		
		// 1.교육일정 그룹정보  저장
		String sqlId = (String)param.get("sqlId");
		List<Map<String, Object>> lst = (List<Map<String, Object>>) param.get("lst");
		
		int cnt = 0;
		if(lst.size() > 0 ){
			// 그룹정보 전체 삭제 처리 
			cmmDAO.delete("deleteEduGrpMap", sEduSeq);
			// 입력된 그룹정보 저장 처리 
			for (Map<String, Object> map : lst) {
				map.put("eduSeq", sEduSeq );//신규등록을 대비해 키 다시 세팅
				map.put("regId", sRegId);
				try{
					int r = cmmDAO.update(sqlId, map);
					if(r>0)	cnt++;
					
				}catch(Exception e){
					throw e;
				}
			}
		}

		// 2.교육일정 과제정보  저장
		String sqlId2 = (String)param.get("sqlId2");
		List<Map<String, Object>> lst2 = (List<Map<String, Object>>) param.get("lst2");
		if(lst2.size() > 0 ){
			// 입력된 그룹정보 저장 처리 
			for (Map<String, Object> map : lst2) {
				map.put("eduSeq", sEduSeq );//신규등록을 대비해 키 다시 세팅
				map.put("regId", sRegId);
				if("D".equals(map.get("_CUD"))){
					try{
						int r = cmmDAO.delete("deleteEduProject", map);
						if(r>0)	cnt++;
						
					}catch(Exception e){
						throw e;
					}
				} else if("C".equals(map.get("_CUD")) || "U".equals(map.get("_CUD"))){
					try{
						int r = cmmDAO.update(sqlId2, map);
						if(r>0)	cnt++;
						
					}catch(Exception e){
						throw e;
					}
				}
			}
		}

		JSONObject result = new JSONObject();
		result.put("cnt", cnt);
		result.put("eduSeq", lEduSeq);
		result.put("result", true);
		return result.toJSONString();
	}

	/**
	 * 교육 일정 저장
	 * @param eduVO 검색조건
	 * @return List<EduModVO> 업무사용자 목록정보
	 * @throws Exception
	 */
	@Override
	public void eduSchdulDelete(EduSchdulModVO eduSchdulModVO) throws Exception{
		eduSchdulDAO.deleteEduSchdulGrp(eduSchdulModVO);
		eduSchdulDAO.deleteEduSchdulProject(eduSchdulModVO);
		eduSchdulDAO.deleteEduSchdul(eduSchdulModVO);
	}
	

	/**
	 * 퀴즈 저장
	 * @param eduVO 검색조건
	 * @return List<EduModVO> 업무사용자 목록정보
	 * @throws Exception
	 */
	@Override
	public String eduQuizSave(Map<String,Object> param) throws Exception {
			
		// 0.신듀등록이면 채번하고, 사업마스터 등록

		String sMnuCd = (String) param.get("mnuCd");
		List<Map<String,Object>> mainData = (List<Map<String,Object>>) param.get("mainData");
		List<Map<String,Object>> detailData = (List<Map<String,Object>>) param.get("detailData");
		List<?> detailData2 = (List<?>) param.get("detailData");
		String sRegId = ""; 

		if(SessionUtil.id != null && !(SessionUtil.id).isEmpty() && !"".equals( SessionUtil.id )   ) {
			sRegId = SessionUtil.id;
		} else {
			sRegId = " "; // SessionUtil.id;
		}
		
		int cnt = 0;
		if(mainData.size() > 0 ){
			try{
				String sQuestionSeq = "";
				// 전체 삭제플레그 업데이트
				cmmDAO.update("deleteEduQuiz", sMnuCd); 
				// 입력된 그룹정보 저장 처리 
				for (Map<String,Object> map : mainData) {
					sQuestionSeq =(String) map.get("questionSeq");
					
					if(sQuestionSeq == null || sQuestionSeq == "-1" || sQuestionSeq == "0"|| sQuestionSeq.isEmpty() ){
						sQuestionSeq = (String) eduSchdulDAO.selectMaxQuestionSeq();
						map.put("questionSeq" , sQuestionSeq);
					}
					
					map.put("regId", sRegId);
					int r = cmmDAO.update("updateEduQuiz", map);
					if(r>0)	cnt++;
					
					for (Map<String, Object> map2 : detailData) {

						if(map.get("quizType").equals(map2.get("quizType"))) {

							map2.put("questionSeq" , sQuestionSeq);
							
							int r2 = cmmDAO.update("updateEduAnswer", map2);
							if(r2>0)	cnt++;
						}
					}
				}
			
			}catch(Exception e){
				throw e;
			}
		}

		JSONObject result = new JSONObject();
		result.put("cnt", cnt);
		result.put("result", true);
		return result.toJSONString();
	}

	/**
	 * 사업관리의 정보를 데이터베이스에서 읽어와 화면에 출력
	 * @param eduVO 검색조건
	 * @return List<EduModVO> 업무사용자 목록정보
	 * @throws Exception
	 */
	@Override
	public EduSchdulModVO selectEduResultInfo(EduVO eduVO) { 
		EduSchdulModVO eduSchdulModVo = eduSchdulDAO.selectEduResultInfo(eduVO);
		return eduSchdulModVo;
	}

	/**
	 * 교육일정의 정보를 데이터베이스에서 읽어와 화면에 출력
	 * @param eduVO 검색조건
	 * @return List<EduModVO> 업무사용자 목록정보
	 * @throws Exception
	 */
	@Override
	public List<?> selectEduResultInfoTab1(EduVO eduVO) {
		List<?> result = eduSchdulDAO.selectEduResultInfoTab1(eduVO);
		return result;
	}

	/**
	 * 교육일정의 정보를 데이터베이스에서 읽어와 화면에 출력
	 * @param eduVO 검색조건
	 * @return List<EduModVO> 업무사용자 목록정보
	 * @throws Exception
	 */
	@Override
	public List<?> selectEduResultInfoTab2(EduVO eduVO) {
		List<?> result = eduSchdulDAO.selectEduResultInfoTab2(eduVO);
		return result;
	}

	/**
	 * 교육일정의 정보를 데이터베이스에서 읽어와 화면에 출력
	 * @param eduVO 검색조건
	 * @return List<EduModVO> 업무사용자 목록정보
	 * @throws Exception
	 */
	@Override
	public List<?> selectEduResultInfoTab3(EduVO eduVO) {
		List<?> result = eduSchdulDAO.selectEduResultInfoTab3(eduVO);
		return result;
	}

	/**
	 * 사업관리의 정보를 데이터베이스에서 읽어와 화면에 출력
	 * @param eduVO 검색조건
	 * @return List<EduModVO> 업무사용자 목록정보
	 * @throws Exception
	 */
	@Override
	public EduSchdulModVO selectEduResultPopup(EduVO eduVO) { 
		EduSchdulModVO eduSchdulModVo = eduSchdulDAO.selectEduResultPopup(eduVO);
		return eduSchdulModVo;
	}
	
	/**
	 * 교육일정의 정보를 데이터베이스에서 읽어와 화면에 출력
	 * @param eduVO 검색조건
	 * @return List<EduModVO> 업무사용자 목록정보
	 * @throws Exception
	 */
	@Override
	public List<?> selectEduResultPlayPopup(EduVO eduVO) {
		List<?> result = eduSchdulDAO.selectEduResultPlayPopup(eduVO);
		return result;
	}

	/**
	 * 교육일정의 정보를 데이터베이스에서 읽어와 화면에 출력
	 * @param eduVO 검색조건
	 * @return List<EduModVO> 업무사용자 목록정보
	 * @throws Exception
	 */
	@Override
	public List<?> selectEduResultQuizPopup(EduVO eduVO) {
		List<?> result = eduSchdulDAO.selectEduResultQuizPopup(eduVO);
		return result;
	}

	/**
	 * 교육일정의 정보를 데이터베이스에서 읽어와 화면에 출력
	 * @param eduVO 검색조건
	 * @return List<EduModVO> 업무사용자 목록정보
	 * @throws Exception
	 */
	@Override
	public List<?> selectEduResultTaskPopup(EduVO eduVO) {
		List<?> result = eduSchdulDAO.selectEduResultTaskPopup(eduVO);
		return result;
	}


	/**
	 * 교육일정의 정보를 데이터베이스에서 읽어와 화면에 출력
	 * @param eduVO 검색조건
	 * @return List<EduModVO> 업무사용자 목록정보
	 * @throws Exception
	 */
	@Override
	public List<?> eduMyResultList(EduVO eduVO) {
		List<?> result = eduSchdulDAO.eduMyResultList(eduVO);
		return result;
	}

	/**
	 * 교육일정목록의 전체수를 확인
	 * @param eduVO 검색조건
	 * @return 총사용자갯수(int)
	 * @throws Exception
	 */
	@Override
	public int eduMyResultListTotCnt(EduVO eduVO) {
		return eduSchdulDAO.selectMyResultListTotCnt(eduVO); 
	}	
}