package egovframework.pms.edu.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import egovframework.pms.edu.service.EduDataModVO;
import egovframework.pms.edu.service.EduVO;
import egovframework.pms.edu.service.SysEduVO;
import egovframework.rte.psl.dataaccess.EgovAbstractDAO;

/**
 * 교육일정에 관한 데이터 접근 클래스를 정의한다.
 * @author 공통서비스 개발팀 
 * @since 2017.04.10
 * @version 1.0
 * @see
 *
 */
@Repository("sysEduDAO")
public class SysEduDAO extends EgovAbstractDAO{

    /**
     * 교육자료 정보를 데이터베이스에서 읽어와 화면에 출력
     * @param bizVO 검색조건
     * @return List 업무사용자 목록정보
     */
	public List<?> selectEduProjectList(EduVO eduVO){
        return list("selectSysEduProjectList_S", eduVO);
    }
    /**
     * 총 갯수를 조회한다.
     * @param bizVO 검색조건
     * @return int 업무사용자 총갯수
     */
    public int selectEduProjectListTotCnt(EduVO eduVO) {
        return (Integer)select("selectSysEduProjectListTotCnt_S", eduVO);
    }


    /**
     * 교육자료 정보를 데이터베이스에서 읽어와 화면에 출력
     * @param bizVO 검색조건
     * @return List 업무사용자 목록정보
     */
	public SysEduVO selectSysEduProjectInfo(EduVO eduVO){
        return (SysEduVO) select("selectSysEduProjectInfo", eduVO);
    }

    /**
     * 교육자료 정보를 데이터베이스에서 읽어와 화면에 출력
     * @param bizVO 검색조건
     * @return List 업무사용자 목록정보
     */
	public Map<String, Object> eduQuizPopup(EduVO eduVO){
        return (Map<String, Object>)select("sysEdu.selectQuizPopupName", eduVO);
    }
}