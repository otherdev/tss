package egovframework.pms.edu.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.json.simple.JSONObject;
import org.springframework.stereotype.Service;

import egovframework.com.cmm.SessionUtil;
import egovframework.pms.cmm.service.CmmService;
import egovframework.pms.cmm.service.impl.CmmDAO;
import egovframework.pms.edu.service.EduDataModVO;
import egovframework.pms.edu.service.EduVO;
import egovframework.pms.edu.service.SysEduService;
import egovframework.pms.edu.service.SysEduVO;
import egovframework.rte.fdl.cmmn.EgovAbstractServiceImpl;

/**
 * 교육일정에 관한 비지니스 클래스를 정의한다.
 * @author 공통서비스 개발팀 
 * @version 1.0
 * @see
 *
 */
@Service("sysEduService")
public class SysEduServiceImpl extends EgovAbstractServiceImpl implements SysEduService {

	/** eduDAO */
	@Resource(name="sysEduDAO")
	private SysEduDAO sysEduDAO;

	/** cmmDAO */
	@Resource(name="cmmDAO")
	private CmmDAO cmmDAO;

	/** cmmService */
	@Resource(name = "cmmService")
	private CmmService cmmService;
	
	/**
	 * 교육일정의 정보를 데이터베이스에서 읽어와 화면에 출력
	 * @param eduVO 검색조건
	 * @return List<EduModVO> 업무사용자 목록정보
	 * @throws Exception
	 */
	@Override
	public List<?> selectEduProjectList(EduVO eduVO) {
		List<?> result = sysEduDAO.selectEduProjectList(eduVO);
		return result;
	}
	/**
	 * 교육일정목록의 전체수를 확인
	 * @param eduVO 검색조건
	 * @return 총사용자갯수(int)
	 * @throws Exception
	 */
	@Override
	public int selectEduProjectListTotCnt(EduVO eduVO) {
		return sysEduDAO.selectEduProjectListTotCnt(eduVO);
	}	

	/**
	 * 사업관리의 정보를 데이터베이스에서 읽어와 화면에 출력
	 * @param eduVO 검색조건
	 * @return List<EduModVO> 업무사용자 목록정보
	 * @throws Exception
	 */
	@Override
	public SysEduVO selectSysEduProjectInfo(EduVO eduVO) {
		SysEduVO sysEduVO = sysEduDAO.selectSysEduProjectInfo(eduVO);
		return sysEduVO;
	}

	/**
	 * 퀴즈 저장
	 * @param eduVO 검색조건
	 * @return List<EduModVO> 업무사용자 목록정보
	 * @throws Exception
	 */
	@Override
	public String sysEduProjectReplyUpdate(Map<String,Object> param) throws Exception {

		Map<String,Object> map = (Map<String,Object>) param.get("mainData");
		String sRegId = ""; 

		if(SessionUtil.id != null && !(SessionUtil.id).isEmpty() && !"".equals( SessionUtil.id )   ) {
			sRegId = SessionUtil.id;
		} else {
			sRegId = " "; // SessionUtil.id;
		}
		
		int cnt = 0;
		
		try{
			map.put("regId", sRegId);
			cnt = cmmDAO.update("sysEdu.updateProjectReply", map);
		
		}catch(Exception e){
			throw e;
		}

		JSONObject result = new JSONObject();
		result.put("cnt", cnt);
		result.put("result", true);
		return result.toJSONString();
	}

	/**
	 * 퀴즈 저장
	 * @param eduVO 검색조건
	 * @return List<EduModVO> 업무사용자 목록정보
	 * @throws Exception
	 */
	@Override
	public String sysEduProjectReplyDelete(Map<String,Object> param) throws Exception {

		Map<String,Object> map = (Map<String,Object>) param.get("mainData");
		String sRegId = ""; 

		if(SessionUtil.id != null && !(SessionUtil.id).isEmpty() && !"".equals( SessionUtil.id )   ) {
			sRegId = SessionUtil.id;
		} else {
			sRegId = " "; // SessionUtil.id;
		}
		
		int cnt = 0;
		
		try{
			map.put("regId", sRegId);
			cnt = cmmDAO.update("sysEdu.deleteProjectReply", map);
		
		}catch(Exception e){
			throw e;
		}

		JSONObject result = new JSONObject();
		result.put("cnt", cnt);
		result.put("result", true);
		return result.toJSONString();
	}

	/**
	 * 퀴즈 저장
	 * @param eduVO 검색조건
	 * @return List<EduModVO> 업무사용자 목록정보
	 * @throws Exception
	 */
	@Override
	public String sysEduProjectReplyInsert(Map<String,Object> param) throws Exception {


		int cnt = 0;
		
		try{
			cnt = cmmDAO.update("sysEdu.insertProjectReply", param);
		
		}catch(Exception e){
			throw e;
		}

		JSONObject result = new JSONObject();
		result.put("cnt", cnt);
		result.put("result", true);
		return result.toJSONString();
	}

	/**
	 * 교육일정의 정보를 데이터베이스에서 읽어와 화면에 출력
	 * @param eduVO 검색조건
	 * @return List<EduModVO> 업무사용자 목록정보
	 * @throws Exception
	 */
	@Override
	public Map<String, Object> eduQuizPopup(EduVO eduVO) {
		return sysEduDAO.eduQuizPopup(eduVO);
	}

	/**
	 * 퀴즈 저장
	 * @param eduVO 검색조건
	 * @return List<EduModVO> 업무사용자 목록정보
	 * @throws Exception
	 */
	@Override
	public String eduQuizSubmit(Map<String,Object> param) throws Exception {

		// Map<String,Object> map = (Map<String,Object>) param.get("objData");
		String sRegId = ""; 

		if(SessionUtil.id != null && !(SessionUtil.id).isEmpty() && !"".equals( SessionUtil.id )   ) {
			sRegId = SessionUtil.id;
		} else {
			sRegId = " "; // SessionUtil.id;
		}
		
		int cnt = 0;
		
		try{

			// 1.교육일정 그룹정보  저장
			List<Map<String, Object>> lst = (List<Map<String, Object>>) param.get("objData");
			
			if(lst.size() > 0 ){
				// 입력된 그룹정보 저장 처리 
				for (Map<String, Object> map : lst) {
					map.put("regId", sRegId);
					try{
						int r = cmmDAO.update("sysEdu.updateQuizAnswer", map);
						if(r>0)	cnt++;
						
					}catch(Exception e){
						throw e;
					}
				}
			}		
		}catch(Exception e){
			throw e;
		}

		JSONObject result = new JSONObject();
		result.put("cnt", cnt);
		result.put("result", true);
		return result.toJSONString();
	}
}