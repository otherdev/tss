package egovframework.pms.edu.service;

/**
 * 교육일정VO클래스로서 사업관리관리 비지니스로직 처리용 항목을 구성한다.
 * @author 공통서비스 개발팀 
 * @since 2017.04.10
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *   
 *   수정일      수정자           수정내용
 *  -------    --------    ---------------------------
 *   2017.04.10           최초 생성
 *
 * </pre>
 */
public class EduSchdulModVO extends EduVO{

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	//EDU_SCHED_MST
	private long 	eduSeq	 = 0L;
	private String 	eduCatCd	 = "";
	private String 	eduCatNm	 = "";
	private String 	sysCd	 = "";
	private String 	sysNm	 = "";
	
	private String 	eduNm	 = "";
	private String 	eduContents	 = "";
	private String 	eduStartDt	 = "";
	private String 	eduEndDt	 = "";
	private String 	eduTarget	 = "";
	private String 	eduSchdul	 = "";
	
	// EDU_GRP_MAP
	private String 	grpId	 = "";
	private String 	grpNm	 = "";
	private String 	grpDesc	 = "";	
	
	// EDU_TASK_MNG
	private long 	taskSeq	 = 0L;

	private String 	catCd	 = "";
	private String 	catNm	 = "";
	private String 	ttl	 = "";
	private String 	contents	 = "";
	// 공통
	private String 	delYn	 = "";
	private String 	regId	 = "";
	private String 	regNm	 = "";
	private String 	regDt	 = "";
	private String 	edtId	 = "";
	private String 	edtDt	 = "";
	private String 	ttl2	 = "";
	private String 	edtYn	 = "";

	public String getEdtYn() {
		return edtYn;
	}
	public void setEdtYn(String edtYn) {
		this.edtYn = edtYn;
	}	
	public String getRegNm() {
		return regNm;
	}
	public void setRegNm(String regNm) {
		this.regNm = regNm;
	}	

	public long getEduSeq() {
		return eduSeq;
	}
	public void setEduSeq(long eduSeq) {
		this.eduSeq = eduSeq;
	}
	
	public long getTaskSeq() {
		return taskSeq;
	}
	public void setTaskSeq(long taskSeq) {
		this.taskSeq = taskSeq;
	}

	public String getEduCatCd() {
		return eduCatCd;
	}
	public void setCatEduCd(String eduCatCd) {
		this.eduCatCd = eduCatCd;
	} 
	public String getEduCatNm() {
		return eduCatNm;
	}
	public void setCatEduNm(String eduCatNm) {
		this.eduCatNm = eduCatNm;
	} 
	
	public String getEduNm() {
		return eduNm;
	}
	public void setEduNm(String eduNm) {
		this.eduNm = eduNm;
	}
	public String getCatCd() {
		return catCd;
	}
	public void setCatCd(String catCd) {
		this.catCd = catCd;
	}
	public String getCatNm() {
		return catNm;
	}
	public void setCatNm(String catNm) {
		this.catNm = catNm;
	}
	public String getEduContents() {
		return eduContents;
	}
	public void setEduContents(String eduContents) {
		this.eduContents = eduContents;
	}
	public String getEduStartDt() {
		return eduStartDt;
	}
	public void setEduStartDt(String eduStartDt) {
		this.eduStartDt = eduStartDt;
	}
	public String getEduEndDt() {
		return eduEndDt;
	}
	public void setEduEndDt(String eduEndDt) {
		this.eduEndDt = eduEndDt;
	}
	public String getEduTarget() {
		return eduTarget;
	}
	public void setEduTarget(String eduTarget) {
		this.eduTarget = eduTarget;
	}

	public String getEduSchdul() {
		return eduSchdul;
	}
	public void setEduSchdul(String eduSchdul) {
		this.eduSchdul = eduSchdul;
	}
		 
	public String getGrpId() {
		return grpId;
	}
	public void setGrpId(String grpId) {
		this.grpId = grpId;
	}
		 
	public String getGrpNm() {
		return grpNm;
	}
	public void setGrpNm(String grpNm) {
		this.grpNm = grpNm;
	}
		 
	public String getGrpDesc() {
		return grpDesc;
	}
	public void setGrpDesc(String grpDesc) {
		this.grpDesc = grpDesc;
	}

	public String getTtl() {
		return ttl;
	}
	public void setTtl(String ttl) {
		this.ttl = ttl;
	}

	public String getTtl2() {
		return ttl2;
	}
	public void setTtl2(String ttl2) {
		this.ttl2 = ttl2;
	}
	public String getContents() {
		return contents;
	}
	public void setContents(String contents) {
		this.contents = contents;
	}
	public String getDelYn() {
		return delYn;
	}
	public void setDelYn(String delYn) {
		this.delYn = delYn;
	}
	public String getRegId() {
		return regId;
	}
	public void setRegId(String regId) {
		this.regId = regId;
	}
	public String getRegDt() {
		return regDt;
	}
	public void setRegDt(String regDt) {
		this.regDt = regDt;
	}
	public String getEdtId() {
		return edtId;
	}
	public void setEdtId(String edtId) {
		this.edtId = edtId;
	}
	public String getEdtDt() {
		return edtDt;
	}
	public void setEdtDt(String edtDt) {
		this.edtDt = edtDt;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getSysCd() {
		return sysCd;
	}
	public void setSysCd(String sysCd) {
		this.sysCd = sysCd;
	}
	public String getSysNm() {
		return sysNm;
	}
	public void setSysNm(String sysNm) {
		this.sysNm = sysNm;
	}
}