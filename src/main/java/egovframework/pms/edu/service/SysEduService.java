package egovframework.pms.edu.service;
import java.util.List;
import java.util.Map;

/**
 * 교육일정에 관한 인터페이스클래스를 정의한다.
 * @author 공통서비스 개발팀 
 * @since 2017.04.10
 * @version 1.0
 * @see
 *
 *
 * </pre>
 */
public interface SysEduService  {
	public List<?> selectEduProjectList(EduVO eduVO) throws Exception;
	public int selectEduProjectListTotCnt(EduVO eduVO) throws Exception;
	public SysEduVO selectSysEduProjectInfo(EduVO eduVO) throws Exception;

	String sysEduProjectReplyUpdate(Map<String,Object> param)throws Exception;
	String sysEduProjectReplyDelete(Map<String,Object> param)throws Exception;
	String sysEduProjectReplyInsert(Map<String,Object> param)throws Exception;

	public Map<String, Object> eduQuizPopup(EduVO eduVO) throws Exception;
	String eduQuizSubmit(Map<String,Object> param)throws Exception;
}