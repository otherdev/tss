package egovframework.pms.edu.service;
import java.util.List;
import java.util.Map;

/**
 * 교육일정에 관한 인터페이스클래스를 정의한다.
 * @author 공통서비스 개발팀 
 * @since 2017.04.10
 * @version 1.0
 * @see
 *
 *
 * </pre>
 */
public interface EduSchdulService  {
	public List<?> eduSchdulList(EduVO eduVO) throws Exception;
	public int eduSchdulListTotCnt(EduVO eduVO) throws Exception;
	public EduSchdulModVO selectEduSchdulInfo(EduVO eduVO) throws Exception;
	public List<?> selectEduSchdulDetail(EduVO eduVO) throws Exception;
	public List<?> selectEduSchdulDetail2(EduVO eduVO) throws Exception;	

	String saveSchdul(Map<String,Object> param)throws Exception;
	public void eduSchdulDelete(EduSchdulModVO eduSchdulModVO) throws Exception;
	
	String eduQuizSave(Map<String,Object> param)throws Exception;

	public EduSchdulModVO selectEduResultInfo(EduVO eduVO) throws Exception;
	public List<?> selectEduResultInfoTab1(EduVO eduVO) throws Exception;
	public List<?> selectEduResultInfoTab2(EduVO eduVO) throws Exception;
	public List<?> selectEduResultInfoTab3(EduVO eduVO) throws Exception;

	public EduSchdulModVO selectEduResultPopup(EduVO eduVO) throws Exception;
	public List<?> selectEduResultPlayPopup(EduVO eduVO) throws Exception;
	public List<?> selectEduResultQuizPopup(EduVO eduVO) throws Exception;
	public List<?> selectEduResultTaskPopup(EduVO eduVO) throws Exception;
	
	public List<?> eduMyResultList(EduVO eduVO) throws Exception;
	public int eduMyResultListTotCnt(EduVO eduVO) throws Exception;
}