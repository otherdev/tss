package egovframework.pms.edu.service;
import java.util.List;

/**
 * 교육자료에 관한 인터페이스클래스를 정의한다.
 * @author 공통서비스 개발팀 
 * @since 2017.04.10
 * @version 1.0
 * @see
 *
 *
 * </pre>
 */
public interface EduDataService  {

	public List<?> selectEduDataList(EduVO eduVO) throws Exception; 
	public int selectEduListTotCnt(EduVO eduVO) throws Exception;
	public EduDataModVO selectEduDataInfo(EduVO eduVO) throws Exception;
	public void updateEdu(EduDataModVO eduDataModVO) throws Exception;
	public void updateEduReadCnt(EduDataModVO eduDataModVO) throws Exception;
	public void deleteEdu(EduDataModVO eduDataModVO) throws Exception;

	public List<?> selectEduFaqList(EduVO eduVO) throws Exception; 
	public int selectEduFaqListTotCnt(EduVO eduVO) throws Exception;
	public EduDataModVO selectEduFaqInfo(EduVO eduVO) throws Exception;
	public void updateEduFaq(EduDataModVO eduDataModVO) throws Exception;
	public void deleteEduFaq(EduDataModVO eduDataModVO) throws Exception;
	public void updateEduFaqReadCnt(EduDataModVO eduDataModVO) throws Exception;	

	public List<?> selectEduQnaList(EduVO eduVO) throws Exception; 
	public int selectEduQnaListTotCnt(EduVO eduVO) throws Exception;
	public EduDataModVO selectEduQnaInfo(EduVO eduVO) throws Exception;
	public EduDataModVO selectEduQnaReplyInfo(EduVO eduVO) throws Exception;
	public void updateEduQna(EduDataModVO eduDataModVO) throws Exception;
	public void deleteEduQna(EduDataModVO eduDataModVO) throws Exception;
	public void updateEduQnaReadCnt(EduDataModVO eduDataModVO) throws Exception;
}