package egovframework.pms.edu.service;

/**
 * 교육VO클래스로서 사업관리관리 비지니스로직 처리용 항목을 구성한다.
 * @author 공통서비스 개발팀 
 * @since 2017.04.10
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *   
 *   수정일      수정자           수정내용
 *  -------    --------    ---------------------------
 *   2017.04.10           최초 생성
 *
 * </pre>
 */
public class SysEduVO extends EduVO{

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;

	/** */
	private String 	ttl	 = "";
	private String 	contents	 = "";
	private String 	delYn	 = "";
	private String 	regId	 = "";
	private String 	regDt	 = "";
	private String 	edtId	 = "";
	private String 	edtDt	 = "";
	
	private String 	catCd	 = "";
	private String 	sysCd	 = "";
	private String 	sysNm	 = "";
	private String 	regNm	 = "";
	private String 	ttl2	 = "";
	private String 	fileSeq	 = "";	

	private long 	eduSeq	 = 0L;
	private long 	taskSeq	 = 0L;
	private long 	submitSeq	 = 0L;
	private String 	eduNm	 = "";
	private String 	catNm	 = "";
	private String 	startDt	 = "";
	private String 	endDt	 = "";

	private String 	projectSchdul	 = "";
	private String 	mode	 = "";
	private String 	closeYn	 = "";


	public String getCloseYn() {
		return closeYn;
	}
	public void setCloseYn(String closeYn) {
		this.closeYn = closeYn;
	}

	public String getCatCd() {
		return catCd;
	}
	public void setCatCd(String catCd) {
		this.catCd = catCd;
	}
	public String getSysCd() {
		return sysCd;
	}
	public void setSysCd(String sysCd) {
		this.sysCd = sysCd;
	}
	public String getRegNm() {
		return regNm;
	}
	public void setRegNm(String regNm) {
		this.regNm = regNm;
	}

	public String getFileSeq() {
		return fileSeq;
	}
	public void setFileSeq(String fileSeq) {
		this.fileSeq = fileSeq;
	}

	public String getTtl2() {
		return ttl2;
	}
	public void setTtl2(String ttl2) {
		this.ttl2 = ttl2;
	}

	public String getSysNm() {
		return sysNm;
	}
	public void setSysNm(String sysNm) {
		this.sysNm = sysNm;
	}
	

	public long getEduSeq() {
		return eduSeq;
	}
	public void setEduSeq(long eduSeq) {
		this.eduSeq = eduSeq;
	}
	
	public long getTaskSeq() {
		return taskSeq;
	}
	public void setTaskSeq(long taskSeq) {
		this.taskSeq = taskSeq;
	}

	public long getSubmitSeq() {
		return submitSeq;
	}
	public void setSubmitSeq(long submitSeq) {
		this.submitSeq = submitSeq;
	}
	
	public String getEduNm() {
		return eduNm;
	}
	public void setEduNm(String eduNm) {
		this.eduNm = eduNm;
	}
	public String getCatNm() {
		return catNm;
	}
	public void setCatNm(String catNm) {
		this.catNm = catNm;
	}
	public String getStartDt() {
		return startDt;
	}
	public void setStartDt(String startDt) {
		this.startDt = startDt;
	}
	public String getEndDt() {
		return endDt;
	}
	public void setEndDt(String endDt) {
		this.endDt = endDt;
	}
	public String getProjectSchdul() {
		return projectSchdul;
	}
	public void setProjectSchdul(String projectSchdul) {
		this.projectSchdul = projectSchdul;
	}

	public String getTtl() {
		return ttl;
	}
	public void setTtl(String ttl) {
		this.ttl = ttl;
	}
	public String getContents() {
		return contents;
	}
	public void setContents(String contents) {
		this.contents = contents;
	}
	public String getDelYn() {
		return delYn;
	}
	public void setDelYn(String delYn) {
		this.delYn = delYn;
	}
	public String getRegId() {
		return regId;
	}
	public void setRegId(String regId) {
		this.regId = regId;
	}
	public String getRegDt() {
		return regDt;
	}
	public void setRegDt(String regDt) {
		this.regDt = regDt;
	}
	public String getEdtId() {
		return edtId;
	}
	public void setEdtId(String edtId) {
		this.edtId = edtId;
	}
	public String getEdtDt() {
		return edtDt;
	}
	public void setEdtDt(String edtDt) {
		this.edtDt = edtDt;
	}
	public String getMode() {
		return mode;
	}
	public void setMode(String mode) {
		this.mode = mode;
	}
	

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
}