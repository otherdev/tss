package egovframework.pms.edu.service;
import java.util.List;

/**
 * 교육에 관한 인터페이스클래스를 정의한다.
 * @author 공통서비스 개발팀 
 * @since 2017.04.10
 * @version 1.0
 * @see
 *
 *
 * </pre>
 */
public interface EduService  {

	public String selectMenuNm(EduVO eduVO) ;

	public List<?> selectEduSchdulGrpPopupList(EduVO eduVO) throws Exception; 
	public List<?> selectEduSchdulProjectPopupList(EduVO eduVO) throws Exception; 

}