package egovframework.pms.edu.service;

/**
 * 교육VO클래스로서 사업관리관리 비지니스로직 처리용 항목을 구성한다.
 * @author 공통서비스 개발팀 
 * @since 2017.04.10
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *   
 *   수정일      수정자           수정내용
 *  -------    --------    ---------------------------
 *   2017.04.10           최초 생성
 *
 * </pre>
 */
public class EduModVO extends EduVO{

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;

	/** */
	private String 	ttl	 = "";
	private String 	contents	 = "";
	private String 	workStartDt	 = "";
	private String 	workEndDt	 = "";
	private long 	runMpw	 = 0L;
	private String 	delYn	 = "";
	private String 	regId	 = "";
	private String 	regDt	 = "";
	private String 	edtId	 = "";
	private String 	edtDt	 = "";
	private String 	aprvId	 = "";
	private String 	aprvDt	 = "";

	private long 	bizCost	 = 0L;
	private String 	bizCatNm	 = "";
	private String 	bizStartDt	 = "";
	private String 	bizEndDt	 = "";
	private String 	bizDesc	 = "";
	private String 	bizNm	 = "";
	private String 	fleRegDt	 = "";
	private String 	fleRegId	 = "";

	private long 	seq	 = 0L;
	private String 	catCd	 = "";
	private String 	sysCd	 = "";
	private String 	sysNm	 = "";
	private long 	readCnt	 = 0L;
	private String 	regNm	 = "";
	private String 	ttl2	 = "";
	private String 	fileSeq	 = "";
	private String 	apndFileId	 = "";
	private String 	bizCat	 = "";
	private String 	schedCat	 = "";
	

	private long 	eduSeq	 = 0L;
	private long 	taskSeq	 = 0L;
	private long 	submitSeq	 = 0L;

	private String 	eduCatCd	 = "";
	private String 	eduCatNm	 = "";
	private String 	eduNm	 = "";
	private String 	catNm	 = "";
	private String 	eduContents	 = "";
	private String 	eduStartDt	 = "";
	private String 	eduEndDt	 = "";
	private String 	eduTarget	 = "";
	private String 	eduSchdul	 = "";
	

	private String 	grpId	 = "";
	private String 	grpNm	 = "";
	private String 	grpDesc	 = "";	

    /** 현재페이지 */
    private int pageIndex = 1;
    
    /** 페이지갯수 */
    private int pageUnit = 10;
    
    /** 페이지사이즈 */
    private int pageSize = 10;

    /** firstIndex */
    private int firstIndex = 1;

    /** lastIndex */
    private int lastIndex = 1;

    /** recordCountPerPage */
    private int recordCountPerPage = 10;
  
	/** 검색조건 */
    private String searchCondition = "";
    
    /** 검색Keyword */
    private String searchKeyword = "";



	public String getApndFileId() {
		return apndFileId;
	}
	public void setApndFileId(String apndFileId) {
		this.apndFileId = apndFileId;
	}
	
	public long getSeq() {
		return seq;
	}
	public void setSeq(long seq) {
		this.seq = seq;
	}


	public String getCatCd() {
		return catCd;
	}
	public void setCatCd(String catCd) {
		this.catCd = catCd;
	}
	public String getSysCd() {
		return sysCd;
	}
	public void setSysCd(String sysCd) {
		this.sysCd = sysCd;
	}
	public long getReadCnt() {
		return readCnt;
	}
	public void setReadCnt(long readCnt) {
		this.readCnt = readCnt;
	}	

	public String getRegNm() {
		return regNm;
	}
	public void setRegNm(String regNm) {
		this.regNm = regNm;
	}

	public String getFileSeq() {
		return fileSeq;
	}
	public void setFileSeq(String fileSeq) {
		this.fileSeq = fileSeq;
	}

	public String getTtl2() {
		return ttl2;
	}
	public void setTtl2(String ttl2) {
		this.ttl2 = ttl2;
	}

	public String getSysNm() {
		return sysNm;
	}
	public void setSysNm(String sysNm) {
		this.sysNm = sysNm;
	}
	
	public String getFleRegDt() {
		return fleRegDt;
	}
	public void setFleRegDt(String fleRegDt) {
		this.fleRegDt = fleRegDt;
	}
	public String getFleRegId() {
		return fleRegId;
	}
	public void setFleRegId(String fleRegId) {
		this.fleRegId = fleRegId;
	}
	

	public long getEduSeq() {
		return eduSeq;
	}
	public void setEduSeq(long eduSeq) {
		this.eduSeq = eduSeq;
	}
	
	public long getTaskSeq() {
		return taskSeq;
	}
	public void setTaskSeq(long taskSeq) {
		this.taskSeq = taskSeq;
	}

	public long getSubmitSeq() {
		return submitSeq;
	}
	public void setSubmitSeq(long submitSeq) {
		this.submitSeq = submitSeq;
	}

	public String getEduCatCd() {
		return eduCatCd;
	}
	public void setCatEduCd(String eduCatCd) {
		this.eduCatCd = eduCatCd;
	} 
	public String getEduCatNm() {
		return eduCatNm;
	}
	public void setCatEduNm(String eduCatNm) {
		this.eduCatNm = eduCatNm;
	} 
	
	public String getEduNm() {
		return eduNm;
	}
	public void setEduNm(String eduNm) {
		this.eduNm = eduNm;
	}
	public String getCatNm() {
		return catNm;
	}
	public void setCatNm(String catNm) {
		this.catNm = catNm;
	}
	public String getEduContents() {
		return eduContents;
	}
	public void setEduContents(String eduContents) {
		this.eduContents = eduContents;
	}
	public String getEduStartDt() {
		return eduStartDt;
	}
	public void setEduStartDt(String eduStartDt) {
		this.eduStartDt = eduStartDt;
	}
	public String getEduEndDt() {
		return eduEndDt;
	}
	public void setEduEndDt(String eduEndDt) {
		this.eduEndDt = eduEndDt;
	}
	public String getEduTarget() {
		return eduTarget;
	}
	public void setEduTarget(String eduTarget) {
		this.eduTarget = eduTarget;
	}
	public String getEduSchdul() {
		return eduSchdul;
	}
	public void setEduSchdul(String eduSchdul) {
		this.eduSchdul = eduSchdul;
	}
		 
	public String getGrpId() {
		return grpId;
	}
	public void setGrpId(String grpId) {
		this.grpId = grpId;
	}
		 
	public String getGrpNm() {
		return grpNm;
	}
	public void setGrpNm(String grpNm) {
		this.grpNm = grpNm;
	}
		 
	public String getGrpDesc() {
		return grpDesc;
	}
	public void setGrpDesc(String grpDesc) {
		this.grpDesc = grpDesc;
	}
	/**
	 * searchCondition attribute 값을  리턴한다.
	 * @return String
	 */
	public String getSearchCondition() {
		return searchCondition;
	}

	/**
	 * searchCondition attribute 값을 설정한다.
	 * @param searchCondition String
	 */
	public void setSearchCondition(String searchCondition) {
		this.searchCondition = searchCondition;
	}

	/**
	 * searchKeyword attribute 값을  리턴한다.
	 * @return String
	 */
	public String getSearchKeyword() {
		return searchKeyword;
	}

	/**
	 * searchKeyword attribute 값을 설정한다.
	 * @param searchKeyword String
	 */
	public void setSearchKeyword(String searchKeyword) {
		this.searchKeyword = searchKeyword;
	}

	

	/**
	 * pageIndex attribute 값을  리턴한다.
	 * @return int
	 */
	public int getPageIndex() {
		return pageIndex;
	}

	/**
	 * pageIndex attribute 값을 설정한다.
	 * @param pageIndex int
	 */
	public void setPageIndex(int pageIndex) {
		this.pageIndex = pageIndex;
	}

	/**
	 * pageUnit attribute 값을  리턴한다.
	 * @return int
	 */
	public int getPageUnit() {
		return pageUnit;
	}

	/**
	 * pageUnit attribute 값을 설정한다.
	 * @param pageUnit int
	 */
	public void setPageUnit(int pageUnit) {
		this.pageUnit = pageUnit;
	}

	/**
	 * pageSize attribute 값을  리턴한다.
	 * @return int
	 */
	public int getPageSize() {
		return pageSize;
	}

	/**
	 * pageSize attribute 값을 설정한다.
	 * @param pageSize int
	 */
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	/**
	 * firstIndex attribute 값을  리턴한다.
	 * @return int
	 */
	public int getFirstIndex() {
		return firstIndex;
	}

	/**
	 * firstIndex attribute 값을 설정한다.
	 * @param firstIndex int
	 */
	public void setFirstIndex(int firstIndex) {
		this.firstIndex = firstIndex;
	}

	/**
	 * lastIndex attribute 값을  리턴한다.
	 * @return int
	 */
	public int getLastIndex() {
		return lastIndex;
	}

	/**
	 * lastIndex attribute 값을 설정한다.
	 * @param lastIndex int
	 */
	public void setLastIndex(int lastIndex) {
		this.lastIndex = lastIndex;
	}

	/**
	 * recordCountPerPage attribute 값을  리턴한다.
	 * @return int
	 */
	public int getRecordCountPerPage() {
		return recordCountPerPage;
	}

	/**
	 * recordCountPerPage attribute 값을 설정한다.
	 * @param recordCountPerPage int
	 */
	public void setRecordCountPerPage(int recordCountPerPage) {
		this.recordCountPerPage = recordCountPerPage;
	}
	
	private String 	mode	 = "";
	
	private String	BIZ_SEQ	= "";
	private String	BIZ_NM	= "";
	private String	SCHED_SEQ	= "";
	private String	SCHED_NM	= "";

//	private String	WORK_SEQ	= "";
//	private String	SCHED_SEQ	= "";
//	private String	TTL	= "";
//	private String	CONTENTS	= "";
//	private String	WORK_START_DT	= "";
//	private String	WORK_END_DT	= "";
//	private String	RUN_MPW	= "";
//	private String	DEL_YN	= "";
//	private String	REG_ID	= "";
//	private String	REG_DT	= "";
//	private String	EDT_ID	= "";
//	private String	EDT_DT	= "";
//	private String	APRV_ID	= "";
//	private String	APRV_DT	= "";

	
	
	
	
	
	public String getBizCat() {
		return bizCat;
	}
	public String getBizNm() {
		return bizNm;
	}
	public void setBizNm(String bizNm) {
		this.bizNm = bizNm;
	}
	public String getBizDesc() {
		return bizDesc;
	}
	public void setBizDesc(String bizDesc) {
		this.bizDesc = bizDesc;
	}
	public long getBizCost() {
		return bizCost;
	}
	public void setBizCost(long bizCost) {
		this.bizCost = bizCost;
	}
	public String getBizCatNm() {
		return bizCatNm;
	}
	public void setBizCatNm(String bizCatNm) {
		this.bizCatNm = bizCatNm;
	}
	public String getBizStartDt() {
		return bizStartDt;
	}
	public void setBizStartDt(String bizStartDt) {
		this.bizStartDt = bizStartDt;
	}
	public String getBizEndDt() {
		return bizEndDt;
	}
	public void setBizEndDt(String bizEndDt) {
		this.bizEndDt = bizEndDt;
	}
	public void setBizCat(String bizCat) {
		this.bizCat = bizCat;
	}

	public String getTtl() {
		return ttl;
	}
	public void setTtl(String ttl) {
		this.ttl = ttl;
	}
	public String getContents() {
		return contents;
	}
	public void setContents(String contents) {
		this.contents = contents;
	}
	public String getWorkStartDt() {
		return workStartDt;
	}
	public void setWorkStartDt(String workStartDt) {
		this.workStartDt = workStartDt;
	}
	public String getWorkEndDt() {
		return workEndDt;
	}
	public void setWorkEndDt(String workEndDt) {
		this.workEndDt = workEndDt;
	}

	
	public long getRunMpw() {
		return runMpw;
	}
	public void setRunMpw(long runMpw) {
		this.runMpw = runMpw;
	}
	public String getDelYn() {
		return delYn;
	}
	public void setDelYn(String delYn) {
		this.delYn = delYn;
	}
	public String getRegId() {
		return regId;
	}
	public void setRegId(String regId) {
		this.regId = regId;
	}
	public String getRegDt() {
		return regDt;
	}
	public void setRegDt(String regDt) {
		this.regDt = regDt;
	}
	public String getEdtId() {
		return edtId;
	}
	public void setEdtId(String edtId) {
		this.edtId = edtId;
	}
	public String getEdtDt() {
		return edtDt;
	}
	public void setEdtDt(String edtDt) {
		this.edtDt = edtDt;
	}
	public String getAprvId() {
		return aprvId;
	}
	public void setAprvId(String aprvId) {
		this.aprvId = aprvId;
	}
	public String getAprvDt() {
		return aprvDt;
	}
	public void setAprvDt(String aprvDt) {
		this.aprvDt = aprvDt;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public String getSchedCat() {
		return schedCat;
	}
	public void setSchedCat(String schedCat) {
		this.schedCat = schedCat;
	}
	public String getBIZ_SEQ() {
		return BIZ_SEQ;
	}
	public void setBIZ_SEQ(String bIZ_SEQ) {
		BIZ_SEQ = bIZ_SEQ;
	}
	public String getBIZ_NM() {
		return BIZ_NM;
	}
	public void setBIZ_NM(String bIZ_NM) {
		BIZ_NM = bIZ_NM;
	}
	public String getSCHED_SEQ() {
		return SCHED_SEQ;
	}
	public void setSCHED_SEQ(String sCHED_SEQ) {
		SCHED_SEQ = sCHED_SEQ;
	}
	public String getSCHED_NM() {
		return SCHED_NM;
	}
	public void setSCHED_NM(String sCHED_NM) {
		SCHED_NM = sCHED_NM;
	}
	public String getMode() {
		return mode;
	}
	public void setMode(String mode) {
		this.mode = mode;
	}

	
//	
//	
//	public String getWORK_SEQ() {
//		return WORK_SEQ;
//	}
//	public void setWORK_SEQ(String wORK_SEQ) {
//		WORK_SEQ = wORK_SEQ;
//	}
//	public String getSCHED_SEQ() {
//		return SCHED_SEQ;
//	}
//	public void setSCHED_SEQ(String sCHED_SEQ) {
//		SCHED_SEQ = sCHED_SEQ;
//	}
//	public String getTTL() {
//		return TTL;
//	}
//	public void setTTL(String tTL) {
//		TTL = tTL;
//	}
//	public String getCONTENTS() {
//		return CONTENTS;
//	}
//	public void setCONTENTS(String cONTENTS) {
//		CONTENTS = cONTENTS;
//	}
//	public String getWORK_START_DT() {
//		return WORK_START_DT;
//	}
//	public void setWORK_START_DT(String wORK_START_DT) {
//		WORK_START_DT = wORK_START_DT;
//	}
//	public String getWORK_END_DT() {
//		return WORK_END_DT;
//	}
//	public void setWORK_END_DT(String wORK_END_DT) {
//		WORK_END_DT = wORK_END_DT;
//	}
//	public String getRUN_MPW() {
//		return RUN_MPW;
//	}
//	public void setRUN_MPW(String rUN_MPW) {
//		RUN_MPW = rUN_MPW;
//	}
//	public String getDEL_YN() {
//		return DEL_YN;
//	}
//	public void setDEL_YN(String dEL_YN) {
//		DEL_YN = dEL_YN;
//	}
//	public String getREG_ID() {
//		return REG_ID;
//	}
//	public void setREG_ID(String rEG_ID) {
//		REG_ID = rEG_ID;
//	}
//	public String getREG_DT() {
//		return REG_DT;
//	}
//	public void setREG_DT(String rEG_DT) {
//		REG_DT = rEG_DT;
//	}
//	public String getEDT_ID() {
//		return EDT_ID;
//	}
//	public void setEDT_ID(String eDT_ID) {
//		EDT_ID = eDT_ID;
//	}
//	public String getEDT_DT() {
//		return EDT_DT;
//	}
//	public void setEDT_DT(String eDT_DT) {
//		EDT_DT = eDT_DT;
//	}
//	public String getAPRV_ID() {
//		return APRV_ID;
//	}
//	public void setAPRV_ID(String aPRV_ID) {
//		APRV_ID = aPRV_ID;
//	}
//	public String getAPRV_DT() {
//		return APRV_DT;
//	}
//	public void setAPRV_DT(String aPRV_DT) {
//		APRV_DT = aPRV_DT;
//	}
    
	
	
	
	
}