package egovframework.com.cmm;

import java.io.Serializable;

/**
 * 세션 저장 클래스
 *  
 * <pre>
 * << 개정이력(Modification Information) >>
 * 
 *   수정일      수정자          수정내용
 *  -------    --------    ---------------------------
 *  
 *  </pre>
 */
public class SessionUtil implements Serializable {
	
	public static final long serialVersionUID = -2848741427493626376L;
	
	/** 아이디 */
	static public String id;
	/** 이름 */
	static public String name;
	static public String deptCd;
	static public String ofcCd;
	static public String posCd;
	static public String authCd;
	static public String isAdmYn;
	
	/** 로그인정보 */
	static public LoginVO loginVO;
	
	public static void setLoginVO(LoginVO vo){

		loginVO = vo;
		
		try{
			id = vo.getUserId();
			name = vo.getUserNm();
			deptCd = vo.getDeptCd();
			ofcCd = vo.getOfcCd();
			posCd = vo.getPosCd();
			authCd = vo.getAuthCd();
			isAdmYn = "ROLE_ADMIN".equals(vo.getAuthCd()) ? "Y" : "N" ;
			
		}catch(Exception e){}
	}

}
