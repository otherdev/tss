package egovframework.let.uat.uia.service.impl;

import java.sql.ResultSet;
import java.sql.SQLException;

import egovframework.com.cmm.LoginVO;

import egovframework.rte.fdl.security.userdetails.EgovUserDetails;
import egovframework.rte.fdl.security.userdetails.jdbc.EgovUsersByUsernameMapping;

import javax.sql.DataSource;

/**
 * mapRow 결과를 사용자 EgovUserDetails Object 에 정의한다.
 *
 * @author ByungHun Woo
 * @since 2009.06.01
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일      수정자           수정내용
 *  -------    -------------    ----------------------
 *   2009.03.10  ByungHun Woo    최초 생성
 *   2009.03.20  이문준          UPDATE
 *   2011.08.31  JJY            경량환경 템플릿 커스터마이징버전 생성
 *
 * </pre>
 */

public class EgovSessionMapping extends EgovUsersByUsernameMapping  {

	/**
	 * 사용자정보를 테이블에서 조회하여 EgovUsersByUsernameMapping 에 매핑한다.
	 * @param ds DataSource
	 * @param usersByUsernameQuery String
	 */
	public EgovSessionMapping(DataSource ds, String usersByUsernameQuery) {
        super(ds, usersByUsernameQuery);
    }

	/**
	 * mapRow Override
	 * @param rs ResultSet 결과
	 * @param rownum row num
	 * @return Object EgovUserDetails
	 * @exception SQLException
	 */
	@Override
	protected EgovUserDetails mapRow(ResultSet rs, int rownum) throws SQLException {
    	logger.debug("## EgovUsersByUsernameMapping mapRow ##");

        String user_id = rs.getString("user_id");
        String user_nm = rs.getString("user_nm");
        String user_pwd = rs.getString("user_pwd");
        String auth_cd  = rs.getString("auth_cd");
        String ofc_cd = rs.getString("ofc_cd");
        String dept_cd = rs.getString("dept_cd");
        String pos_cd = rs.getString("pos_cd");

        // 세션 항목 설정
        LoginVO loginVO = new LoginVO();
        loginVO.setUserId(user_id);
        loginVO.setUserPwd(user_pwd);
        loginVO.setUserNm(user_nm);
        loginVO.setAuthCd(auth_cd);
        loginVO.setOfcCd(ofc_cd);
        loginVO.setDeptCd(dept_cd);
        loginVO.setPosCd(pos_cd);

        return new EgovUserDetails(user_id, user_pwd, true, loginVO);
    }
}
