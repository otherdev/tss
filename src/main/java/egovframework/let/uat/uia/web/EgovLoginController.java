package egovframework.let.uat.uia.web;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;

import org.springframework.context.ApplicationContext;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.support.WebApplicationContextUtils;

import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.LoginVO;
import egovframework.com.cmm.SessionUtil;
import egovframework.let.uat.uap.service.EgovLoginPolicyService;
import egovframework.let.utl.sim.service.EgovFileScrty;
import egovframework.pms.cmm.service.CmmService;
import egovframework.rte.fdl.cmmn.trace.LeaveaTrace;
import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.fdl.security.userdetails.util.EgovUserDetailsHelper;

/**
 * 일반 로그인, 인증서 로그인을 처리하는 컨트롤러 클래스
 * @author 공통서비스 개발팀 박지욱
 * @since 2009.03.06
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일      수정자          수정내용
 *  -------    --------    ---------------------------
 *  2009.03.06  박지욱          최초 생성
 *  2011.08.31  JJY            경량환경 템플릿 커스터마이징버전 생성
 *
 *  </pre>
 */
@Controller
public class EgovLoginController {


	/** EgovMessageSource */
	@Resource(name = "egovMessageSource")
	EgovMessageSource egovMessageSource;

	/** EgovLoginPolicyService */
	@Resource(name = "egovLoginPolicyService")
	EgovLoginPolicyService egovLoginPolicyService;

	/** EgovPropertyService */
	@Resource(name = "propertiesService")
	protected EgovPropertyService propertiesService;

	/** TRACE */
	@Resource(name = "leaveaTrace")
	LeaveaTrace leaveaTrace;

	/** cmmService */
	@Resource(name = "cmmService")
	private CmmService cmmService;
	
	/**
	 * 로그인 화면으로 들어간다
	 * @param vo - 로그인후 이동할 URL이 담긴 LoginVO
	 * @return 로그인 페이지
	 * @exception Exception
	 */
	@RequestMapping(value = "/uat/uia/egovLoginUsr.do")
	public String loginUsrView(@ModelAttribute("loginVO") LoginVO loginVO, HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
		return "uat/uia/EgovLoginUsr";
	}

	/**
	 * 일반(스프링 시큐리티) 로그인을 처리한다
	 * @param vo - 아이디, 비밀번호가 담긴 LoginVO
	 * @param request - 세션처리를 위한 HttpServletRequest
	 * @return result - 로그인결과(세션정보)
	 * @exception Exception
	 */
	@RequestMapping(value = "/uat/uia/actionSecurityLogin.do")
	public String actionSecurityLogin(@ModelAttribute("loginVO") LoginVO loginVO
			, @RequestParam(value = "login_error", required = false) String login_error
			, HttpServletResponse response, HttpServletRequest request, ModelMap model) throws Exception {

		//로그인 실패처리
		if("1".equals(login_error)){
			model.addAttribute("message", egovMessageSource.getMessage("fail.common.login"));
			return "uat/uia/EgovLoginUsr";
		}
		
		
		/// 스프링시큐리티 관련 인증처리 //////////////////////////////////////
		UsernamePasswordAuthenticationFilter springSecurity = null;
		
		/// UsernamePasswordAuthenticationFilter 인증필터객체 가져와서, 필터세팅 
		ApplicationContext act = WebApplicationContextUtils.getRequiredWebApplicationContext(request.getSession().getServletContext());
		
		@SuppressWarnings("rawtypes")
		Map beans = act.getBeansOfType(UsernamePasswordAuthenticationFilter.class);
		if (beans.size() > 0) {
			springSecurity = (UsernamePasswordAuthenticationFilter) beans.values().toArray()[0];
		} else {
			throw new IllegalStateException("No AuthenticationProcessingFilter");
		}

		/// 인증필터 사용여부
		springSecurity.setContinueChainBeforeSuccessfulAuthentication(false); // false 이면 chain 처리 되지 않음.. (filter가 아닌 경우 false로...)
		/// 인증필터를 사용해서 세션정보 저장

		
		/**
		 * 통합로그인 접속 처리
		 */
		String userId = "";
		String enpassword = "";
		if("Y".equals(loginVO.getUnionLoginYn())){
			Map<String,Object> map = new HashMap<String,Object>();
			map.put("userId", loginVO.getUserId());
			List<Map<String,Object>> lst = (List<Map<String, Object>>) cmmService.selectList("selectUserDtl", map);
			if(lst != null && !lst.isEmpty()){
				userId = lst.get(0).get("userId").toString();
				enpassword = lst.get(0).get("userPwd").toString();
			}
			
			springSecurity.setPostOnly(false); //통합로그인만 get방식 허용
		}
		//일반로그인
		else{
			userId = loginVO.getUserId();
			enpassword = EgovFileScrty.encryptPassword(loginVO.getUserPwd(), loginVO.getUserId());
			springSecurity.setPostOnly(true); //통합로그인만 get방식 허용
		}
		
		
		springSecurity.doFilter(new RequestWrapperForSecurity(request,  userId, enpassword), response, null);
		
		LoginVO resultVO = (LoginVO)EgovUserDetailsHelper.getAuthenticatedUser(); //세션정보가 스프링시큐리티에으해 저장되었는지 확인...
		
		//로깅 aop 임시 포인트
		cmmService.insertLoginLog(loginVO);
		
		

		boolean loginPolicyYn = true;
		/*		
				// 접속IP
				String userIp = EgovClntInfo.getClntIP(request);
		 		LoginPolicyVO loginPolicyVO = new LoginPolicyVO();
				loginPolicyVO.setEmplyrId(resultVO.getId());
				loginPolicyVO = egovLoginPolicyService.selectLoginPolicy(loginPolicyVO);
		
				if (loginPolicyVO == null) {
					loginPolicyYn = true;
				} else {
					// 로그인IP 정책에 걸리면 IP체크 
					if (loginPolicyVO.getLmttAt().equals("Y")) {
						if (!userIp.equals(loginPolicyVO.getIpInfo())) {
							loginPolicyYn = false;
						}
					}
				}
	 	*/		
		if (resultVO != null && resultVO.getUserId() != null && !resultVO.getUserId().equals("") && loginPolicyYn) {

			// 2. spring security 연동
			request.getSession().setAttribute("loginVO", resultVO);
			// 세션유틸에 정보를 저장한다.
			SessionUtil.setLoginVO(resultVO); 
			
			// servlet 세션에 담기(자동으로 처리되는것이 아닌지...)
			request.getSession().setAttribute("id", resultVO.getUserId());
			request.getSession().setAttribute("name", resultVO.getUserNm());
			request.getSession().setAttribute("deptCd", resultVO.getDeptCd());
			request.getSession().setAttribute("ofcCd", resultVO.getOfcCd());
			request.getSession().setAttribute("posCd", resultVO.getPosCd());
			request.getSession().setAttribute("authCd", resultVO.getAuthCd());
			request.getSession().setAttribute("isAdmYn", "ROLE_ADMIN".equals(resultVO.getAuthCd()) ? "Y" : "N" );
			
			
			return "forward:/cmm/main/mainPage.do"; // 성공 시 페이지.. (redirect 불가)

		} else {

			model.addAttribute("message", egovMessageSource.getMessage("fail.common.login"));
			return "uat/uia/EgovLoginUsr";
		}
	}

	/**
	 * 로그인 후 메인화면으로 들어간다
	 * @param
	 * @return 로그인 페이지
	 * @exception Exception
	 */
	@RequestMapping(value = "/uat/uia/actionMain.do")
	public String actionMain(ModelMap model, @RequestParam(value = "login_error", required = false) String login_error) throws Exception {
		// 1. Spring Security 사용자권한 처리
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
		if (!isAuthenticated) {
			return "uat/uia/EgovLoginUsr";
		}

		// 2. 메인 페이지 이동
		return "forward:/cmm/main/mainPage.do";
	}

	/**
	 * 로그아웃한다.
	 * @return String
	 * @exception Exception
	 */
	@RequestMapping(value = "/uat/uia/actionLogout.do")
	public String actionLogout(HttpServletRequest request, ModelMap model) throws Exception {
		request.getSession().setAttribute("loginVO", null);
		request.getSession().setAttribute("login_error", "-1");
		SessionUtil.setLoginVO(null); 
		
		//임시 로그아웃 aop 포인트
		cmmService.insertLogoutLog();

		return "redirect:/j_spring_security_logout";
	}
}

class RequestWrapperForSecurity extends HttpServletRequestWrapper {
	private String username = null;
	private String password = null;

	public RequestWrapperForSecurity(HttpServletRequest request, String username, String password) {
		super(request);

		this.username = username;
		this.password = password;
	}

	@Override
	public String getRequestURI() {
		return ((HttpServletRequest) super.getRequest()).getContextPath() + "/j_spring_security_check";
	}

	@Override
	public String getParameter(String name) {
		if (name.equals("j_username")) {
			return username;
		}

		if (name.equals("j_password")) {
			return password;
		}

		return super.getParameter(name);
	}
}